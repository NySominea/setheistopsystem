<?php

use Illuminate\Database\Seeder;
use App\Model\UserAccountType;

class UserAccountTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Main Account',
            '5D Bonus Account',
            'Ball Bonus Account',
        ];
        foreach($data as $row){
            UserAccountType::create([
                'name' => $row
            ]);
        }
    }
}
