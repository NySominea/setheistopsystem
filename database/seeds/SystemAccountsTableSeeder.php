<?php

use Illuminate\Database\Seeder;

class SystemAccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('system_accounts')->insert([
            [
                'id' => 1,
                'title' => 'Cash Top Up',
                'number' => 'Cash Top Up',
                'name' => 'Cash Top Up',
                'balance' => 0,
                'state' => 1,
                'sort' => 0
            ],
            [
                'id' => 2,
                'title' => 'Cash Widthdraw',
                'number' => 'Cash Widthdraw',
                'name' => 'Cash Widthdraw',
                'balance' => 0,
                'state' => 1,
                'sort' => 0
            ],
            [
                'id' => 3,
                'title' => 'Finance',
                'number' => 'Finance',
                'name' => 'Finance',
                'balance' => 0,
                'state' => 1,
                'sort' => 0
            ],
            
        ]);
    }
}
