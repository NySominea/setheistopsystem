<?php

use Illuminate\Database\Seeder;
use App\Model\LanguageAdmin;

class ApplicationSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $abstracts = [
            'LANG_LABEL_5D_OWN_REBATE_SUM' => 'Sethei 5D own commission summary',
            'LANG_LABEL_5D_DOWNLINE_REBATE_SUM' => 'Sethei 5D donwline commission summary',
            'LANG_LABEL_5D_BET_SUM' => 'Sethei 5D betting summary',
            'LANG_LABEL_5D_WIN_SUM' => 'Sethei 5D award commission summary',
            'LANG_LABEL_BALL_OWN_REBATE_SUM' => 'Sethei Ball own commission summary',
            'LANG_LABEL_BALL_DOWNLINE_REBATE_SUM' => 'Sethei Ball donwline commission summary',
            'LANG_LABEL_BALL_BET_SUM' => 'Sethei Ball betting summary',
            'LANG_LABEL_BALL_WIN_SUM' => 'Sethei Ball award commission summary'
        ];

        foreach($abstracts as $key => $row){
            if(!LanguageAdmin::whereAbstract($key)->first()){
                LanguageAdmin::create([
                    'module' => 'label',
                    'chinese' => $row,
                    'khmer' => $row,
                    'english' => $row,
                    'label' => $key
                ]);
            }
        }
    }
}
