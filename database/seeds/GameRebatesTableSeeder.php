<?php

use Illuminate\Database\Seeder;
use App\Model\Sethei5D\GameRebate as GameRebate5D;
use App\Model\SetheiBall\GameRebate as GameRebateBall;

class GameRebatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        GameRebate5D::create(['l1_rebate' => 0.3,'l2_rebate' => 0.4,'l3_rebate' => 0.6,'l4_rebate' => 1,'l5_rebate' => 6,'category_id' => 1]);
        GameRebate5D::create(['l1_rebate' => 0.3,'l2_rebate' => 0.4,'l3_rebate' => 0.6,'l4_rebate' => 1,'l5_rebate' => 6,'category_id' => 2]);
        GameRebate5D::create(['l1_rebate' => 0.3,'l2_rebate' => 0.4,'l3_rebate' => 0.6,'l4_rebate' => 1,'l5_rebate' => 6,'category_id' => 3]);

        GameRebateBall::create(['l1_rebate' => 0.3,'l2_rebate' => 0.4,'l3_rebate' => 0.6,'l4_rebate' => 1,'l5_rebate' => 6,'game_type_id' => 1]);
    }
}
