<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'username' => 'seakliv',
            'account_number' => '010248482',
            'phone' => '010248482',
            'password' => Hash::make('123456')
        ]);
    }
}
