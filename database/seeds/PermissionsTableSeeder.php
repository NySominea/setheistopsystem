<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Model\Manager;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        // DB::table('d_roles')->delete();
        // DB::table('d_permissions')->delete(); 
        
        $permissions = []; 
        foreach(permissions()['en'] as $m){
            foreach($m['permissions'] as $index => $p){ 
                $permissions[] = $p['value'];
                Permission::create(['name' => $p['value']]);
            }
        }
        
        $role = Role::create(['name' => 'Super Administrator']);
        $role->syncPermissions($permissions);

        $user = Manager::where('username','kimhour')->first();
        if($user){
            $user->syncRoles([$role->name]);
        }
    }
}
