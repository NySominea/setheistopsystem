<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(UserAccountTypesTableSeeder::class);
        $this->call(GameRebatesTableSeeder::class);
        $this->call(SystemAccountsTableSeeder::class);
        $this->call(ApplicationSettingTableSeeder::class);
    }
}
