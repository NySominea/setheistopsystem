<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemAccountsTable extends Migration
{
    public function up()
    {
        Schema::create('system_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            // $table->unsignedTinyInteger('category_id');
            // $table->unsignedTinyInteger('type_id')->index();
            $table->string('title',50);
            $table->string('number',50);
            $table->string('name',50);
            $table->double('balance')->default(0.00);
            $table->boolean('state')->default(1);
            $table->unsignedTinyInteger('sort')->default(0);
            $table->timestamps();
        });
    }

   
    public function down()
    {
        Schema::dropIfExists('system_accounts');
    }
}
