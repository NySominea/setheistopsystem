<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBallOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ball_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('cycle_id')->index();
            $table->unsignedInteger('user_id')->index();
            $table->string('ticket',30)->index();
            $table->boolean('is_settle')->default(0);
            $table->boolean('is_win')->default(0);
            $table->enum('win_prize',['3D','4D','5D','jackpot'])->nullable();
            $table->string('win_number',100)->default('[]')->nullable();
            $table->double('win_amount')->default(0.00);
            $table->double('amount')->default(0.00);
            $table->string('no1',2);
            $table->string('no2',2);
            $table->string('no3',2);
            $table->string('no4',2);
            $table->string('no5',2);
            $table->string('no6',2);

            $table->unsignedInteger('parent_id')->default(0)->index();

            $table->boolean('state')->default(1);
            $table->unsignedTinyInteger('game_type_id')->index();
            $table->boolean('is_test')->default(FALSE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ball_orders');
    }
}
