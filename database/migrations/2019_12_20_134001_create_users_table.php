<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username',50)->index();
            $table->string('account_number',20)->unique()->index();
            $table->string('email',50)->nullable();
            $table->string('phone',50)->unique()->index();
            $table->string('national_id_number',50)->nullable();
            $table->string('password');
            $table->string('payment_password');
            $table->unsignedInteger('parent_id')->default(0)->nullable()->index();
            $table->unsignedTinyInteger('level')->default(0)->nullable();
            $table->double('current_own_commission_5d')->default(0);
            $table->double('current_line_commission_5d')->default(0);
            $table->double('current_own_commission_ball')->default(0);
            $table->double('current_line_commission_ball')->default(0);
            $table->unsignedInteger('province_id')->nullable();
            $table->unsignedInteger('district_id')->nullable();
            $table->unsignedInteger('commune_id')->nullable();
            $table->string('address')->nullable();
            $table->boolean('state')->default(1);
            $table->boolean('has_verified')->default(0);
            $table->boolean('is_approved')->default(1);
            $table->boolean('is_test')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
