<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBallGameRebatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ball_game_rebates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('l1_rebate');
            $table->double('l2_rebate');
            $table->double('l3_rebate');
            $table->double('l4_rebate');
            $table->double('l5_rebate');
            $table->string('game_type_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ball_game_rebates');
    }
}
