<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemAccountLogsTable extends Migration
{
   
    public function up()
    {
        Schema::create('system_account_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('account_id')->index();
            $table->unsignedInteger('to_account_id')->default(0)->index();
            $table->unsignedInteger('manager_id')->default(0)->index();
            $table->double('amount')->default(0.00);
            $table->double('balance')->default(0.00);
            $table->string('log_number',40)->nullable();
            $table->string('abstract',50);
            $table->unsignedTinyInteger('log_type')->default(1);
            $table->unsignedTinyInteger('to_type')->default(1);
            $table->string('user_abstract',50)->nullable();
          
            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('system_account_logs');
    }
}
