<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDOrderBetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('d_order_bets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ticket',50)->index();
            $table->string('prize',10);
            $table->string('bet',30);
            $table->string('unit',15);
            $table->double('amount')->default(0.00);
            $table->enum('type',['2D','3D','5D']);
            $table->boolean('is_win')->default(false);
            $table->double('win_amount')->default(0.00);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('d_order_bets');
    }
}
