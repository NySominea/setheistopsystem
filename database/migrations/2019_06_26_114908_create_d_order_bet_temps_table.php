<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDOrderBetTempsTable extends Migration
{
    
    public function up()
    {
        Schema::create('d_order_bet_temps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ticket',100)->index();
            $table->string('prize',10);
            $table->string('bet',30);
            $table->string('unit',15);
            $table->double('amount')->default(0.00);
            $table->enum('type',['2D','3D','5D']);
            $table->boolean('is_win')->default(false);
            $table->double('win_amount')->default(0.00);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('d_order_bet_temps');
    }
}
