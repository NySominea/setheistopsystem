<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAccountsTable extends Migration
{
   
    public function up()
    {
        Schema::create('user_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id')->index();
            $table->unsignedTinyInteger('type_id')->default(1)->index();
            $table->string('title');
            $table->string('number');
            $table->string('name');
            $table->double('balance')->default(0.00);
            $table->double('frozen')->default(0.00);
            $table->boolean('state');
            $table->unsignedTinyInteger('sort')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_accounts');
    }
}
