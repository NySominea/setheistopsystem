<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAccountLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_account_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('account_id')->index();
            $table->unsignedInteger('to_user_id')->index()->default(0);
            $table->unsignedInteger('to_account_id')->index()->default(0);
            $table->boolean('is_transfer')->default(FALSE);
            $table->double('amount')->default(0.00);
            $table->double('balance')->default(0.00);
            $table->double('commission')->default(0.00);
            $table->double('win_money')->default(0.00);
            $table->unsignedInteger('manager_id')->default(0)->index();
            $table->string('abstract',50)->index();
            $table->string('log_number',50)->nullable();
            $table->unsignedTinyInteger('log_type');
            $table->unsignedTinyInteger('to_type');
            $table->string('meta')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_account_logs');
    }
}
