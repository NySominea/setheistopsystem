<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDOrderBetDetailTempsTable extends Migration
{
    
    public function up()
    {
        Schema::create('d_order_bet_detail_temps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_bet_id')->index();
            $table->string('number');
            $table->double('win_amount');
            $table->boolean('is_win')->default(0);
            $table->string('ticket',100);
            $table->string('prize');
            $table->string('length');
            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('d_order_bet_detail_temps');
    }
}
