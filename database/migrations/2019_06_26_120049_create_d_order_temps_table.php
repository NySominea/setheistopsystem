<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDOrderTempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('d_order_temps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('cycle_id')->index();
            $table->boolean('is_settle')->default(0);
            $table->boolean('is_win')->default(0);
            $table->double('win_amount')->default(0.00);
            $table->double('win_2d_amount')->default(0.00);
            $table->double('win_3d_amount')->default(0.00);
            $table->double('win_5d_amount')->default(0.00);
            $table->string('ticket',50)->index();
            $table->double('amount')->default(0.00);
            $table->text('bet');
            $table->string('bet_content',15000);

            $table->unsignedInteger('parent_id')->nullable();
            $table->unsignedInteger('l1_id')->default(0)->index();
            $table->unsignedInteger('l2_id')->default(0)->index();
            $table->unsignedInteger('l3_id')->default(0)->index();
            $table->unsignedInteger('l4_id')->default(0)->index();
            $table->unsignedInteger('l5_id')->default(0)->index();

            $table->double('l1_rebate')->default(0.00);
            $table->double('l2_rebate')->default(0.00);
            $table->double('l3_rebate')->default(0.00);
            $table->double('l4_rebate')->default(0.00);
            $table->double('l5_rebate')->default(0.00);
            
            $table->boolean('state')->default(1);
            $table->boolean('is_test')->default(FALSE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('d_order_temps');
    }
}
