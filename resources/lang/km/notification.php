<?php
return [
    '5d_result_title' => 'លទ្ធផលឆ្នោត 5D!',
    'result_time' => 'ម៉ោង',
    'result' => 'លទ្ធផល',
    'congratulation' => 'សូមអបអរសាទរ!',
    'win_msg' => ':user បានឈ្នះទឹកប្រាក់ចំនួន :amount $.',

    'balance_transfer' => 'ការផ្ញើទឹកប្រាក់​​ 5D',
    'balance_recharge' => 'ការបញ្ចូលទឹកប្រាក់ 5D',
    'balance_withdraw' => 'ការដកទឹកប្រាក់ 5D',
    'recharge_msg' => ':amount (R) ត្រូវបានផ្ញើទៅកាន់គណនីរបស់លោកអ្នកដោយ​ :name.',
    'withdraw_msg' => ':amount (R) ត្រូវបានដកចេញពីគណនីរបស់លោកអ្នកដោយ :name.',
    'receive_msg' => 'អ្នកបានទទួលទឹកប្រាក់ចំនួន :amount (R) ពី :name.',
    'send_to_another_user' => 'អ្នកបានទទួលទឹកប្រាក់ចំនួន :amount (R) ពីគណនី :user នៅវេលាម៉ោង :time',
];