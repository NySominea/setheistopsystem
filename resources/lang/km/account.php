<?php

return [
    'failed_to_trasfer,' => 'ប្រតិបត្ដិការផ្ញើរបរាជ័យ! សូមព្យាយាមម្តងទៀត!',
    'failed_amount_input' => 'សូមបញ្ចូលទឹកប្រាក់លើសពី 0R!',
    'wrong_pay_password' => 'លេខសម្ងាត់មិនត្រឹមត្រូវ!',
    'over_balance' => 'ទឹកប្រាក់ប្រតិបត្តិការពុំគ្រប់គ្រាន់!',
    'out_of_balance' => 'ទឹកប្រាក់ប្រតិបត្តិការពុំគ្រប់គ្រាន់!',
    'unknown_account' => 'Unknown account proccess!',
    'unknown_target_user' => 'Unknown target user!',
    'insufficient_balance' => 'គណនីពុំមានទឹកប្រាក់ទេ!',
    
];