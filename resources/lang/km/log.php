<?php

return [
    'bet_summary' => 'សរុបលុយភ្នាល់',
    'profit' => 'ប្រាក់ចំណេញ',
    'rebate_summary' => 'សរុបកម្រៃជើងសារ',
    'top_up' => 'បញ្ចូលលុយ',
    'top_up_bonus' => 'ប្រាក់បន្ថែមពេលបញ្ចូលលុយ',
    'top_up_bonus_added' => '',
    'return_money' => 'សងលុុយពី​ System',
    'cancel_ticket' => 'លុបសំបុត្រ',
    'take_cash_cancel' => '',
    'win_summary' => 'សរុបលុយឈ្នះ',
    'withdraw' => 'ដកលុយ',
    'first_bonus_top_up' => 'បញ្ចូលលុយលើកដំបូង'
];