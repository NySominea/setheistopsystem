<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'ឈ្មោះនិងលេខសម្ងាត់របស់អ្នកមិនត្រឹមត្រូវ',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'successful_login' => 'ចូលកម្មវិធីដោយជោកជ័យ',
    'successful_logout' => 'ចាកចេញពីកម្មវិធីដោយជោកជ័យ',
    'field_required' => 'សូមបំពេញព័ត៌មានដែលទាមទារ',
    'permission_denied' => 'មិនអនុញ្ញាត!!!',
    'not_found' => 'រកមិនឃើញ!',
    'cannot_bet' => 'សូមបញ្ចូលទឹកប្រាក់ឲ្យបាន 100,000 R ជាមុនសិន!'
];
