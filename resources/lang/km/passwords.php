<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'លេខសំងាត់ថ្មីត្រូវមានយ៉ាងតិច ៦ តួ',
    'reset' => 'Your password has been reset!',
    'sent' => 'We have e-mailed your password reset link!',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that e-mail address.",
    'new_password_same_as_old_password' => 'លេខសំងាត់ថ្មីមិនអាចដូចលេខសំងាត់ចាស់បានទេ',
    'old_password_not_correct' => 'លេខសំងាត់ចាស់មិនត្រឹមត្រូវ',
    'change_password_success' => 'លេខសំងាត់ផ្លាស់ប្តូរបានជោគជ័យ',
    'password_not_match' => 'លេខសំងាត់ថ្មី មិនត្រឹមត្រូវ'
];
