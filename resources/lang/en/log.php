<?php

return [
    'bet_summary' => 'Bet Summary',
    'profit' => 'Profit',
    'rebate_summary' => 'Commission Summary',
    'top_up' => 'Top up',
    'top_up_bonus' => 'Top us bonus',
    'top_up_bonus_added' => '',
    'return_money' => 'Return money',
    'cancel_ticket' => 'Cancel ticket',
    'take_cash_cancel' => '',
    'win_summary' => 'Win summary',
    'withdraw' => 'Withdraw',
    'first_bonus_top_up' => 'First topup'
];