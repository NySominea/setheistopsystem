<?php

return [
    '5d_result_title' => 'Sethei 5D Result!',
    'result_time' => 'Result Time',
    'result' => 'Result',
    'congratulation' => 'Congratulation!',
    'win_msg' => ':user won :amount $.',

    'balance_transfer' => '5D Balance Transfer',
    'balance_recharge' => '5D Balance Recharge',
    'balance_withdraw' => '5D Balance Withdraw',
    'recharge_msg' => ':amount (R) has been recharged to your account by :name.',
    'withdraw_msg' => ':amount (R) has been withdrawed to your account by :name.',
    'receive_msg' => 'You receive :amount (R) from :name.',
    'send_to_another_user' => 'You\'re received :amount (R) from :user at :time',
];