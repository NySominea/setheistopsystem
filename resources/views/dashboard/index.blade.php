@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Dashboard</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <span class="breadcrumb-item active"><i class="icon-home2 mr-2"></i> Dashboard</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <h1 class="text-center mb-4">Welcome to Sethei Shop Admin Management</h1>

    {{-- @can('view-dasbhoard')
    <label class="h5">Current Cycle</label>
    <div class="row mb-3">
        <div class="col-lg-4">
            <div class="card bg-primary-600">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0"><i class="icon-users4"></i> {{$data['current']['totalMembers']}}</h3>
                    </div>
                    
                    <div>   
                        Members Online
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card bg-success-600">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0"><i class="icon-cash3"></i> $ {{currencyFormat($data['current']['totalRevenue'] * $settings['exchangeRate']['rielToDollar'])}}</h3>
                    </div>
                    
                    <div>
                        Current Revenue
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card bg-danger-600">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0"><i class="icon-cash3"></i> $ {{ currencyFormat($data['current']['totalRebate'] * $settings['exchangeRate']['rielToDollar'])}}</h3>
                    </div>
                    
                    <div>
                        Current Rebate
                    </div>
                </div>
            </div>
        </div>
    </div>

    <label class="h5">Current Cycle Revenue By Line</label>
    <div class="row mb-3">
        <div class="col-lg-3">
            <div class="card bg-teal-600">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0"><i class="icon-cash3"></i> $ {{currencyFormat($data['line']['pp'] * $settings['exchangeRate']['rielToDollar'])}}</h3>
                    </div>
                    
                    <div>
                        Phnom Penh
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card bg-green-600">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0"><i class="icon-cash3"></i> $ {{ currencyFormat($data['line']['pp_master'] * $settings['exchangeRate']['rielToDollar'])}}</h3>
                    </div>
                    
                    <div>
                        Phnom Penh Master
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card bg-blue-600">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0"><i class="icon-cash3"></i> $ {{currencyFormat($data['line']['province'] * $settings['exchangeRate']['rielToDollar'])}}</h3>
                    </div>
                    
                    <div>
                        Province
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card bg-indigo-600">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0"><i class="icon-cash3"></i> $ {{ currencyFormat($data['line']['shop'] * $settings['exchangeRate']['rielToDollar'])}}</h3>
                    </div>
                    
                    <div>
                        Shop
                    </div>
                </div>
            </div>
        </div>
    </div>

    <label class="h5">Monthly Report</label>
    <div class="row mb-3">
        <div class="col-lg-6">
            <div class="card bg-success-800">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0"><i class="icon-cash3"></i> $ {{currencyFormat($data['monthly']['totalRevenue'] * $settings['exchangeRate']['rielToDollar'])}}</h3>
                    </div>
                    
                    <div>
                        Monthly Revenue - {{date('F')}}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card bg-danger-800">
                <div class="card-body">
                    <div class="d-flex">
                        <h3 class="font-weight-semibold mb-0"><i class="icon-cash3"></i> $ {{ currencyFormat($data['monthly']['totalProfit'] * $settings['exchangeRate']['rielToDollar'])}}</h3>
                    </div>
                    
                    <div>
                        Monthly Profit - {{date('F')}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endcan --}}
</div>

@endsection

@section('page-script')

<script src="{{asset('global_assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
<script src="{{asset('global_assets/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
<script src="/global_assets/js/demo_pages/dashboard.js"></script>
@endsection