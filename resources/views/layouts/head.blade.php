<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Sethei Lottery Company</title>
<link rel="icon" href="{{asset('assets/images/5d.png')}}">

<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">

<link href="{{ asset('assets/css/all.min.css')}}" rel="stylesheet" type="text/css">
<link href="/assets/css/custom.css" rel="stylesheet" type="text/css">

@yield('custom-css')