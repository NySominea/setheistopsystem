<li class="nav-item">
    <a href="{{route('dashboard')}}" class="nav-link"><i class="icon-home4"></i> <span>Dashboard</span></a>
</li>


<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main Application</div> <i class="icon-menu" title="Main"></i></li>

@canany(['view-user-account','add-new-user-account','user-account-modification','user-recharge','view-user-account-log','user-take-cash'])
    <li class="nav-item">
        <a href="{{ route('users.sale-network.index') }}" class="nav-link
            {{ in_array(Route::currentRouteName(),['users.account.index','users.account.create'
            ,'users.account.edit','users.log.index','users.log.create','users.log.edit','users.sale-network.index','users.sale-network.create',
            'users.sale-network.edit','users.direct-user.index','users.direct-user.create','users.direct-user.edit']) ? ' active' : '' }}">
            <i class="icon-users4"></i> {{ $settings['language']['LANG_MENU_SALE_NET'] }}
        </a>
    </li>
@endcanany

@canany(['view-balance-request','balance-request-modification'])
    <li class="nav-item">
        <a href="{{ route('users.balance-request.index') }}" class="nav-link
            {{ in_array(Route::currentRouteName(),['users.balance-request.index','users.balance-request.create']) ? ' active' : '' }}">
            @php $balance_request_count = App\Model\BalanceRequest::whereIn('status',[UserAccountConstant::PENDING,UserAccountConstant::IN_PROGRESS])->get()->count() @endphp
            
            <i class="icon-bell3"></i> Balance Request &nbsp; 
            @if($balance_request_count > 0)
                <span class="badge badge-pill badge-danger">{{ $balance_request_count }}</span>
            @endif
        </a>
    </li>
@endcanany

@canany(['view-transaction-log','view-platform-account','platform-account-access-transfer','view-agent-tracking','balance-review'])
<li class="nav-item nav-item-submenu
    {{ in_array(Route::currentRouteName(), ['accounts.categories.index','accounts.categories.create','accounts.categories.edit','accounts.create','accounts.edit',
        'accounts.deposit.edit','accounts.transfer.edit','accounts.review.form-selection','accounts.review.index']) 
    ? ' nav-item-expanded nav-item-open' : ''}}">
    <a href="#" class="nav-link"><i class="icon-coins"></i> <span>{{ $settings['language']['LANG_MENU_FINANCE'] }}</span></a>
    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
        @canany(['view-platform-account','platform-account-access-transfer'])
        <li class="nav-item">
            <a href="{{route('accounts.index')}}" class="nav-link 
                {{ in_array(Route::currentRouteName(), ['accounts.categories.index','accounts.categories.create',
                'accounts.categories.edit','accounts.create','accounts.edit','accounts.deposit.edit',
                'accounts.transfer.edit']) ? ' active' : ''}}">
                {{ $settings['language']['LANG_MENU_ACCOUNT'] }}
            </a>
        </li>
        @endcanany
        @can('view-transaction-log')
        <li class="nav-item">
            <a href="{{route('accounts.logs.index')}}" class="nav-link 
            {{ in_array(Route::currentRouteName(), ['accounts.logs.index']) ? ' active' : ''}}">
                {{ $settings['language']['LANG_MENU_ACCOUNT_LOG'] }}
            </a>
        </li>
        @endcan
    </ul>
</li>
@endcanany

<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Products</div> <i class="icon-menu" title="Main"></i></li>

@canany(['view-5d-order-list','export-5d-order-list','view-5d-history-report','export-5d-history-report', 'view-5d-game-setting', '5d-game-setting-modification'])
<li class="nav-item nav-item-submenu
    {{ in_array(Route::currentRouteName(), ['view-5d-order-list','export-5d-order-list']) 
    ? ' nav-item-expanded nav-item-open' : ''}}">
    <a href="#" class="nav-link"><i class="icon-coins"></i> <span>Sethei 5D</span></a>
    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
        @canany(['view-5d-order-list','export-5d-order-list'])
        <li class="nav-item">
            <a href="{{route('5d.orders.temp.index')}}" class="nav-link">
                <i class="icon-stack4"></i><span>{{ $settings['language']['LANG_MENU_THIS_ORDER'] }}</span>
            </a>
        </li>
        @endcanany

        @canany(['view-5d-history-report','export-5d-history-report'])
        <li class="nav-item">
            <a href="{{route('5d.orders.report.selection-form')}}" class="nav-link" data-active="{{ in_array(Route::currentRouteName(), ['5d.orders.report.index']) ? true : false}}">
                <i class="icon-stack4"></i><span>{{ $settings['language']['LANG_MENU_REPORT'] }}</span>
            </a>
        </li>
        @endcanany

        @canany(['view-5d-cycle-log'])
        <li class="nav-item">
            <a href="{{ route('5d.cycles.logs.index')}}" class="nav-link">
                <i class="icon-cog3"></i><span>{{ $settings['language']['LANG_MENU_CYCLE_LOG'] }}</span>
            </a>
        </li>
        @endcanany

        @canany(['view-5d-game-setting','5d-game-setting-modification'])
        <li class="nav-item">
            <a href="{{ route('5d.game.setting.index')}}" class="nav-link">
                <i class="icon-cog3"></i><span>{{ $settings['language']['LANG_MENU_GAME_SET'] }}</span>
            </a>
        </li>
        @endcanany
    </ul>
</li>
@endcanany

@canany(['view-ball-order-list','export-ball-order-list','view-ball-history-report','export-ball-history-report', 'view-ball-game-setting', 'ball-game-setting-modification'])
<li class="nav-item nav-item-submenu
    {{ in_array(Route::currentRouteName(), ['view-ball-order-list','export-ball-order-list']) 
    ? ' nav-item-expanded nav-item-open' : ''}}">
    <a href="#" class="nav-link"><i class="icon-coins"></i> <span>Sethei Ball</span></a>
    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
        @canany(['view-ball-order-list','export-ball-order-list'])
        <li class="nav-item">
            <a href="{{route('ball.orders.temp.index')}}" class="nav-link">
                <i class="icon-stack4"></i><span>{{ $settings['language']['LANG_MENU_THIS_ORDER'] }}</span>
            </a>
        </li>
        @endcanany

        @canany(['view-ball-history-report','export-ball-history-report'])
        <li class="nav-item">
            <a href="{{route('ball.orders.report.selection-form')}}" class="nav-link" data-active="{{ in_array(Route::currentRouteName(), ['ball.orders.report.index']) ? true : false}}">
                <i class="icon-stack4"></i><span>{{ $settings['language']['LANG_MENU_REPORT'] }}</span>
            </a>
        </li>
        @endcanany

        @canany(['view-ball-cycle-log'])
        <li class="nav-item">
            <a href="{{ route('ball.cycles.logs.index')}}" class="nav-link">
                <i class="icon-cog3"></i><span>{{ $settings['language']['LANG_MENU_CYCLE_LOG'] }}</span>
            </a>
        </li>
        @endcanany

        @canany(['view-ball-game-setting','ball-game-setting-modification'])
        <li class="nav-item">
            <a href="{{ route('ball.game.setting.index')}}" class="nav-link">
                <i class="icon-cog3"></i><span>{{ $settings['language']['LANG_MENU_GAME_SET'] }}</span>
            </a>
        </li>
        @endcanany
    </ul>
</li>
@endcanany


<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Application Setting</div> <i class="icon-menu" title="Main"></i></li>

@canany(['view-manager-account','add-new-manager-account','manager-account-modification','view-role','add-new-role','role-modification','view-manager-log'])

<li class="nav-item nav-item-submenu 
    {{ in_array(Route::currentRouteName(), ['managers.create','managers.edit','managers.roles.create','managers.roles.edit']) 
        ? ' nav-item-expanded nav-item-open' : ''}}">
    <a href="#" class="nav-link"><i class="icon-user-check"></i> <span>{{ $settings['language']['LANG_MENU_MANAGER']}}</span></a>

    <ul class="nav nav-group-sub" data-submenu-title="Layouts">
        @canany(['view-manager-account','add-new-manager-account','manager-account-modification'])
        <li class="nav-item"><a href="{{ route('managers.index') }}" class="nav-link {{ in_array(Route::currentRouteName(), ['managers.create','managers.edit']) ? ' active' : ''}}">{{ $settings['language']['LANG_MENU_MANAGER_LIST']}}</a></li>
        @endcanany
        @canany(['add-new-role','view-role','role-modification'])
        <li class="nav-item"><a href="{{ route('managers.roles.index') }}" class="nav-link {{ in_array(Route::currentRouteName(), ['managers.roles.create','managers.roles.edit']) ? ' active' : ''}}">{{ $settings['language']['LANG_MENU_MANAGER_ROLE']}}</a></li>
        @endcanany
        @canany(['view-manager-log'])
            <li class="nav-item"><a href="{{ route('managers.logs.index') }}" class="nav-link">{{ $settings['language']['LANG_MENU_MANAGER_LOG']}}</a></li>
        @endcanany
    </ul>
</li>
@endcanany


<li class="nav-item">
    <a href="{{route('promotions.index')}}" class="nav-link">
        <i class="icon-chart"></i><span>Promotion</span>
    </a>
</li>

