@extends('layouts.master')
@section('costom-css')
<style>
    .dropzone .dz-preview .dz-image img{
        width: 100% !important;
    }
    .dropzone .dz-preview .dz-image{
        width: 100% !important;
        height: 100px !important;
    }
</style>
@endsection

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Add Promotion</span></h4>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="{{ route("promotions.index") }}" class="btn btn-primary btn-sm">
                    <i class="icon-square-left mr-1"></i> Back
                </a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{ route('promotions.index') }}" class="breadcrumb-item">Promotion</a>
                <span class="breadcrumb-item active">{{ isset($promotion) ? 'Edit' : 'Add'}}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        @if(isset($promotion))
        {{ Form::model($promotion,['route' => ['promotions.update',$promotion->id], 'method' => 'PUT']) }}
        @else
        {{ Form::open(['route' => 'promotions.store', 'method' => 'POST']) }}
        @endif
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('title')?'text-danger':'' }}">Promotion Title</label>
                        <div class="position-relative">
                            {{Form::text("title",old("title") ? old("title") : (isset($promotion) ? $promotion->title : ''),
                                ["class" => "form-control ".($errors->has('username')?'border-danger':''),"placeholder" => "Enter account title"])
                            }}
                            @if($errors->has('title'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('title'))
                            <span class="form-text text-danger">{{ $errors->first('title') }}</span>
                        @endif
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('description')?'text-danger':'' }}">Description </label>
                        <div class="position-relative">
                            {{Form::textarea("description",old("description") ? old("description") : (isset($promotion) ? $promotion->description : ''),
                                ["class" => "form-control ".($errors->has('number') ? 'border-danger' : ''),"placeholder" => "Enter description", 'rows' => 3])
                            }}
                            @if($errors->has('description'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('description'))
                            <span class="form-text text-danger">{{ $errors->first('description') }}</span>
                        @endif
                    </div>

                    <div class="row form-group form-group-float form-group-feedback form-group-feedback-right"">
                        
                        <div class="col-md-6 input-group input-group-sm">
                            <div class="input-group-prepend">
                                <div class="input-group-text">{{ $settings['language']['LANG_LABEL_START_DT'] }}</div>
                            </div>
                            <input type="text" class="form-control datetimepicker" value="{{isset($promotion) ? $promotion->begin_date : date('Y-m-d 00:00:00')}}" name="begin_date">
                        </div>

                        <div class="col-md-6 input-group input-group-sm">
                            <div class="input-group-prepend">
                                <div class="input-group-text">{{ $settings['language']['LANG_LABEL_END_DT'] }}</div>
                            </div>
                            <input type="text" class="form-control datetimepicker" value="{{isset($promotion) ? $promotion->end_date : date('Y-m-d 23:59:00')}}" name="end_date">
                        </div>
                    </div>
                    
                    <div class="form-group m-form__group {{ $errors->has('image') ? 'has-danger' : '' }}">
                        <label class="col-form-label" for="image">Thumnail Promotion Image (Ratio 1 : 0.57)</label>
                        <div class="m-dropzone dropzone m-dropzone--primary dz-clickable d-center" action="{{ route('promotions.saveThumnail') }}" data-url-delete="{{ route('promotions.deleteThumnail')}}" id="dropzonePromotionThumnail" data-key="promotionThumnail">
                            @csrf
                            <div class="m-dropzone__msg dz-message needsclick">
                                <h3 class="m-dropzone__msg-title">Drop image here or click to upload.</h3>
                                <span class="m-dropzone__msg-desc">Single Image Upload</span>
                            </div>
                        </div>
                        @if($errors->has('image'))
                            <div id="image-error" class="form-control-feedback">{{ $errors->first('image')}}</div>
                        @endif
                    </div>
                    
                    <div class="form-group mb-3 mb-md-2">
                        <label class="d-block font-weight-semibold">State</label>
                        <div class="form-check custom-control-inline">
                            <label class="form-check-label">
                                <input type="radio" name="state" value="1" class="form-check-input-styled-success" data-fouc {{ isset($promotion) && $promotion->state == 1 ? 'checked' : '' }} checked>
                                Enable
                            </label>
                        </div>
                        <div class="form-check custom-control-inline">
                            <label class="form-check-label">
                                <input type="radio" name="state" value="0" class="form-check-input-styled-danger" data-fouc 
                                    {{ isset($promotion) && $promotion->state == 0 ? 'checked' : '' }}>
                                    Disable
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success">
                <i class="icon-folder mr-1"></i>{{ $settings['language']['LANG_BTN_SAVE'] }}
            </button>
        </div>
        
            @php $image = isset($promotion) ? $promotion->getMedia('images')->first() : null; @endphp
            @if(isset($promotion) && $image)
                <input type="hidden" name="image" id="image" value="" data-model-id="{{ $promotion->id }}" data-name="{{$image->file_name}}" data-size="{{$image->size}}" data-url="{{ asset($image->getUrl()) }}">
            @else
                <input type="hidden" name="image" id="image" value="{{old('image')}}">
            @endif
            <input type="hidden" name="action" id="action" value="{{ isset($promotion) ? 'update' : 'add' }}">

        {{ Form::close() }}
    </div>
</div>
@endsection

@section('page-script')
    <script src="/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script src="/global_assets/js/demo_pages/form_checkboxes_radios.js"></script>
    <script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
    <script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>
    <script src="{{ asset('assets/js/dropzone.js')}}"></script>
@endsection