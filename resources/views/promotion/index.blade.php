@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Promotions</span></h4>
        </div>
        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                @canany(['add-new-account'])
                <a href="{{ route('promotions.create') }}" class="btn btn-link btn-primary btn-sm text-white">
                    <i class="icon-plus-circle2 text-white mr-1"></i> Add Promotion
                </a>
                @endcanany
            </div>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Promotions</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    @include('includes.success-msg')
    @include('includes.error-msg')
    
    <div class="card">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>#</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if($promotions->count() > 0)
                        @foreach($promotions as $key => $row)
                        <tr>
                            <td>{{($promotions->perPage() * ($promotions->currentPage() - 1)) + $key + 1}}</td>
                            <td></td>
                            <td>{{ $row->title }}</td>
                            <td>{{ $row->description }}</td>
                            <td>
                                @if($row->state)
                                    <span class="badge badge-success">Active</span>
                                @else 
                                    <span class="badge badge-danger">Deactive</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('promotions.edit',$row->id) }}" class="badge badge-success"><i class="icon-pen6 sm"></i> Edit</a>
                            </td>
                        </tr>
                        @endforeach
                    @else 
                        <tr><td colspan="8">No Data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            @if($promotions->hasMorePages())
                <div class="mb-2">
                    {!! $promotions->appends(Input::except('page'))->render() !!}
                </div>
            @endif
            <div>
                Showing {{$promotions->firstItem() }} to {{ $promotions->lastItem()}}
                of  {{$promotions->total()}} entries
            </div>
        </div>
    </div>
</div>
@endsection