@extends('layouts.master')

@section('custom-css')
<style>
    .select2-container--default .select2-selection--multiple .select2-selection__choice{
        background-color:dodgerblue !important;
        border: none !important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove{
        color:white !important;
    }
</style>
@endsection

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Sale KPI Form</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Sale KPI</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        {{ Form::open(['route' => 'user.salekpi.showkpi', 'method' => 'GET', 'id' => 'form']) }}
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-lg-7">
                    <div id="msg"></div>
                    @include('includes.error-msg')
                    @include('includes.success-msg')

                    <div class="form-group">
                        <label class="form-group-float-label font-weight-semibold is-visible">Select Users</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="icon-users4"></i>
                                </span>
                            </span>
                            <select name="users[]" data-placeholder="Select users..." multiple="multiple" class="form-control select">
                                @foreach($users as $row)
                                    <option value="{{ $row->id }} ">{{ $row->account_number }} {{ $row->username }}</option>
                                @endforeach
                            </select>
                        </div>									
                    </div>
                    <div class="form-group">
                        <label class="form-group-float-label font-weight-semibold is-visible">Begin Time</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar3"></i></span>
                            </span>
                            {{Form::text("start_dt", date('Y-m-d 00:00:00'),
                                ["class" => "form-control datetimepicker"])
                            }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-group-float-label font-weight-semibold is-visible">End Time</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar3"></i></span>
                            </span>
                            {{Form::text("end_dt", date('Y-m-d 00:00:00'),
                                ["class" => "form-control datetimepicker"])
                            }}
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-info"><i class="icon-thumbs-up2 mr-1"></i> {{$settings['language']['LANG_LABEL_SEARCH'] }}</button>
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection

@section('page-script')

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>
@endsection

@section('custom-js')
    <script>
        $(document).ready(function(){
            $('.select').select2();
        })
    </script>
@endsection