@extends('layouts.master')

@section('custom-css')
<style>
</style>
@endsection

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Sale KPI</span></h4>
        </div>
        <div class="header-elements d-none">
            @canany(['download-sale-kpi'])
                <div class="d-flex justify-content-center">
                    <button class="btn btn-link btn-primary btn-sm text-white" id="downloadExcelButton" 
                    data-route="{{route('user.salekpi.download')}}" 
                    data-toggle="modal" data-target="#downlod_modal">
                        <i class="icon-file-excel text-white mr-1"></i> {{ $settings['language']['LANG_LABEL_DOWN'] }}
                    </button>
                </div>  
            @endcanany
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{route('user.selection.form')}}" class="breadcrumb-item"></i>Sale KPI</a>
                <span class="breadcrumb-item active">{{request()->start_dt.' To '.request()->end_dt  }}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        <div class="card-header">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr class="bg-slate-800">
                            <th>#</th>
                            <th>Account No</th>
                            <th>User Name</th>
                            <th>Province</th>
                            <th>Bet Amount</th>
                            <th>Win Amount</th>
                            <th>Top Up Finance</th>
                            <th>Withdraw Finance</th>
                            <th>Top Up Sale</th>
                            <th>Withdraw Sale</th>
                        </tr>
                    </thead>
                    <tbody>
                           
                        @if(isset($data) && count($data) > 0)
                            @php $i = 1; @endphp
                            @foreach($data as $user)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ isset($user['accountno']) ? $user['username'] : 'N/A' }}</td>
                                    <td>{{ isset($user['username']) ? $user['username'] : 'N/A'}}</td>
                                    <td>{{ isset($user['province']) && !empty($user['province']) ? $user['province'] : 'N/A' }}</td>
                                    <th>{{ isset($user['amount']) ? $user['amount'] : '0' }}</th>
                                    <th>{{ isset($user['win_amount']) ? $user['win_amount'] : '0' }}</th>
                                    <td>{{ isset($user['finance']['recharge']) ? $user['finance']['recharge'] : '0' }}</td>
                                    <td>{{ isset($user['finance']['withdraw']) ? $user['finance']['withdraw'] : '0' }}</td>
                                    <td>{{ isset($user['sale']['recharge']) ? $user['sale']['recharge'] : '0' }}</td>
                                    <td>{{ isset($user['sale']['withdraw']) ? $user['sale']['withdraw']  : '0' }}</td>
                                </tr> 
                                @php $i++; @endphp
                            @endforeach   
                        @else 
                            <tr>
                            <td colspan = "10">No data!</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="downlod_modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-teal-400">
                <h5 class="modal-title">Download Excel</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body text-center p-4 h5 m-0" id="downloadModalBody">
                <i class='icon-spinner2 spinner'></i> Downloading...
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>
@endsection

@section('custom-js')
    <script>
        $(document).ready(function(){
            $('.select').select2();
            $('.select').val(1);
        })
    </script>
@endsection