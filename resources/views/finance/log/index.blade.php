@extends('layouts.master')

@section('custom-css')
<style>
    #print_preview_modal table{
        font-size: 13px;
    }
    #print_preview_modal table tr td:first-child{
        font-weight: 600;
        padding: 5px;
    }
    @media print {
        table td{
            font-size:22px;
            padding: 10px !important;
        }
    }
    .table td.group-btn-action{
        min-width: 50px;
    }
    .table td{
        font-size:11px;
    }
</style>
@endsection

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Account Log</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        <div class="header-elements d-none">
            @canany(['download-transaction-log'])
            <div class="d-flex justify-content-center">
                <button class="btn btn-link btn-primary btn-sm text-white" id="downloadExcelButton" 
                data-route="{{route('accounts.logs.download')}}" 
                data-toggle="modal" data-target="#downlod_modal">
                    <i class="icon-file-excel text-white mr-1"></i> {{ $settings['language']['LANG_LABEL_DOWN'] }}
                </button>
            </div>
            @endcanany
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{ route('accounts.index') }}" class="breadcrumb-item">{{ $settings['language']['LANG_MENU_FINANCE'] }}</a>
                <a href="{{ route('accounts.index') }}" class="breadcrumb-item">{{ $settings['language']['LANG_MENU_ACCOUNT'] }}</a>
                <span class="breadcrumb-item active">Log</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    {{ Form::open(['route' => 'accounts.logs.index', 'method' => 'GET', 'class' => 'form-inline d-block d-sm-flex']) }}
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">{{ $settings['language']['LANG_LABEL_ACCOUNT_LOG_TYPE'] }}</div>
                </div>
                {{Form::select("log_type",['all' => 'All',Account::IN => 'In', Account::OUT => 'Out'], isset($_GET['log_type']) ? $_GET['log_type'] : null,["class" => "form-control input-sm"])}}
            </div>
        </div>
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">{{ $settings['language']['LANG_MENU_ACCOUNT'] }}</div>
                </div>
                {{Form::select("account",$accounts, isset($_GET['account']) ? $_GET['account'] : null,["class" => "form-control input-sm"])}}
            </div>
        </div>
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">{{ $settings['language']['LANG_LABEL_START_DT'] }}</div>
                </div>
                <input type="text" class="form-control datetimepicker" value="{{isset($_GET['begin_time']) ? $_GET['begin_time'] : date('Y-m-d 00:00:00',strtotime('-7 days'))}}" name="begin_time">
            </div>
        </div>
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">{{ $settings['language']['LANG_LABEL_END_DT'] }}</div>
                </div>
                <input type="text" class="form-control datetimepicker" value="{{isset($_GET['end_time']) ? $_GET['end_time'] : date('Y-m-d 23:59:00')}}" name="end_time">
            </div>
        </div>
        {{Form::submit('Search',['class' => 'btn btn-sm btn-primary mb-2 mr-2'])}}
        <a href="{{route('accounts.logs.index')}}" class="btn btn-warning btn-sm mb-2">Reset</a>
    {{ Form::close() }}
    
    <div class="card">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>Transaction</th>
                        <th>{{ $settings['language']['LANG_MENU_ACCOUNT'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_ACCOUNT_NO'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_IN'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_OUT'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_LOG_BALANCE'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_TO_ACCOUNT'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_MANAGER'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_DATETIME'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_ABSTRACT'] }}</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if($logs->count() > 0)
                        @foreach($logs as $key => $row)
                        <tr>
                            <td>{{ $row->log_number }}</td>
                            <td>{{ $row->account ? $row->account->name : '' }}</td>
                            <td>{{ $row->account ? $row->account->number : '' }}</td>
                            <td>{{ $row->log_type == Account::IN ? currencyFormat($row->amount,0) : currencyFormat(0) }}</td>
                            <td>{{ $row->log_type == Account::OUT ?  currencyFormat($row->amount* (-1),0) : currencyFormat(0) }}</td>
                            <td>(R){{ currencyFormat($row->balance,0) }}</td>
                            <td>
                                @if($row->to_type == Account::SYSTEM_ACCOUNT_TYPE) 
                                    {{ $settings['language'][Account::systemAccountType($row->to_type)] }} {{ $row->toSystemAccount ? '('.$row->toSystemAccount->name.')' : '' }}
                                @elseif($row->to_type == Account::USER_ACCOUNT_TYPE) 
                                    {{ $settings['language'][Account::systemAccountType($row->to_type)] }} {{ $row->toUser ? '('.$row->toUser->username.')' : '' }}
                                @endif
                            </td>
                            <td>{{ $row->manager ? $row->manager->username : '' }}</td>
                            <td>{{ $row->created_at }}</td>
                            <td>{{ $settings['language'][$row->abstract] }}</td>
                            <td class="group-btn-action">
                                @canany('view-transaction-log')
                                    <button class="btn btn-sm btn-outline bg-teal-400 border-teal text-teal-400 btn-icon border-2 print" 
                                    data-popup="tooltip" title="" data-placement="bottom" data-original-title="Print"
                                    data-toggle="modal" data-target="#print_preview_modal" 
                                    data-route="{{ route('system.account.getLogInfo',$row->id) }}">
                                        <i class="icon-printer"></i>
                                    </button>
                                @endcanany
                            </td>
                        </tr>
                        @endforeach
                    @else 
                        <tr><td colspan="10">No Data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            @if($logs->hasMorePages())
                <div class="mb-2">
                    {!! $logs->appends(Input::except('page'))->render() !!}
                </div>
            @endif
            <div>
                Showing {{$logs->firstItem()}} to {{$logs->lastItem()}}
                of  {{$logs->total()}} entries
            </div>
        </div>
    </div>
</div>
<div id="print_preview_modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-teal-400">
                <h5 class="modal-title">{{ $settings['language']['LANG_LABEL_ACCOUNT_LOG'] }}</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <table class="w-100" id="table">
                    <tbody>
                        <tr><td>Transaction No:</td><td class="transaction"></td></tr>
                        <tr><td>{{ $settings['language']['LANG_LABEL_MANAGER'] }}:</td><td class="manager"></td></tr>
                        <tr><td>{{ $settings['language']['LANG_MENU_ACCOUNT'] }}:</td><td class="account"></td></tr>
                        <tr><td>{{ $settings['language']['LANG_LABEL_ACCOUNT_NO'] }}:</td><td class="account-no"></td></tr>
                        <tr><td>Log Type:</td><td class="log-type"></td></tr>
                        <tr><td>Amount:</td><td class="amount"></td></tr>
                        <tr><td>{{ $settings['language']['LANG_LABEL_LOG_BALANCE'] }}:</td><td class="balance"></td></tr>
                        <tr><td>{{ $settings['language']['LANG_LABEL_TO_ACCOUNT'] }}:</td><td class="to-account"></td></tr>
                        <tr><td>{{ $settings['language']['LANG_LABEL_DATETIME'] }}:</td><td class="date-time"></td></tr>
                        <tr><td>{{ $settings['language']['LANG_LABEL_ABSTRACT'] }}:</td><td class="abstract"></td></tr>
                        <tr><td></td><td></td></tr>
                        <tr><td>Auditor:</td><td class="auditor"></td></tr>
                    </tbody>
                </table>
            </div>

            <div class="modal-footer mx-auto">
                <button type="button" class="btn bg-teal-400" onclick="window.print();"><i class="icon-printer2 mr-2"></i> {{ $settings['language']['LANG_LABEL_PRINT'] }}</button>
            </div>
        </div>
    </div>
    <div class="printable mt-5">    
        <h1 class="text-center printable text-teal" style="font-size:36px; font-weight:bold">{{ $settings['language']['LANG_LABEL_ACCOUNT_LOG'] }}</h1>
        <div class="table-responsive mx-5">
            <table class="w-100 mx-auto mt-4">
                <tr class="w-100"><td style="width:50%">Transaction No:</td><td class="transaction" style="width:70%"></td></tr>
                <tr><td>{{ $settings['language']['LANG_LABEL_MANAGER'] }}:</td><td class="manager"></td></tr>
                <tr><td>{{ $settings['language']['LANG_MENU_ACCOUNT'] }}:</td><td class="account"></td></tr>
                <tr><td>{{ $settings['language']['LANG_LABEL_ACCOUNT_NO'] }}:</td><td class="account-no"></td></tr>
                <tr><td>Log Type:</td><td class="log-type"></td></tr>
                <tr><td>Amount:</td><td class="amount"></td></tr>
                <tr><td>{{ $settings['language']['LANG_LABEL_LOG_BALANCE'] }}:</td><td class="balance"></td></tr>
                <tr><td>{{ $settings['language']['LANG_LABEL_TO_ACCOUNT'] }}:</td><td class="to-account"></td></tr>
                <tr><td>{{ $settings['language']['LANG_LABEL_DATETIME'] }}:</td><td class="date-time"></td></tr>
                <tr><td>{{ $settings['language']['LANG_LABEL_ABSTRACT'] }}:</td><td class="abstract"></td></tr>
                <tr><td>Auditor:</td><td class="auditor"></td></tr>
            </table>
        </div>
    </div>

</div>
<div id="downlod_modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-teal-400">
                <h5 class="modal-title">Download Excel</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body text-center p-4 h5 m-0" id="downloadModalBody">
                <i class='icon-spinner2 spinner'></i> Downloading...
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>
@endsection