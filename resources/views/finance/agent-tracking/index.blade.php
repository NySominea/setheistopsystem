@extends('layouts.master')

@section('custom-css')
<style>
    .table td.group-btn-action{
        min-width: 50px;
    }
    .table td{
        font-size:11px;
        text-align: justify;
    }
    #cycle{
        width:200px;
    }

</style>
@endsection

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">List of doubtful agents</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        <div class="header-elements d-none">
            @canany(['download-agent-tracking'])
            <div class="d-flex justify-content-center">
                <button class="btn btn-link btn-primary btn-sm text-white" id="downloadExcelButton" 
                    data-route="{{ route('finance.agent-tracking.download') }}" 
                    data-toggle="modal" data-target="#downlod_modal">
                    <i class="icon-file-excel text-white mr-1"></i> {{ $settings['language']['LANG_LABEL_DOWN'] }}
                </button>
            </div>
            @endcanany
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ route('dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Finance</span>
                <span class="breadcrumb-item active">Agents Tracking</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    {{ Form::open(['route' => 'finance.agent-tracking', 'method' => 'GET', 'class' => 'form-inline d-block d-sm-flex']) }}
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">Cycle Number</div>
                </div>
                {{ Form::select('cycle', ['0' => 'Choose'] + $cycles,request()->cycle ? : $cid, ['class' => 'form-control', 'id' => 'cycle']) }}
            </div>
        </div>
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">Minimum Number</div>
                </div>
                {{ Form::number('num', request()->num ? : $num, ['class' => 'form-control'.($errors->has('num') ? ' is-invalid':''), 'id' => 'num', 'placeholder' => 'Ex: 30, 40, 50, ...', 'autocomplete'=>'off']) }}
            </div>
        </div>
                 
    {{ Form::submit('Search',['class' => 'btn btn-sm btn-primary mb-2 mr-2']) }}
        <a href="{{ route('finance.agent-tracking') }}" class="btn btn-warning btn-sm mb-2">Reset</a>
    {{ Form::close() }}
    
    <div class="">
        <ul class="nav nav-tabs nav-justified">
            <li class="nav-item"><a href="#agent" class="nav-link active show" data-toggle="tab">Agent</a></li>
            <li class="nav-item"><a href="#sale" class="nav-link" data-toggle="tab">Sale</a></li>
            <li class="nav-item"><a href="#area" class="nav-link" data-toggle="tab">Area Manager</a></li>
            <li class="nav-item"><a href="#director" class="nav-link" data-toggle="tab">Director</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane fade show active" id="agent">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr class="bg-slate-800">
                                <th>Province</th>
                                <th>Director</th>
                                <th>Area Manager</th>
                                <th>Sale</th>
                                <th>UserName</th>
                                <th>AccountNo</th>
                                <th>Count</th>
                                <th style="min-width:300px;width:500px">Bet Number</th>
                                <th>Download Ticket</th>
                            </tr>
                        </thead>
                        <tbody>
                                @if(isset($data['agent']) && count($data) > 0)
                                @foreach($data['agent'] as $agent)
                                    @if(count($agent['bets']) >= request()->get('num',30))
                                    <tr>

                                        <td>{{ $agent['province'] }}</td>
                                        <td>{{ $agent['director'] }}</td>
                                        <td>{{ $agent['area'] }}</td>
                                        <td>{{ $agent['sale'] }}</td>
                                        <td>{{ $agent['username'] }}</td>
                                        <td>{{ $agent['accountno'] }}</td>
                                        <td>{{ count($agent['bets']) }}</td>
                                        <td style="line-height:25px;">{{ implode(', ',(array) $agent['bets']) }}</td>
                                        <td class="text-center">
                                            <a href="{{route('finance.agent-tracking.download-by-user')}}" 
                                                class="btn btn-sm btn-success downloadExcelButton"
                                                data-route="{{ route('finance.agent-tracking.download-by-user') }}?level={{$agent['level']}}&parent_id={{$agent['id']}}&cycle_id={{$cid}}" 
                                                data-toggle="modal" data-target="#downlod_modal">
                                                <i class="icon-file-excel text-white">
                                            </a>
                                        </td>
                                    </tr>
                                    @endif  
                                @endforeach   
                            @else 
                                <tr>
                                <td colspan = "5">No data!</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="tab-pane fade" id="sale">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr class="bg-slate-800">
                                <th>Province</th>
                                <th>Director</th>
                                <th>Area Manager</th>
                                <th>Sale</th>
                                <th style="min-width:100px;width:200px">Agent</th>
                                <th style="min-width:100px;width:200px">Agent AccountNo</th>
                                <th>Count</th>
                                <th style="min-width:300px;width:500px">Bet Number</th>
                                {{-- <th>Download Ticket</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                                @if(isset($data['sale']) && count($data) > 0)
                                @foreach($data['sale'] as $agent)
                                    @if(count($agent['bets']) >= request()->get('num',30))
                                    <tr> 
                                        <td>{{ $agent['province'] }}</td>
                                        <td>{{ $agent['director'] }}</td>
                                        <td>{{ $agent['area'] }}</td>
                                        <td>{{ $agent['sale'] }}</td>
                                        <td>{{ implode(', ',(array)$agent['username']) }}</td>
                                        <td>{{ implode(', ',(array)$agent['accountno']) }}</td>
                                        <td>{{ count($agent['bets']) }}</td>
                                        <td style="line-height:25px;"> {{ implode(', ',(array) $agent['bets']) }}</td>
                                        {{-- <td class="text-center">
                                            <a href="{{route('finance.agent-tracking.download-by-user')}}" 
                                                class="btn btn-sm btn-success downloadExcelButton"
                                                data-route="{{ route('finance.agent-tracking.download-by-user') }}?level={{$agent['level']}}&parent_id={{$agent['id']}}&cycle_id={{$cid}}" 
                                                data-toggle="modal" data-target="#downlod_modal">
                                                <i class="icon-file-excel text-white">
                                            </a>
                                        </td> --}}
                                    </tr>  
                                    @endif
                                @endforeach   
                            @else 
                                <tr>
                                <td colspan = "5">No data!</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="tab-pane fade" id="area">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr class="bg-slate-800">
                                <th>Province</th>
                                <th>Director</th>
                                <th >Area Manager</th>
                                <th style="min-width:150px;width:200px">Sale</th>
                                <th style="min-width:100px;width:250px">Agent</th>
                                <th style="min-width:150px;width:250px">Agent AccountNo</th>
                                <th>Count</th>
                                <th style="min-width:300px;width:500px">Bet Number</th>
                                {{-- <th>Download Ticket</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                                @if(isset($data['area']) && count($data) > 0)
                                @foreach($data['area'] as $agent)
                                    @if(count($agent['bets']) >= request()->get('num',30))
                                    <tr> 
                                        <td>{{ $agent['province'] }}</td>
                                        <td>{{ $agent['director'] }}</td>
                                        <td>{{ $agent['area'] }}</td>
                                        <td>{{ implode(', ',(array)$agent['sale']) }}</td>
                                        <td>{{ implode(', ',(array)$agent['username']) }}</td>
                                        <td>{{ implode(', ',(array)$agent['accountno']) }}</td>
                                        <td>{{ count($agent['bets']) }}</td>
                                        <td style="line-height:25px;"> {{ implode(', ',(array) $agent['bets']) }}</td>
                                        {{-- <td class="text-center">
                                            <a href="{{route('finance.agent-tracking.download-by-user')}}" 
                                                class="btn btn-sm btn-success downloadExcelButton"
                                                data-route="{{ route('finance.agent-tracking.download-by-user') }}?level={{$agent['level']}}&parent_id={{$agent['id']}}&cycle_id={{$cid}}" 
                                                data-toggle="modal" data-target="#downlod_modal">
                                                <i class="icon-file-excel text-white">
                                            </a>
                                        </td> --}}
                                    </tr>  
                                    @endif
                                @endforeach   
                            @else 
                                <tr>
                                <td colspan = "5">No data!</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="tab-pane fade" id="director">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr class="bg-slate-800">
                                <th>Province</th>
                                <th>Director</th>
                                <th>Area Manager</th>
                                <th style="min-width:150px;width:200px">Sale</th>
                                <th style="min-width:150px;width:250px">Agent</th>
                                <th style="min-width:150px;width:250px">Agent AccountNo</th>
                                <th>Count</th>
                                <th style="min-width:300px;width:500px">Bet Number</th>
                                {{-- <th>Download Ticket</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                                @if(isset($data['director']) && count($data) > 0)
                                @foreach($data['director'] as $agent)
                                    @if(count($agent['bets']) >= request()->get('num',30))
                                    <tr> 
                                        <td>{{ $agent['province'] }}</td>
                                        <td>{{ $agent['director'] }}</td>
                                        <td>{{ implode(', ',(array)$agent['area']) }}</td>
                                        <td>{{ implode(', ',(array)$agent['sale']) }}</td>
                                        <td>{{ implode(', ',(array)$agent['username']) }}</td>
                                        <td>{{ implode(', ',(array)$agent['accountno']) }}</td>
                                        <td>{{ count($agent['bets']) }}</td>
                                        <td style="line-height:25px;"> {{ implode(', ',(array) $agent['bets']) }}</td>
                                        {{-- <td class="text-center">
                                            <a href="{{route('finance.agent-tracking.download-by-user')}}" 
                                                class="btn btn-sm btn-success downloadExcelButton"
                                                data-route="{{ route('finance.agent-tracking.download-by-user') }}?level={{$agent['level']}}&parent_id={{$agent['id']}}&cycle_id={{$cid}}" 
                                                data-toggle="modal" data-target="#downlod_modal">
                                                <i class="icon-file-excel text-white">
                                            </a>
                                        </td> --}}
                                    </tr>  
                                    @endif
                                @endforeach   
                            @else 
                                <tr>
                                <td colspan = "5">No data!</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        {{-- <div class="card-footer">
            @if($data->hasMorePages())
                <div class="mb-2">
                    {!! $data->appends(Input::except('page'))->render() !!}
                </div>
            @endif
            <div>
                Showing {{$data->firstItem()}} to {{$data->lastItem()}}
                of  {{$data->total()}} entries
            </div>
        </div> --}}
    </div>
</div>
<div id="downlod_modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-teal-400">
                <h5 class="modal-title">Download Excel</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body text-center p-4 h5 m-0" id="downloadModalBody">
                <i class='icon-spinner2 spinner'></i> Downloading...
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>
@endsection