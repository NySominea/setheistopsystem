@extends('layouts.master')

@section('custom-css')
<style>
</style>
@endsection

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Profit Review on {{ request()->start_date  }} To {{ request()->end_date  }}</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{route('finance.form-selection')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Profit Review</a>
                <span class="breadcrumb-item active">{{request()->date}}</span>
            </div>
        </div>
        <div class="d-flex justify-content-center ml-1">
            <button class="btn btn-link btn-primary btn-sm text-white" id="downloadExcelButton" 
            data-route="{{route('finance.profit.download')}}" 
            data-toggle="modal" data-target="#downlod_modal">
                <i class="icon-file-excel text-white mr-1"></i> {{ $settings['language']['LANG_LABEL_DOWN'] }}
            </button>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        <div class="card-header">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr class="bg-slate-800">
                            <th>#</th>
                            <th>Account No</th>
                            <th>User Name</th>
                            <th>Bet Amount</th>
                            <th>Win Amount</th>
                            <th>Profit</th>
                            <th>Type</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($data) && count($data) > 0)
                            @php $i = 1; @endphp
                            @foreach($data as $user)
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $user['accountno'] }}</td>
                                <td>{{ $user['username'] }}</td>
                                <td>R {{ currencyFormat($user['bet_amount']) }}</td>
                                <td>R {{ currencyFormat($user['win_amount']) }}</td>
                                <td class= "{{ $user['profit'] > 0 ? 'text-success' : 'text-danger' }}" >R {{ currencyFormat($user['profit']) }}</td>
                                <td> {{ $user['type']}} </td>
                            </tr> 
                            @php $i++; @endphp
                           
                            @endforeach   
                        @else 
                            <tr>
                            <td colspan = "4">No data!</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="downlod_modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-teal-400">
                <h5 class="modal-title">Download Excel</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body text-center p-4 h5 m-0" id="downloadModalBody">
                <i class='icon-spinner2 spinner'></i> Downloading...
            </div>
        </div>
    </div>
</div>

@endsection

@section('page-script')

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>
@endsection

@section('custom-js')
    <script>
        $(document).ready(function(){
            $('.select').select2();
        })
    </script>
@endsection