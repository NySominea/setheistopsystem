@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{$settings['language']['LANG_LABEL_ACCOUNT']}}</span></h4>
        </div>
        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                {{-- @canany(['add-class-manager'])
                <a href="{{ route('accounts.categories.index') }}" class="btn btn-link bg-teal-400 btn-sm text-white mr-2">
                    <i class="icon-eye text-white mr-1"></i> Class Manager
                </a>
                @endcanany --}}
                @canany(['add-new-account'])
                <a href="{{ route('accounts.create') }}" class="btn btn-link btn-primary btn-sm text-white">
                    <i class="icon-plus-circle2 text-white mr-1"></i> Add Account
                </a>
                @endcanany
            </div>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_MENU_FINANCE'] }}</span>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_MENU_ACCOUNT'] }}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    @include('includes.success-msg')
    @include('includes.error-msg')
    
    <div class="card">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>#</th>
                        {{-- <th>{{ $settings['language']['LANG_LABEL_ACCOUNT_LOG_TYPE'] }}</th>
                        <th>Class</th> --}}
                        <th>Title</th>
                        <th>{{ $settings['language']['LANG_LABEL_NUMBER'] }}</th>
                        <th>Name</th>
                        <th colspan="2">{{ $settings['language']['LANG_LABEL_BALANCE'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_STATE'] }}</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if($accounts->count() > 0)
                        @foreach($accounts as $key => $row)
                        <tr>
                            <td>{{($accounts->perPage() * ($accounts->currentPage() - 1)) + $key + 1}}</td>
                            {{-- <td>{{ $row->type ? $row->type->name : '' }}</td>
                            <td>{{ $row->category ? $row->category->name : '' }}</td> --}}
                            <td>{{ $row->title }}</td>
                            <td>{{ $row->number }}</td>
                            <td>{{ $row->name }}</td>
                            <td>(R) {{ currencyFormat($row->balance) }}</td>
                            <td>($) {{ currencyFormat($row->balance * $settings['exchangeRate']['rielToDollar']) }}</td>
                            <td>
                                @if($row->state)
                                    <span class="badge badge-success">Active</span>
                                @else 
                                    <span class="badge badge-danger">Deactive</span>
                                @endif
                            </td>
                            <td class="group-btn-action">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-outline bg-teal-400 text-teal-400 border-teal-400 border-2 dropdown-toggle mr-1" data-toggle="dropdown" aria-expanded="false"><i class="icon-cog5"></i> Action</button>
                                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(135px, 36px, 0px);">
                                        @canany(['platform-account-access-transfer'])
                                        <a href="{{route('accounts.deposit.edit',$row->id)}}" class="dropdown-item"><i class="icon-coin-dollar"></i> Deposit</a>
                                        <div class="dropdown-divider m-0"></div>
                                        <a href="{{ route('accounts.transfer.edit',$row->id) }}" class="dropdown-item"><i class="icon-paperplane"></i> Transfer</a>
                                        <div class="dropdown-divider m-0"></div>
                                        @endcanany
                                        @canany(['view-transaction-log'])
                                        <a href="{{route('accounts.logs.index')}}?account={{$row->id}}" class="dropdown-item"><i class="icon-list"></i> Account Log</a>
                                        <div class="dropdown-divider m-0"></div>
                                        @endcanany
                                        @canany(['account-modification'])
                                        <a href="{{route('accounts.edit',$row->id) }}" class="dropdown-item"><i class="icon-pen6"></i> Edit</a>
                                        @endcanany
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    @else 
                        <tr><td colspan="8">No Data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            @if($accounts->hasMorePages())
                <div class="mb-2">
                    {!! $accounts->appends(Input::except('page'))->render() !!}
                </div>
            @endif
            <div>
                Showing {{$accounts->firstItem() }} to {{ $accounts->lastItem()}}
                of  {{$accounts->total()}} entries
            </div>
        </div>
    </div>
</div>
@endsection