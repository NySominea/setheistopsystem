@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Account Transfer</span></h4>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="{{ route('accounts.index') }}" class="btn btn-primary btn-sm">
                    <i class="icon-square-left mr-1"></i> Back
                </a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{ route('accounts.index') }}" class="breadcrumb-item">{{ $settings['language']['LANG_MENU_FINANCE'] }}</a>
                <a href="{{ route('accounts.index') }}" class="breadcrumb-item">{{ $settings['language']['LANG_LABEL_ACCOUNT'] }}</a>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_LABEL_TRANS'] }}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        {{ Form::open(['route' => ['accounts.transfer.update',$account->id], 'method' => 'PUT']) }}
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Transfer Account</label>
                        <div class="position-relative">
                            {{Form::text("title",isset($account) ? $account->title.'('.$account->name.')' : '',
                                ["class" => "form-control", 'readonly'])
                            }}
                        </div>
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Account Balance (R)</label>
                        <div class="position-relative">
                            {{Form::text("balance",isset($account) ? currencyFormat($account->balance) : '',
                                ["class" => "form-control", 'readonly'])
                            }}
                        </div>
                    </div>
                    <div class="form-group select2">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('to_account_id')?'text-danger':'' }}">Transfer to Account</label>
                        {{Form::select("to_account_id",$accounts,null,["class" => "form-control"])}}
                        @if($errors->has('to_account_id'))
                            <span class="form-text text-danger">{{ $errors->first('to_account_id') }}</span>
                        @endif
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('amount')?'text-danger':'' }}">Transfer Amount (R)</label>
                        <div class="position-relative">
                            {{Form::text("amount",0,
                                ["class" => "form-control input-amount ".($errors->has('amount')?'border-danger':''), "min" => 0,'maxlength' => 32])
                            }}
                            @if($errors->has('amount'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('amount'))
                            <span class="form-text text-danger">{{ $errors->first('amount') }}</span>
                        @endif
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('pay_password')?'text-danger':'' }}">{{ $settings['language']['LANG_LABEL_PAYPASS'] }}</label>
                        <div class="position-relative">
                            {{Form::password("pay_password",
                                ["class" => "form-control ".($errors->has('pay_password')?'border-danger':''),"placeholder" => "Enter pay password"])
                            }}
                            @if($errors->has('pay_password'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('pay_password'))
                            <span class="form-text text-danger">{{ $errors->first('pay_password') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success">
                <i class="icon-folder mr-1"></i> {{ $settings['language']['LANG_BTN_SAVE'] }}
            </button>
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection