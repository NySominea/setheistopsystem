@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Add Account</span></h4>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="{{ route("accounts.index") }}" class="btn btn-primary btn-sm">
                    <i class="icon-square-left mr-1"></i> Back
                </a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{ route('accounts.index') }}" class="breadcrumb-item">{{ $settings['language']['LANG_MENU_FINANCE'] }}</a>
                <a href="{{ route('accounts.index') }}" class="breadcrumb-item">{{ $settings['language']['LANG_LABEL_ACCOUNT'] }}</a>
                <span class="breadcrumb-item active">{{ isset($account) ? 'Edit' : 'Add'}}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        @if(isset($account))
        {{ Form::model($account,['route' => ['accounts.update',$account->id], 'method' => 'PUT']) }}
        @else
        {{ Form::open(['route' => 'accounts.store', 'method' => 'POST']) }}
        @endif
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    {{-- <div class="form-group select2">
                        <label>Account Type</label>
                        {{Form::select("type_id",$types, isset($account) && $account->type_id ? $account->type->id : null,["class" => "form-control"])}}
                    </div>
                    <div class="form-group select2">
                        <label>Account Class</label>
                        {{Form::select("category_id",$categories, isset($account) && $account->category_id ? $account->category->id : null,["class" => "form-control"])}}
                    </div> --}}
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('title')?'text-danger':'' }}">{{ $settings['language']['LANG_LABEL_ACCOUNT_TITLE'] }}</label>
                        <div class="position-relative">
                            {{Form::text("title",old("title") ? old("title") : (isset($account) ? $account->title : ''),
                                ["class" => "form-control ".($errors->has('username')?'border-danger':''),"placeholder" => "Enter account title"])
                            }}
                            @if($errors->has('title'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('title'))
                            <span class="form-text text-danger">{{ $errors->first('title') }}</span>
                        @endif
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('number')?'text-danger':'' }}">{{ $settings['language']['LANG_LABEL_ACCOUNT_NUM'] }} </label>
                        <div class="position-relative">
                            {{Form::text("number",old("number") ? old("number") : (isset($account) ? $account->number : ''),
                                ["class" => "form-control ".($errors->has('number') ? 'border-danger' : ''),"placeholder" => "Enter account number"])
                            }}
                            @if($errors->has('number'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('number'))
                            <span class="form-text text-danger">{{ $errors->first('number') }}</span>
                        @endif
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('name')?'text-danger':'' }}">{{ $settings['language']['LANG_LABEL_ACCOUNT_NAME'] }} </label>
                        <div class="position-relative">
                            {{Form::text("name",old("name") ? old("name") : (isset($account) ? $account->name : ''),
                                ["class" => "form-control ".($errors->has('name')?'border-danger':''),"placeholder" => "Enter account name"])
                            }}
                            @if($errors->has('name'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('name'))
                            <span class="form-text text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Sort (0 ~ 99)</label>
                        <div class="position-relative">
                            {{Form::number("sort",old("sort") ? old("sort") : (isset($account) ? $account->sort : '0'),
                                ["class" => "form-control", "min" => 0])
                            }}
                        </div>
                    </div>
                    <div class="form-group mb-3 mb-md-2">
                        <label class="d-block font-weight-semibold">State</label>
                        <div class="form-check custom-control-inline">
                            <label class="form-check-label">
                                <input type="radio" name="state" value="1" class="form-check-input-styled-success" data-fouc {{ isset($account) && $account->state = 1 ? 'checked' : '' }} checked>
                                Enable
                            </label>
                        </div>
                        <div class="form-check custom-control-inline">
                            <label class="form-check-label">
                                <input type="radio" name="state" value="0" class="form-check-input-styled-danger" data-fouc 
                                    {{ isset($account) && $account->state = 0 ? 'checked' : '' }}>
                                    Disable
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success">
                <i class="icon-folder mr-1"></i>{{ $settings['language']['LANG_BTN_SAVE'] }}
            </button>
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection

@section('page-script')
<script src="/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="/global_assets/js/demo_pages/form_checkboxes_radios.js"></script>
@endsection