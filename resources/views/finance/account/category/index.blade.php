@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Account Class Manager</span></h4>
        </div>
        <div class="header-elements d-none">
            {{-- @canany('view-platform-account') --}}
                <div class="d-flex justify-content-center">
                    <a href="{{ route('accounts.categories.create') }}" class="btn btn-link btn-primary btn-sm text-white">
                        <i class="icon-plus-circle2 text-white mr-1"></i> Add Class
                    </a>
                </div>
            {{-- @endcanany --}}
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{ route('accounts.index') }}" class="breadcrumb-item">{{ $settings['language']['LANG_MENU_FINANCE'] }}</a>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_MENU_ACCOUNT'] }}</span>
                <span class="breadcrumb-item active">Class Manager</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    @include('includes.success-msg')
    @include('includes.error-msg')
    <div class="card">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>#</th>
                        <th>{{ $settings['language']['LANG_LABEL_ACCOUNT_TYPE'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_CLASS_NAME'] }}</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if($categories->count() > 0)
                        @foreach($categories as $key => $row)
                        <tr>
                            <td>{{($categories->perPage() * ($categories->currentPage() - 1)) + $key + 1}}</td>
                            <td>{{ $row->type ? $row->type->name : '' }}</td>
                            <td>{{ $row->name }}</td>
                            @canany('view-platform-account')
                            <td class="group-btn-action">
                                <a href="{{ route('accounts.categories.edit',$row->id) }}" class="btn btn-sm btn-outline bg-teal-400 text-teal-400 border-teal-400 btn-icon border-2" data-popup="tooltip" title="" data-placement="bottom" data-original-title="Edit"><i class="icon-pencil7"></i> Edit</a>
                            </td>
                            @endcanany
                            {{-- <td class="group-btn-action">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-outline bg-teal-400 text-teal-400 border-teal-400 border-2 dropdown-toggle mr-1" data-toggle="dropdown" aria-expanded="false"><i class="icon-cog5"></i> Action</button>
                                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(135px, 36px, 0px);">
                                        <a href="{{route('accounts.categories.edit',$row->id)}}" class="dropdown-item"><i class="icon-pen6"></i> Edit</a>
                                    </div>
                                </div>
                            </td> --}}
                        </tr>
                        @endforeach
                    @else 
                        <tr><td colspan="3">No Data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            @if($categories->hasMorePages())
                <div class="mb-2">
                    {!! $categories->appends(Input::except('page'))->render() !!}
                </div>
            @endif
            <div>
                Showing {{$categories->firstItem()}} to {{$categories->lastItem()}}
                of  {{$categories->total()}} entries
            </div>
        </div>
    </div>
</div>
@endsection