@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Balance Request List</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">User</span>
                <span class="breadcrumb-item active">Balance Request</span>
                <span class="breadcrumb-item active">List</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    @include('includes.success-msg')
    @include('includes.error-msg')
    {{ Form::open(['route' => 'users.balance-request.index', 'method' => 'GET', 'class' => 'form-inline d-block d-sm-flex']) }}
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                {{Form::text("keyword",request()->keyword,["class" => "form-control", "placeholder" => 'Keyword','id' => 'keyword'])}}
            </div>
        </div>
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">Operation</div>
                </div>
                {{Form::select("operation",['all' => 'All','recharge' => 'Recharge', 'withdraw' => 'Withdraw'], request()->get('operation','all'),["class" => "form-control input-sm"])}}
            </div>
        </div>
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">Status</div>
                </div>
                {{Form::select("status",['all' => 'All'] + UserAccountConstant::getRequestStatusList(), request()->get('status',NULL),["class" => "form-control input-sm"])}}
            </div>
        </div>
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">{{ $settings['language']['LANG_LABEL_START_DT'] }}</div>
                </div>
                <input type="text" class="form-control datetimepicker" value="{{isset($_GET['begin_time']) ? $_GET['begin_time'] : date('Y-m-d 00:00:00',strtotime('-7 days'))}}" name="begin_time">
            </div>
        </div>
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">{{ $settings['language']['LANG_LABEL_END_DT'] }}</div>
                </div>
                <input type="text" class="form-control datetimepicker" value="{{isset($_GET['end_time']) ? $_GET['end_time'] : date('Y-m-d 23:59:00')}}" name="end_time">
            </div>
        </div>
        
        {{Form::submit('Search',['class' => 'btn btn-sm btn-primary mb-2 mr-2'])}}
        <a href="{{route('users.balance-request.index')}}" class="btn btn-warning btn-sm mb-2">Reset</a>
    {{ Form::close() }}

    @php $requestStatus = $requests->groupBy('status') @endphp
    <a href="{{route('users.balance-request.index')}}"><span class="badge badge-pill badge-secondary"> All </span></a>
    @if(isset($requestStatus[UserAccountConstant::PENDING]) && $requestStatus[UserAccountConstant::PENDING]->count() > 0) 
        <a href="{{route('users.balance-request.index')}}?status={{UserAccountConstant::PENDING}}"><span class="badge badge-pill badge-warning"> {{ $requestStatus[UserAccountConstant::PENDING]->count() }} {{UserAccountConstant::getRequestStatusLabel(UserAccountConstant::PENDING)}}</span></a>
    @endif
    @if(isset($requestStatus[UserAccountConstant::IN_PROGRESS]) && $requestStatus[UserAccountConstant::IN_PROGRESS]->count() > 0) 
        <a href="{{route('users.balance-request.index')}}?status={{UserAccountConstant::IN_PROGRESS}}"><span class="badge badge-pill badge-primary"> {{ $requestStatus[UserAccountConstant::IN_PROGRESS]->count() }} {{UserAccountConstant::getRequestStatusLabel(UserAccountConstant::IN_PROGRESS)}}</span></a>
    @endif
    <div class="card mt-2">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>#</th>
                        <th>Username / Account No.</th>
                        <th>Phone</th>
                        <th>Ref.</th>
                        <th>Operation</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Requested Date</th>
                        <th>Issued Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($requests) && $requests->count() > 0)
                        @foreach($requests as $key => $row)
                        <tr>
                            <td>{{($requests->perPage() * ($requests->currentPage() - 1)) + $key + 1}} </td>
                            <td>{{ $row->user->username }} / {{ $row->user->account_number }}</td>
                            <td>{{ $row->user->phone }}</td>
                            <td>{{ $row->agent_name }} #{{ $row->agent_ref_no }}</td>
                            <td>{{ ucfirst($row->operation) }} @if($row->status == UserAccountConstant::PENDING) <span class="badge badge-pill badge-success">New</span> @endif</td>
                            <td>{{ currencyFormat($row->amount) }} R</td>
                            <td>
                                @php $label = UserAccountConstant::getRequestStatusLabel($row->status); @endphp
                                @if($row->status == UserAccountConstant::PENDING) <span class="badge badge-pill badge-warning">{{ $label }}</span>
                                @elseif($row->status == UserAccountConstant::IN_PROGRESS) <span class="badge badge-pill badge-primary">{{ $label }}</span>
                                @elseif($row->status == UserAccountConstant::COMPLETED) <span class="badge badge-pill badge-success">{{ $label }}</span>
                                @elseif($row->status == UserAccountConstant::REJECTED) <span class="badge badge-pill badge-danger">{{ $label }}</span>
                                @endif
                            </td>
                            <td>{{ $row->created_at->format('Y-m-d H:i:s') }}</td>
                            <td>{{ $row->updated_at->format('Y-m-d H:i:s') }}</td>
                            <td class="group-btn-action">
                                @canany(['balance-request-modification'])
                                <div class="btn-group">
                                    <a href="{{ route('users.balance-request.edit',$row->id) }}" class="btn btn-sm btn-outline bg-primary border-primary text-primary-800 btn-icon border-2"><i class="icon-pencil7"></i> {{ $settings['language']['LANG_LABEL_EDIT']}}</a>
                                    @if($row->status == UserAccountConstant::PENDING || $row->status == UserAccountConstant::IN_PROGRESS)
                                        @php $route = $row->operation == 'recharge' ? 'users.account.recharge.edit' : 'users.account.takecash.edit' @endphp
                                        <a href="{{route($route,$row->user->id)}}?amount={{$row->amount}}&br={{$row->id}}" class="btn btn-sm btn-outline bg-danger border-danger text-danger-800 btn-icon border-2"><i class="icon-pencil7"></i> {{ ucfirst($row->operation) }}</a>
                                    @endif
                                </div>
                                @endcanany    
                            </td>

                        </tr>
                        @endforeach
                    @else 
                        <tr><td colspan="9">No Data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        @if(isset($requests) )
        <div class="card-footer">
            @if($requests->hasMorePages())
            <div class="mb-2">
                {!! $requests->appends(Input::except('page'))->render() !!}
            </div>
            @endif
            <div>
                Showing {{ $requests->firstItem() }} to {{ $requests->lastItem() }}
                of  {{$requests->total()}} entries
            </div>
        </div>
        @endif
    </div>
</div>
@endsection

@section('page-script')
<script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>
@endsection