@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Balance Request – {{ $request->user->username }} – {{ ucfirst($request->operation) }}</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="{{ route('users.balance-request.index') }}" class="btn btn-primary">
                    <i class="icon-square-left mr-1"></i> Back
                </a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{route('users.balance-request.index')}}" class="breadcrumb-item">Balance Requests</a>
                <span class="breadcrumb-item active">{{ ucfirst($request->operation) }}</span>
                <span class="breadcrumb-item active">{{ isset($request) ? 'Edit' : 'Add'}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        @if(isset($request))
        {{ Form::model($request,['route' => ['users.balance-request.update',$request->id], 'method' => 'PUT']) }}
        @else
        {{ Form::open(['route' => 'users.balance-request.store', 'method' => 'POST']) }}
        @endif
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    @include('includes.success-msg')
                    @include('includes.error-msg')
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Status</label>
                        <div class="position-relative">
                            {{Form::select("status", UserAccountConstant::getRequestStatusList(),null,
                                ["class" => "form-control"])
                            }}
                        </div>
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Username</label>
                        <div class="position-relative">
                            {{Form::text("username",$request->user->username,["class" => "form-control", 'disabled'])}}
                        </div>
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Account Number</label>
                        <div class="position-relative">
                            {{Form::text("account_number",$request->user->account_number,["class" => "form-control", 'disabled'])}}
                        </div>
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Phone</label>
                        <div class="position-relative">
                            {{Form::text("account_number",$request->user->phone,["class" => "form-control", 'disabled'])}}
                        </div>
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Amount (R)</label>
                        <div class="position-relative">
                            {{Form::text("amount",isset($request) ? currencyFormat($request->amount) : '',
                                ["class" => "form-control", 'disabled'])
                            }}
                        </div>
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Agent – Ref.</label>
                        <div class="position-relative">
                            {{Form::text("agent", $request->agent_name . ' – '. $request->agent_ref_no,
                                ["class" => "form-control", 'disabled'])
                            }}
                        </div>
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Note</label>
                        <div class="position-relative">
                            {{Form::textarea("note", $request->note,
                                ["class" => "form-control", 'disabled', 'rows' => 4])
                            }}
                        </div>
                    </div>
                </div>


                @php $image = $request->getFirstMedia('images'); @endphp
                @if($image)
                <div class="col-lg-6">
                    <img src="{{$image->getUrl()}}" class="w-100" alt="">
                </div>
                @endif
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success">
                <i class="icon-folder mr-1"></i> Save
            </button>
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection

@section('page-script')
<script src="/global_assets/js/plugins/forms/styling/uniform.min.js"></script>}
<script src="/global_assets/js/demo_pages/form_checkboxes_radios.js"></script>
@endsection