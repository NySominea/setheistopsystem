@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Add Manager</span></h4>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="{{route('managers.index')}}" class="btn btn-primary">
                    <i class="icon-square-left mr-1"></i> Back
                </a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{route('managers.index')}}" class="breadcrumb-item">Manager</a>
                <span class="breadcrumb-item active">Add</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        @if(isset($manager))
        {{ Form::model($manager,['route' => ['managers.update',$manager->id], 'method' => 'PUT']) }}
        @else
        {{ Form::open(['route' => 'managers.store', 'method' => 'POST']) }}
        @endif
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('username')?'text-danger':'' }}">Manager Name</label>
                        <div class="position-relative">
                            @if(isset($manager))
                            {{Form::text("username",null,
                                ["class" => "form-control ".($errors->has('username')?'border-danger':''),"placeholder" => "Enter manager name",'readonly'])
                            }}
                            @else 
                            {{Form::text("username",null,
                                ["class" => "form-control ".($errors->has('username')?'border-danger':''),"placeholder" => "Enter manager name"])
                            }}
                            @endif
                            @if($errors->has('username'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('username'))
                            <span class="form-text text-danger">{{ $errors->first('username') }}</span>
                        @endif
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('nick_name')?'text-danger':'' }}">{{ $settings['language']['LANG_LABEL_NICKNAME'] }}</label>
                        <div class="position-relative">
                            {{Form::text("nick_name", null,
                                ["class" => "form-control ".($errors->has('nick_name')?'border-danger':''),"placeholder" => "Enter nickname"])
                            }}
                            @if($errors->has('nick_name'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('nick_name'))
                            <span class="form-text text-danger">{{ $errors->first('nick_name') }}</span>
                        @endif
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('password')?'text-danger':'' }}">Login Password</label>
                        <div class="position-relative">
                            {{Form::password("password",
                                ["class" => "form-control ".($errors->has('password')?'border-danger':''),"placeholder" => "Enter more than 6 characters",'autocomplete' => 'off'])
                            }}
                            @if($errors->has('password'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('password'))
                            <span class="form-text text-danger">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('password_confirmation ')?'text-danger':'' }}">Confirm Login Password</label>
                        <div class="position-relative">
                            {{Form::password("password_confirmation",
                                ["class" => "form-control ".($errors->has('password_confirmation')?'border-danger':''),"placeholder" => "Enter more than 6 characters", 'autocomplete' => 'off'])
                            }}
                            @if($errors->has('password_confirmation'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('password_confirmation'))
                            <span class="form-text text-danger">{{ $errors->first('password_confirmation') }}</span>
                        @endif
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('pay_pass')?'text-danger':'' }}">Payment Password</label>
                        <div class="position-relative">
                            {{Form::password("pay_pass",
                                ["class" => "form-control ".($errors->has('pay_pass')?'border-danger':''),"placeholder" => "Enter more than 6 characters",'autocomplete' => 'off'])
                            }}
                            @if($errors->has('pay_pass'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('pay_pass'))
                            <span class="form-text text-danger">{{ $errors->first('pay_pass') }}</span>
                        @endif
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('pay_pass_confirmation ')?'text-danger':'' }}">Confirm Payment Password</label>
                        <div class="position-relative">
                            {{Form::password("pay_pass_confirmation",
                                ["class" => "form-control ".($errors->has('pay_pass_confirmation')?'border-danger':''),"placeholder" => "Enter more than 6 characters", 'autocomplete' => 'off'])
                            }}
                            @if($errors->has('pay_pass_confirmation'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('pay_pass_confirmation'))
                            <span class="form-text text-danger">{{ $errors->first('pay_pass_confirmation') }}</span>
                        @endif
                    </div>
                    <div class="form-group select2">
                        <label>Role Authority</label>
                        {{Form::select("role_id",$roles, isset($manager) && $manager->role ? $manager->role->id : null,["class" => "form-control"])}}
                    </div>
                </div>
            </div>
            
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success">
                <i class="icon-folder mr-1"></i> {{ $settings['language']['LANG_BTN_SAVE'] }}
            </button>
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection