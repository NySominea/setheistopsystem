@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Role List</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">
            {{-- @canany(['add-new-role']) --}}
            <div class="d-flex justify-content-center">
                <a href="{{ route('managers.roles.create') }}" class="btn btn-primary btn-sm">
                    <i class="icon-eye-plus text-white"></i> Add New
                </a>
            </div>
            {{-- @endcanany --}}
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Manager</span>
                <span class="breadcrumb-item active">Role</span>
                <span class="breadcrumb-item active">List</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    @include('includes.success-msg')
    @include('includes.error-msg')
    <div class="card">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>#</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($roles) && $roles->count() > 0)
                        @foreach($roles as $key => $row)
                        <tr>
                            <td>{{($roles->perPage() * ($roles->currentPage() - 1)) + $key + 1}}</td>
                            <td>{{ $row->name }}</td>
                            <td class="group-btn-action">
                                @canany(['role-modification'])
                                <div class="btn-group">
                                    <a href="{{ route('managers.roles.edit',$row->id) }}" class="btn btn-sm btn-outline bg-primary border-primary text-primary-800 btn-icon border-2"><i class="icon-pencil7"></i> {{ $settings['language']['LANG_LABEL_EDIT']}}</a>
                                    <button type="button" class="btn btn-sm btn-outline bg-danger border-danger text-danger-800 btn-icon border-2 delete" data-route="{{route('managers.roles.destroy',$row->id)}}"><i class="icon-trash"></i> Delete</a>
                                </div>
                                @endcanany    
                            </td>

                        </tr>
                        @endforeach
                    @else 
                        <tr><td colspan="3">No Data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        @if(isset($roles) )
        <div class="card-footer">
            @if($roles->hasMorePages())
            <div class="mb-2">
                {!! $roles->appends(Input::except('page'))->render() !!}
            </div>
            @endif
            <div>
                Showing {{ $roles->firstItem() }} to {{ $roles->lastItem() }}
                of  {{$roles->total()}} entries
            </div>
        </div>
        @endif
    </div>
</div>
@endsection