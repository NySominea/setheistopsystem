@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Role Set</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="{{ route('managers.roles.index') }}" class="btn btn-primary">
                    <i class="icon-square-left mr-1"></i> Back
                </a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{route('managers.index')}}" class="breadcrumb-item">Manager</a>
                <a href="{{route('managers.roles.index')}}" class="breadcrumb-item">Role</a>
                <span class="breadcrumb-item active">{{ isset($role) ? 'Edit' : 'Add'}}</span>
            </div>

            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        @if(isset($role))
        {{ Form::model($role,['route' => ['managers.roles.update',$role->id], 'method' => 'PUT']) }}
        @else
        {{ Form::open(['route' => 'managers.roles.store', 'method' => 'POST']) }}
        @endif
        @csrf
        <div class="card-body">
            <div class="form-group row form-group-float form-group-feedback form-group-feedback-right">
                <div class="col-lg-6">
                    <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('name')?'text-danger':'' }}">Role Name</label>
                    <div class="position-relative">
                        {{Form::text("name",old("name") ? old("name") : (isset($role) ? $role->name : ''),
                            ["class" => "form-control ".($errors->has('name')?'border-danger':''),"placeholder" => "Enter role name"])
                        }}
                        @if($errors->has('name'))
                            <div class="form-control-feedback text-danger">
                                <i class="icon-spam"></i>
                            </div>
                        @endif
                    </div>
                    @if($errors->has('name'))
                        <span class="form-text text-danger">{{ $errors->first('name') }}</span>
                    @endif
                </div>
            </div>
            <div class="form-group row"> 
                <div class="col-lg-6">
                    <label class="form-group-float-label font-weight-semibold is-visible">Permissions</label>
                    @foreach($permissions as $m => $module)
                    <div class="mb-3">
                        <div class="form-check mb-0 module">
                            <label class="form-check-label">
                                {{Form::checkbox("permissions[$m][]",'0',isset($modules) && in_array($m,$modules)?true:false,["class" => "form-check-input-styled-success",'data-fouc'])}}
                                <strong>{{ $module['module'] }}</strong>
                            </label>
                        </div>
                        @foreach($module['permissions'] as $p)
                            <div class="form-check custom-control-inline mb-0 permission">
                                <label class="form-check-label">
                                    {{Form::checkbox("permissions[$m][]",$p['value'], isset($role) && in_array($p['value'],$role->permissions->pluck('name')->toArray())?true:false,["class" => "form-check-input-styled-success",'data-fouc'])}}
                                    {{ $p['text'] }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success">
                <i class="icon-folder mr-1"></i> Save
            </button>
        </div>
        {{ Form::close() }}
    </div>
</div>
@endsection

@section('page-script')
<script src="/global_assets/js/plugins/forms/styling/uniform.min.js"></script>}
<script src="/global_assets/js/demo_pages/form_checkboxes_radios.js"></script>
@endsection