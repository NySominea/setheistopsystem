@extends('layouts.master')

@section('custom-css')
<style>
    .table td.group-btn-action{
        min-width: 50px;
    }
    .table td{
        font-size:11px;
    }
</style>
@endsection

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Agent Lists</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                @canany('download-agent-list')
                    <button class="btn btn-link btn-primary btn-sm text-white" id="downloadExcelButton" 
                    data-route="{{route('users.logs.download')}}" 
                    data-toggle="modal" data-target="#downlod_modal">
                        <i class="icon-file-excel text-white mr-1"></i> {{ $settings['language']['LANG_LABEL_DOWN'] }}
                    </button>
                @endcanany
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ route('dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="#" class="breadcrumb-item">{{ $settings['language']['LANG_MENU_SALE_NET'] }}</a>
                <span class="breadcrumb-item active">Agent Lists</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    {{ Form::open(['route' => 'users.agent-lists.index', 'method' => 'GET', 'class' => 'form-inline d-block d-sm-flex']) }}
        <div class="form-group mb-2 mr-2">
            {{Form::select("state",['all' => 'ALL','1' => 'OPEN','0' => 'CLOSE'], isset($_GET['state']) ? $_GET['state'] : null,["class" => "form-control form-control-sm", 'id' => 'state'])}}
        </div>
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                {{Form::text("keyword",request()->keyword,["class" => "form-control", "placeholder" => 'User Name / Account No','id' => 'keyword'])}}
            </div>
        </div>
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">{{ $settings['language']['LANG_LABEL_START_DT'] }}</div>
                </div>
                <input type="text" class="form-control datetimepicker" value="{{isset($_GET['begin_time']) ? $_GET['begin_time'] : date('Y-m-d 00:00:00',strtotime('-7 days'))}}" name="begin_time">
            </div>
        </div>
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm"> 
                <div class="input-group-prepend">
                    <div class="input-group-text">{{ $settings['language']['LANG_LABEL_END_DT'] }}</div>
                </div>
                <input type="text" class="form-control datetimepicker" value="{{isset($_GET['end_time']) ? $_GET['end_time'] : date('Y-m-d 23:59:00')}}" name="end_time">
            </div>
        </div>
        {{ Form::submit('Search',['class' => 'btn btn-sm btn-primary mb-2 mr-2']) }}
            <a href="{{ route('users.agent-lists.index') }}" class="btn btn-warning btn-sm mb-2">Reset</a>
        {{ Form::close() }}
    
    <div class="card">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>#</th>
                        <th>UserName</th>
                        <th>AccountNo</th>
                        <th>Phone</th>
                        <th>Province</th>
                        <th>District</th>
                        <th>Sale Supervisor</th>
                        <th>Create Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if($users->count() > 0)
                        @foreach($users as $key => $row)
                        <tr>
                            <td>{{($users->perPage() * ($users->currentPage() - 1)) + $key + 1}}</td>
                            <td>{{ $row->username }}</td>
                            <td>{{ $row->account_number}}</td>
                            <td>{{ isset($row->phone) && !empty($row->phone) ? $row->phone : 'No Data' }}</td>
                            <td>{{ isset($row->province )&& !empty($row->province)  ? $row->province : 'No Data'  }}</td>
                            <td>{{ isset($row->district) && !empty($row->district) ? $row->district : 'No Data' }}</td>
                            @if( in_array($row->l1_id,$id) || in_array($row->l2_id,$id))
                                <td>{{ $row->parent->username }}</td>
                            @else 
                                <td>{{ $row->parent->parent->username }}</td>
                            @endif
                            <td>{{ isset($row->created_at) ? $row->created_at : now() }}</td>
                            @canany('agent-list-modification')
                                <td class="group-btn-action">
                                    <a href="{{ route('users.agent-lists.edit',$row->id) }}" class = "btn btn-sm btn-outline bg-primary border-primary text-primary-800 btn-icon border-2"><i class="icon-pencil7"></i> {{$settings['language']['LANG_LABEL_EDIT']}}</a>
                                </td>
                            @endcanany
                        </tr>
                        @endforeach
                    @else 
                        <tr><td colspan="9">No Data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            @if($users->hasMorePages())
                <div class="mb-2">
                    {!! $users->appends(Input::except('page'))->render() !!}
                </div>
            @endif
            <div>
                Showing {{$users->firstItem()}} to {{$users->lastItem()}}
                of  {{$users->total()}} entries
            </div>
        </div>
    </div>
</div>
<div id="downlod_modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-teal-400">
                <h5 class="modal-title">Download Excel</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body text-center p-4 h5 m-0" id="downloadModalBody">
                <i class='icon-spinner2 spinner'></i> Downloading...
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>
@endsection