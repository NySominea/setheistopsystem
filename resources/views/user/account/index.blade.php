@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{ $user->username }} - Account</span></h4>
        </div>
        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                @if($user->com_direct == 0)
                    <a href="{{ route('users.sale-network.index') }}?{{$user->level != 1 && $user->level != 0 ? 'level='.($user->level - 1).'&parent_id='.$user->parent_id : ''}}" class="btn btn-primary btn-sm">
                        <i class="icon-square-left mr-1"></i> Back
                    </a>
                @else 
                    <a href="{{ route('users.direct-user.index') }}" class="btn btn-primary btn-sm">
                        <i class="icon-square-left mr-1"></i> Back
                    </a>
                @endif
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{route('users.sale-network.index')}}" class="breadcrumb-item">User</a>
                @if($user->com_direct == 0)
                    <a href="{{route('users.sale-network.index')}}" class="breadcrumb-item">{{ $settings['language']['LANG_MENU_SALE_NET'] }}</a>
                @else 
                    <a href="{{route('users.direct-user.index')}}" class="breadcrumb-item">{{ $settings['language']['LANG_MENU_DIRECT_USER'] }}</a>
                @endif
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_LABEL_ACCOUNT'] }}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    @include('includes.success-msg')
    @include('includes.error-msg')
    <div class="card">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>#</th>
                        <th>{{ $settings['language']['LANG_LABEL_ACCOUNT_TYPE'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_ACCOUNT_TITLE'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_ACCOUNT_NUM'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_ACCOUNT_NAME'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_BALANCE'] }}(R)</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($accounts) && $accounts->count() > 0)
                        @foreach($accounts as $key => $row)
                        <tr>
                            <td>{{ $key + 1}}</td>
                            <td>{{ $settings['language'][Account::userAccountType($row->type_id)] }}</td>
                            <td>{{ $row->title }}</td>
                            <td>{{ $row->number }}</td>
                            <td>{{ $row->name }}</td>
                            <td>(R) {{ currencyFormat($row->balance) }}</td>
                        </tr>
                        @endforeach
                    @else 
                        <tr><td colspan="3">No Data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection