@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">User Account Withdraw – {{ $user->username }}</span></h4>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="{{ URL::previous() }}" class="btn btn-primary btn-sm">
                    <i class="icon-square-left mr-1"></i> Back
                </a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ route('dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{ route('users.sale-network.index') }}" class="breadcrumb-item">{{ $settings['language']['LANG_MENU_USER_LIST'] }}</a>
                <a href="{{ route('users.sale-network.index') }}" class="breadcrumb-item">{{ $settings['language']['LANG_MENU_SALE_NET'] }}</a>
                <span class="breadcrumb-item active">User Withdraw</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        {{ Form::open(['route' => ['users.account.takecash.update',$user->id], 'method' => 'PUT','autocomplete' => "off"]) }}
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    @include('includes.success-msg')
                    @include('includes.error-msg')
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">{{ $settings['language']['LANG_LABEL_ACCOUNT'] }}</label>
                        <div class="position-relative">
                            {{Form::select("account",$accounts, null, ["class" => "form-control input-sm"])}}
                        </div>
                        @if($errors->has('account'))
                            <span class="form-text text-danger">{{ $errors->first('account') }}</span>
                        @endif
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">{{ $settings['language']['LANG_LABEL_USERNAME'] }}</label>
                        <div class="position-relative">
                            {{Form::text("username", $user->username,
                                ["class" => "form-control", 'disabled'])
                            }}
                        </div>
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">{{ $settings['language']['LANG_LABEL_BALANCE'] }} (R)</label>
                        <div class="position-relative">
                            {{Form::text("balance",isset($user) && $user->cashAccount ? currencyFormat($user->cashAccount->balance) : 0,
                                ["class" => "form-control", 'disabled'])
                            }}
                        </div>
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('amount')?'text-danger':'' }}">{{ $settings['language']['LANG_LABEL_AMOUNT'] }} (R)</label>
                        <div class="position-relative">
                            {{Form::text("amount",number_format(request()->get('amount',0),0),
                                ["class" => "form-control input-amount ".($errors->has('amount')?'border-danger':''), "min" => 0, 'maxlength' => 32])
                            }}
                            @if($errors->has('amount'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('amount'))
                            <span class="form-text text-danger">{{ $errors->first('amount') }}</span>
                        @endif
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('user_pay_password')?'text-danger':'' }}">User Pay Password</label>
                        <div class="position-relative">
                            {{Form::password("user_pay_password",
                                ["class" => "form-control ".($errors->has('user_pay_password')?'border-danger':''),"placeholder" => "Enter pay password"])
                            }}
                            @if($errors->has('user_pay_password'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('user_pay_password'))
                            <span class="form-text text-danger">{{ $errors->first('user_pay_password') }}</span>
                        @endif
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible {{ $errors->has('pay_password')?'text-danger':'' }}">{{ $settings['language']['LANG_LABEL_PAYPASS'] }}</label>
                        <div class="position-relative">
                            {{Form::password("pay_password",
                                ["class" => "form-control ".($errors->has('pay_password')?'border-danger':''),"placeholder" => "Enter pay password"])
                            }}
                            @if($errors->has('pay_password'))
                                <div class="form-control-feedback text-danger">
                                    <i class="icon-spam"></i>
                                </div>
                            @endif
                        </div>
                        @if($errors->has('pay_password'))
                            <span class="form-text text-danger">{{ $errors->first('pay_password') }}</span>
                        @endif
                    </div>
                </div>

                @php $image = $user->getFirstMedia('id_card'); @endphp
                @if($image)
                <div class="col-lg-6">
                    <img src="{{$image->getUrl()}}" class="w-100" alt="">
                </div>
                @endif
            </div>
            
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success">
                <i class="icon-folder mr-1"></i> {{ $settings['language']['LANG_BTN_SAVE'] }}
            </button>
        </div>
        <input type="hidden" name="br" value="{{ request()->get('br','') }}">
        {{ Form::close() }}
    </div>
</div>
@endsection