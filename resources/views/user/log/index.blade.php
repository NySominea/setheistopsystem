@extends('layouts.master')

@section('custom-css')
{{-- <style>
    #print_preview_modal table{
        font-size: 13px;
    }
    #print_preview_modal table tr td:first-child{
        font-weight: 600;
        padding: 5px;
    }
    @media print {
        table td{
            font-size:22px;
            padding: 10px !important;
        }
    }
</style> --}}
@endsection

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{$user->username}} - Account Log</span></h4>
        </div>
        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                @if($user->com_direct == 0)
                    <a href="{{ route('users.sale-network.index') }}?{{$user->level != 1 && $user->level != 0 ? 'level='.($user->level - 1).'&parent_id='.$user->parent_id : ''}}" class="btn btn-primary btn-sm">
                        <i class="icon-square-left mr-1"></i> Back
                    </a>
                @else 
                    <a href="{{ route('users.direct-user.index') }}" class="btn btn-primary btn-sm">
                        <i class="icon-square-left mr-1"></i> Back
                    </a>
                @endif
            </div>
            <div class="d-flex justify-content-center ml-1">
                <button class="btn btn-link btn-primary btn-sm text-white" id="downloadExcelButton" 
                data-route="{{ route('user.logs.download',$user->id) }}" 
                data-toggle="modal" data-target="#downlod_modal">
                    <i class="icon-file-excel text-white mr-1"></i> {{ $settings['language']['LANG_LABEL_DOWN'] }}
                </button>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ route('dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{ route('users.sale-network.index') }}" class="breadcrumb-item">User</a>
                @if($user->com_direct == 0)
                    <a href="{{ route('users.sale-network.index') }}" class="breadcrumb-item">{{ $settings['language']['LANG_MENU_SALE_NET'] }}</a>
                @else 
                    <a href="{{route('users.direct-user.index')}}" class="breadcrumb-item">{{ $settings['language']['LANG_MENU_DIRECT_USER'] }}</a>
                @endif
                <span class="breadcrumb-item active">Log</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    {{ Form::open(['route' => ['users.logs.show',$user->id], 'method' => 'GET', 'class' => 'form-inline d-block d-sm-flex']) }}
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">{{ $settings['language']['LANG_LABEL_ACCOUNT_LOG_TYPE'] }}</div>
                </div>
                {{
                    Form::select("log_type",['all' => $settings['language']['LANG_LABEL_ALL'],
                    Account::LOG_TYPE_IN => $settings['language'][Account::accountInOut(Account::LOG_TYPE_IN)], 
                    Account::LOG_TYPE_OUT => $settings['language'][Account::accountInOut(Account::LOG_TYPE_OUT)]], 
                    isset($_GET['abstract']) ? $_GET['abstract'] : null,["class" => "form-control input-sm"])
                }}
            </div>
        </div>
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">Abstract</div>
                </div>
                {{
                Form::select("abstract",['all' => $settings['language']['LANG_LABEL_ALL'],
                    'LANG_LABEL_TRANS' => $settings['language']['LANG_LABEL_TRANS'], 
                    'LANG_PROFIT_TO_BALANCE' => $settings['language']['LANG_PROFIT_TO_BALANCE'],
                    'LANG_LABEL_WIN_SUM' => $settings['language']['LANG_LABEL_WIN_SUM'],
                    'LANG_LABEL_REBATE_SUM' => $settings['language']['LANG_LABEL_REBATE_SUM'],
                ], 
                    isset($_GET['abstract']) ? $_GET['abstract'] : null,["class" => "form-control input-sm"])
                }}
            </div>
        </div>
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">{{ $settings['language']['LANG_LABEL_START_DT'] }}</div>
                </div>
                <input type="text" class="form-control datetimepicker" value="{{isset($_GET['begin_time']) ? $_GET['begin_time'] : date('Y-m-d 00:00:00',strtotime('-7 days'))}}" name="begin_time">
            </div>
        </div>
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                    <div class="input-group-text">{{ $settings['language']['LANG_LABEL_END_DT'] }}</div>
                </div>
                <input type="text" class="form-control datetimepicker" value="{{isset($_GET['end_time']) ? $_GET['end_time'] : date('Y-m-d 23:59:00')}}" name="end_time">
            </div>
        </div>
        {{Form::submit('Search',['class' => 'btn btn-sm btn-primary mb-2 mr-2'])}}
        <a href="{{route('users.logs.show',$user->id)}}" class="btn btn-warning btn-sm mb-2">Reset</a>
    {{ Form::close() }}
    
    <div class="card">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>Transaction</th>
                        <th>{{ $settings['language']['LANG_LABEL_DATETIME'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_ABSTRACT'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_ACCOUNT'] }}</th>
                        <th>{{ $settings['language']['LANG_LABEL_AMOUNT'] }}(R)</th>
                        <th>{{ $settings['language']['LANG_LABEL_BALANCE'] }}(R)</th>
                        <th>{{ $settings['language']['LANG_LABEL_REBATE'] }}(R)</th>
                        <th>{{ $settings['language']['LANG_LABEL_WIN_AMOUNT'] }}(R)</th>
                        <th>{{ $settings['language']['LANG_LABEL_TO_ACCOUNT'] }}</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($logs) && $logs->count() > 0)
                        @foreach($logs as $key => $row)
                        <tr>
                            <td>{{ $row->log_number }}</td>
                            <td>{{ $row->created_at }}</td>
                            {{-- @php dd($row->abstract == 'LANG_LABEL_TRANS' && $row->log_type == Account::LOG_TYPE_IN)  @endphp --}}
                            <td>{{ $row->abstract != 'LANG_LABEL_TRANS' ? $settings['language'][$row->abstract] : ($row->log_type == Account::LOG_TYPE_IN ? 'Received' : 'Transfered') }}</td>
                            <td>{{ $row->account ? $row->account->title : '' }}</td>
                            <td>{{ currencyFormat($row->amount) }}</td>
                            <td>{{ $row->balance ? currencyFormat($row->balance) : '' }}</td>
                            <td>{{ $row->commission ? currencyFormat($row->commission) : '' }}</td>
                            <td>{{ $row->win_money ? currencyFormat($row->win_money) : '' }}</td>
                            <td>
                                @if($row->to_type == Account::SYSTEM_ACCOUNT_TYPE) 
                                    {{ $settings['language'][Account::systemAccountType($row->to_type)] }}
                                @elseif($row->to_type == Account::USER_ACCOUNT_TYPE) 
                                     {{ $row->toUser ? $row->toUser->username : '' }} {{ $row->toUser ? '('.$row->toUser->province .')' : ''  }}
                                @endif
                            </td>
                            <td class="group-btn-action">
                                <button class="btn btn-sm btn-outline bg-teal-400 border-teal text-teal-400 btn-icon border-2 print" 
                                data-popup="tooltip" title="" data-placement="bottom" data-original-title="Print"
                                data-toggle="modal" data-target="#print_preview_user_modal" 
                                data-route="{{route('user.account.getLogInfo',$row->id)}}">
                                    <i class="icon-printer"></i>
                                </button>
                            </td>
                        </tr>
                        @endforeach
                    @else 
                        <tr><td colspan="10">No Data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        @if(isset($logs) && count($logs) > 0)
        <div class="card-footer">
            @if($logs->hasMorePages())
                <div class="mb-2">
                    {!! $logs->appends(Input::except('page'))->render() !!}
                </div>
            @endif
            <div>
                Showing {{$logs->firstItem()}} to {{$logs->lastItem()}}
                of  {{$logs->total()}} entries
            </div>
        </div>
        @endif
    </div>
</div>
<div id="print_preview_user_modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-teal-400">
                <h5 class="modal-title">{{ $settings['language']['LANG_LABEL_ACCOUNT_LOG'] }}</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body">
                <table class="w-100">
                    <tbody>
                        <tr><td>Transaction No:</td><td class="transaction"></td></tr>
                        <tr><td>User Name:</td><td class="username"></td></tr>
                        <tr><td>Log Type:</td><td class="log-type"></td></tr>
                        <tr><td>Amount:</td><td class="amount"></td></tr>
                        <tr><td>Balance:</td><td class="balance"></td></tr>
                        <tr><td>To Account:</td><td class="to-account"></td></tr>
                        <tr><td>Manager:</td><td class="manager"></td></tr>
                        <tr><td>Date Time:</td><td class="date-time"></td></tr>
                        <tr><td>Abstract:</td><td class="abstract"></td></tr>
                        <tr><td></td><td></td></tr>
                        <tr><td>Auditor:</td><td class="auditor"></td></tr>
                        <tr><td>Customer signature:</td><td class="custom-signature"></td></tr>
                    </tbody>
                </table>
            </div>

            <div class="modal-footer mx-auto">
                <button type="button" class="btn bg-teal-400" onclick="window.print();"><i class="icon-printer2 mr-2"></i> Print</button>
            </div>
        </div>
    </div>
    <div class="printable mt-5">
        <h1 class="text-center text-teal">{{ $settings['language']['LANG_LABEL_ACCOUNT_LOG'] }}</h1>
        <div class="table-responsive mx-5">
            <table class="w-100 mx-auto mt-4">
                <tr class="w-100"><td style="width:50%">Transaction No:</td><td class="transaction" style="width:70%"></td></tr>
                <tr><td>User Name:</td><td class="username"></td></tr>
                <tr><td>Log Type:</td><td class="log-type"></td></tr>
                <tr><td>Amount:</td><td class="amount"></td></tr>
                <tr><td>Balance:</td><td class="balance"></td></tr>
                <tr><td>To Account:</td><td class="to-account"></td></tr>
                <tr><td>Manager:</td><td class="manager"></td></tr>
                <tr><td>Date Time:</td><td class="date-time"></td></tr>
                <tr><td>Abstract:</td><td class="abstract"></td></tr>
                <tr><td>Auditor:</td><td class="auditor"></td></tr>
                <tr><td>Customer signature:</td><td class="custom-signature"></td></tr>
            </table>
        </div>
    </div>
    
    
</div>
<div id="downlod_modal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header bg-teal-400">
                    <h5 class="modal-title">Download Excel</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body text-center p-4 h5 m-0" id="downloadModalBody">
                    <i class='icon-spinner2 spinner'></i> Downloading...
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
<script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>
@endsection