@extends('layouts.master')

@section('content')

@php $auth = auth()->user(); @endphp

<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{ isset($user) ? 'Edit User' : 'Add User' }} </span></h4>
        </div>

        <div class="header-elements d-none">
            <div class="d-flex justify-content-center">
                <a href="{{route('users.sale-network.index')}}" class="btn btn-primary btn-sm">
                    <i class="icon-square-left mr-1"></i> Back
                </a>
            </div>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <a href="{{route('users.sale-network.index')}}" class="breadcrumb-item">User</a>
                <span class="breadcrumb-item active">{{ isset($user) ? 'Edit' : 'Add' }}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        @if(isset($user))
        {{ Form::model($user,['route' => ['users.sale-network.update',$user->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) }}
        @else
        {{ Form::open(['route' => 'users.sale-network.store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
        @endif
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right row">
                        <label class="col-md-3 form-group-float-label font-weight-semibold is-visible {{ $errors->has('username')?'text-danger':'' }}">{{ $settings['language']['LANG_LABEL_USERNAME'] }}</label>
                        <div class="col-md-9">
                            <div class="position-relative">
                                {{Form::text("username",isset($user) ? $user->username : '',
                                    ["class" => "form-control ".($errors->has('username')?'border-danger':''),"placeholder" => "Enter user name", 
                                    !isset($user) || (isset($user) && !$user->is_approved) || $auth->can('update-user-credentials-info') ? "" : 'readonly'])
                                }}
                                @if($errors->has('username'))
                                    <div class="form-control-feedback text-danger">
                                        <i class="icon-spam"></i>
                                    </div>
                                @endif
                                {{ Form::hidden('parent_id', isset($parent_id) ? $parent_id :'') }}
                                {{ Form::hidden('level', isset($level) ? $level : '') }}
                            </div>
                            @if($errors->has('username'))
                                <span class="form-text text-danger">{{ $errors->first('username') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right row">
                        <label class="col-md-3 form-group-float-label font-weight-semibold is-visible {{ $errors->has('username')?'text-danger':'' }}">{{ $settings['language']['LANG_LABEL_ACCOUNT_NUM'] }}</label>
                        <div class="col-md-9">
                            <div class="position-relative">
                                {{Form::text("account_number",isset($user) ? $user->account_number : $account_number,
                                    ["class" => "form-control ".($errors->has('account_number')?'border-danger':''),"placeholder" => "Enter account number", 'readonly'])
                                }}
                                @if($errors->has('account_number'))
                                    <div class="form-control-feedback text-danger">
                                        <i class="icon-spam"></i>
                                    </div>
                                @endif
                            </div>
                            @if($errors->has('account_number'))
                                <span class="form-text text-danger">{{ $errors->first('account_number') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right row">
                        <label class="col-md-3 form-group-float-label font-weight-semibold is-visible {{ $errors->has('email')?'text-danger':'' }}">Email</label>
                        <div class="col-md-9">
                            <div class="position-relative">
                                {{Form::text("email",isset($user) ? $user->email : '',
                                    ["class" => "form-control ".($errors->has('email')?'border-danger':''),"placeholder" => "Enter email", 
                                    !isset($user) || (isset($user) && !$user->is_approved) || $auth->can('update-user-credentials-info') ? "" : 'readonly'])
                                }}
                                @if($errors->has('email'))
                                    <div class="form-control-feedback text-danger">
                                        <i class="icon-spam"></i>
                                    </div>
                                @endif
                            </div>
                            @if($errors->has('email'))
                                <span class="form-text text-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right row">
                        <label class="col-md-3 form-group-float-label font-weight-semibold is-visible {{ $errors->has('phone')?'text-danger':'' }} ">Phone</label>
                        <div class="col-md-9">
                            <div class="position-relative">
                                {{Form::text("phone",isset($user) ? $user->phone : '',
                                    ["class" => "form-control","placeholder" => "Enter phone Number"])
                                }}
                            </div>
                            @if($errors->has('phone'))
                                <span class="form-text text-danger">{{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right row">
                        <label class="col-md-3 form-group-float-label font-weight-semibold is-visible">National ID Number</label>
                        <div class="col-md-9">
                            <div class="position-relative">
                                {{Form::text("national_id_number",isset($user) ? $user->national_id_number : '',
                                    ["class" => "form-control","placeholder" => "Enter National ID Number"])
                                }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right row">
                        <label class="col-md-3 form-group-float-label font-weight-semibold is-visible">Address</label>
                        <div class="col-md-9">
                            <div class="position-relative">
                                {{Form::text("address",isset($user) ? $user->address : '',
                                    ["class" => "form-control","placeholder" => "Enter address"])
                                }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 form-group-float-label font-weight-semibold is-visible">{{ $settings['language']['LANG_LABEL_PROVINCE'] }}</label>
                        <div class="col-md-9">
                            <select name="province_id" class="select2 form-control" id="province" >
                                @foreach($provinces as $province)
                                    <option value = "{{ $province->id }}">{{ $province->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group select2 row">
                        <label class="col-md-3 form-group-float-label font-weight-semibold is-visible">{{ $settings['language']['LANG_LABEL_DISTRICT'] }}</label>
                        <div class="col-md-9">
                            <select name="district_id" class="select2 form-control" id="district">
                                @if(count($district) > 0)
                                    @foreach($district as $data)
                                        <option value = "{{ $data->id == $user->id ? 'selected' : '' }}"> {{ $data->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group select2 row">
                        <label class="col-md-3 form-group-float-label font-weight-semibold is-visible">{{ $settings['language']['LANG_LABEL_COMMUNE'] }}</label>
                       <div class="col-md-9">
                            <select name="commune_id" class="select2 form-control" id="commune">
                                @if(count($commune) > 0)
                                    @foreach($commune as $data)
                                        <option value = "{{ $data->id == $user->id ? 'selected' : '' }}">{{ $data->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                       </div>
                    </div>

                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right row">
                        <label class="col-md-3 form-group-float-label font-weight-semibold is-visible {{ $errors->has('password')?'text-danger':'' }}">Login Password</label>
                        <div class="col-md-9">
                            <div class="position-relative">
                                {{Form::password("password",
                                    ["class" => "form-control ".($errors->has('password')?'border-danger':''),"placeholder" => "Enter more than 6 characters",'autocomplete' => 'off',
                                    !isset($user) || (isset($user) && !$user->is_approved) || $auth->can('update-user-credentials-info') ? "" : 'readonly'])
                                }}
                                @if($errors->has('password'))
                                    <div class="form-control-feedback text-danger">
                                        <i class="icon-spam"></i>
                                    </div>
                                @endif
                            </div>
                            @if($errors->has('password'))
                                <span class="form-text text-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                       
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right row">
                        <label class="col-md-3 form-group-float-label font-weight-semibold is-visible {{ $errors->has('password_confirmation ')?'text-danger':'' }}">Confirm Login Password</label>
                        <div class="col-md-9">
                            <div class="position-relative">
                                {{Form::password("password_confirmation",
                                    ["class" => "form-control ".($errors->has('password_confirmation')?'border-danger':''),"placeholder" => "Enter more than 6 characters", 'autocomplete' => 'off',
                                    !isset($user) || (isset($user) && !$user->is_approved) || $auth->can('update-user-credentials-info') ? "" : 'readonly'])
                                }}
                                @if($errors->has('password_confirmation'))
                                    <div class="form-control-feedback text-danger">
                                        <i class="icon-spam"></i>
                                    </div>
                                @endif
                            </div>
                            @if($errors->has('password_confirmation'))
                                <span class="form-text text-danger">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right row">
                        <label class="col-md-3 form-group-float-label font-weight-semibold is-visible {{ $errors->has('pay_pass')?'text-danger':'' }}">{{ $settings['language']['LANG_LABEL_PAYPASS'] }}</label>
                        <div class="col-md-9">
                            <div class="position-relative">
                                {{Form::password("payment_password",
                                    ["class" => "form-control ".($errors->has('payment_password')?'border-danger':''),"placeholder" => "Enter more than 6 characters",'autocomplete' => 'off',
                                    !isset($user) || (isset($user) && !$user->is_approved) || $auth->can('update-user-credentials-info') ? "" : 'readonly'])
                                }}
                                @if($errors->has('payment_password'))
                                    <div class="form-control-feedback text-danger">
                                        <i class="icon-spam"></i>
                                    </div>
                                @endif
                            </div>
                            @if($errors->has('payment_password'))
                                <span class="form-text text-danger">{{ $errors->first('payment_password') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right row">
                        <label class="col-md-3 form-group-float-label font-weight-semibold is-visible {{ $errors->has('pay_pass_confirmation ')?'text-danger':'' }}">Confirm Payment Password</label>
                        <div class="col-md-9">
                            <div class="position-relative">
                                {{Form::password("payment_password_confirmation",
                                    ["class" => "form-control ".($errors->has('payment_password_confirmation')?'border-danger':''),"placeholder" => "Enter more than 6 characters", 'autocomplete' => 'off',
                                    !isset($user) || (isset($user) && !$user->is_approved) || $auth->can('update-user-credentials-info') ? "" : 'readonly'])
                                }}
                                @if($errors->has('payment_password_confirmation'))
                                    <div class="form-control-feedback text-danger">
                                        <i class="icon-spam"></i>
                                    </div>
                                @endif
                            </div>
                            @if($errors->has('payment_password_confirmation'))
                                <span class="form-text text-danger">{{ $errors->first('pay_pass_confirmation') }}</span>
                            @endif
                        </div>
                    </div>
                    
                    <div class="form-group mb-3 mb-md-2">
                        <label class="d-block font-weight-semibold">State</label>
                        <div class="form-check custom-control-inline">
                            <label class="form-check-label">
                                <input type="radio" name="state" value="1" class="form-check-input-styled-success" data-fouc checked
                                {{ (isset($user) && $user->state == 1) ? 'checked' : '' }} 
                                {{$auth->can('add-new-user-account') ? 'checked' : ''}} 
                                {{ !isset($user) || $auth->can('update-user-credentials-info') ? "" : 'disabled'}}>
                                Enable
                            </label>
                        </div>
                        
                        <div class="form-check custom-control-inline">
                            <label class="form-check-label">
                                <input type="radio" name="state" value="0" class="form-check-input-styled-danger" data-fouc 
                                    {{ (isset($user) && $user->state == 0) ? 'checked' : '' }} 
                                    {{ !isset($user) || $auth->can('update-user-credentials-info') ? "" : 'disabled' }} >
                                    Disable
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @canany(['user-account-modification'])
        <div class="card-footer">
            <button type="submit" class="btn btn-success">
                <i class="icon-folder mr-1"></i>Submit
            </button>
        </div>
        @endcanany
        {{ Form::close() }}
    </div>
</div>
@endsection

@section('page-script')
<script src="/global_assets/js/plugins/forms/styling/uniform.min.js"></script>
<script src="/global_assets/js/demo_pages/form_checkboxes_radios.js"></script>
@endsection

@section('custom-js')
<script>

    $(document).ready(function(){

        $('input[type="file"]').change(function(e){
            var fileName = e.target.files[0].name;
            $(this).next('.custom-file-label').html(fileName);
        });

        $('#province').change(function(){
            var id = $('#province').val();
            var baseUrl = window.location.origin ;
            var route = baseUrl +'/users/getDistrict?id='+ id;

            $.ajax({
                type: 'GET',
                url: route,
                data: {},
                success: function(response){
                    $('#district').html("");
                   $.each(response, function( index, value ) {
                       option = '<option value='+value.id+'>'+value.name+'</option>';
                       $('#district').append(option);
                    });
                },
                error: function(){
                    modal.html('Error!!!')
                }
            })
        });

        $('#district').change(function(){
            var id = $(this).children("option:selected").val();
           
            var baseUrl = window.location.origin ;
            var route = baseUrl +'/users/getCommune?id='+ id;

            $.ajax({
                type: 'GET',
                url: route,
                data: {},
                success: function(response){
                    $('#commune').html("");
                   $.each(response, function( index, value) {
                       option = '<option value='+value.id+'>'+value.name+'</option>';
                       $('#commune').append(option);
                    });
                },
                error: function(){
                    modal.html('Error!!!')
                }
            })
        });

    });
</script>
@endsection