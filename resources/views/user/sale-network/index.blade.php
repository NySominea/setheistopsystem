
@extends('layouts.master')
@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Sale Network</span></h4>
        </div>
        <div class="d-flex ustify-content-between pull-right" >
            {{-- @canany(['user-account-modification']) --}}
            <a href="{{route('users.sale-network.create')}}?parent_id={{request()->parent_id ?: 0}}" class="btn btn-link btn-primary btn-sm text-white ">
                    <i class="icon-user-plus text-white mr-1"></i> Add New
                </a>
            {{-- @endcanany --}}
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{ route('dashboard') }}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_MENU_USER_LIST'] }}</span>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_MENU_SALE_NET'] }}</span>
            </div>
        </div>
    </div>

</div>
<!-- /page header -->
<div class="content">
    {{ Form::open(['route' => 'users.sale-network.index', 'method' => 'GET', 'class' => 'form-inline d-block d-sm-flex']) }}
        <div class="form-group mb-2 mr-2">
            {{Form::select("state",['all' => 'ALL','1' => 'OPEN','0' => 'CLOSE'], isset($_GET['state']) ? $_GET['state'] : null,["class" => "form-control form-control-sm", 'id' => 'state'])}}
        </div>
        <div class="form-group mb-2 mr-2">
            <div class="input-group input-group-sm">
                {{Form::text("keyword",request()->keyword,["class" => "form-control", "placeholder" => 'User Name / Account No / Phone','id' => 'keyword'])}}
                <span class="input-group-append">
                    {{Form::submit('Search',['class' => 'btn btn-primary appendQueryBtn'])}}
                </span>
            </div>
        </div>
        <a href="{{route('users.sale-network.index')}}" class="btn btn-warning btn-sm mb-2">Reset</a>
    {{ Form::close() }}
    

    <div class="breadcrumb pt-2">
        <a href="{{route('users.sale-network.index')}}" class="breadcrumb-item pt-0">{{ $settings['language']['LANG_MENU_SALE_NET'] }}</a>
        @if(isset($topLevelUsers))
            @foreach($topLevelUsers as $key => $row)
                @if( $key != count($topLevelUsers)-1 )
                    <a href="{{route('users.sale-network.index').'?parent_id='.$row->id}}" class="breadcrumb-item pt-0">{{$row->username}}</a>
                @else 
                    <span class="breadcrumb-item active pt-0"> {{ $row->username }}</span>
                @endif
            @endforeach
        @endif
    </div>

    <div class="card">
        <div class="table-responsive">
            @if(!request()->keyword)
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>Username</th>
                        <th>{{ $settings['language']['LANG_LABEL_ACCOUNT_NO'] }}</th>
                        <th>Phone</th>
                        <th>{{ $settings['language']['LANG_LABEL_BALANCE'] }}(R)</th>
                        <th>Own Rebate 5d(R)</th>
                        <th>Line Rebate 5d(R)</th>
                        <th>Own Rebate Ball(R)</th>
                        <th>Line Rebate Ball(R)</th>
                        <th>Action</th>			
                    </tr>
                </thead>
                <tbody>
                    @if(isset($users) && $users->count() > 0)
                        @foreach($users as $key => $row)
                            <tr>
                                <td><a href="{{ route('users.sale-network.index').'?parent_id='.$row->id}}">{{ $row->username }}</a></td>
                                <td><a href="{{route('users.sale-network.index').'?parent_id='.$row->id}}">{{ $row->account_number }}</a></td>
                                <td><a href="{{route('users.sale-network.index').'?parent_id='.$row->id}}">{{ $row->phone }}</a></td>
                                <td>{{ currencyFormat($row->cashAccount ? $row->cashAccount->balance : 0) }}</td>
                                <td>{{ currencyFormat($row->current_own_commission_5d) }}</td>
                                <td>{{ currencyFormat($row->current_line_commission_5d) }}</td>
                                <td>{{ currencyFormat($row->current_own_commission_ball) }}</td>
                                <td>{{ currencyFormat($row->current_line_commission_ball) }}</td>
                                
                                <td class="group-btn-action">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-outline bg-teal-400 text-teal-400 border-teal-400 border-2 dropdown-toggle mr-1" data-toggle="dropdown" aria-expanded="false"><i class="icon-cog5"></i> Action</button>
                                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(135px, 36px, 0px);">
                                            @canany(['user-account-modification'])
                                                <a href="{{route('users.sale-network.edit',$row->id)}}" class="dropdown-item"><i class="icon-pen6"></i> Setting</a>
                                                <div class="dropdown-divider m-0"></div>
                                            @endcanany
                                            @canany(['user-recharge'])
                                            <a href="{{route('users.account.recharge.edit',$row->id)}}" class="dropdown-item"><i class="icon-coin-dollar"></i> Recharge</a>
                                            <div class="dropdown-divider m-0"></div>
                                            @endcanany
                                            @canany(['user-take-cash'])
                                            <a href="{{route('users.account.takecash.edit',$row->id)}}" class="dropdown-item"><i class="icon-coin-dollar"></i> Take Cash</a>
                                            <div class="dropdown-divider m-0"></div>
                                            @endcanany
                                            @canany(['view-user-account'])
                                            <a href="{{route('users.accounts.show',$row->id)}}" class="dropdown-item"><i class="icon-credit-card"></i> Account</a>
                                            <div class="dropdown-divider m-0"></div>
                                            @endcanany
                                            @canany(['view-user-account-log'])
                                            <a href="{{route('users.logs.show',$row->id)}}" class="dropdown-item"><i class="icon-list"></i> Account Log</a>
                                            @endcanany
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else 
                        <tr><td colspan="9">No Data</td></tr>
                    @endif
                </tbody>
            </table>
            @else 
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>Username</th>
                        <th>{{ $settings['language']['LANG_LABEL_ACCOUNT_NO'] }}</th>
                        <th>Phone</th>
                        <th>{{ $settings['language']['LANG_LABEL_BALANCE'] }}(R)</th>
                        <th>Own Rebate 5d(R)</th>
                        <th>Line Rebate 5d(R)</th>
                        <th>Own Rebate Ball(R)</th>
                        <th>Line Rebate Ball(R)</th>
                        <th>Action</th>		
                    </tr>
                </thead>
                <tbody>
                    @if(isset($users) && $users->count() > 0)
                        @foreach($users as $key => $row)
                            <tr>
                                <td>{{ $row->username }}</td>
                                <td>{{ $row->account_number }}</td>
                                <td>{{ $row->phone }}</td>
                                <td>{{ currencyFormat($row->cashAccount ? $row->cashAccount->balance : 0) }}</td>
                                <td>{{ currencyFormat($row->current_own_commission_5d) }}</td>
                                <td>{{ currencyFormat($row->current_line_commission_5d) }}</td>
                                <td>{{ currencyFormat($row->current_own_commission_ball) }}</td>
                                <td>{{ currencyFormat($row->current_line_commission_ball) }}</td>
                                <td class="group-btn-action">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-outline bg-teal-400 text-teal-400 border-teal-400 border-2 dropdown-toggle mr-1" data-toggle="dropdown" aria-expanded="false"><i class="icon-cog5"></i> Action</button>
                                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(135px, 36px, 0px);">
                                            <a href="{{route('users.sale-network.index')}}{{'?parent_id='.$row->parent_id }}" class="dropdown-item"><i class="icon-users4"></i> Sale Network</a>
                                            @canany(['user-account-modification'])
                                                <a href="{{route('users.sale-network.edit',$row->id)}}" class="dropdown-item"><i class="icon-pen6"></i> Setting</a>
                                                <div class="dropdown-divider m-0"></div>
                                            @endcanany
                                            @canany(['user-recharge'])
                                            <a href="{{route('users.account.recharge.edit',$row->id)}}" class="dropdown-item"><i class="icon-coin-dollar"></i> Recharge</a>
                                            <div class="dropdown-divider m-0"></div>
                                            @endcanany
                                            @canany(['user-take-cash'])
                                            <a href="{{route('users.account.takecash.edit',$row->id)}}" class="dropdown-item"><i class="icon-coin-dollar"></i> Take Cash</a>
                                            <div class="dropdown-divider m-0"></div>
                                            @endcanany
                                            @canany(['view-user-account'])
                                            <a href="{{route('users.accounts.show',$row->id)}}" class="dropdown-item"><i class="icon-credit-card"></i> Account</a>
                                            <div class="dropdown-divider m-0"></div>
                                            @endcanany
                                            @canany(['view-user-account-log'])
                                            <a href="{{route('users.logs.show',$row->id)}}" class="dropdown-item"><i class="icon-list"></i> Account Log</a>
                                            @endcanany
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else 
                        <tr><td colspan="9">No Data</td></tr>
                    @endif
                </tbody>
            </table>
            @endif
        </div>
        @if(isset($users) && count($users) > 0)
        <div class="card-footer">
            @if($users->hasMorePages())
                <div class="mb-2">
                    {!! $users->appends(Input::except('page'))->render() !!}
                </div>
            @endif
            <div>
                Showing {{$users->firstItem()}} to {{$users->lastItem()}}
                of  {{$users->total()}} entries
            </div>
        </div>
        @endif
    </div>
    
</div>
@endsection