@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{ $settings['language']['LANG_LABEL_REPORT_SELECT'] }}</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Order</span>
                <span class="breadcrumb-item active">Report</span>
                <span class="breadcrumb-item active">Selection Form</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    {{ Form::open(['route' => 'ball.orders.report.index', 'method' => 'GET']) }}
                    <div class="form-group form-group-float form-group-feedback form-group-feedback-right">
                        <label class="form-group-float-label font-weight-semibold is-visible">Cycle SN</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar3"></i></span>
                            </span>
                            {{Form::select("cycle_id",$cycles, null,["class" => "form-control",'id' => 'cycle'])}}
                        </div>
                    </div>
                    {{ Form::close() }}

                    {{ Form::open(['route' => 'ball.orders.report.index', 'method' => 'GET','id' => 'form']) }}
                    <div class="form-group">
                        <label class="form-group-float-label font-weight-semibold is-visible">{{ $settings['language']['LANG_LABEL_START_DT'] }}</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar3"></i></span>
                            </span>
                            {{Form::text("begin_date",old("begin_date") ?: date('Y-m-d 16:40:00',strtotime('-1 days')),
                                ["class" => "form-control datetimepicker"])
                            }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="form-group-float-label font-weight-semibold is-visible">{{ $settings['language']['LANG_LABEL_END_DT'] }}</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar3"></i></span>
                            </span>
                            {{Form::text("end_date",old("end_date") ?: date('Y-m-d 16:40:00'),
                                ["class" => "form-control datetimepicker"])
                            }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="form-group-float-label font-weight-semibold is-visible">Username / Account No. / Phone</label>
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar3"></i></span>
                            </span>
                            {{Form::text("keyword",old("keyword"),
                                ["class" => "form-control"])
                            }}
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
            
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success" form="form">
                <i class="icon-paperplane mr-1"></i> {{ $settings['language']['LANG_LABEL_QUERY'] }}
            </button>
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>
@endsection

@section('custom-js')
<script>
    $(document).ready(function(){
        if($('#cycle').length > 0){
            $('#cycle').change(function(){
                $(this).parents('form').submit();
            });
        }
    })
</script>
@endsection