@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
<div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4>
                <span class="font-weight-semibold">History List Report </span>
                @if(request()->cycle_id && isset($cycle) && $cycle)
                    <i>For cycle {{ $cycle->cycle_sn}}</i>
                @elseif(request()->begin_date && request()->end_date)
                    <i>Form {{ request()->begin_date }} To {{request()->end_date}}</i>
                @endif
            </h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        <div class="header-elements d-none">
            @canany(['export-ball-order-list'])
            {{-- <div class="d-flex justify-content-center">
                <button class="btn btn-link btn-primary btn-sm text-white" id="downloadExcelButton" 
                data-route="{{route('ball.orders.report.download')}}" 
                data-toggle="modal" data-target="#downlod_modal">
                    <i class="icon-file-excel text-white mr-1"></i> {{ $settings['language']['LANG_LABEL_DOWN'] }}
                </button>   
            </div> --}}
            @endcanany
        </div>
        
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Sethei Ball</span>
                <a href="{{route('ball.orders.report.selection-form')}}" class="breadcrumb-item">{{ $settings['language']['LANG_MENU_REPORT'] }}</a>
                <a href="{{route('ball.orders.report.selection-form')}}" class="breadcrumb-item">Selection Form</a>
                <span class="breadcrumb-item active">List</span>
            </div>
        </div>
    </div>
</div>
@php 
    $queryString = "";
    if(request()->cycle_id && isset($cycle) && $cycle){
        $queryString = "?cycle_id=".request()->cycle_id;
    }elseif(request()->begin_date && request()->end_date){
        $queryString = "?begin_date=".request()->begin_date."&end_date=".request()->end_date;
    }
@endphp
<!-- /page header -->
<div class="content">
    @if(request()->has('user_id'))
        <div class="breadcrumb">
            <a href="{{route('ball.orders.report.selection-form')}}" class="breadcrumb-item">Selection Form</a>
            
            @if(request()->cycle_id && isset($cycle) && $cycle)
                <a href="{{route('ball.orders.report.index').$queryString}}" class="breadcrumb-item">Cycle {{ $cycle->cycle_sn }}</a>
            @elseif(request()->begin_date && request()->end_date)
                <a href="{{route('ball.orders.report.index').$queryString}}" class="breadcrumb-item">Date {{ request()->begin_date }} To {{request()->end_date}}</a>
            @endif

            <span class="breadcrumb-item active">{{ $data['data'][0]['username'] }} – {{ $data['data'][0]['account_number'] }}</span>
        </div>
    @else 
        <div class="breadcrumb">
            <a href="{{route('ball.orders.report.selection-form')}}" class="breadcrumb-item">Selection Form</a>
        </div>

        @if(isset($data['total']))
            <div class="card">
                <div class="card-header p-2 font-weight-bold text-success">Grand Total</div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr class="bg-slate-800 font-weight-bold">
                                <th class="font-weight-bold">Users</th>
                                <th class="font-weight-bold">Tickets</th>
                                <th class="font-weight-bold">Order</th>
                                <th class="font-weight-bold" colspan="2">Amount</th>
                                <th class="font-weight-bold" colspan="2">Bet Rebate</th>
                                <th class="font-weight-bold" colspan="2">Line Rebate</th>
                                <th class="font-weight-bold" colspan="2">Win Amount</th>
                                <th class="font-weight-bold" colspan="2">Profit</th> 	 		
                            </tr>
                        </thead>
                        
                        <tbody>
                            <tr>
                                <td class="font-weight-bold"> {{ $data['total']['grandTotalUser'] }}</td>
                                <td class="font-weight-bold"> {{ $data['total']['grandTotalTicket'] }}</td>
                                <td class="font-weight-bold">{{ $data['total']['grandTotalOrder'] }}</td>
                                <td class="font-weight-bold">(R) {{ currencyFormat($data['total']['grandTotalAmountR']) }}</td>
                                <td class="font-weight-bold">($) {{ currencyFormat($data['total']['grandTotalAmountD']) }}</td>
                                <td class="font-weight-bold">(R) {{ currencyFormat($data['total']['grandTotalRebateR']) }}</td>
                                <td class="font-weight-bold">($) {{ currencyFormat($data['total']['grandTotalRebateD']) }}</td>
                                <td class="font-weight-bold">(R) {{ currencyFormat($data['total']['grandTotalParentRebateR']) }}</td>
                                <td class="font-weight-bold">($) {{ currencyFormat($data['total']['grandTotalParentRebateD']) }}</td>
                                <td>(R){{ currencyFormat($data['total']['grandTotalWinAmountR']) }}</td>
                                <td>($){{ currencyFormat($data['total']['grandTotalWinAmountD']) }}</td>
                                <td class="{{$data['total']['grandTotalProfitAmountR'] > 0 ? 'text-success' : 'text-danger'}}">(R){{ currencyFormat($data['total']['grandTotalProfitAmountR']) }}</td>
                                <td class="{{$data['total']['grandTotalProfitAmountD'] > 0 ? 'text-success' : 'text-danger'}}">($){{ currencyFormat($data['total']['grandTotalProfitAmountD']) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    @endif

    <div class="card">
        @if(!request()->has('user_id'))
            <div class="card-header p-2 font-weight-bold text-primary">Total by user</div>
        @endif
        <div class="table-responsive">
            <table class="table table-bordered">
                @if(!request()->has('user_id'))
                    <thead>
                        <tr class="bg-slate-800">
                            <th>{{ $settings['language']['LANG_LABEL_ACCOUNT_NO'] }}</th>
                            <th>{{ $settings['language']['LANG_LABEL_USERNAME'] }}</th>
                            <th>Tickets</th>
                            <th>Order</th>
                            <th colspan="2">Amount</th>
                            <th colspan="2">Rebate</th> 	
                            <th colspan="2">Win Amount</th> 	
                            <th colspan="2">Profit</th> 	 		
                        </tr>
                    </thead>
                    
                    <tbody>
                        @if(isset($data['data']) && count($data['data']) > 0)
                            @foreach($data['data'] as $row)
                                <tr>
                                    <td>
                                        <a href="{{route('ball.orders.report.index').$queryString.'&user_id='.$row['user']['id']}}">{{ $row['user']['account_number'] }}</a>
                                    </td>
                                    <td>
                                        <a href="{{route('ball.orders.report.index').$queryString.'&user_id='.$row['user']['id']}}">{{ $row['user']['username'] }}</a>
                                    </td>
                                    <td> {{ $row['totalTicket'] }}</td>
                                    <td>{{ $row['totalOrder'] }}</td>
                                    <td>(R) {{ currencyFormat($row['totalAmountR']) }}</td>
                                    <td>($) {{ currencyFormat($row['totalAmountD']) }}</td>
                                    <td>(R) {{ currencyFormat($row['totalRebateR']) }}</td>
                                    <td>($) {{ currencyFormat($row['totalRebateD']) }}</td>
                                    <td>(R){{ currencyFormat($row['totalWinAmountR']) }}</td>
                                    <td>($){{ currencyFormat($row['totalWinAmountD']) }}</td>
                                    <td class="{{$row['totalProfitAmountR'] > 0 ? 'text-success' : 'text-danger'}}">(R){{ currencyFormat($row['totalProfitAmountR']) }}</td>
                                    <td class="{{$row['totalProfitAmountD'] > 0 ? 'text-success' : 'text-danger'}}">($){{ currencyFormat($row['totalProfitAmountD']) }}</td>
                                </tr>
                            @endforeach
                        @else 
                            <tr><td colspan="8">No Data</td></tr>
                        @endif
                    </tbody>
                @else 
                    <thead>
                        <tr class="bg-slate-800">
                            <th>{{ $settings['language']['LANG_LABEL_TICKET'] }}</th>
                            <th>{{ $settings['language']['LANG_LABEL_POST_TIME'] }}</th>
                            <th>Order Bet</th>
                            <th colspan="2">{{ $settings['language']['LANG_LABEL_AMOUNT'] }}</th>
                            <th colspan="2">{{ $settings['language']['LANG_LABEL_TICKET_SUM'] }}</th> 	 		
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($data['data']) && count($data['data']) >0 )
                            @foreach($data['data'][0]['tickets'] as $ticket)
                                @foreach($ticket['orders'] as $index => $row)
                                    @if($index == 0)
                                        <tr style="background-color:#eaf9eb">
                                            <td style="vertical-align: top; cursor:pointer" class="show-order-detail" data-toggle="modal" data-target="#order_detail_modal" 
                                                data-route="{{route('ball.orders.report.get-order-detail',$ticket['ticket'])}}">{{ $ticket['ticket'] }}</td>
                                            <td style="vertical-align: top;">{{ $ticket['created_at'] }}</td>
                                            <td>{{$row['no1']}}, {{$row['no2']}}, {{$row['no3']}}, {{$row['no4']}}, {{$row['no5']}}, {{$row['no6']}}, </td>
                                            <td>(R) {{ currencyFormat($row['amountR']) }}</td>
                                            <td>($) {{ currencyFormat($row['amountD']) }}</td>
                                            <td style="vertical-align: top;">(R) {{ currencyFormat($ticket['ticketAmountR']) }}</td>
                                            <td style="vertical-align: top;">($) {{ currencyFormat($ticket['ticketAmountD'])  }}</td>
                                        </tr>
                                    @else 
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>{{$row['no1']}}, {{$row['no2']}}, {{$row['no3']}}, {{$row['no4']}}, {{$row['no5']}}, {{$row['no6']}}, </td>
                                            <td>(R) {{ currencyFormat($row['amountR']) }}</td>
                                            <td>($) {{ currencyFormat($row['amountD']) }}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endforeach
                        @else
                                <tr><td colspan="7">No Data</td></tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="5">Grand Total</th>
                            <th>(R) {{ currencyFormat($data['total']['totalAmountR']) }}</th>
                            <th>($) {{ currencyFormat($data['total']['totalAmountD']) }}</th>
                        </tr>
                    </tfoot>
                @endif
            </table>
        </div>
    </div>
</div>
<div id="downlod_modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-teal-400">
                <h5 class="modal-title">Download Excel</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body text-center p-4 h5 m-0" id="downloadModalBody">
                <i class='icon-spinner2 spinner'></i> Downloading...
            </div>
        </div>
    </div>
</div>
<div id="order_detail_modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-teal-400">
                <h5 class="modal-title">Order Detail</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="body">
                
            </div>
        </div>
    </div>
    
    
</div>
@endsection