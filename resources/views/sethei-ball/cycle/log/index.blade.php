@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">Ball Cycle Result Log</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Sethei Ball</span>
                <span class="breadcrumb-item active">Cycle</span>
                <span class="breadcrumb-item active">Log</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-slate-800">
                        <th>Cycle SN</th>
                        <th>Stop Time</th>
                        <th>Result Time</th>
                        <th colspan="2">Prize</th>
                        <th>D1</th>
                        <th>D2</th>
                        <th>D3</th>
                        <th>D4</th>
                        <th>D5</th>
                        <th>D6</th>
                        <th>Jackpot</th>
                        <th>{{ $settings['language']['LANG_LABEL_STATE'] }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if($logs->count() > 0)
                        @foreach($logs as $key => $row)
                        <tr>
                            <td>{{ $row->cycle_sn }}</td>
                            <td>{{  date('Y-m-d H:i:s',$row->stopped_time) }}</td>
                            <td>{{  date('Y-m-d H:i:s',$row->result_time) }} </td>
                            <td>R {{ currencyFormat($row->prize) }}</td>
                            <td>$ {{ currencyFormat($row->prize * exchangeRateFromRielToDollar())}}</td>
                            <td>{{ $row->result_number[0] }}</td>
                            <td>{{ $row->result_number[1] }}</td>
                            <td>{{ $row->result_number[2] }}</td>
                            <td>{{ $row->result_number[3] }}</td>
                            <td>{{ $row->result_number[4] }}</td>
                            <td>{{ $row->result_number[5] }}</td>
                                @if($row->is_jackpot == 1)
                                    <td><div class="badge badge-info badge-pill">Win</div></td>
                                @else
                                    <td><div class="badge badge-danger badge-pill">No Jackpot</div></td>
                                @endif
                            </td>
                            <td>
                                <span class="badge badge-flat border-success text-success-600">Awarded</span>
                            </td>
                        </tr>
                        @endforeach
                    @else 
                        <tr><td colspan="11">No Data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        @if(isset($logs) && count($logs) > 0)
            <div class="card-footer">
                @if($logs->hasMorePages())
                    <div class="mb-2">
                        {!! $logs->appends(Input::except('page'))->render() !!}
                    </div>
                @endif
                <div>
                    Showing {{$logs->firstItem()}} to {{$logs->lastItem()}}
                    of  {{$logs->total()}} entries
                </div>
            </div>
        @endif
    </div>
</div>
@endsection