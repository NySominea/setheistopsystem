@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{ $settings['language']['LANG_MENU_GAME_SET'] }}</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Sethei 5D</span>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_MENU_GAME_SET'] }}</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    @include('includes.error-msg')
    @include('includes.success-msg')
    {!! Form::open(['route' => '5d.game.setting.store','method' => 'POST']) !!}
    @csrf
    <div class="card">
        <div class="card-header">
            <div>
                <h5>5D Game Rebate</h5>
                <div class="table-responsive mb-3">
                    <table class="table table-bordered">
                        <thead>
                            <tr class="bg-slate-800">
                                <th style="min-width:80px;">{{ $settings['language']['LANG_LABEL_GAME_CAT'] }}</th>
                                @for($i=1;$i<6;$i++)
                                    <th>{{getUserLevelTitle($i)}}</th>
                                @endfor
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($rebates) && $rebates->count() > 0)
                                @foreach($rebates as $row)
                                <tr>
                                    <td>{{ $row->category->name }}</td>
                                    @for($i=1;$i<6;$i++)
                                        <td style="min-width:60px;">{{Form::number("rebates[$row->id][l".$i."_rebate]",$row->{'l'.$i.'_rebate'},['class' => 'w-100 form-control form-control-sm', 'step' => '0.01'])}}</td>
                                    @endfor
                                </tr>
                                @endforeach
                            @else 
                                <tr><td colspan="6">No Data</td></tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="mt-3">
                <h5>5D Game Odds</h5>
                <div class="row">
                    <div class="col-md-5">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="bg-slate-800">
                                        <th style="min-width:80px;">{{ $settings['language']['LANG_LABEL_GAME_CAT'] }}</th>
                                        <th>{{ $settings['language']['LANG_LABEL_ODDS'] }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($categories) && $categories->count() > 0)
                                        @foreach($categories as $row)
                                        <tr>
                                            <td>{{ $row->name }}</td>
                                            <td style="min-width:60px;">{{ $row->rate }}</td>
                                        </tr>
                                        @endforeach
                                    @else 
                                        <tr><td colspan="6">No Data</td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @canany(['5d-game-setting-modification'])
        @if(isset($rebates) && $rebates->count() > 0)
        <div class="card-footer">
            {!! Form::button('<i class="icon-folder mr-1"></i> '.$settings['language']['LANG_LABEL_POST'], [ 'class' => 'btn btn-success', 'type' => 'submit']) !!}
        </div>
        @endif
        @endcanany
    </div>
    {!! Form::close() !!}
</div>
@endsection