@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">{{ $settings['language']['LANG_MENU_THIS_ORDER'] }}</span></h4>
            <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
        </div>
        <div class="header-elements d-none">
            @canany(['export-5d-order-list'])
            {{-- <div class="d-flex justify-content-center">
                <button class="btn btn-link btn-primary btn-sm text-white" id="downloadExcelButton" 
                data-route="{{route('5d.orders.temp.download')}}" 
                data-toggle="modal" data-target="#downlod_modal">
                    <i class="icon-file-excel text-white mr-1"></i> {{ $settings['language']['LANG_LABEL_DOWN'] }}
                </button>
            </div> --}}
            @endcanany
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Sethei 5D</span>
                <span class="breadcrumb-item active">{{ $settings['language']['LANG_MENU_THIS_ORDER'] }}</span>
                <span class="breadcrumb-item active">Current</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->
<div class="content">

    @if(request()->has('user_id'))
        <div class="breadcrumb">
            <a href="{{route('5d.orders.temp.index')}}" class="breadcrumb-item pt-0">{{ $settings['language']['LANG_LABEL_ALL'] }}</a>
            <span class="breadcrumb-item active pt-0">{{ $data['data'][0]['username'] }} – {{ $data['data'][0]['account_number'] }}</span>
        </div>
    @else 
        {{ Form::open(['route' => '5d.orders.temp.index', 'method' => 'GET', 'class' => 'form-inline d-block d-sm-flex']) }}
            <div class="form-group mb-2 mr-2">
                <div class="input-group input-group-sm">
                    {{Form::text("keyword",request()->keyword,["class" => "form-control", "placeholder" => 'keyword','id' => 'keyword'])}}
                </div>
            </div>
            <div class="form-group mb-2 mr-2">
                <div class="input-group input-group-sm">
                    <div class="input-group-prepend">
                        <div class="input-group-text">{{ $settings['language']['LANG_LABEL_START_DT'] }}</div>
                    </div>
                    <input type="text" class="form-control datetimepicker" value="{{isset($_GET['begin_date']) ? $_GET['begin_date'] : date('Y-m-d 16:45:00',strtotime('-1 days'))}}" name="begin_date">
                </div>
            </div>
            <div class="form-group mb-2 mr-2">
                <div class="input-group input-group-sm">
                    <div class="input-group-prepend">
                        <div class="input-group-text">{{ $settings['language']['LANG_LABEL_END_DT'] }}</div>
                    </div>
                    <input type="text" class="form-control datetimepicker" value="{{isset($_GET['end_date']) ? $_GET['end_date'] : date('Y-m-d 16:45:00')}}" name="end_date">
                </div>
            </div>
            {{Form::submit('Query',['class' => 'btn btn-sm btn-primary mb-2 mr-2'])}}
            <a href="{{route('5d.orders.temp.index')}}" class="btn btn-warning btn-sm mb-2">Reset</a>
        {{ Form::close() }}

        @if(isset($data['total']))
            <div class="card">
                <div class="card-header p-2 font-weight-bold text-success">Grand Total</div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr class="bg-slate-800 font-weight-bold">
                                <th class="font-weight-bold">Grand Total Users</th>
                                <th class="font-weight-bold">Grand Total Tickets</th>
                                <th class="font-weight-bold">Grand Total Order</th>
                                <th class="font-weight-bold" colspan="2">Grand Total Amount</th>
                                <th class="font-weight-bold" colspan="2">Grand Total Rebate</th> 	 		
                            </tr>
                        </thead>
                        
                        <tbody>
                            <tr>
                                <td class="font-weight-bold"> {{ $data['total']['grandTotalUser'] }}</td>
                                <td class="font-weight-bold"> {{ $data['total']['grandTotalTicket'] }}</td>
                                <td class="font-weight-bold">{{ $data['total']['grandTotalOrder'] }}</td>
                                <td class="font-weight-bold">(R) {{ currencyFormat($data['total']['grandTotalAmountR']) }}</td>
                                <td class="font-weight-bold">($) {{ currencyFormat($data['total']['grandTotalAmountD']) }}</td>
                                <td class="font-weight-bold">(R) {{ currencyFormat($data['total']['grandTotalRebateR']) }}</td>
                                <td class="font-weight-bold">($) {{ currencyFormat($data['total']['grandTotalRebateD']) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    @endif

    <div class="card">
        @if(!request()->has('user_id'))
            <div class="card-header p-2 font-weight-bold text-primary">Total by user</div>
        @endif
        <div class="table-responsive">
            <table class="table table-bordered">
                @if(!request()->has('user_id'))
                    <thead>
                        <tr class="bg-slate-800">
                            <th>{{ $settings['language']['LANG_LABEL_ACCOUNT_NO'] }}</th>
                            <th>{{ $settings['language']['LANG_LABEL_USERNAME'] }}</th>
                            <th>Total Tickets</th>
                            <th>Total Order</th>
                            <th colspan="2">Total Amount</th>
                            <th colspan="2">Total Rebate</th> 	 		
                        </tr>
                    </thead>
                    
                    <tbody>
                        @if(isset($data['data']) && count($data['data']) > 0)
                            @foreach($data['data'] as $row)
                                <tr>
                                    <td>
                                        <a href="{{route('5d.orders.temp.index').'?user_id='.$row['user']['id']}}">{{ $row['user']['account_number'] }}</a>
                                    </td>
                                    <td>
                                        <a href="{{route('5d.orders.temp.index').'?user_id='.$row['user']['id']}}">{{ $row['user']['username'] }}</a>
                                    </td>
                                    <td> {{ $row['totalTicket'] }}</td>
                                    <td>{{ $row['totalOrder'] }}</td>
                                    <td>(R) {{ currencyFormat($row['totalAmountR']) }}</td>
                                    <td>($) {{ currencyFormat($row['totalAmountD']) }}</td>
                                    <td>(R) {{ currencyFormat($row['totalRebateR']) }}</td>
                                    <td>($) {{ currencyFormat($row['totalRebateD']) }}</td>
                                </tr>
                            @endforeach

                        @else 
                            <tr><td colspan="8">No Data</td></tr>
                        @endif
                    </tbody>
                @else 
                    <thead>
                        <tr class="bg-slate-800">
                            <th>{{ $settings['language']['LANG_LABEL_TICKET'] }}</th>
                            <th>{{ $settings['language']['LANG_LABEL_POST_TIME'] }}</th>
                            <th>{{ $settings['language']['LANG_LABEL_PRIZE'] }}</th>
                            <th>{{ $settings['language']['LANG_LABEL_BET'] }}</th>
                            <th>PC/U</th>
                            <th colspan="2">{{ $settings['language']['LANG_LABEL_AMOUNT'] }}</th>
                            <th colspan="2">{{ $settings['language']['LANG_LABEL_TICKET_SUM'] }}</th> 	 		
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data['data'][0]['tickets'] as $t => $ticket)
                            @foreach($ticket['orders'] as $index => $row)
                                @if($index == 0)
                                    <tr style="background-color:#eaf9eb">
                                        <td style="vertical-align: top; cursor:pointer" class="show-order-detail" data-toggle="modal" data-target="#order_detail_modal" 
                                            data-route="{{route('5d.orders.temp.get-order-detail',$ticket['ticket'])}}">{{ $ticket['ticket'] }}</td>
                                        <td style="vertical-align: top;">{{ $ticket['created_at'] }}</td>
                                        <td>{{ $row['prize'] }}</td>
                                        <td>{{ $row['bet'] }}</td>
                                        <td>{{ currencyFormat($row['unit']) }}</td>
                                        <td>(R) {{ currencyFormat($row['amountR']) }}</td>
                                        <td>($) {{ currencyFormat($row['amountD']) }}</td>
                                        <td style="vertical-align: top;">(R) {{ currencyFormat($ticket['ticketAmountR']) }}</td>
                                        <td style="vertical-align: top;">($) {{ currencyFormat($ticket['ticketAmountD'])  }}</td>
                                    </tr>
                                @else 
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>{{ $row['prize'] }}</td>
                                        <td>{{ $row['bet'] }}</td>
                                        <td>{{ currencyFormat($row['unit']) }}</td>
                                        <td>(R) {{ currencyFormat($row['amountR']) }}</td>
                                        <td>($) {{ currencyFormat($row['amountD']) }}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @endif
                            @endforeach
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th><th></th><th></th><th></th><th></th>
                            <th colspan="2">Grand Total</th>
                            <th>(R) {{ currencyFormat($data['total']['totalAmountR']) }}</th>
                            <th>($) {{ currencyFormat($data['total']['totalAmountD']) }}</th>
                        </tr>
                    </tfoot>
                @endif
            </table>
        </div>
    </div>
</div>
<div id="downlod_modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-teal-400">
                <h5 class="modal-title">{{$settings['language']['LANG_LABEL_DOWN']}}</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <div class="modal-body text-center p-4 h5 m-0" id="downloadModalBody">
                <i class='icon-spinner2 spinner'></i> Downloading...
            </div>
        </div>
    </div>
</div>
<div id="order_detail_modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-teal-400">
                <h5 class="modal-title">Order Detail</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="body">
                
            </div>
        </div>
    </div>
    
</div>
@endsection

@section('page-script')
<script src="/global_assets/js/plugins/ui/moment/moment.min.js"></script>
<script src="/global_assets/js/plugins/pickers/daterangepicker.js"></script>
@endsection