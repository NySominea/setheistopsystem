@extends('layouts.master')

@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><span class="font-weight-semibold">5D Cycle Result Log</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
        <div class="d-flex">
            <div class="breadcrumb">
                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                <span class="breadcrumb-item active">Sethei 5D</span>
                <span class="breadcrumb-item active">Cycle</span>
                <span class="breadcrumb-item active">Log</span>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="card">
        <div class="table-responsive">
            <table class="table table-bordered text-center">
                <thead>
                    <tr class="bg-slate-800">
                        <th>Cycle SN</th>
                        <th>Stop Time</th>
                        <th>Result Time</th>
                        <th>Prize A</th>
                        <th>Prize B</th>
                        <th>Prize C</th>
                        <th>Prize D</th>
                        <th style="width:10px">{{ $settings['language']['LANG_LABEL_STATE'] }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if($logs->count() > 0)
                        @foreach($logs as $key => $row)
                        <tr>
                            <td>{{ $row->cycle_sn }}</td>
                            <td>{{ date('Y-m-d H:i:s',$row->stopped_time) }}</td>
                            <td>{{ date('Y-m-d H:i:s',$row->result_time) }}</td>
                            <td>
                                @for($i=1;$i<6;$i++)
                                    @php $A = 'A'.$i; @endphp 
                                    {{ $row->$A }}
                                @endfor
                            </td>
                            <td>
                                @for($i=1;$i<6;$i++)
                                    @php $B = 'B'.$i; @endphp 
                                    {{ $row->$B }}
                                @endfor
                            </td>
                            <td>
                                @for($i=1;$i<6;$i++)
                                    @php $C = 'C'.$i; @endphp 
                                    {{ $row->$C }}
                                @endfor
                            </td>
                            <td>
                                @for($i=1;$i<6;$i++)
                                    @php $D = 'D'.$i; @endphp 
                                    {{ $row->$D }}
                                @endfor
                            </td>
                            <td>
                                <span class="badge badge-flat border-success text-success-600">Awarded</span>
                            </td>
                        </tr>
                        @endforeach
                    @else 
                        <tr><td colspan="11">No Data</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        @if(isset($logs) && count($logs) > 0)
            <div class="card-footer">
                @if($logs->hasMorePages())
                    <div class="mb-2">
                        {!! $logs->appends(Input::except('page'))->render() !!}
                    </div>
                @endif
                <div>
                    Showing {{$logs->firstItem()}} to {{$logs->lastItem()}}
                    of  {{$logs->total()}} entries
                </div>
            </div>
        @endif
    </div>
</div>
@endsection