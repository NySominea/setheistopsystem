<?php
namespace App\Constants;

class UserAccountConstant
{
    // Cash Balance
    const MAIN_ACCOUNT = 1;
    const BONUS_5D_ACCOUNT = 2;
    const BONUS_BALL_ACCOUNT = 3;

    // Request Balance Status
    const PENDING = 0;
    const IN_PROGRESS = 1;
    const COMPLETED = 2;
    const REJECTED = 3;

    public static function getRequestStatusLabel($status){
        $label = 'Pending';

        switch ($status) {
            case self::PENDING:
                $label = 'Pending';
                break;
            case self::IN_PROGRESS:
                $label = 'In Progress';
                break;
            case self::COMPLETED:
                $label = 'Completed';
                break;
            case self::REJECTED:
                $label = 'Rejected';
                break;
            default:
                $label = 'Pending';
                break;
        }
        return $label;
    }

    public static function getRequestStatusList(){
        return [
            self::PENDING => self::getRequestStatusLabel(self::PENDING),
            self::IN_PROGRESS => self::getRequestStatusLabel(self::IN_PROGRESS),
            self::COMPLETED => self::getRequestStatusLabel(self::COMPLETED),
            self::REJECTED => self::getRequestStatusLabel(self::REJECTED),
        ];
    }
}