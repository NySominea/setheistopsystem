<?php
namespace App\Constants;

class Account
{
    const IN = 1;
    const OUT = 2;

    const LOG_TYPE_IN = 1;
    const LOG_TYPE_OUT = 2;
    const LOG_TYPE_OWN_ACCOUNT = 3;

    const SYSTEM_ACCOUNT_TYPE = 1;
    const USER_ACCOUNT_TYPE = 2;

    const TAKE_CASH_PAYED = 1;
    const TAKE_CASH_NO_PAY = 2;
    const TAKE_CASH_CANCEL = 3;

    const CASH_ACCOUNT = 1;
    const DEFAULT_ACCOUNT = 2;
    const THIRD_PARTY_ACCOUNT = 3;
    const PROFIT_ACCOUNT = 4;
    const BONUS_ACCOUNT = 5;

    const CASH_TOP_UP_SYSTEM_ACCOUNT = 1;
    const CASH_WITHDRAW_SYSTEM_ACCOUNT = 2;
    const FINANCE_SYSTEM_ACCOUNT = 3;

    public static function accountInOut($type){
        $label = "";
        switch($type){
            case self::LOG_TYPE_IN: $label = "LANG_LABEL_IN"; break;
            case self::LOG_TYPE_OUT: $label = "LANG_LABEL_OUT"; break;
            case self::LOG_TYPE_OWN_ACCOUNT: $label = "LANG_LABEL_IN"; break;
            default: $label = ""; break;
        }
        return $label;
    }

    public static function rechargeType($type){
        $label = "";
        switch($type){
            case self::APPROVED: $label = "LANG_LABEL_APPROVED"; break;
            case self::UNAPPROVED: $label = "LANG_LABEL_NOT_APPROVED"; break;
            case self::NOT_APPROVED: $label = "LANG_LABEL_UNAPPROVED"; break;
            default: $label = ""; break;
        }
        return $label;
    }

    public static function cashWayType($type){
        $label = "";
        switch($type){
            case self::CASH_WAY_TAKE_CASH: $label = "LANG_MENU_TAKE_CASH"; break;
            case self::CASH_WAY_TO_BANK: $label = "LANG_LABEL_TO_BANK"; break;
            default: $label = ""; break;
        }
        return $label;
    }

    public static function accountTakeCashType($type){
        $label = "";
        switch($type){
            case self::TAKE_CASH_PAYED: $label = "LANG_LABEL_PAYED"; break;
            case self::TAKE_CASH_NO_PAY: $label = "LANG_LABEL_NOPAY"; break;
            case self::TAKE_CASH_CANCEL: $label = "LANG_LABEL_CANCEL"; break;
            default: $label = ""; break;
        }
        return $label;
    }

    public static function systemAccountType($type){
        $label = "";
        switch($type){
            case self::SYSTEM_ACCOUNT_TYPE: $label = "LANG_LABEL_SYS_ACCOUNT"; break;
            case self::USER_ACCOUNT_TYPE: $label = "LANG_LABEL_USER_ACCOUNT"; break;
            default: $label = ""; break;
        }
        return $label;
    }

    public static function userAccountType($type){
        $label = "";
        switch($type){
            case self::CASH_ACCOUNT: $label = "LANG_LABEL_CASH_AC"; break;
            case self::DEFAULT_ACCOUNT: $label = "LANG_LABEL_DEFAULT_AC"; break;
            case self::THIRD_PARTY_ACCOUNT: $label = "LANG_LABEL_THIRD_AC"; break;
            case self::PROFIT_ACCOUNT: $label = "LANG_LABEL_PROFIT_AC"; break;
            case self::BONUS_ACCOUNT: $label = "LANG_LABEL_BONUS_AC"; break;
            default: $label = "LANG_LABEL_DEFAULT_AC"; break;
        }
        return $label;
    }
}