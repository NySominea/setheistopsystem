<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Constants\UserAccountConstant;

class BalanceRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'amount' => currencyFormat($this->amount)." R",
            'status' => $this->status,
            'status_label' => UserAccountConstant::getRequestStatusLabel($this->status),
            'user_id' => $this->user_id,
            'note' => $this->note,
            'created_at' => $this->created_at->format('Y-m-d h:i:s A')
        ];
    }
}
