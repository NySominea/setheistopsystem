<?php

namespace App\Http\Controllers\Analysis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\OrderNumberTemp;

class ResultAnalysisController extends Controller
{
    public function index(Request $request)
    {   
        $numbers = OrderNumberTemp::orderBy('amount','Desc')
                                    ->when(request()->prize && request()->prize != 'all',function($q){
                                        return $q->wherePrize(request()->prize);
                                    })
                                    ->when(request()->type && request()->type != 'all',function($q){
                                        return $q->whereType(request()->type);
                                    })
                                    ->get();
        
        return view('analyse.index',compact('numbers'));
    }
}
