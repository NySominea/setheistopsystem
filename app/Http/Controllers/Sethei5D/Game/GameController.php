<?php

namespace App\Http\Controllers\Sethei5D\Game;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Sethei5D\GameRebate;
use App\Model\Sethei5D\GameCategory;
use DB;

class GameController extends Controller
{
    public function __construct(){
        $this->middleware('permission:view-5d-history-report')->only(['index']);
        $this->middleware('permission:5d-game-setting-modification')->only(['store']);
    }

    public function index()
    {   
        $rebates = GameRebate::with('category')->get();
        $categories = GameCategory::all();
        return view('sethei-5d.game.index',compact('rebates','categories'));
    }


    public function store(Request $request)
    {
        $this->saveToDB($request->all());
        return redirect()->route('5d.game.setting.index')->withSuccess('You have just updated game rebates successfully!');
    }


    public function saveToDB($data){ 
        
        foreach($data['rebates'] as $id => $row){ 
            $rebate = GameRebate::find($id);
            if($rebate){
                DB::beginTransaction();
                try{
                    $rebate->fill($row);
                    $rebate->save();
                    DB::commit();
                }catch(Exception $ex){
                    DB::rollback();
                }
            }
        }
    }
}
