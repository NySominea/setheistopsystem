<?php

namespace App\Http\Controllers\Sethei5D\Cycle;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Sethei5D\Cycle;

class CycleLogController extends Controller
{
    public function __construct(){
        $this->middleware('permission:view-5d-cycle-log')->only(['index']);
    }

    public function index()
    {
        $logs = Cycle::where('state',0)
                    ->where('has_released',1)
                    ->orderBy('id','DESC')
                    ->paginate(28);
        
        return view('sethei-5d.cycle.log.index',compact('logs'));
    }
}
