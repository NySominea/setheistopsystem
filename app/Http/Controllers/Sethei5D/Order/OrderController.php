<?php

namespace App\Http\Controllers\Sethei5D\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Sethei5D\OrderTemp;
use App\Model\Sethei5D\Order;
use App\Model\Sethei5D\Cycle;
use App\User;
use DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Http\Controllers\Sethei5D\Order\OrderTempController;

class OrderController extends Controller
{
    public function __construct(){
        $this->middleware('permission:view-5d-history-report')->only(['ajaxGetOrderDetail','showSelectionForm','index']);
        $this->middleware('permission:export-5d-history-report')->only(['downloadExcel','downloadSumBet']);
    }

    public function index(Request $request)
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        
        $data = [];
        $cycle = request()->cycle_id ? Cycle::findOrFail(request()->cycle_id) : null;
        $orderTempController = OrderTempController::withTable(Order::getModel()->getTable());

        $data = request()->user_id ? $orderTempController->getOrderDataByUser() : $orderTempController->getOrderDataSummary();
        
        return view('sethei-5d.order.report.index',compact('data','cycle'));
    }

    public function downloadExcel($requests = null){
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        
        $orderTempController = new OrderTempController;
        $table = Order::getModel()->getTable(); 
        
        $request = $requests ?: request();
        if($request->account){
            $user = User::where('account_number',$request->account)->first();
            $request->request->add([
                'level' => $user->level,
                'parent_id' => $user->id
            ]);
            
        }
        
        $data = $orderTempController->structureTicketDataForEachUser($table,$request);
        $path = $orderTempController->writeExcel($data,true);
        
        if($requests){
            return $path;
        }else{
            return response()->json(['path' => $path, 'success' => true]);
        }

    }

    public function showSelectionForm(){
        $cycles = Cycle::orderBy('cycle_sn','DESC')->whereStateAndHasReleased(0,1)->pluck('cycle_sn','id')->toArray();
        $cycles = ['0' => 'Choose'] + $cycles;
        return view('sethei-5d.order.report.selection-form',compact('cycles'));
    }

    public function ajaxGetOrderDetail($ticket){
        $success = false;
        $data = [];
        $orders = Order::with('user')->whereTicket($ticket)->get();
        if($orders && $orders->count() > 0){
            $data = [
                'ticket' => $orders[0]->ticket,
                'betAmount' => currencyFormat($orders->sum('amount')),
                'winAmount' => currencyFormat($orders->sum('win_amount')),
                'created_at'=> date('Y-m-d H:i A', strtotime($orders[0]->created_at)),
            ];

            // if($orders[0]->user->level != 0){
            //     for($i=1 ; $i < $orders[0]->user->level + 1 ; $i++){ 
            //         $data['rebates'][] = [
            //             'levelTitle' => getUserLevelTitle($i),
            //             'username' => $orders[0]->parent($i)->username,
            //             'rebate' => currencyFormat($orders->sum('l'.$i.'_rebate'))
            //         ];
            //     }
            // }else{
            //     $data['rebates'][] = [
            //         'levelTitle' => getAdminLanguageData()['LANG_MENU_DIRECT_USER'],
            //         'username' => $orders[0]->user->username,
            //         'rebate' => currencyFormat(0)
            //     ];
            // }
            $success = true;
        }
        return response()->json(['success' => $success, 'data' => $data]);
    }

    public function writeBetWorksheet($spreadsheet, $sheet, $data){
    
        $columnNames = [
                'UserName',
                'AccountNo',
                'Province',
                'District',
                'Address',
                'Create At',
                'Sale Supervisor',
                'Sale Area Manager',
                'Sale Director',
                'Bet Amount',
                'Win Amount',
                'Type',
                'Phone'
            
        ];
        
        $sheet = $this->writeBetHeaderWorksheet($sheet, $columnNames);
        $i = 2;
        foreach($data as $value){
            $sheet->setCellValue('A'.$i,$value['username']);
            $sheet->setCellValue('B'.$i,$value['accountno']);
            $sheet->setCellValue('C'.$i,$value['province']);
            $sheet->setCellValue('D'.$i,$value['district']);
            $sheet->setCellValue('E'.$i,$value['address']);
            $sheet->setCellValue('F'.$i,$value['created_at']);
            $sheet->setCellValue('G'.$i,$value['saleSupervisor']);
            $sheet->setCellValue('H'.$i,$value['saleAreaManager']);
            $sheet->setCellValue('I'.$i,$value['saleDirector']);
            $sheet->setCellValue('J'.$i,$value['betAmount']);
            $sheet->setCellValue('K'.$i,$value['winAmount']);
            $sheet->setCellValue('L'.$i,$value['type']);
            $sheet->setCellValue('M'.$i,$value['phone']);
            $i++;
        }

        return $spreadsheet;
    }

    public function writeBetHeaderWorksheet($sheet, $columnNames){
        $columns = [];
        $a = 'A';
        foreach($columnNames as $index => $value){
            array_push($columns, $a++);
        }

        for($iterator=0;$iterator<count($columns);$iterator++){
            $sheet->setCellValue($columns[$iterator].'1', $columnNames[$iterator]);
            $sheet->getStyle($columns[$iterator].'1')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('888888');

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->getColor()
                ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setSize(11);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setBold(true);

                $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setBold(true);
            $sheet->getColumnDimension($columns[$iterator])->setAutoSize(true);
        }

        return $sheet;
    }

}
