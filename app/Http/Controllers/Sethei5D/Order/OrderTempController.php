<?php

namespace App\Http\Controllers\Sethei5D\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Sethei5D\OrderTemp;
use App\Model\Sethei5D\Order;
use App\Model\Sethei5D\Cycle;
use App\User;
use DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class OrderTempController extends Controller
{
    public function __construct(){
        $this->middleware('permission:view-5d-order-list')->only(['ajaxGetOrderDetail','index']);
        $this->middleware('permission:export-5d-order-list')->only(['downloadExcel']);
    }

    public static function withTable( $table ) {
        $instance = new self();
        $instance->table = $table;
        return $instance;
    }

    public function index(Request $request)
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        
        $this->table = OrderTemp::getModel()->getTable();

        $data = request()->user_id ? $this->getOrderDataByUser() : $this->getOrderDataSummary();
        
        return view('sethei-5d.order.temp.index',compact('data'));
    }

    public function downloadExcel(){
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        $table = OrderTemp::getModel()->getTable(); 
        $historyReport = false;
        $request = request(); 

        $data = $this->structureTicketDataForEachUser($table,$request);
        $path = $this->writeExcel($data); 
        return response()->json(['path' => $path, 'success' => true]);
    }

    public function writeExcel($data, $isHistoryList = false){
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Sale Report');
        $spreadsheet = $this->writeDetailWorksheet($spreadsheet, $sheet, $data['data'], $isHistoryList);
        
        $writer = new Xlsx($spreadsheet);
        if(!is_dir(public_path('backup'))) mkdir(public_path('backup'),0777,true);
        $filename = "5D_Sale_Report(".date('Y-m-d').").xlsx";
        
        if(request()->account){
            $filename = "5D_Sale_Report(".request()->account.").xlsx";
        }elseif(request()->cycle_id){
            $cycle = Cycle::whereId(request()->cycle_id)->first();
            if($cycle) $filename = "5D_Sale_Report(".$cycle->cycle_sn.").xlsx";
        }
        
        $writer->save(public_path('backup').'/'.$filename);
        $path = asset('backup/'.$filename);
        return $path;
    }

    public function writeDetailWorksheet($spreadsheet, $sheet, $data, $isHistoryList){
        $columnNames = $isHistoryList 
                        ? ['Account No','User','Ticket','Post Time','Prize', 'Type', 'Bet', 'P/Unit (R)','Amount', 'Win Amount (R)','Ticket Amount (R)', 'Ticket Win Amount (R)']
                        : ['Account No','User','Ticket','Post Time','Prize', 'Type', 'Bet', 'P/Unit (R)','Amount', 'Win Amount (R)', 'Ticket Amount (R)', 'Ticket Win Amount (R)'];
        
        $sheet = $this->writeHeaderWorksheet($sheet, $columnNames);
        $i = 2;
        foreach( $data as $l => $user){
            $sheet->setCellValue('A'.$i, $user['accountno']);
            $sheet->setCellValue('B'.$i, $user['username']);
            $j = 0; 
            foreach($user['ticket'] as $key => $ticket){
                $totalTicket = 0;
                $sheet->setCellValueExplicit('C'.($i + $j), $ticket['ticket'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $sheet->setCellValue('D'.($i + $j), $ticket['created_at']);
                $sheet->setCellValue('K'.($i + $j), $ticket['ticketAmountR']);
                $sheet->setCellValue('L'.($i + $j), $ticket['ticketWinAmountR']);
                foreach($ticket['orders'] as $index => $o){
                    $sheet->setCellValueExplicit('E'.($i + $j + $index), $o['prize'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $sheet->setCellValue('F'.($i + $j + $index), $o['type']); 
                    $sheet->setCellValueExplicit('G'.($i + $j + $index), $o['bet'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $sheet->setCellValue('H'.($i + $j + $index), $o['unit']);
                    $sheet->setCellValue('I'.($i + $j + $index), $o['amountR']);
                    $sheet->setCellValue('J'.($i + $j + $index), $o['winAmountR']);
                    $totalTicket += $o['amountR'];
                }
                if($ticket['ticketAmountR'] != $totalTicket){
                    $sheet->setCellValueExplicit('E'.($i + $j + $index + 1), 'Lost Number',\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $sheet->setCellValue('F'.($i + $j + $index + 1), 'Lost Number');
                    $sheet->setCellValueExplicit('G'.($i + $j + $index + 1), 'Lost Number',\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $sheet->setCellValue('H'.($i + $j + $index + 1), 'Lost Number');
                    $sheet->setCellValue('I'.($i + $j + $index + 1), $ticket['ticketAmountR']-$totalTicket); 
                    $j+=1;
                }
                $j+=count($ticket['orders']);
            }
            $i+=$j;
        }
        
        return $spreadsheet;
    }

    public function writeHeaderWorksheet($sheet, $columnNames){
        $columns = [];
        $a = 'A';
        foreach($columnNames as $index => $value){
            array_push($columns, $a++);
        }

        for($iterator=0;$iterator<count($columns);$iterator++){
            $sheet->setCellValue($columns[$iterator].'1', $columnNames[$iterator]);
            $sheet->getStyle($columns[$iterator].'1')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('888888');

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->getColor()
                ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setSize(11);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setBold(true);
            $sheet->getColumnDimension($columns[$iterator])->setAutoSize(true);
        }

        return $sheet;
    }

    public function getOrderDataSummary(){ 
        $data = [];
        $total = [];
        $rielToDollar = exchangeRateFromRielToDollar();
        $relationship = $this->table == OrderTemp::getModel()->getTable() ? 'dOrderTemps' : 'dOrders';
        
        // Get List of users for current orders
        $users = User::with($relationship)
                    ->when(request()->keyword != '',function($q){
                        return $q->where('username','LIKE','%'.request()->keyword.'%')
                                ->orWhere('account_number','LIKE','%'.request()->keyword.'%')
                                ->orWhere('phone','LIKE','%'.request()->keyword.'%');
                    })
                    ->orderBy('account_number','ASC')
                    ->has($relationship, '>', 0)
                    ->when(request()->has('cycle_id'),function($query) use ($relationship){
                        $query->whereHas($relationship,function($q){
                            $q->where('cycle_id', request()->cycle_id);
                        });
                    })
                    ->when(request()->has('begin_date') && request()->has('end_date'),function($query) use ($relationship){
                        $query->whereHas($relationship,function($q){
                            $q->whereBetween('created_at', [request()->begin_date, request()->end_date]);
                        });
                    })
                    ->select('id','username','account_number','phone','parent_id')
                    ->get();
        
        // End Get List of users
        
        $grandTotalOrder = 0;
        $grandTotalTicket = 0; 
        $grandTotalAmount = 0;
        $grandTotalRebate = 0;
        $grandTotalWinAmount = 0;
        $grandTotalProfitAmount = 0;
        
        foreach($users as $user){
            $totalTicket = $user->{$relationship}->groupBy('ticket')->count();
            $totalOrder = 0;
            $user->{$relationship}->each(function($val) use(&$totalOrder){
                $totalOrder+=count($val->bet_content);
            });
            $totalAmount = $user->{$relationship}->sum('amount');
            $totalRebate = 0;
            $totalWinAmount = $user->{$relationship}->sum('win_amount');

            $data[] = [
                'user'      => $user->toArray(),
                'totalTicket'   => $totalTicket,
                'totalOrder'    => $totalOrder,
                'totalAmountR'    => $totalAmount,
                'totalAmountD'    => $totalAmount * $rielToDollar,
                'totalRebateR' => $totalRebate,   
                'totalRebateD' => $totalRebate * $rielToDollar,
                'totalWinAmountR'=> $totalWinAmount,
                'totalWinAmountD'=> $totalWinAmount * $rielToDollar,
                'totalProfitAmountR' => $totalAmount - $totalWinAmount - $totalRebate,
                'totalProfitAmountD' => ($totalAmount - $totalWinAmount - $totalRebate)  * $rielToDollar,
            ]; 
            
            $grandTotalTicket += $totalTicket;
            $grandTotalOrder += $totalOrder;
            $grandTotalAmount += $totalAmount;
            $grandTotalRebate += $totalRebate;
            $grandTotalWinAmount += $totalWinAmount;
            $grandTotalProfitAmount += $totalAmount - $totalWinAmount - $totalRebate;
            
        }
        $total = [
            'grandTotalUser' => $users->count(),
            'grandTotalTicket' => $grandTotalTicket,
            'grandTotalOrder' => $grandTotalOrder,
            'grandTotalAmountR' => $grandTotalAmount,
            'grandTotalAmountD' => $grandTotalAmount * $rielToDollar,
            'grandTotalRebateR' => $grandTotalRebate,
            'grandTotalRebateD' => $grandTotalRebate * $rielToDollar,
            'grandTotalWinAmountR' => $grandTotalWinAmount,
            'grandTotalWinAmountD' => $grandTotalWinAmount * $rielToDollar,
            'grandTotalProfitAmountR' => $grandTotalProfitAmount,
            'grandTotalProfitAmountD' => $grandTotalProfitAmount * $rielToDollar
        ];
        
        return ['data' => $data, 'total' => $total];
    }

    public function getOrderDataByUser(){
        $data = [];
        $totalAmount = 0;
        $totalWinAmount = 0;
        $user_id = request()->user_id;
        $rielToDollar = exchangeRateFromRielToDollar();
        $query = $this->table == Order::getModel()->getTable() ? Order::select()
                                    : OrderTemp::select();
        $userOrders = $query
                    ->when($user_id ,function($q) use ($user_id){
                        return $q->where(['user_id' => $user_id]);
                    })
                    ->when($user_id && request()->cycle_id,function($q) use ($user_id){
                        return $q->where(['user_id' => $user_id, 'cycle_id' => request()->cycle_id]);
                    })
                    ->when(request()->cycle_id,function($q) {
                        return $q->where('cycle_id',request()->cycle_id);
                    })
                    ->when($user_id && request()->begin_date && request()->end_date,function($q) use ($user_id){
                        return $q->where(['user_id' => $user_id])->whereBetween('created_at', [request()->begin_date, request()->end_date]);
                    })
                    ->when(request()->begin_date && request()->end_date,function($q){
                        return $q->whereBetween('created_at', [request()->begin_date, request()->end_date]);
                    })
                    ->with('cycle')
                    ->orderBy('created_at')
                    ->get(); 
                         
        $totalWinAmount = 0;
        $totalAmount = 0; 
        $users = User::whereIn('id',array_keys($userOrders->groupBy('user_id')->toArray()))->get()->keyBy('id');
        foreach($userOrders->groupBy('user_id') as $userId => $user){ 
            $t = []; 
            foreach($user->groupBy('ticket') as $no => $ticket){  
                $o = []; 
                
                foreach($ticket as $order){ 
                    $bets = $order->bet_content;

                    if(isset($bets)){
                        foreach($bets as $bet){
                            $o[] = [
                                'amountR' => $bet[2],
                                'amountD' => $bet[2] * $rielToDollar,
                                'winAmountR' => $bet[5],
                                'winAmountD' => $bet[5] * $rielToDollar,
                                'prize' => $bet[0],
                                'bet' => $bet[1],
                                'unit' => substr_replace($bet[3],"",-1).'00',
                                'type' => $bet[4]
                            ];
                        }
                        
                        $t[] = [
                            'cycle'     => $order->cycle ? $order->cycle->cycle_sn : '',
                            'ticket'    => $order->ticket,
                            'created_at'=> $order->created_at,
                            'ticketAmountR' => $order->amount,
                            'ticketAmountD' => $order->amount * $rielToDollar,
                            'ticketWinAmountR'=> $order->win_amount,
                            'ticketWinAmountD'=> $order->win_amount * $rielToDollar,
                            'orders'    => $o
                        ];
                    }
                    
                }
                
            } 
            
            if(isset($users[$userId])){ 
                $data[] = [ 
                    'username'  => $users[$userId]->username,
                    'account_number' => $users[$userId]->account_number,
                    'amountR' => $user->sum('amount'),
                    'amountD' => $user->sum('amount') * $rielToDollar,
                    'winAmountR' => $user->sum('win_amount'),
                    'winAmountD' => $user->sum('win_amount') * $rielToDollar,
                    'orders_no' => $user->count(),
                    'tickets'    => $t,
                ]; 
                $totalWinAmount += $user->sum('win_amount');
                $totalAmount += $user->sum('amount');
            }
        }   
        $total = [
            'totalWinAmountR' => $totalWinAmount,
            'totalWinAmountD' => $totalWinAmount * $rielToDollar,
            'totalAmountR' => $totalAmount,
            'totalAmountD' => $totalAmount * $rielToDollar
        ];
        
        return ['data' => $data, 'total' => $total];
    }

    public function ajaxGetOrderDetail($ticket){
        $success = false;
        $data = [];
        $orders = OrderTemp::with('user')->whereTicket($ticket)->get();
        if($orders && $orders->count() > 0){
            $data = [
                'ticket' => $orders[0]->ticket,
                'betAmount' => currencyFormat($orders->sum('amount')),
                'winAmount' => currencyFormat($orders->sum('win_amount')),
                'created_at'=> date('Y-m-d H:i A', strtotime($orders[0]->created_at)),
            ];

            // if($orders[0]->user->level != 0){
            //     for($i=1;$i<$orders[0]->user->level + 1;$i++){ 
            //         $data['rebates'][] = [
            //             'levelTitle' => getUserLevelTitle($i),
            //             'username' => $orders[0]->parent($i)->username,
            //             'rebate' => currencyFormat($orders->sum('l'.$i.'_rebate'))
            //         ];
            //     }
            // }else{
            //     $data['rebates'][] = [
            //         'levelTitle' => getAdminLanguageData()['LANG_MENU_DIRECT_USER'],
            //         'username' => $orders[0]->user->username,
            //         'rebate' => currencyFormat(0)
            //     ];
            // }
            $success = true;
        }
        return response()->json(['success' => $success, 'data' => $data]);
    }
}
