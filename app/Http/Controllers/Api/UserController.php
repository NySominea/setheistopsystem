<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    public function getProfile(){

        $user = auth()->user();

        $data = [
            'username' => $user->username,
            'account_number' => $user->account_number,
            'phone' => $user->phone,
            'email' => $user->email,
            'national_id_number' => $user->national_id_number,
            'image' => ''
            // 'main_balance' => $user->cashAccount->balance,
            // 'bonus_5d' => $user->bonus5dAccount->balance,
            // 'bonus_ball' => $user->bonusBallAccount->balance,
        ];

        return responseSuccess($data, 'success');
    }

    public function get_user_by_account_number(){

        $list = [];
        $user = User::whereAccountNumber(request()->account_number)->first();

        if(!$user){
            return responseError('User Account Not Found');
        }

        $parent = User::find($user->id);
        $child = $parent ? $parent->children()->get() : [];

        $data = [
            'userId' => $parent->id,
            'username' => $parent->username,
            'account_number' => $parent->account_number,
            'phone' => $parent->phone,
            'image' => ''
        ];
        
        foreach($child as $children){
            $list [] = [
                'userId' => $children->id,
                'username' => $children->username,
                'account_number' => $children->account_number,
                'phone' => $children->phone,
                'image' => ''
            ];
        }
       
        return responseSuccess([ 'parent' => $data , 'children' => $list ]);
    }
}
