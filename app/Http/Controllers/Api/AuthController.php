<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SecurityCode;
use App\User;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Model\UserAccount;
use DB;
use Exception;
class AuthController extends Controller
{
    private $client;

    public function __construct()
    {   
        $basic  = new \Nexmo\Client\Credentials\Basic('de2ed9d7', 'hyrnUffMXbOL4DKk');
        $this->client = new \Nexmo\Client($basic);
       
    }

    function changePassword(){
        $this->validate(request(),[
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_new_password' => 'required',
            'type' => 'required'
        ]);
        
        $user = auth()->user();
        $old_password = request()->get('old_password');
        $new_password = request()->get('new_password');
        $confirm_new_password = request()->get('confirm_new_password');
        $type = request()->get('type');

        if(strlen($new_password) < 6){
            return responseError(api_trans('passwords.password'));
        }

        if($new_password != $confirm_new_password){
            return responseError(api_trans('passwords.password_not_match'));
        }

        if($old_password == $new_password){
            return responseError(api_trans('passwords.new_password_same_as_old_password'));
        }

        if(!Hash::check($old_password,$user->password)){
            return responseError(api_trans('passwords.old_password_not_correct'));
        }

        $user->password = Hash::make($new_password);

        $user->save();
        
        return responseSuccess(api_trans('passwords.change_password_success'));
    }

    function login(){
        $this->validate(request(),[
            'phone_or_account_number' => 'required',
            'password' => 'required'
        ]);

        $user = User::where('phone',str_replace(' ','',request()->phone_or_account_number))
                    ->orWhere('account_number',request()->phone_or_account_number)
                    ->first();
        
        if(!$user){
            return responseError(api_trans('auth.not_found'));
        }
       
        if (!Hash::check(request()->password,$user->password)){

            return responseError(api_trans('auth.failed'));
        }

        if(!$user->state){
            return responseError('User is disabled');
        }

        if(!$user->has_verified)
        $verification = $this->client->verify()->start([ 
            'number' => '855'.$user->phone,
            'brand'  => 'Tongjin Investment',
            'code_length'  => '4',
            'pin_expiry' => 120
         ]);

        $access_token = $user->createToken('Token')->accessToken;

        return responseSuccess([
            'token' => $access_token,
            'username' => $user->username,
            'has_verified' => $user->has_verified,
            'account_number' => $user->account_number,
            'phone' => $user->phone,
            'request_id' => !$user->has_verified ? $verification->getRequestId() : 0,
            'image' => ''
        ]);
    }

    function logout(){
        auth()->token()->revoke();

        return responseSuccess('Logged Out');
    }

    public function register(Request $request){
        
        $validator = Validator::make($request->all(), [
                        'username' => 'required|unique:users,username',
                        'phone' => 'required|unique:users,phone|regex:/^([0-9\s\-\+\(\)]*)$/|min:9',
                        'password' => 'required|confirmed|min:6',
                        'password_confirmation' => 'required|min:6'
                    ]);
        if ($validator->fails()) {
            return responseError($validator->errors()->first());
        }

        DB::beginTransaction();
        try{
            $data = $request->all();
            $user = new User();
            $data['password'] = Hash::make($request->password);
            $data['payment_password'] = Hash::make($request->password);
            $data['account_number'] = User::generateAccountNumber();
            $data['phone'] = str_replace(' ','',$data['phone']);
            $data['parent_id'] = request()->get('parent_id',0);
            $data['has_verified'] = FALSE;
    
            $user->fill($data);
        if($user->save()){
            UserAccount::createCashAccount($user->id);
            UserAccount::createBonus5DAccount($user->id);
            UserAccount::createBonusBallAccount($user->id);

            $access_token = $user->createToken('Token')->accessToken;
            $number = '855'.$data['phone'];
    
            $verification = $this->client->verify()->start([ 
                'number' => $number,
                'brand'  => 'Tongjin Investment',
                'code_length'  => '4',
                'pin_expiry' => 120
             ]);
          }
            DB::commit();
            return responseSuccess([
                'token' => $access_token,
                'username' => $user->username,
                'has_verified' => $user->has_verified,
                'account_number' => $user->account_number,
                'phone' => $user->phone,
                'request_id' => $verification->getRequestId()
            ]); 

        }catch(Exception $ex){
            DB::rollback();
            return responseError($ex->getMessage());
        } 
    }

    public function verify(){ // parameter: verify_code , request_id
       
        DB::beginTransaction();
        try{
            $request_id = request()->get('request_id');
            $verification = new \Nexmo\Verify\Verification($request_id);
            $result = $this->client->verify()->check($verification, request()->get('verify_code'));
            $userId = auth()->user()->id;
           
            if(isset($result) && $result->getResponseData()['status'] == 0){
                $user = User::find($userId);
                if($user){
                    $user->has_verified = 1;
                    $user->save(); 
                }
            }
                
                $access_token = $user->createToken('Token')->accessToken;
                DB::commit();
                return responseSuccess([
                    'token' => $access_token,
                    'username' => $user->username,
                    'has_verified' => $user->has_verified,
                    'account_number' => $user->account_number,
                    'phone' => $user->phone,
                    'image' => ''
                ]);

            }catch(Exception $ex){
                DB::rollback();
                return responseError($ex->getMessage());
        }
    }

    public function resendVerifyCode(){ // parameter : request_id , phone

        DB::beginTransaction();
        try {
            $result = $this->client->verify()->cancel(request()->get('request_id'));
            $number = '855'.auth()->user()->phone;

            if($result->getResponseData()['status'] == 0){
                $verification = $this->client->verify()->start([ 
                    'number' => $number,
                    'brand'  => 'Tongjin Investment',
                    'code_length'  => '4',
                    'pin_expiry' => 120
                    ]);
            }
            DB::commit();
            return responseSuccess($verification->getRequestId());

        }catch(Exception $ex){
            DB::rollback();
            
            return responseError($ex->getMessage());
        }
    }

    public function getUserData(){
        $user = User::whereAccountNumber(request()->account_number)->first();
        if(!$user){
            return responseError(api_trans('auth.not_found'));
        }

        return responseSuccess([
            'username' => $user->username,
            'account_number' => $user->account_number,
            'userId' => $user->id
        ]);
    }
}
