<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\BalanceRequestResource;
use DB;
use Validator;

use App\Model\BalanceRequest;
use App\User;

class BalanceRequestController extends Controller
{
    public function requestBalanceRecharge(){
        $validator = Validator::make(request()->all(), [
            'amount' => 'required|numeric',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return $validator->errors()->toJson();
        }
        
        if(!User::checkPayPassword(auth()->id(),request()->password))
            return responseError(api_trans('auth.wrong_password'));

        DB::beginTransaction();
        try {
            $br = BalanceRequest::create([
                'user_id' => auth()->id(),
                'operation' => 'recharge',
                'amount' => request()->amount,
                'note' => request()->note
            ]);
            
            if($br){
                if(request()->file){
                    $br->addMediaFromRequest('file')
                            ->toMediaCollection('images');
                }  
            }
            
            DB::commit();
            return responseSuccess('','Successful');
        } catch (Exception $ex) {
            DB::rollback();
            return responseError(api_trans('account.failed_operation'));
        }
    }

    public function requestBalanceWithdraw(){

        $validator = Validator::make(request()->all(), [
            'amount' => 'required|numeric',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return $validator->errors()->toJson();
        }

        if(!User::checkPayPassword(auth()->id(),request()->password))
            return responseError(api_trans('auth.wrong_password'));

        DB::beginTransaction();
        try {
            BalanceRequest::create([
                'user_id' => auth()->id(),
                'operation' => 'withdraw',
                'amount' => request()->amount,
                'note' => request()->note
            ]);
            DB::commit();
            return responseSuccess('','Successful');
        } catch (Exception $ex) {
            DB::rollback();
            return responseError(api_trans('account.failed_operation'));
        }
    }

    public function getRequestBalanceRecharge(){
        $requests = BalanceRequest::whereUserId(auth()->id())
                                    ->whereOperation('recharge')
                                    ->orderBy('created_at','DESC')
                                    ->paginate(15);
                                    
        return BalanceRequestResource::collection($requests);
    }

    public function getRequestBalanceWithdraw(){
        $requests = BalanceRequest::whereUserId(auth()->id())
                                    ->whereOperation('withdraw')
                                    ->orderBy('created_at','DESC')
                                    ->paginate(15);

        return BalanceRequestResource::collection($requests);
    }
}
