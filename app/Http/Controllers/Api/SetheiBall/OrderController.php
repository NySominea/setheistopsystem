<?php

namespace App\Http\Controllers\Api\SetheiBall;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SetheiBall\Cycle;
use App\Model\SetheiBall\Order;
use App\Model\SetheiBall\OrderTemp;
use App\Model\SetheiBall\GameType;
use App\Model\SetheiBall\GameRebate;
use App\Model\SetheiBall\OrderRebate;
use App\Model\SetheiBall\OrderRebateTemp;
use Illuminate\Pagination\LengthAwarePaginator;
use App\User;
use DB;
use App\Model\UserAccount;
use Batch;

class OrderController extends Controller
{
    private $orders;
    private $orderIds;
    private $user;
    private $cycle;
    private $ticket;
    private $rebate;
    private $totalAmount;

    public function createOrder(){
        // dd('do');
        ini_set('max_execution_time', 300);

        // Check order content
        $this->orders = request()->get('data',[]);
        if(count($this->orders) <= 0) 
            return responseError(api_trans('cycle.no_order'));


        // Check game type config
        $this->game = GameType::whereCode('L101')->first();
        if(!$this->game) 
            return responseError(api_trans('cycle.no_config'));

        // Check valid bet number
        if(!$this->checkValidBetNumber()) 
            return responseError(api_trans('cycle.out_of_range_bet'));
            
        // Check authentication
        $this->user = auth()->user()->load('cashAccount','bonusBallAccount');
        if(!$this->user || !$this->user->state) 
            return responseError(api_trans('auth.permission_denied'));
            
        // Check active cycle
        $this->cycle = Cycle::whereStateAndHasReleased(1,0)->first();
        if(!$this->cycle) 
            return responseError(api_trans('cycle.no_cycle'));
            
        // Check cycle stop time
        if($this->cycle->stopped_time < strtotime("NOW")) 
            return responseError(api_trans('cycle.cycle_stop'));
           
        // Check if balance is sufficient
        if(!$this->checkSufficientBalance())
            return responseError(api_trans('account.insufficient_balance'));
        
        $this->rebate = GameRebate::whereGameTypeId($this->game->id)->first();
        
        $this->ticket = $this->generateTicketNumber();
        $orderTemps = [];
        $now = date('Y-m-d H:i:s');
        
        DB::beginTransaction();
        try{
            foreach($this->orders as $order){
                $orderTemps[] = [
                    'ticket' => $this->ticket,
                    'cycle_id' => $this->cycle->id,
                    'user_id' => $this->user->id,
                    'amount' => $this->game->order_price,
                    'parent_id' => $this->user->parent_id,
                    'game_type_id' => $this->game->id,
                    'no1' => addPrefixStringPad($order[0], 2, '0'),
                    'no2' => addPrefixStringPad($order[1], 2, '0'),
                    'no3' => addPrefixStringPad($order[2], 2, '0'),
                    'no4' => addPrefixStringPad($order[3], 2, '0'),
                    'no5' => addPrefixStringPad($order[4], 2, '0'),
                    'no6' => addPrefixStringPad($order[5], 2, '0'),
                    'is_test' => $this->user->is_test,
                    'created_at' => $now
                ];
                $this->totalAmount+=$this->game->order_price;
            }
            
            // Save orders & proceed balance and Rebate
            if(OrderTemp::insert($orderTemps)){
                $this->proceedPayment();
                $this->proceedRebate();
            }
            
            DB::commit();

            // $pusher = new \Pusher\Pusher("e39a4e096510340c985b", "b2ec5375407bbe9e618e", "797571", array('cluster' => 'ap1'));
            // $pusher->trigger('sethei-ball-cycle', 'jackpot-increase', array('amount' => $cycle->prize * exchangeRateFromRielToDollar()));
            // $pusher->trigger('sethei-ball-cycle', 'lucky-draw-increase', array('amount' => $cycle->lucky_draw * exchangeRateFromRielToDollar()));


            return responseSuccess(['username' => $this->user->username, 'account_no' => $this->user->account_number,
                                    'date' => date('Y-m-d H:i A',strtotime($now)), 
                                    'amount' => currencyFormat($this->totalAmount,0)." R",
                                    'ticket' => $this->ticket, 'result_time' => date('Y-m-d H:i A',$this->cycle->result_time), 
                                    'balance' => currencyFormat($this->user->cashAccount->balance)." R",
                                    'bonus' => currencyFormat($this->user->bonusBallAccount->balance)." R",
                                    'orders' => $this->orders
                                    ]);
        }catch(Exception $exception){
            DB::rollback();
            return responseError(api_trans('cycle.failed_to_order'));
        }
        
    }

    private function proceedPayment(){

        if($this->user->bonusBallAccount && $this->user->bonusBallAccount->balance > 0){
            if($this->user->bonusBallAccount->balance > $this->totalAmount){
                $this->user->bonusBallAccount->decrement('balance',$this->totalAmount);
            }else{
                $remainAmount = $this->totalAmount - $this->user->bonusBallAccount->balance;
                $this->user->bonusBallAccount->update(['balance' => 0]);
                $this->user->cashAccount->decrement('balance',$remainAmount);
            }
        }else{
            $this->user->cashAccount->decrement('balance',$this->totalAmount);
        }

        $this->cycle->increment('prize',$this->totalAmount * $this->game->addon_percentage / 100);
        $this->cycle->increment('lucky_draw',$this->totalAmount * $this->game->lucky_draw / 100);
    }

    private function proceedRebate(){
        
        if(!$this->user->is_test){
            $order_rebate_column = ['user_id','cycle_id','ticket','rebate_amount','created_at','updated_at'];
            $order_rebate_data = [];
            $users = $this->user->ancestorsAndSelf5Level();

            foreach($users as $i => $u){
                $key_column = $i == 0 ? 'current_own_commission_ball' : 'current_line_commission_ball';
                $rebateAmount = $this->totalAmount * $this->rebate->{'l'.(5-$i).'_rebate'} / 100;
                $u->increment($key_column,$rebateAmount);

                $order_rebate_data[] = [$u->id, $this->cycle->id, $this->ticket, $rebateAmount, date('Y-m-d H:i:s'), date('Y-m-d H:i:s')];
            }

            Batch::insert(new OrderRebateTemp, $order_rebate_column, $order_rebate_data);
        }
    }

    private function checkSufficientBalance(){

        if(!$this->user->cashAccount){ 
            $this->user->cashAccount = UserAccount::createCashAccount($this->user->id);
        }

        if(!$this->user->bonusBallAccount){ 
            $this->user->bonusBallAccount = UserAccount::createBonusBallAccount($this->user->id);
        }
        
        $totalAmount = count($this->orders) * $this->game->order_price;

        if($this->user->bonusBallAccount){
            if($this->user->bonusBallAccount->balance < $totalAmount){
                if($this->user->bonusBallAccount->balance + $this->user->cashAccount->balance < $totalAmount){
                    return false;
                }
            }
        }
        else{
            if($this->user->cashAccount < $totalAmount){
                return false;
            }
        }
        return true;
    }

    private function checkValidBetNumber(){
        foreach($this->orders as $order){ 
            for($i=0; $i<6; $i++){ 
                if(!in_array($order[$i], range($this->game->min, $this->game->max))){
                    return  false;
                }
            }
            
            if(count($order) != count(array_unique($order))){
                return false;
            }
        }

        return true;
    }

    private function generateTicketNumber(){
        $lastTicket = OrderTemp::whereUserId($this->user->id)->orderBy('ticket','desc')->first();
        
        $lastTicketNumber = 0;
        if($lastTicket){
            $lastTicketNumber = (int) substr($lastTicket->ticket, -5);
        }
        
        $numberOfOrders = addPrefixStringPad($lastTicketNumber + 1,5  ,'0');
        $userId = addPrefixStringPad($this->user->id,4,'0');
        return $this->cycle->cycle_sn.$userId.$numberOfOrders; 
    }
}
