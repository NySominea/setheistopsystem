<?php

namespace App\Http\Controllers\Api\SetheiBall;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SetheiBall\Cycle;
use App\Model\SetheiBall\Order;
use App\Model\SetheiBall\OrderTemp;
use App\Model\SetheiBall\GameType;
use DB;

class CycleController extends Controller
{
    public function getOwnCycles(){
        $user_id = auth()->id();
        $rateRielToDollar = exchangeRateFromRielToDollar();
        $currentCycle = [];

        $limit = 15;
        $page = request()->get('page',1);
        $offset = $limit * ($page-1);
        
        $cycles = Order::select('cycle_id')->whereUserId($user_id)->skip($offset)->take($limit)->distinct('cycle_id')->pluck('cycle_id');
        $cycles = Cycle::whereIn('id',$cycles->toArray())->orderBy('id','desc')->paginate($limit)->toArray();
        
        if(!$cycles){
            return  responseError(api_trans('cycle.no_cycle'));
        }
        
        $cycles['cycles'] = $cycles['data'];
        unset($cycles['data']);
        
        foreach($cycles['cycles'] as $i => $cycle){
            $cycles['cycles'][$i]['prize'] = currencyFormat($cycle['prize'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['prize_5d'] = currencyFormat($cycle['prize_5d'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['prize_4d'] = currencyFormat($cycle['prize_4d'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['prize_3d'] = currencyFormat($cycle['prize_3d'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['result_number'] = $cycle['result_number'] ? json_encode($cycle['result_number']) : null;
            $cycles['cycles'][$i]['result_time'] = date('Y-m-d H:i A',$cycle['result_time']);
            $cycles['cycles'][$i]['stopped_time'] = date('Y-m-d H:i A',$cycle['stopped_time']);
        }
        
        if($page == 1){
            $currentCycle = OrderTemp::whereUserId($user_id)->distinct('cycle_id')->pluck('cycle_id');
            $currentCycle = Cycle::whereIn('id',$currentCycle->toArray())->first();
            
            if(isset($currentCycle)){
                $currentCycle = $currentCycle->toArray();
                $currentCycle['prize'] = currencyFormat($currentCycle['prize'] * $rateRielToDollar)." $";
                $currentCycle['prize_5d'] = currencyFormat($currentCycle['prize_5d'] * $rateRielToDollar)." $";
                $currentCycle['prize_4d'] = currencyFormat($currentCycle['prize_4d'] * $rateRielToDollar)." $";
                $currentCycle['prize_3d'] = currencyFormat($currentCycle['prize_3d'] * $rateRielToDollar)." $";
                $currentCycle['result_number'] = $currentCycle['result_number'] ? json_encode($currentCycle['result_number']) : null;
                $currentCycle['result_time'] = date('Y-m-d H:i A',$currentCycle['result_time']);
                $currentCycle['stopped_time'] = date('Y-m-d H:i A',$currentCycle['stopped_time']);

                $cycles['cycles'] = array_merge([$currentCycle],$cycles['cycles']);
            }
            
        }
        
        
        return responseSuccess($cycles, 'OK');
    }

    public function getCycles(){
        $currentCycle = [];

        $cycles = Cycle::orderBy('id','DESC')
                        ->paginate(15)
                        ->toArray();
        $rateRielToDollar = exchangeRateFromRielToDollar();
        $cycles['cycles'] = $cycles['data'];
        unset($cycles['data']);
        if(!$cycles){
            return  $this->sendError(api_trans('cycle.no_cycle'));
        }

        foreach($cycles['cycles'] as $i => $cycle){
            $cycles['cycles'][$i]['prize'] = currencyFormat($cycle['prize'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['prize_5d'] = currencyFormat($cycle['prize_5d'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['prize_4d'] = currencyFormat($cycle['prize_4d'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['prize_3d'] = currencyFormat($cycle['prize_3d'] * $rateRielToDollar)." $";
            $cycles['cycles'][$i]['result_number'] = $cycle['result_number'] ? json_encode($cycle['result_number']) : null;
            $cycles['cycles'][$i]['result_time'] = date('Y-m-d H:i A',$cycle['result_time']);
            $cycles['cycles'][$i]['stopped_time'] = date('Y-m-d H:i A',$cycle['stopped_time']);
        }
        
        return responseSuccess($cycles, 'OK');
    }

    public function getTicketsByCycle(){
        $cycle = Cycle::find(request()->cycle_id);
        $user_id = auth()->id();

        if(!$cycle) 
            return  responseError(api_trans('cycle.no_cycle'));

        if($cycle->has_released && !$cycle->state){ 
            $tickets = Order::selectRaw('count(*) as qty, sum(amount) as amount, sum(win_amount) as win_money, ticket, created_at')
                                ->whereCycleIdAndUserId($cycle->id,$user_id)
                                ->groupBy('ticket','created_at')->orderBy('created_at','DESC')
                                ->paginate(15)
                                ->toArray();
        }else{
            $tickets = OrderTemp::selectRaw('count(*) as qty, sum(amount) as amount, sum(win_amount) as win_money, ticket, created_at')
                                    ->whereCycleIdAndUserId($cycle->id,$user_id)
                                ->groupBy('ticket','created_at')->orderBy('created_at','DESC')
                                ->paginate(15)
                                ->toArray();
        }

        if(!$tickets) 
            return  responseError(api_trans('cycle.no_ticket'));
        
        $tickets['tickets'] = $tickets['data'];
        unset($tickets['data']);

        foreach($tickets['tickets'] as $i => $ticket){
            $tickets['tickets'][$i]['amount'] = currencyFormat( $ticket['amount'],0);
            $tickets['tickets'][$i]['win_money'] = $ticket['win_money'];
            $tickets['tickets'][$i]['created_at'] = date('Y-m-d H:i A',strtotime($ticket['created_at']));
        }

        return responseSuccess($tickets, 'OK');
    }

    public function getOrdersByTicket(){
        $ticket = request()->get('ticket');
        $is_settle = request()->get('is_settle');
        
        if(!$ticket && !$is_settle){
            return  responseError(api_trans('cycle.no_ticket'));
        }

        $orders = $is_settle 
                        ? Order::where(['ticket' => $ticket])->with('user', 'cycle')->get() 
                        : OrderTemp::where(['ticket' => $ticket])->with('user', 'cycle')->get();
        
        if($orders->count() <= 0){
            return  responseError(api_trans('cycle.no_order'));
        }

        $ordersFormat = [];
        foreach($orders as $order){
            $ordersFormat[] = [
                'numbers' => json_encode([$order->no1,$order->no2,$order->no3,$order->no4,$order->no5,$order->no6]),
                'win_amount' => ((string) $order->win_amount),
                'win_number' => json_encode($order->win_number)
            ];
        }
        
        return responseSuccess(['ticket' => $ticket, 'total_amount' => currencyFormat($orders->sum('amount'),0)." R",
                                    'date' => date('Y-m-d H:i A',strtotime($orders[0]->created_at)),
                                    'result_time' => date('Y-m-d H:i A',$orders[0]->cycle->result_time),
                                    'username' =>   $orders[0]->user->username, 'account_number' => $orders[0]->user->account_number,
                                    'total_win_amount' => currencyFormat($orders->sum('win_amount'))." R", 'orders' => $ordersFormat,
                                    ], 'succesfully.', 200);
    }

    public function getActiveCycle(){
        
        $cycle = Cycle::orderBy('id','DESC')->first();
        $remain = 0;
        if($cycle && $cycle->stopped_time > strtotime('NOW')){
            $now = strtotime('NOW');
            $endtime = $cycle->stopped_time;
            $remain = $endtime - $now;
        }
        
        $balance = auth()->user()->cashAccount->balance;
        $data = [
            'remain' => $remain,
            'balance' => currencyFormat($balance)." R"
        ];
        
        return responseSuccess(json_encode($data), 'OK');
    }

    public function getCycleById(){
        
        $cycle = Cycle::find(request()->cycle_id);
        $rateRielToDollar = exchangeRateFromRielToDollar();

        if(!$cycle){
            return  responseError(api_trans('cycle.no_cycle'));
        }

        $cycle = [
            'prize' => currencyFormat($cycle->prize * $rateRielToDollar)." $",
            'prize_5d' => currencyFormat($cycle->prize_5d * $rateRielToDollar)." $",
            'prize_4d' => currencyFormat($cycle->prize_4d * $rateRielToDollar)." $",
            'prize_3d' => currencyFormat($cycle->prize_3d * $rateRielToDollar)." $",
            'result_number' => $cycle->result_number ? json_encode($cycle->result_number) : null,
            'stopped_time' => date('Y-m-d H:i A',$cycle->stopped_time),
            'result_time' => date('Y-m-d H:i A',$cycle->result_time),
        ];
        
        return responseSuccess($cycle, 'OK');
    }
}
