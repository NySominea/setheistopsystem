<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UserAccount;
use App\Model\UserAccountLog;
use App\User;
use Carbon\Carbon;
use App\Constants\Account;
use DB;

class BalanceController extends Controller
{
    function get_username_by_account_number(){
        $this->validate(request(),[
            'account_number' => 'required'
        ]);

        $user = User::whereAccountNumber(request()->get('account_number'))->whereState(1)->first();

        if($user){
            return responseSuccess(
                $user->username
            );
        }else{
            return responseError(api_trans('auth.not_found'));
        } 
    }

    function transfer_to_other_account(){

        $this->validate(request(),[
            'account_number' => 'required',
            'amount' => 'required',
            'pay_password' => 'required'
        ]);

        $account_number = request()->get('account_number');
        $amount = request()->get('amount');
        $pay_password = request()->get('pay_password');
        $user = auth()->user();
        
        $accounts = UserAccount::whereUserId($user->id)->get();

        if($user->account_number == $account_number) return responseError('Can\'t be yourself');

        $main_account = $user->main_balance;

        if($main_account->balance < $amount) return responseError(api_trans('account.insufficient_balance'));

        $to_user = User::whereAccountNumber($account_number)->first();

        if($user->is_shop || $to_user->is_shop){
            return responseError(api_trans('auth.permission_denied'));
        }

        if(!$to_user) return responseError(api_trans('auth.not_found'));

        if(!$to_user->state) return responseError('This user is disabled');

        if($user->pay_pass != md5($pay_password.$user->pass_salt)){
            return responseError(api_trans('account.wrong_pay_password'));
        }

        $to_user_main_account = $to_user->main_balance;

        $to_user_main_account->balance += $amount;
        $main_account->balance -= $amount;

        $user_logs = [];
        $now = Carbon::now();
        $user_logs[] = [
            'user_id' => $user->id,
            'account_id' => $main_account->id,
            'log_type' => 2,
            'is_transfer' => 1,
            'amount' => $amount,
            'balance' => $main_account->balance,
            'to_type' => 2,
            'to_user_id' => $to_user->id,
            'to_account_id' => $to_user_main_account->id,
            'manager_id' => 0,
            'abstract' => 'LANG_LABEL_TRANS',
            'log_number' => UserAccountLog::generateLogNumber(2,$user->id),
            'created_at' => $now
        ];

        $user_logs[] = [
            'user_id' => $to_user->id,
            'account_id' => $to_user_main_account->id,
            'log_type' => 1,
            'is_transfer' => 1,
            'amount' => $amount,
            'balance' => $to_user_main_account->balance,
            'to_type' => 2,
            'to_user_id' => $user->id,
            'to_account_id' => $main_account->id,
            'manager_id' => 0,
            'abstract' => 'LANG_LABEL_TRANS',
            'log_number' => UserAccountLog::generateLogNumber(1,$to_user->id),
            'created_at' => $now
        ];
            
        $to_user_main_account->save();
        $main_account->save();

        UserAccountLog::insert($user_logs);

        pushNotificationToUser('Sethei 5D',api_trans('notification.send_to_another_user',[
            'amount' => $amount,
            'user' => $user->username,
            'time' => Carbon::now()->format('g:i A')
        ]),[(string) $to_user->id]);

        return responseSuccess([
            'main_balance' => $main_account->balance,
            'commission' => $user->commission_5d,
            'win_money' => $user->win_money_5d,
            'profit' => $accounts->where('type_id',4)->first() ? $accounts->where('type_id',4)->first()->balance : 0,
            'bonus' => $accounts->where('type_id',5)->first() ? $accounts->where('type_id',5)->first()->balance : 0
        ]);;
    }

    function account_to_main(){
    
        $this->validate(request(),[
           'type' => 'required',
           'amount' => 'required' 
        ]);

        $type = request()->get('type');
        $amount = request()->get('amount');
        $user = auth()->user();
        $accounts = UserAccount::whereUserId($user->id)->get();

        if($user->is_shop){
            return responseError(api_trans('auth.permission_denied'));
        }

        if($amount > 0){
            $main_account = $user->main_balance;
            $log = [
                'user_id' => $user->id,
                'account_id' => $main_account->id,
                'log_type' => 1,
                'is_transfer' => 0,
                'amount' => $amount,
                'win_money' => 0,
                'commission' => $user->commission_5d,
                'to_type' => 1,
                'to_user_id' => $user->id,
                'to_account_id' => $main_account->id,
                'manager_id' => 0,
                'log_number' => null,
                'created_at' => Carbon::now()
            ];
            switch($type){
                case 'win':
                    if($user->win_money_5d - $amount >= 0){
                        $log['abstract'] = 'LANG_WIN_MONEY_TO_BALANCE';
                        $user->win_money_5d -= $amount;
                        $main_account->balance += $amount;
                        $user->save();
                        $main_account->save();
                    }else{
                        return responseError(api_trans('account.insufficient_balance'));
                    }
                    break;
                case 'commission':
                    if($user->commission_5d - $amount >= 0 || $user->commission_5d + $user->current_commission_5d - $amount >= 0){
                        $log['abstract'] = 'LANG_COMMISSION_TO_BALANCE';
                        $user->commission_5d -= $amount;

                        if($user->commission_5d < 0){
                            $user->current_commission_5d += $user->commission_5d;
                            $user->commission_5d = 0;
                        }
                        $main_account->balance += $amount;
                        $user->save();
                        $main_account->save();
                    }else{
                        return responseError(api_trans('account.insufficient_balance'));
                    }
                    break;
                case 'profit': 
                    $account = $user->profit_account;
                    if(!$account || ($account && $account->balance < $amount)) return responseError(api_trans('account.insufficient_balance'));
                    $log['abstract'] = 'LANG_PROFIT_TO_BALANCE';
                    $account->balance -= $amount;
                    $main_account->balance += $amount;
                    $account->save();
                    $main_account->save();
            }

            $log['balance'] = $main_account->balance;

            UserAccountLog::create($log);

            return responseSuccess([
                'main_balance' => $main_account->balance,
                'commission' => $user->commission_5d + $user->current_commission_5d,
                'win_money' => $user->win_money_5d,
                'profit' => $accounts->where('type_id',4)->first() ? $accounts->where('type_id',4)->first()->balance : 0,
                'bonus' => $accounts->where('type_id',5)->first() ? $accounts->where('type_id',5)->first()->balance : 0
            ]);
        }

        return responseError(api_trans('account.insufficient_balance'));
    }
    
    function get_balance(){
        
        $user_id = auth()->id();
        $user = auth()->user();
        
        return responseSuccess([
            'main_balance' => $user->cashAccount->balance,
            'bonus_5d' => $user->bonus5dAccount->balance,
            'bonus_ball' => $user->bonusBallAccount->balance,
            'format_main_balance' => number_format($user->cashAccount->balance).(' R'),
            'format_bonus_5d' => number_format($user->bonus5dAccount->balance).(' R'),
            'format_bonus_ball' => number_format($user->bonusBallAccount->balance).(' R'),
            'current_own_commission_5d' => $user->current_own_commission_5d,
            'current_line_commission_5d' => $user->current_line_commission_5d,
            'current_own_commission_ball' => $user->current_own_commission_ball,
            'current_line_commission_ball' => $user->current_line_commission_ball,
            'format_current_own_commission_5d' => number_format($user->current_own_commission_5d).(' R'),
            'format_current_line_commission_5d' => number_format($user->current_line_commissio_5d).(' R'),
            'format_current_own_commission_ball' => number_format($user->current_own_commission_ball).(' R'),
            'format_current_line_commission_ball' => number_format($user->current_line_commission_ball).(' R'), 
        ], 'success');
    }

}
