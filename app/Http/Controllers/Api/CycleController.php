<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Sethei5D\Cycle;
use App\Model\Sethei5D\Order;
use App\Model\Sethei5D\OrderTemp;
use App\User;
use DB;
use Str;

class CycleController extends Controller
{
    function get_cycle_info(){
        $user = auth()->user();
        $main_account = $user->main_balance;
        $cycle = Cycle::whereHasReleased(0)->orderBy('id','desc')->first();
        $data = [
            'balance' => number_format($main_account->balance).' (R)',
            'remain' => $cycle->stopped_time - time()
        ];
        return responseSuccess(json_encode($data));
    }
    
    function get_cycles(){

        $cycles = Cycle::orderBy('id','desc');

        if(!request()->get('all')){
            $cycles->whereHasReleased(1);
        }

        $cycles = $cycles->paginate(15);

        $cycles = $cycles->keyBy(function ($value, $key) {
            if ($key == 'data') {
                return 'cycles';
            } else {
                return $key;
            }
        })->values()->all();
        
        return responseSuccess($cycles);
    }

    function ticket_info(){
        $cycle_id = request()->get('cycle_id');
        $is_normal_mode = request()->get('is_normal_mode',1) == 1? 'win_amount >= 0' : 'win_amount > 0';
        $user_id = auth()->id();
        $order_table = Order::getModel()->getTable();
        $user_table = User::getModel()->getTable();
        $cycle_table = 'sethei_mysql.'.Cycle::getModel()->getTable();
        $page = request()->get('page',1);
        $limit = 15;
        $offset = ($page - 1) * $limit;

        $cycle = Cycle::find($cycle_id);
        if(!$cycle->has_released){
            $order_table = OrderTemp::getModel()->getTable();
        }
        
        $orders = DB::select(
            DB::raw("SELECT 
            ticket,
            DATE_FORMAT($order_table.created_at, '%Y-%m-%d %h:%i %p') as created_at,
            amount,
            $cycle->result_time as result_time,
            win_amount,
            bet,
            bet_content,
            username,
            account_number,
            is_settle
            FROM $order_table,$user_table WHERE
            $order_table.user_id = $user_table.id 
            AND user_id = $user_id
            AND $order_table.cycle_id = $cycle->id
            AND $is_normal_mode
            ORDER BY $order_table.created_at desc
            LIMIT $limit OFFSET $offset"));

        foreach($orders as $order){
            $order->cycle = $cycle;
        }

        if(!$cycle->has_released){
            $total_amount = OrderTemp::whereCycleId($cycle->id)->whereUserId($user_id)->sum('amount');
            $total_win_amount = null;
        }else{
            $total_amount = Order::whereCycleId($cycle->id)->whereUserId($user_id)->sum('amount');
            $total_win_amount = Order::whereCycleId($cycle->id)->whereUserId($user_id)->sum('win_amount');;
        } 

        
        return responseSuccess($orders,number_format($total_amount).' (R)|'.number_format($total_win_amount).' (R)');
    }

    function record_cycles(){
        $limit = 15;
        $page = request()->get('page',1);
        $order_table = Order::getModel()->getTable();
        $order_temp_table = OrderTemp::getModel()->getTable();
        $cycle_table = Cycle::getModel()->getTable(); 
        $current_cycle = [];

        $offset = $limit * ($page-1);
        $user_id = auth()->id();
        
        $cycles = Order::whereUserId($user_id)->orderBy('id','desc')->skip($offset)->take($limit)->distinct('id')->pluck('cycle_id');
        $cycles = Cycle::select('id','result_time')->whereIn('id',$cycles->toArray())->orderBy('id','desc')->get();


        if($page == 1){
            $current_cycle = OrderTemp::whereUserId($user_id)->pluck('cycle_id');
	        $current_cycle = Cycle::select('id','result_time')->whereIn('id',$current_cycle->toArray())->get();

            $current_cycle = $current_cycle ? $current_cycle->toArray() : [];
        }

        $cycles = array_merge($current_cycle,$cycles ? $cycles->toArray() : []);

        return responseSuccess($cycles);
    }

}
