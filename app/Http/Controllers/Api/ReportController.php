<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Order;
use App\Model\OrderTemp;
use DB;

class ReportController extends Controller
{
    function get_report_range(){
        $page = request()->get('page',1);
        $limit = 15;
        $offset = ($page - 1) * $limit;
    }
    
    function get_report(){
        $page = request()->get('page',1);
        $limit = 15;
        $offset = ($page - 1) * $limit;
        $user = auth()->user();
        $user_level = 'l'.$user->level.'_rebate';
        $user_id = $user->id;
        $data_order_temp = [];
        $type = request()->get('type','day');

        $order_table = Order::getModel()->getTable();
        $order_temp_table = OrderTemp::getModel()->getTable();

        if($type == 'month'){
            return $this->get_report_by_month($user_level,$limit,$offset);
        }
        
        $sql = $this->get_sql($user_level,$order_table,$type,$limit,$offset);
        $data_order = collect(
            DB::select(
                DB::raw($sql)
            )
        );

        if($page == 1){
            $sql = $this->get_sql($user_level,$order_temp_table,$type);
            $data_order_temp = collect(
                DB::select(
                    DB::raw($sql)
                )
            );
        }

        $final_data = [];
        $data_order_temp = collect($data_order_temp);
        foreach($data_order as $temp_data){
            $data = $data_order_temp->where('date',$temp_data->date)->first();
            $final_data[] = [
                'date' => $temp_data->date,
                'total_order' => $temp_data->total_order + (isset($data) ? $data->total_order : 0),
                'total_amount' => $temp_data->total_amount + (isset($data) ? $data->total_amount : 0),
                'total_win_amount' => $temp_data->total_win_amount + (isset($data) ? $data->total_win_amount : 0),
                'total_rebate' => $temp_data->total_rebate + (isset($data) ? $data->total_rebate : 0)
            ];

            if(isset($data)){
                $data_order_temp = $data_order_temp->filter(function($item) use ($temp_data) {
                        return $item->date != $temp_data->date;
                });
            }
        }

        $final_data = $data_order_temp->merge(collect($final_data));

        return responseSuccess($final_data);
    }

    private function get_report_by_month($user_level,$limit,$offset){
        $user_id = auth()->id();
        $data_order_temp = [];
        $data_order = [];

        $sql = "SELECT 
        MONTH(created_at) as month,
        YEAR(created_at) as year,
        COUNT(*) as total_order,
        SUM(amount) as total_amount,
        SUM(win_amount) as total_win_amount,
        SUM($user_level) as total_rebate
        FROM d_orders
        WHERE user_id = $user_id
        OR l".auth()->user()->level."_id = $user_id
        GROUP BY month,year
        ORDER BY year DESC,month DESC
        LIMIT $limit OFFSET $offset";
        $data_order = collect(
            DB::select(
                DB::raw($sql)
            )
        );

        if(request()->get('page',1) == 1){
            $sql = "SELECT 
            MONTH(created_at) as month,
            YEAR(created_at) as year,
            COUNT(*) as total_order,
            SUM(amount) as total_amount,
            SUM(win_amount) as total_win_amount,
            SUM($user_level) as total_rebate
            FROM d_order_temps
            WHERE user_id = $user_id
            OR l".auth()->user()->level."_id = $user_id
            GROUP BY month,year
            ORDER BY year DESC,month DESC";

            $data_order_temp = collect(
                DB::select(
                    DB::raw($sql)
                )
            );
        }

        $final_data = [];
        $data_order_temp = collect($data_order_temp);
        foreach($data_order as $temp_data){
            $data = $data_order_temp->where('month',$temp_data->month)->where('year',$temp_data->year)->first();
            $final_data[] = [
                'date' => str_pad($temp_data->month,2,'0',STR_PAD_LEFT).' ,'.$temp_data->year,
                'total_order' => $temp_data->total_order + (isset($data) ? $data->total_order : 0),
                'total_amount' => $temp_data->total_amount + (isset($data) ? $data->total_amount : 0),
                'total_win_amount' => $temp_data->total_win_amount + (isset($data) ? $data->total_win_amount : 0),
                'total_rebate' => $temp_data->total_rebate + (isset($data) ? $data->total_rebate : 0)
            ];

            if(isset($data)){
                $data_order_temp = $data_order_temp->filter(function($item) use ($temp_data) {
                        return $item->month != $temp_data->month && $item->year != $temp_data->year;
                });
            }
        }

        if(!$final_data && $data_order_temp->first() && request()->get('page',1) == 1){
            $final_data[] = [
                'date' => str_pad($data_order_temp->first()->month,2,'0',STR_PAD_LEFT).' ,'.$data_order_temp->first()->year,
                'total_order' => $data_order_temp->first()->total_order,
                'total_amount' => $data_order_temp->first()->total_amount ,
                'total_win_amount' => $data_order_temp->first()->total_win_amount ,
                'total_rebate' => $data_order_temp->first()->total_rebate
            ];
        }else{
            $final_data = $data_order_temp->merge(collect($final_data));
        }

        return responseSuccess($final_data);
    }

    private function get_sql($user_level,$table,$type,$limit = 0,$offset = 0){
        if(!$limit) $limit_clause = '';
        else $limit_clause = "LIMIT $limit OFFSET $offset";

        if($type == 'day'){
            $select = 'DATE(created_at)';
            $group_by = $select;
            $order_by = $select;
        } else if($type == 'month'){
            $select = "DATE_FORMAT(created_at,'%m ,%Y')";
            $group_by = $select;
            $order_by = 'YEAR(created_at) DESC, FIELD( MONTH(created_at),12,11,10,9,8,7,6,5,4,3,2,1)';
        } else {
            $select = 'YEAR(created_at)';
            $group_by = $select;
            $order_by = $select;
        }

        $user_id = auth()->id();

        return "SELECT 
                    $select as date,
                    COUNT(*) as total_order,
                    SUM(amount) as total_amount,
                    SUM(win_amount) as total_win_amount,
                    SUM($user_level) as total_rebate
            FROM $table
            WHERE user_id = $user_id
            OR l".auth()->user()->level."_id = $user_id
            GROUP BY $group_by 
            ORDER BY $order_by DESC $limit_clause";
    }

}
