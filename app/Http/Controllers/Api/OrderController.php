<?php

namespace App\Http\Controllers\Api;

use Request;
use App\Http\Controllers\Controller;
use App\Http\Services\OrderService;
use Exception;
use DB;

class OrderController extends Controller
{
    function order(){
    
        $this->validate(request(),[
            'bet_body' => 'required'
        ]); 

        $bet_body = request()->get('bet_body');
        $device_name = request()->get('device_name');
        $device_uuid = request()->get('device_uuid');

        DB::beginTransaction();
        try{
            $orderService = new OrderService($bet_body,$device_name,$device_uuid);
            $response = $orderService->process_bet();

            if($response){
                DB::commit();
                return $response;
            }
        }catch(Exception $ex){
            DB::rollback();
            return responseError($ex->getMessage());
        }
    }
}
