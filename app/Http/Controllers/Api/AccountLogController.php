<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UserAccountLog;
use Carbon\Carbon;

class AccountLogController extends Controller
{
    function get_system_logs(){

        $user = auth()->user();

        $logs = UserAccountLog::whereUserId($user->id)
                ->whereIn('abstract',[
                    'LANG_LABEL_BET_SUM',
                    'LANG_LABEL_PROFIT_SHARING',
                    'LANG_LABEL_REBATE_SUM',
                    'LANG_LABEL_RECHARGE',
                    'LANG_LABEL_RECHARGE_BONUS',
                    'LANG_LABEL_RECHARGE_BONUS_ADDED',
                    'LANG_LABEL_RETURN_MONEY',
                    'LANG_LABEL_RETURN_MONEY_CANCEL_TICKET',
                    'LANG_LABEL_TAKE_CASH_CANCEL',
                    'LANG_LABEL_WIN_SUM',
                    'LANG_LABEL_WITHDRAW',
                    'LANG_LABEL_BONUS_FIRST_TOP_UP'
                ])
                ->orderBy('id','desc')
                ->paginate(50);

        $temp = [];
        foreach($logs as $log){
            $temp[] = [
                'amount' => number_format($log->amount).' (R)',
                'balance' => number_format($log->balance ? : ($log->win_money ? : $log->commission)).' (R)',
                'date' => Carbon::parse($log->created_at)->format('Y-m-d h:i A'),
                'human_time' => Carbon::createFromTimeStamp(strtotime($log->created_at))->diffForHumans(),
                'type' => $this->get_type($log->abstract)
            ];
        }

        return responseSuccess($temp);
    }

    function get_type($type){
        switch($type){
            case 'LANG_LABEL_BET_SUM': $key = 'bet_summary'; break;
            case 'LANG_LABEL_PROFIT_SHARING': $key = 'profit'; break;
            case 'LANG_LABEL_REBATE_SUM': $key = 'rebate_summary'; break;
            case 'LANG_LABEL_RECHARGE': $key = 'top_up'; break;
            case 'LANG_LABEL_RECHARGE_BONUS': $key = 'top_up_bonus'; break;
            case 'LANG_LABEL_RECHARGE_BONUS_ADDED': $key = 'top_up_bonus_added'; break;
            case 'LANG_LABEL_RETURN_MONEY': $key = 'return_money'; break;
            case 'LANG_LABEL_RETURN_MONEY_CANCEL_TICKET': $key = 'cancel_ticket'; break;
            case 'LANG_LABEL_TAKE_CASH_CANCEL': $key = 'take_cash_cancel'; break;
            case 'LANG_LABEL_WIN_SUM': $key = 'win_summary'; break;
            case 'LANG_LABEL_WITHDRAW': $key = 'withdraw'; break;
            case 'LANG_LABEL_BONUS_FIRST_TOP_UP': $key = 'first_bonus_top_up'; break;
        }

        return api_trans("log.$key");
    }

    function get_logs(){
        $this->validate(request(),[
            'type' => 'required'
        ]);

        $type = request()->get('type');
        $user = auth()->user();
        $abstract = '';

        switch($type){
            case 'win':
                $abstract = 'LANG_WIN_MONEY_TO_BALANCE';
                break;
            case 'commission':
                $abstract = 'LANG_COMMISSION_TO_BALANCE';
                break;
            case 'profit':
                $abstract = 'LANG_PROFIT_TO_BALANCE';
                break;
            case 'transfer': 
                return $this->transfer_logs();
                break;
        }
        $logs = UserAccountLog::whereUserId($user->id)
                            ->whereAbstract($abstract)
                            ->orderBy('id','desc')
                            ->paginate(15);

        $logs = $logs->keyBy(function ($value, $key) {
            if ($key == 'data') {
                return 'logs';
            }
            return $key;
        })->values()->all();

        $temp = [];
        foreach($logs as $log){
            $temp[] = [
                'amount' => number_format($log->amount),
                'balance' => number_format($log->balance),
                'date' => Carbon::parse($log->created_at)->format('Y-m-d h:i A'),
                'human_time' => Carbon::createFromTimeStamp(strtotime($log->created_at))->diffForHumans(),
            ];
        }

        return responseSuccess($temp);
    }

    function transfer_logs(){
        $user = auth()->user();

        $logs = UserAccountLog::whereUserId($user->id)
                            ->whereAbstract('LANG_LABEL_TRANS')
                            ->whereIsTransfer(true)
                            ->with('toUser')
                            ->orderBy('id','desc')
                            ->paginate(15);


        $logs = $logs->keyBy(function ($value, $key) {
            if ($key == 'data') {
                return 'logs';
            }
            return $key;
        })->values()->all();

        $temp = [];
        foreach($logs as $log){
            $temp[] = [
                'amount' => number_format($log->amount),
                'balance' => number_format($log->balance),
                'date' => Carbon::parse($log->created_at)->format('Y-m-d h:i A'),
                'human_time' => Carbon::createFromTimeStamp(strtotime($log->created_at))->diffForHumans(),
                'type' => $log->to_type == 1 ? 'In' : 'Out',
                'is_transfer' => 1,
                'to_user_name' => isset($log->toUser) ? $log->toUser->username : '',
                'to_user_account' => isset($log->toUser) ? $log->toUser->account_number : '',
                'log_type' => $log->log_type
            ];
        }

        return responseSuccess($temp);
    }
}
