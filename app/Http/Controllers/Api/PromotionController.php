<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Promotion;

class PromotionController extends Controller
{
    public function getPromotion(){
        $promotions = Promotion::whereState('1')->get();
        $data = [];
    
        if($promotions->isEmpty()){
            return responseSuccess([
                [
                    'image' => asset("/assets/images/lotto.jpg"),
                ],
                [
                    'image' => asset("/assets/images/5dd.jpg"),
                ]
            ]);
        }    

        

        foreach($promotions as $promotion){
            $image = isset($promotion) ? $promotion->getMedia('images')->first() : null;
            $data [] = [
                'title'=> $promotion['title'],
                'description' => $promotion['description'],
                'begin_date' => $promotion['begin_date'],
                'end_date' => $promotion['end_date'],
                'image' => isset($image)?$image->getFullUrl(): asset("/assets/images/5dd.jpg") 
            ];
        }

        return responseSuccess($data, 'success');
    }
}
