<?php

namespace App\Http\Controllers\Api\ResultApp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Cycle;

class Cycle5DController extends Controller
{

    public function get_current_cycle(){
        $cycle = Cycle::whereHasReleased(0)->first();

        echo json_encode([
            'remain' => $cycle->result_time - time(),
            'result_time' => date('Y-m-d H:i A',$cycle->result_time)
        ]);
    }
    
    public function get_cycles_5d(){
        $cycles = Cycle::whereHasReleased(1)->orderBy('id','desc')->paginate(15)->toArray();
        $format_cycles = [];

        $prizes = ['A','B','C','D'];

        foreach($cycles['data'] as $cycle){
            $data_cycle = [];
            $result = [];
            foreach($prizes as $prize){
                for($i=1;$i<=5;$i++){
                    $result[] = $cycle[$prize.$i];
                }
            }
            $data_cycle = [
                'result' => $result,
                'result_date' => date('Y-m-d',$cycle['result_time']),
                'result_time' => date('H:i A',$cycle['result_time'])
            ];
            $format_cycles[] = $data_cycle;
        }

        return $format_cycles;
    }

}
