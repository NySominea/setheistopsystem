<?php

namespace App\Http\Controllers\Api\ResultApp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\BallCycle;

class CycleBallController extends Controller
{

    public function get_current_cycle_ball(){
        $cycle = BallCycle::whereHasReleased(0)->first();

        echo json_encode([
            'remain' => $cycle->result_time - time(),
            'result_time' => date('Y-m-d H:i A',$cycle->result_time)
        ]);
    }

    public function get_cycles_ball(){
        $cycles = BallCycle::whereHasReleased(1)->orderBy('id','desc')->paginate(15)->toArray();
        $format_cycles = [];
        foreach($cycles['data'] as $cycle){
            $data_cycle = [
                'jackpot' => $cycle['prize']/4000,
                'result' => json_decode($cycle['result_number']),
                'result_date' => date('Y-m-d',$cycle['result_time']),
                'result_time' => date('H:i A',$cycle['result_time'])
            ];
            $format_cycles[] = $data_cycle;
        }

        return $format_cycles;
    }
}
