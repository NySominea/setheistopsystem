<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
Use App\Model\OrderTemp;
use App\Model\Order;
use App\User;

class DashboardController extends Controller
{
    public function index()
    {   
        $data = [];
        
        $orderTemps = collect(
            DB::select(
                DB::raw(
                    "SELECT
                        COUNT(DISTINCT user_id) as total_members,
                        SUM(amount) as total_amount,
                        (SUM(l1_rebate) + SUM(l2_rebate) + SUM(l3_rebate) + SUM(l4_rebate) + SUM(l5_rebate) ) as total_rebate
                    FROM d_order_temps
                    "
                )
            )
        );
        
        $data['current']['totalMembers'] = isset($orderTemps[0]) ? $orderTemps[0]->total_members : 0;
        $data['current']['totalRevenue'] = isset($orderTemps[0]) ? $orderTemps[0]->total_amount : 0;
        $data['current']['totalRebate'] = isset($orderTemps[0]) ? $orderTemps[0]->total_rebate : 0;

        // By Line Total
        $lines = collect(
            DB::select(
                DB::raw(
                    "SELECT l1_id,SUM(amount) as amount FROM d_order_temps 
                        WHERE l1_id IN (2563,2730,2444,2509,2518,3)
                        GROUP BY l1_id
                    "
                )
            )
        );
        $lines = $lines->mapWithKeys(function ($item) {
            return [$item->l1_id => $item->amount];
        })->all();
        
        $data['line']['pp'] = isset($lines[2563]) ? $lines[2563] : 0;
        $data['line']['pp_master'] = isset($lines[2730]) ? $lines[2730] : 0;
        $data['line']['shop'] = (isset($lines[2444]) ? $lines[2444] : 0)
                                + (isset($lines[2509]) ? $lines[2509] : 0) 
                                + (isset($lines[2518]) ? $lines[2518] : 0) ;
        $data['line']['province'] = isset($lines[3]) ? $lines[3] : 0;

        $start_date = date( 'Y-m-d', strtotime( 'first day of ' . date( 'F Y')));
        $end_date = date("Y-m-t"); 
        
        // Monthly
        $orders = collect(
            DB::select(
                DB::raw(
                    "SELECT 
                        SUM(amount) AS total_amount,
                        SUM(win_amount) AS total_win_amount,
                        (SUM(l1_rebate) + SUM(l2_rebate) + SUM(l3_rebate) + SUM(l4_rebate) + SUM(l5_rebate)) as total_rebate
                    FROM d_orders
                    WHERE created_at BETWEEN '$start_date 00:00:00' AND '$end_date 23:59:59'
                    GROUP BY YEAR(created_at), MONTH(created_at)"
                )
            )
        );
        
        $data['monthly']['totalRevenue'] = isset($orders[0]) ? $orders[0]->total_amount : 0 + $data['current']['totalRevenue'];
        $data['monthly']['totalProfit'] = isset($orders[0]) ? $orders[0]->total_amount - $orders[0]->total_win_amount - $orders[0]->total_rebate  : 0 - $data['current']['totalRebate'];
        
        
        return view('dashboard.index',compact('data'));
    }
}
