<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Model\Manager;
use Cache;
use DB;

class ManagerController extends Controller
{
    public function index()
    {
        $managers = Manager::with('roles')->orderBy('id','DESC')->paginate(20);
        
        return view('admin.manager.index',compact('managers'));
    }

    public function create()
    {
        $roles = Role::all()->pluck('name','id');
        return view('admin.manager.add-update',compact('roles'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'username' => 'required|unique:managers,username',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required',
            'pay_pass' => 'required|confirmed|min:6',
            'pay_pass_confirmation' => 'required'
        ]);
        $this->saveToDB($request->all());
        return redirect()->route('managers.index')->withSuccess('You have just added a manager successfully!');
    }

    public function edit($id)
    {
        $roles = Role::all()->pluck('name','id');
        $manager = Manager::findOrFail($id);
        return view('admin.manager.add-update',compact('roles','manager'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'username' => 'required|unique:managers,username,'.$id,
        ]);
        
        if($request->password){
            $this->validate($request,[
                'password' => 'required|confirmed|min:6',
                'password_confirmation' => 'required',
            ]);
        }

        if($request->pay_pass){
            $this->validate($request,[
                'pay_pass' => 'required|confirmed|min:6',
                'pay_pass_confirmation' => 'required'
            ]);
        }

        $this->saveToDB($request->all(),$id);
        return redirect()->route('managers.index')->withSuccess('You have just updated a manager successfully!');
    }

    public function destroy($id)
    {
        //
    }
    public function saveToDB($data, $id=null){
        DB::beginTransaction();
        try{
            $manager = isset($id) ? Manager::find($id) : new Manager;
            if(!$manager) return redirect()->back()->withError('There is no record found!');

            $data['pass_salt'] = isset($id) ? $manager->pass_salt : rand(10000000,99999999);
            if($data['password']){
                $data['password'] = md5($data['password'].$data['pass_salt']);
            }else{
                unset($data['password']);
            }
            if($data['pay_pass']){
                $data['pay_pass'] = md5($data['pay_pass'].$data['pass_salt']);
            }else{
                unset($data['pay_pass']);
            }
            $data['last_login'] = time();
            $data['last_edit_password'] = time();
            $manager->fill($data);
            $manager->save();

            $role = Role::find($data['role_id']);
            if($role) $manager->syncRoles([$role->name]);
            
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $manager;
    }
}
