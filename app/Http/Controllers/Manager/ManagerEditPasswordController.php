<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManagerEditPasswordController extends Controller
{
    public function edit($id)
    {
        $manager = auth()->user();
        if(!$manager) abort(404);
        return view('admin.password.index',compact('manager'));
    }

   
    public function update(Request $request, $id)
    {
        if($request->type == 'login_password'){
            $this->validate($request,[
                'old_login_password' => 'required|min:6',
                'login_password' => 'required|confirmed|min:6',
                'login_password_confirmation' => 'required|min:6',
            ]);
            $manager = auth()->user();
          
            if(!$manager || $manager->password != md5($request->old_login_password.$manager->pass_salt) ) 
                return back()->withInput()->withErrors(['old_login_password' =>'Please input the correct old login password!']);
            $manager->update(['password' => md5($request->login_password.$manager->pass_salt)]);
            
        }
        elseif($request->type == 'pay_password'){
            $this->validate($request,[
                'old_pay_pass' => 'required|min:6',
                'pay_pass' => 'required|confirmed|min:6',
                'pay_pass_confirmation' => 'required|min:6',
            ]);
            $manager = auth()->user();
            
            if(!$manager || $manager->pay_pass != md5($request->old_pay_pass.$manager->pass_salt) ) 
                return back()->withInput()->withErrors(['old_pay_pass' =>'Please input the correct old pay password!']);
            $manager->update(['pay_pass' => md5($request->pay_pass.$manager->pass_salt)]);
        }
        return back()->withSuccess('You have just updated your account successfully!');
    }


    public function destroy($id)
    {
        //
    }
}
