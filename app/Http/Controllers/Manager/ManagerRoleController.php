<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class ManagerRoleController extends Controller
{
    public function index()
    {   
        $roles = Role::orderBy('id','ASC')->paginate(15);
        return view('admin.role.index',compact('roles')); 
    }

    public function create()
    {
        $permissions = isset(permissions()[app()->getLocale()]) ? permissions()[app()->getLocale()] : permissions()['en'];
        
        return view('admin.role.add-update',compact('permissions'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:d_roles,name'
        ]);
        $this->saveToDB($request->all());
        return redirect()->route('managers.roles.index')->withSuccess('You have just added a role successfully!');    
    }

    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $permissions = isset(permissions()[app()->getLocale()]) ? permissions()[app()->getLocale()] : permissions()['en'];
        $modules = [];
        $managerPermissions = $role->permissions->pluck('name')->toArray();
        foreach($permissions as $key => $m){
        
            foreach($m['permissions'] as $p){
                if(in_array($p['value'],$managerPermissions)) {
                    $modules[] = $key;
                    break;
                }
            }
        }
        
        return view('admin.role.add-update',compact('permissions','role','modules'));
    }

    public function update(Request $request, $id)
    {   
        $this->validate($request,[
            'name' => 'required|unique:d_roles,name,'.$id
        ]);
        $this->saveToDB($request->all(),$id);
        return redirect()->route('managers.roles.index')->withSuccess('You have just updated a role successfully!');    
    }

    public function destroy($id)
    {
        $result = false;
        $item = Role::findOrFail($id);
        if($item){
            DB::beginTransaction();
            try{
                if($item->delete()){
                    $result = true;
                }
                DB::commit();
            }catch(Exception $exception){
                DB::rollback();
                $result = false;
            }
        }
        return response()->json(['success' => $result]);
    }

    public function saveToDB($data, $id=null){
        $permissions = [];
        foreach($data['permissions'] as $m){
            foreach($m as $index => $p){
                if($index != 0){
                    $permissions[] =  $p ;
                    $per = Permission::whereName($p)->first();
                    if(!$per) Permission::create(['name' => $p]);
                }
            }
        }
        
        $role = isset($id) ? Role::find($id) : new Role;
        DB::beginTransaction();
        try{
            if(!$role) return redirect()->back()->withError('There is no record found!');
            $role->fill(['name' => $data['name']]);
            $role->save();
            $role->syncPermissions($permissions);
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $role;
    }
}
