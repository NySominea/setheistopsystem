<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\ManagerLog;
use App\Model\Manager;

class ManagerLogController extends Controller
{
    public function index()
    {
        $managers = Manager::orderBy('id','DESC')->pluck('username','id');
        $logs = ManagerLog::with('manager')
                        ->when(request()->manager_id,function($q){
                            return $q->whereManagerId(request()->manager_id);
                        })
                        ->when(request()->begin_time && request()->end_time,function($q){
                            return $q->whereBetween('created_at',[request()->begin_time,request()->end_time]);
                        })
                        ->orderBy('created_at','desc')->paginate(30);
        return view('admin.log.index',compact('logs','managers'));
    }
}
