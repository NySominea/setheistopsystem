<?php

namespace App\Http\Controllers\SystemSet\Location;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Province;
use DB;

class ProvinceController extends Controller
{
    public function index()
    {
        $provinces = Province::orderBy('name','ASC')->paginate(40);
        return view('system-set.location.province.index',compact('provinces'));
    }

    public function create()
    {
        return view('system-set.location.province.add-update');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|unique:provinces,name'
        ]);
        $this->saveToDB($request->all());
        return redirect()->route('system-set.locations.provinces.index')->withSuccess('You have just added a province successfully!');    
    }

    public function edit($id)
    {
        $province = Province::findOrFail($id);
        return view('system-set.location.province.add-update',compact('province'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|unique:provinces,name,'.$id
        ]);
        $this->saveToDB($request->all(),$id);
        return redirect()->route('system-set.locations.provinces.index')->withSuccess('You have just updated a province successfully!');  
    }

    public function destroy($id)
    {
        $result = false;
        $item = Province::findOrFail($id);
        if($item){
            DB::beginTransaction();
            try{
                if($item->delete()){
                    $result = true;
                }
                DB::commit();
            }catch(Exception $exception){
                DB::rollback();
                $result = false;
            }
        }
        return response()->json(['success' => $result]);
    }

    public function saveToDB($data, $id=null){
        $province = isset($id) ? Province::find($id) : new Province;
        DB::beginTransaction();
        try{
            if(!$province) return redirect()->back()->withError('There is no record found!');
            $province->fill($data);
            $province->save();
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
        return $province;
    }
}
