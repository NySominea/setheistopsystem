<?php

namespace App\Http\Controllers\SystemSet\Language;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class LanguageController extends Controller
{
    public function setLocale($locale){
        Session::put('locale',$locale);
        return redirect()->back();
    }
}
