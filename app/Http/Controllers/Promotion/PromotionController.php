<?php

namespace App\Http\Controllers\Promotion;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Promotion;
use DB;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promotions = Promotion::paginate(10);
        return view('promotion.index', compact('promotions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('promotion.add-update');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'file' => 'max:5120',
        ]);

        $this->saveDB($request->all());
        return \redirect(route('promotions.index'))->withSuccess('You just add a new promotion record');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $promotion = Promotion::findOrFail($id);
        return view('promotion.add-update', compact('promotion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'file' => 'max:5120',
        ]);

        $this->saveDB($request->all(), $id);
        return \redirect(route('promotions.index'))->withSuccess("You've just updated a new promotion record");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function saveDB($data, $id = null){
       
        $data['begin_date'] = date("Y-m-d H:i:s",strtotime($data['begin_date']));
        $data['end_date'] = date("Y-m-d H:i:s",strtotime($data['end_date']));
    
        DB::beginTransaction();
        try {
                $promotion = isset($id) ? Promotion::findOrFail($id) : new Promotion;
                $promotion->fill($data);
                $promotion->save();

                if($promotion){
                    if(isset($data['image']) && !empty($data['image'])){
                        $promotion->clearMediaCollection('images');
                        $promotion->addMedia($data['image'])
                             ->toMediaCollection('images');
                        }  
                    }
                DB::commit();        
        } catch (Exception $ex) {
            DB::rollback();
        }
    }


    public function saveThumnail(Request $request){   

        if($request->hasFile('file')){
            $image = $request->file('file');
            $filename = $image->getClientOriginalName();
            $destination = 'asset/admin/images/temp';
    
            $image->move(public_path($destination), $filename);
            $path = $destination.'/'.$filename;
            
           // $path = saveTempImage($request->file('file'));
           // dd($path);
            return response()->json(['Status' => true, 'path' => $path]);
        }
        return response()->json(['Status' => false]);
    }


    public function deleteThumnail(Request $request){
        $result = false;
        $thumnail_id = $request->input('model_id');
        
        DB::beginTransaction();
        try{
            $promotion = Promotion::findOrFail($thumnail_id);
            if($promotion){
                $promotion->clearMediaCollection('images');
                $result = true;
            }

            DB::commit();
        }catch(Exception $exception){
            DB::rollback();
            $result = false;
        }   
        return response()->json(['success' => $result]);
    }
}
