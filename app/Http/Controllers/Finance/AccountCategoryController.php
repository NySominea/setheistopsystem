<?php

namespace App\Http\Controllers\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SystemAccountCategory;
use App\Model\SystemAccountType;
use DB;
class AccountCategoryController extends Controller
{
    
    public function index()
    {
        $categories = SystemAccountCategory::with('type')->orderBy('sort','ASC')->paginate(15);
        return view('finance.account.category.index',compact('categories'));
    }

   
    public function create()
    {
        $types = SystemAccountType::all()->pluck('name','id');
        return view('finance.account.category.add-update',compact('types'));
    }

    
    public function store(Request $request)
    { 
        $validateData = $request->validate([
            'name' => 'required|unique:d_system_account_categories,name',
        ]);

        $this->saveDB($request->all());
        return redirect()->route('accounts.categories.index')->withSuccess('Added Class Successful');
    }

   
    public function show($id)
    { 
        //
    }

    
    public function edit($id)
    {
        $category = SystemAccountCategory::findOrFail($id);
        $types = SystemAccountType::all()->pluck('name','id');
        return view('finance.account.category.add-update',compact('category','types'));
    }

   
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'name' => 'required|unique:d_system_account_categories,name,'.$id,
        ]);

        $this->saveDB($request->all(),$id);

        return redirect()->route('accounts.categories.index')->withSuccess('Updated Category Successful');
    }

    
    public function destroy($id)
    {
        //
    }
    public function saveDB($data,$id = null){

        $category = null;
        DB::beginTransaction();
        try{
            $category = isset($id) ? SystemAccountCategory::find($id) : new SystemAccountCategory();
            if(!$category) return redirect()->back()->withError('There is no record found!');
            $category->fill($data);
            $category->save($data);
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError('There was an error during operation!');
        }
    }
}
