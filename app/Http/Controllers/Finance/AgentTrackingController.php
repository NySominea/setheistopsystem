<?php

namespace App\Http\Controllers\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Model\Cycle;
use App\Model\Order;
use App\User;
use App\Http\Controllers\Order\OrderController;

class AgentTrackingController extends Controller
{
    public function index(){

        $current_cycle = Cycle::whereStateAndHasReleased(0,1)->orderBy('id','DESC')->pluck('id')->first();
        $cid = request()->cycle ?? $current_cycle;
        $num = request()->num ?? 30;
        
        $cycles = Cycle::whereStateAndHasReleased('0','1')->orderBy('id','DESC')->pluck('cycle_sn','id')->toArray();
        $data = $this->getInvestigateData($cid,$num);
        
        return view('finance.agent-tracking.index',compact('cycles','data','cid','num'));
    }

    public function investigatedDownload(){
        ini_set('max_execution_time', 3600);
        ini_set('memory_limit', '-1');

        $current_cycle = Cycle::whereStateAndHasReleased(0,1)->orderBy('id','DESC')->first();
        $cid = request()->cycle ?? $current_cycle->id;
        $num = request()->get('num',30);

        $data = $this->getInvestigateData($cid,$num);

        // === By Agent Sheet ===
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Agent');
        $spreadsheet = $this->writeSheetByAgent($spreadsheet, $sheet, $data['agent']);

        // === By Sale Sheet ===
        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(1);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Sale');
        $spreadsheet = $this->writeSheetBySale($spreadsheet, $sheet, $data['sale']);

        // === By Area Sheet ===
        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(2);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Area Manager');
        $spreadsheet = $this->writeSheetByAreA($spreadsheet, $sheet, $data['area']);

        // === By Director Sheet ===
        $spreadsheet->createSheet();
        $spreadsheet->setActiveSheetIndex(3);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Director');
        $spreadsheet = $this->writeSheetByDirector($spreadsheet, $sheet, $data['director']);


        $spreadsheet->setActiveSheetIndex(0);
        $writer = new Xlsx($spreadsheet);

        if(!is_dir(public_path('backup'))) mkdir(public_path('backup'),0777,true);
        $writer->save(public_path('backup').'/'.'Doubtful Agent List('.$current_cycle->cycle_sn.').xlsx');
        $path = asset('backup/Doubtful Agent List('.$current_cycle->cycle_sn.').xlsx');

        return response()->json(['path' => $path, 'success' => true]);
      
    }

    public function getInvestigateData($cid,$num){
     
        $agents = Order::where('cycle_id',$cid)
                        ->orderBy('user_id')
                        ->orderBy('parent_id')
                        ->get()->groupBy('user_id');

        $users = User::all()->keyBy('id');
        
        $groups = [];
        $doubtfulAgents = [];
        foreach($agents as $id => $user_orders){
            $bets = [];
            foreach($user_orders as $order){
                foreach($order['bet_content'] as $bet){
                    if($bet[4] == '2D'){
                        if(preg_match('/x/', $bet[1])){
                            $no = preg_replace("/\[[^)]+\]/","",$bet[1]);
                            $no = str_replace('x', '',$no);
                            $arr_no = str_split($no);
                            $bets[] = $arr_no[0].$arr_no[1];
                            $bets[] = $arr_no[1].$arr_no[0];
                        }elseif(preg_match('/s/', $bet[1])){
                            $no = preg_replace("/\[[^)]+\]/","",$bet[1]);
                            for($i=0;$i<10;$i++){
                                $bets[] = str_replace('s', $i,$no);
                            }
                            
                        }elseif(preg_match('/-/', $bet[1])){

                            $string = explode('-',$bet[1]); 
                            $st  = preg_replace("/\[[^)]+\]/","",$string[0]);
                            $end = preg_replace("/\[[^)]+\]/","",$string[1]);
                            foreach(range($st, $end) as $n){
                                $bets[] = (string) str_pad($n, 2, '0', STR_PAD_LEFT);
                            } 
                        }else{
                            $no = preg_replace("/\[[^)]+\]/","",$bet[1]);
                            $bets[] = $no;
                        }
                        
                    }
                }
            }

            sort($bets);
            $bets = array_unique($bets);
            
            $user = isset($users[$id]) ? $users[$id] : null;
            
            $doubtfulAgents[] = [
                'id' => $user ? $user->id : '',
                'username' => $user ? $user->username : '',
                'accountno'=> $user ? $user->account_number : '',
                'level' => $user ? $user->level : '',
                'parent_id' => $user ? $user->parent_id : '',
                'province' => $user ? $user->province : '',
                'sale' => $user ? $user->sale_supervisor : '',
                'area' => $user ? $user->sale_area_manager : '',
                'director' => $user ? $user->sale_director : '',
                'bets' => $bets
            ];
            
            
        }
        
        $sale_names = [];
        $area_names = [];
        $director_names = [];
        foreach($doubtfulAgents as $index => $sale){
            $sale_names[] = $sale['sale'];
            $sale_names = array_unique($sale_names);

            $area_names[] = $sale['area'];
            $area_names = array_unique($area_names);

            $director_names[] = $sale['director'];
            $director_names = array_unique($director_names);
        }

        // Group By Sale
        $doubtfulSales = [];
        foreach($sale_names as $sale_name){
            foreach($doubtfulAgents as $sale){
                if($sale_name == $sale['sale']){
                    if(!isset($doubtfulSales[$sale['sale']]))
                        $doubtfulSales[$sale['sale']] = [];
                   
                    $doubtfulSales[$sale['sale']] = array_merge_recursive($doubtfulSales[$sale['sale']],  $sale);
                    
                }
            }
        }
        
        foreach($doubtfulSales as $index => $sale){
            sort($sale['bets']);
            $bets = array_unique($sale['bets']);
            $doubtfulSales[$index] = [
                'id' => $sale['id'],
                'username' => $sale['username'],
                'accountno'=> $sale['accountno'],
                'province' => is_array($sale['province']) ? $sale['province'][0] : $sale['province'],
                'sale' => is_array($sale['sale']) ? $sale['sale'][0] : $sale['sale'],
                'area' => is_array($sale['area']) ? $sale['area'][0] : $sale['area'],
                'director' => is_array($sale['director']) ? $sale['director'][0] : $sale['director'],
                'bets' => $bets
            ];
        }
        
        // Group by Area 
        $doubtfulAreas = [];
        foreach($area_names as $area_name){
            foreach($doubtfulAgents as $sale){
                if($area_name == $sale['area']){
                    if(!isset($doubtfulAreas[$sale['area']]))
                        $doubtfulAreas[$sale['area']] = [];
                   
                    $doubtfulAreas[$sale['area']] = array_merge_recursive($doubtfulAreas[$sale['area']],  $sale);
                    
                }
            }
        }
        // dd($doubtfulAreas);
        foreach($doubtfulAreas as $index => $sale){
            sort($sale['bets']);
            $bets = array_unique($sale['bets']);
            $sales = is_array($sale['sale']) ? array_unique($sale['sale']) : $sale['sale'];
            $doubtfulAreas[$index] = [
                'id' => $sale['id'],
                'username' => $sale['username'],
                'accountno'=> $sale['accountno'],
                'province' => is_array($sale['province']) ? $sale['province'][0] : $sale['province'],
                'sale' => $sales,
                'area' => is_array($sale['area']) ? $sale['area'][0] : $sale['area'],
                'director' => is_array($sale['director']) ? $sale['director'][0] : $sale['director'],
                'bets' => $bets
            ];
        }

        // Group by Director
        $doubtfulDirectors = [];
        foreach($director_names as $director_name){
            foreach($doubtfulAgents as $sale){
                if($director_name == $sale['director']){
                    if(!isset($doubtfulDirectors[$sale['director']]))
                        $doubtfulDirectors[$sale['director']] = [];
                   
                    $doubtfulDirectors[$sale['director']] = array_merge_recursive($doubtfulDirectors[$sale['director']],  $sale);
                    
                }
            }
        }
        
        foreach($doubtfulDirectors as $index => $sale){
            sort($sale['bets']);
            $bets = array_unique($sale['bets']);
            $sales = is_array($sale['sale']) ? array_unique($sale['sale']) : $sale['sale'];
            $areas = is_array($sale['area']) ? array_unique($sale['area']) : $sale['area'];
            $doubtfulDirectors[$index] = [
                'id' => $sale['id'],
                'username' => $sale['username'],
                'accountno'=> $sale['accountno'],
                'province' => is_array($sale['province']) ? $sale['province'][0] : $sale['province'],
                'sale' => $sales,
                'area' => $areas,
                'director' => is_array($sale['director']) ? $sale['director'][0] : $sale['director'],
                'bets' => $bets
            ];
        }
        

        return [
            'agent' => $doubtfulAgents,
            'sale' => $doubtfulSales,
            'area' => $doubtfulAreas,
            'director' => $doubtfulDirectors
        ];
    }

    public function writeSheetByAgent($spreadsheet, $sheet, $data){
        $columns = ['A','B','C','D','E','F','G','H'];
        $columnNames = [
            'Province',
            'Director',
            'Area Manager',
            'Sale',
            'Agent Name',
            'Agent Account',
            'Count',
            'Bet List',
        ];
        for($iterator=0;$iterator<count($columns);$iterator++){
            $sheet->setCellValue($columns[$iterator].'1', $columnNames[$iterator]);
            $sheet->getStyle($columns[$iterator].'1')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('888888');

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->getColor()
                ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setSize(11);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setBold(true);

                $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setBold(true);
            $sheet->getColumnDimension($columns[$iterator])->setAutoSize(true);
        }
        $i=2;
        
        foreach($data as $agent){
            if(count($agent['bets']) > request()->get('num',30)){
                $sheet->setCellValue('A'.$i,$agent['province']);
                $sheet->setCellValue('B'.$i,$agent['director']);
                $sheet->setCellValue('C'.$i,$agent['area']);
                $sheet->setCellValue('D'.$i,$agent['sale']);
                $sheet->setCellValue('E'.$i,$agent['username']);
                $sheet->setCellValue('F'.$i,$agent['accountno']);
                $sheet->setCellValue('G'.$i,(int) count($agent['bets']));
                $sheet->setCellValue('H'.$i,implode(', ', $agent['bets']));
                $i++;
            }
        }

        return $spreadsheet;
    }

    public function writeSheetBySale($spreadsheet, $sheet, $data){
        $columns = ['A','B','C','D','E','F','G','H'];
        $columnNames = [
            'Province',
            'Director',
            'Area Manager',
            'Sale',
            'Agent Name',
            'Agent Account',
            'Count',
            'Bet List',
        ];
        for($iterator=0;$iterator<count($columns);$iterator++){
            $sheet->setCellValue($columns[$iterator].'1', $columnNames[$iterator]);
            $sheet->getStyle($columns[$iterator].'1')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('888888');

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->getColor()
                ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setSize(11);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setBold(true);

                $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setBold(true);
            $sheet->getColumnDimension($columns[$iterator])->setAutoSize(true);
        }
        $i=2;
        
        foreach($data as $agent){
            if(count($agent['bets']) > request()->get('num',30)){
                $sheet->setCellValue('A'.$i,$agent['province']);
                $sheet->setCellValue('B'.$i,$agent['director']);
                $sheet->setCellValue('C'.$i,$agent['area']);
                $sheet->setCellValue('D'.$i,$agent['sale']);
                $sheet->setCellValue('E'.$i,implode(', ',(array)$agent['username']));
                $sheet->setCellValue('F'.$i,implode(', ',(array)$agent['accountno']));
                $sheet->setCellValue('G'.$i,(int) count($agent['bets']));
                $sheet->setCellValue('H'.$i,implode(', ', $agent['bets']));
                $i++;
            }
        }

        return $spreadsheet;
    }

    public function writeSheetByArea($spreadsheet, $sheet, $data){
        $columns = ['A','B','C','D','E','F','G','H'];
        $columnNames = [
            'Province',
            'Director',
            'Area Manager',
            'Sale',
            'Agent Name',
            'Agent Account',
            'Count',
            'Bet List',
        ];
        for($iterator=0;$iterator<count($columns);$iterator++){
            $sheet->setCellValue($columns[$iterator].'1', $columnNames[$iterator]);
            $sheet->getStyle($columns[$iterator].'1')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('888888');

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->getColor()
                ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setSize(11);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setBold(true);

                $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setBold(true);
            $sheet->getColumnDimension($columns[$iterator])->setAutoSize(true);
        }
        $i=2;
        
        foreach($data as $agent){
            if(count($agent['bets']) > request()->get('num',30)){
                $sheet->setCellValue('A'.$i,$agent['province']);
                $sheet->setCellValue('B'.$i,$agent['director']);
                $sheet->setCellValue('C'.$i,$agent['area']);
                $sheet->setCellValue('D'.$i,implode(', ',(array)$agent['sale']));
                $sheet->setCellValue('E'.$i,implode(', ',(array)$agent['username']));
                $sheet->setCellValue('F'.$i,implode(', ',(array)$agent['accountno']));
                $sheet->setCellValue('G'.$i,(int) count($agent['bets']));
                $sheet->setCellValue('H'.$i,implode(', ', $agent['bets']));
                $i++;
            }
        }

        return $spreadsheet;
    }

    public function writeSheetByDirector($spreadsheet, $sheet, $data){
        $columns = ['A','B','C','D','E','F','G','H'];
        $columnNames = [
            'Province',
            'Director',
            'Area Manager',
            'Sale',
            'Agent Name',
            'Agent Account',
            'Count',
            'Bet List',
        ];
        for($iterator=0;$iterator<count($columns);$iterator++){
            $sheet->setCellValue($columns[$iterator].'1', $columnNames[$iterator]);
            $sheet->getStyle($columns[$iterator].'1')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('888888');

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->getColor()
                ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setSize(11);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setBold(true);

                $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setBold(true);
            $sheet->getColumnDimension($columns[$iterator])->setAutoSize(true);
        }
        $i=2;
        
        foreach($data as $agent){
            if(count($agent['bets']) > request()->get('num',30)){
                $sheet->setCellValue('A'.$i,$agent['province']);
                $sheet->setCellValue('B'.$i,$agent['director']);
                $sheet->setCellValue('C'.$i,implode(', ',(array)$agent['area']));
                $sheet->setCellValue('D'.$i,implode(', ',(array)$agent['sale']));
                $sheet->setCellValue('E'.$i,implode(', ',(array)$agent['username']));
                $sheet->setCellValue('F'.$i,implode(', ',(array)$agent['accountno']));
                $sheet->setCellValue('G'.$i,(int) count($agent['bets']));
                $sheet->setCellValue('H'.$i,implode(', ', $agent['bets']));
                $i++;
            }
        }

        return $spreadsheet;
    }

    public function downloadByUser(){
        $orderController = new OrderController();

        $path = $orderController->downloadExcel(request());

        return response()->json(['path' => $path, 'success' => true]);
    }
}


