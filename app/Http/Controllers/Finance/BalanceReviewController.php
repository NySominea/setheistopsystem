<?php

namespace App\Http\Controllers\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\UserAccountLog;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class BalanceReviewController extends Controller
{
    public function formSelection(){

        $users = User::select('id','account_number','username')->where(['state' => 1])->get();
        return view('finance.balance-review.selection',compact('users'));
    }

    public function index(){

        ini_set('max_execution_time', 3600);
        ini_set('memory_limit', '-1');

        $userIds = request()->users;
        $accountLogs = [];
        
        if($userIds[0] == 'all' || request()->users == null){

            $ids = User::where('state',1)->where('is_test',0)->where('is_approved',1)->get()->pluck('id');
           
            $abstract = ['LANG_COMMISSION_TO_BALANCE','LANG_LABEL_BET_SUM','LANG_LABEL_RECHARGE',
                    'LANG_LABEL_RETURN_MONEY_CANCEL_TICKET','LANG_LABEL_TRANS','LANG_WIN_MONEY_TO_BALANCE','LANG_PROFIT_TO_BALANCE','LANG_LABEL_WITHDRAW'];

            $accountLogs = UserAccountLog::with('user','user.bonusAccount')->whereIn('abstract',$abstract)->whereIn('user_id',$ids)
                            ->where('created_at','<=',request()->date)
                            ->orderBy('created_at','DESC')->get()
                            ->groupBy('user_id');
                            
        }else{
            
            $accountLogs = UserAccountLog::with('user','user.bonusAccount')->whereIn('user_id',$userIds)
                            ->where('created_at','<=',request()->date)
                            ->orderBy('created_at','DESC')
                            ->get()
                            ->groupBy('user_id');
        }
    
       
        return view('finance.balance-review.index',compact('accountLogs'));
    }

    public function download(){
        
        ini_set('max_execution_time', 3600);
        ini_set('memory_limit', '-1');

        $userIds = request()->users;
        $date = request()->date;

        if($userIds[0] == 'all' || request()->users == null){

            $ids = User::where('state',1)->where('is_test',0)->where('is_approved',1)->get()->pluck('id');
            
            $abstract = ['LANG_COMMISSION_TO_BALANCE','LANG_LABEL_BET_SUM','LANG_LABEL_RECHARGE',
            'LANG_LABEL_RETURN_MONEY_CANCEL_TICKET','LANG_LABEL_TRANS','LANG_WIN_MONEY_TO_BALANCE','LANG_PROFIT_TO_BALANCE','LANG_LABEL_WITHDRAW'];

            $logs = UserAccountLog::with('user','user.bonusAccount')->whereIn('abstract',$abstract)->whereIn('user_id',$ids)
                                ->where('created_at','<=',request()->date)
                                ->orderBy('created_at','DESC')
                                ->get()
                                ->groupBy('user_id');
                        
        }else{
            
            $logs = UserAccountLog::with('user','user.bonusAccount')->whereIn('user_id',$userIds)
                                    ->where('created_at','<=',request()->date)
                                    ->orderBy('created_at','DESC')
                                    ->get()
                                    ->groupBy('user_id');
        }
    
        $path = $this->writeExcel($logs);
        
        return response()->json(['path' => $path, 'success' => true]);
    }

    public function writeDetailWorksheet($spreadsheet, $sheet, $data){
        
        $columnNames = ['#','AccountNo','UserName','Balance','Bonus','Review Date','Type'];
        $sheet = $this->writeHeaderWorksheet($sheet, $columnNames);
        
        $i = 2;
        foreach($data as $l => $log){

            $bonus = isset($log[0]->user->bonusAccount) ? $log[0]->user->bonusAccount->balance : '0';

            $sheet->setCellValue('A'.$i,$i-1);
            $sheet->setCellValue('B'.$i,isset($log[0]->user) ? $log[0]->user->account_number : '' );
            $sheet->setCellValue('C'.$i,isset($log[0]->user) ? $log[0]->user->username : '' );
            $sheet->setCellValue('D'.$i,isset($log[0]->user) ? $log[0]->balance : '0' );
            $sheet->setCellValue('E'.$i,$bonus);
            $sheet->setCellValue('F'.$i, request()->date);
            $sheet->setCellValue('G'.$i,$this->checkType($log[0]->user->id));
            $i++;
        }
        
        return $spreadsheet;
    }
    public function writeExcel($data){
        
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Balance Review List');
        $spreadsheet = $this->writeDetailWorksheet($spreadsheet, $sheet, $data);
        
        $writer = new Xlsx($spreadsheet);
        if(!is_dir(public_path('backup'))) mkdir(public_path('backup'),0777,true);
        $writer->save(public_path('backup').'/'.'Balance_Review('.date('Y-m-d ', strtotime(request()->date)).').xlsx');
        $path = asset('backup/Balance_Review('.date('Y-m-d ', strtotime(request()->date)).').xlsx');
        return $path;
    }

    public function writeHeaderWorksheet($sheet, $columnNames){
        $columns = [];
        $a = 'A';
        foreach($columnNames as $index => $value){
            array_push($columns, $a++);
        }

        for($iterator=0;$iterator<count($columns);$iterator++){
            $sheet->setCellValue($columns[$iterator].'1', $columnNames[$iterator]);
            $sheet->getStyle($columns[$iterator].'1')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('888888');

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->getColor()
                ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setSize(11);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setBold(true);
            $sheet->getColumnDimension($columns[$iterator])->setAutoSize(true);
        }

        return $sheet;
    }

    public function checkType($key){    

        $type = '';
        $user = User::where('id',$key)->first();

        if($user->is_shop == 1){
            $type = "Shop";
        }else if($user->user_type == 1 && $user->com_direct == 1){
            $type = "Direct User";
        }else if($user->level == 8){
            $type = "Agent";
        }else{
            $type = "Upline";
        }
        return $type;
    }
}
