<?php

namespace App\Http\Controllers\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Order;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ShopReviewController extends Controller
{
    public function formSelection(){

        return view('finance.shop-review.selection-form');

    }

    public function getProfit(){

        $type = request()->type;
        $start_date = request()->start_date;
        $end_date = request()->end_date;
        $data = [];
        $users = User::select();

        if($type == 'Shop'){
            $users->where(['state' => 1,'is_shop'=> 1,'is_approved' => 1])->get();
        }

        $userIds = $users->pluck('id');
        $orders = Order::with('user')->whereIn('user_id', $userIds)
                        ->whereBetween('created_at',[$start_date,$end_date])
                              ->get()->groupBy('user_id');
       
        foreach($orders as $key => $order){
           
            $data [] = [
                'username' => $order[0]->user->username,
                'accountno' => $order[0]->user->account_number,
                'bet_amount' => $order->sum('amount'),
                'win_amount' => $order->sum('win_amount'),
                'profit' => $order->sum('amount') - $order->sum('win_amount'),
                'type' => $order[0]->user->is_shop == 1 ? 'Shop' : 'Agent'
            ];
           
        }
        return $data;
    }

    public function index(){

        $data = $this->getProfit();

        return view('finance.shop-review.index',compact('data'));                      

    }

    public function download(){
        
        ini_set('max_execution_time', 3600);
        ini_set('memory_limit', '-1');

        $data = $this->getProfit();
    
        $path = $this->writeExcel($data);
        
        return response()->json(['path' => $path, 'success' => true]);
    }

    public function writeDetailWorksheet($spreadsheet, $sheet, $data){
        
        $columnNames = ['#','AccountNo','UserName','Bet Amount','Win Amount','Profit','Type'];
        $sheet = $this->writeHeaderWorksheet($sheet, $columnNames);
        
        $i = 2;
        foreach($data as $l => $user){

            $sheet->setCellValue('A'.$i,$i-1);
            $sheet->setCellValue('B'.$i, $user['accountno']);
            $sheet->setCellValue('C'.$i, $user['username'] );
            $sheet->setCellValue('D'.$i, $user['bet_amount']);
            $sheet->setCellValue('E'.$i, $user['win_amount']);
            $sheet->setCellValue('F'.$i, $user['profit']);
            $sheet->setCellValue('G'.$i, $user['type']);
            $i++;

        }
        
        return $spreadsheet;
    }
    public function writeExcel($data){
        
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Profit Review List');
        $spreadsheet = $this->writeDetailWorksheet($spreadsheet, $sheet, $data);
        
        $writer = new Xlsx($spreadsheet);
        if(!is_dir(public_path('backup'))) mkdir(public_path('backup'),0777,true);
        $writer->save(public_path('backup').'/'.'Profit_Review('.date('Y-m-d').').xlsx');
        $path = asset('backup/Profit_Review('.date('Y-m-d').').xlsx');

        return $path;
    }

    public function writeHeaderWorksheet($sheet, $columnNames){
        $columns = [];
        $a = 'A';
        foreach($columnNames as $index => $value){
            array_push($columns, $a++);
        }

        for($iterator=0;$iterator<count($columns);$iterator++){
            $sheet->setCellValue($columns[$iterator].'1', $columnNames[$iterator]);
            $sheet->getStyle($columns[$iterator].'1')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('888888');

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->getColor()
                ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setSize(11);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setBold(true);
            $sheet->getColumnDimension($columns[$iterator])->setAutoSize(true);
        }

        return $sheet;
    }

}
