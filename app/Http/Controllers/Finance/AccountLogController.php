<?php

namespace App\Http\Controllers\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SystemAccount;
use App\Model\SystemAccountLog;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Model\Mananger;
use App\Constants\Account;
use DB;


class AccountLogController extends Controller
{
    
    public function index()
    {
        $log_type = request()->log_type ?? 'all';
        $begin_time = request()->begin_time ?? '';
        $end_time = request()->end_time ?? '';
        $account = request()->account ?? 'all';
        $accounts = ['all' => 'All'] + SystemAccount::where('state',1)->orderBy('id','DESC')->get()->pluck('title','id')->toArray();
        
        $logs = SystemAccountLog::with('manager','account','toSystemAccount','toUser');
        
        if($log_type!='all'){
            $logs->where('log_type',$log_type);
        }
        if($account != 'all'){
            $logs->where('account_id',$account);
        }
        if($begin_time && $end_time){
            $logs->whereBetween('created_at',[$begin_time,$end_time]);
        }
       
        $logs = $logs->orderBy('created_at','DESC')->paginate(20); 
        
        return view('finance.log.index',compact('logs','accounts')); 
    }
    
    public function download(){

        ini_set('max_execution_time', 3600);
        ini_set('memory_limit', '-1');
        $s = microtime(true);
        $log_type = request()->log_type ?? 'all';
        $begin_time = request()->begin_time ?? '';
        $end_time = request()->end_time ?? '';
        $account = request()->account ?? 'all';
        $s = microtime(true);
        $logs = SystemAccountLog::with('manager','account','toSystemAccount','toUser'); 
      
        if($log_type!='all'){
            $logs->where('log_type',$log_type);
        }
        if($account != 'all'){
            $logs->where('account_id',$account);
        }
        if($begin_time && $end_time){
            $logs->whereBetween('created_at',[$begin_time,$end_time]);
        }
        $logs = $logs->orderBy('created_at','DESC')->get();
        
        $path = $this->writeExcel($logs);
        return response()->json(['path' => $path, 'success' => true]);
    }

    public function writeExcel($data){
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('System Log List');
        $spreadsheet = $this->writeDetailWorksheet($spreadsheet, $sheet, $data);
        
        $writer = new Xlsx($spreadsheet);
        if(!is_dir(public_path('backup'))) mkdir(public_path('backup'),0777,true);
        $writer->save(public_path('backup').'/'.'Log('.date('Y-m-d').').xlsx');
        $path = asset('backup/Log('.date('Y-m-d').').xlsx');
        return $path;
    }

    public function writeDetailWorksheet($spreadsheet, $sheet, $data){
        $columnNames = ['Transaction','Account','Account No','In (R)','Out (R)','Balance (R)','To Account','AccountNo','Province','Manager','Date Time','Abstract'];
        $sheet = $this->writeHeaderWorksheet($sheet, $columnNames);
        $languages = getAdminLanguageData();
     
        $i = 2;
        foreach( $data as $l => $log){
            $sheet->setCellValueExplicit('A'.$i, $log->log_number, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $sheet->setCellValue('B'.$i, isset($log->account) ? $log->account->name : '');
            $sheet->setCellValue('C'.$i, isset($log->account) ? $log->account->number : '');
            $sheet->setCellValue('D'.$i, $log->log_type == Account::LOG_TYPE_IN ? $log->amount : 0);
            $sheet->setCellValue('E'.$i, $log->log_type == Account::LOG_TYPE_OUT ? $log->amount * (-1) : 0);
            $sheet->setCellValue('F'.$i, $log->balance);
          
            if($log->to_type == Account::SYSTEM_ACCOUNT_TYPE) {
                $type = $languages[Account::systemAccountType($log->to_type)];
                $user =  $log->toSystemAccount ? '('.$log->toSystemAccount->name.')' : '';
                $sheet->setCellValueExplicit('G'.$i,$type . $user, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            }elseif($log->to_type == Account::USER_ACCOUNT_TYPE){
                $type = $languages[Account::systemAccountType($log->to_type)];
                $user =  $log->toUser ? $log->toUser->username : '';
                $accountno =  $log->toUser ? $log->toUser->account_number : '';
                $province = $log->toUser  ? $log->toUser->province : '';
                $sheet->setCellValueExplicit('G'.$i,$user, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $sheet->setCellValueExplicit('H'.$i,$accountno, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $sheet->setCellValueExplicit('I'.$i,$province, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            }
            
            $sheet->setCellValue('J'.$i,$log->manager ? $log->manager->username : '');
            $sheet->setCellValue('K'.$i,$log->created_at);
            $sheet->setCellValue('L'.$i,$languages[$log->abstract]);
            $i++;
        }
        
        return $spreadsheet;
    }

    public function writeHeaderWorksheet($sheet, $columnNames){
        $columns = [];
        $a = 'A';
        foreach($columnNames as $index => $value){
            array_push($columns, $a++);
        }

        for($iterator=0;$iterator<count($columns);$iterator++){
            $sheet->setCellValue($columns[$iterator].'1', $columnNames[$iterator]);
            $sheet->getStyle($columns[$iterator].'1')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('888888');

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->getColor()
                ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setSize(11);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setBold(true);
            $sheet->getColumnDimension($columns[$iterator])->setAutoSize(true);
        }

        return $sheet;
    }

    public function ajaxGetLogInformation($id){
        $data = [];
        $result = false;
        $log = SystemAccountLog::find($id);
        if($log){
            $result = true;
            $data = [
                'transaction' => $log->log_number,
                'manager' => $log->manager ? $log->manager->username : '',	
                'account' => $log->account ? $log->account->name : '',	
                'accountNo' => $log->account ? $log->account->number : '',	
                'logType' => $log->log_type == Account::LOG_TYPE_IN ? 'In' : 'Out',	
                'amount' => currencyFormat($log->amount,0),	
                'balance' => currencyFormat($log->balance,0),	
                'toAccount' => $log->to_type == Account::SYSTEM_ACCOUNT_TYPE 
                            ? getAdminLanguageData()[Account::systemAccountType($log->to_type)] ." ". ($log->toSystemAccount ? '('.$log->toSystemAccount->name.')' : '') 
                            : getAdminLanguageData()[Account::systemAccountType($log->to_type)] ." ".($log->toUser ? '('.$log->toUser->username.')' : ''),	
                'dateTime' => (string)$log->created_at,
                'abstract' => getAdminLanguageData()[$log->abstract],
                'auditor' => '',
            ];
        }
        
        
        return response()->json(['success' => $result, 'log' => $data]);
    }
}
