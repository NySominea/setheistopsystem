<?php

namespace App\Http\Controllers\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SystemAccountType;
use App\Model\SystemAccount;
use App\Model\SystemAccountCategory;
use DB;

class AccountController extends Controller
{
    
    public function index()
    {
    
      //  $accounts = SystemAccount::with('type','category')->where('state',1)->OrderBy('sort','ASC')->paginate(15);
        $accounts = SystemAccount::where('state',1)->OrderBy('sort','ASC')->paginate(15);
        return view("finance.account.index",compact('accounts'));
    }

    public function create()
    {
        // $types = SystemAccountType::all()->pluck('name','id');
        // $categories = SystemAccountCategory::all()->pluck('name','id');
       
        return view("Finance.account.add-update",compact('types','categories'));
    }

  
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'title' => 'required|unique:system_accounts,title',
            'number' => 'required'
        ]);

        $this->saveDB($request->all());
        return redirect()->route('accounts.index')->withSucess('You have just Added an Account Successful');
    }

  
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        $account = SystemAccount::findOrFail($id);
        // $types = SystemAccountType::all()->pluck('name','id');
        // $categories = SystemAccountCategory::all()->pluck('name','id');
        return view("finance.account.add-update",compact('account','types','categories'));
    }

   
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'title' => 'required|unique:system_accounts,title,'.$id,
            'number' => 'required'
        ]);
        
        $this->saveDB($request->all(),$id);
        return redirect()->route('accounts.index')->withSucess('You have just Added an Account Successful');
    }

  
    public function destroy($id)
    {
        //
    }

    public function saveDB($data , $id=null){
       
        $account = null;
        DB::beginTransaction();
        try{
            $account = isset($id) ? SystemAccount::find($id) : new SystemAccount();
            if(!$account) return redirect()->back()->withError('There is no record found!');
            $account->fill($data);
            $account->save($data);
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withError("There error with during operation");
        }
        return $account;
    }
}
