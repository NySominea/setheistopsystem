<?php

namespace App\Http\Controllers\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SystemAccount;
use DB;
use App\Model\SystemAccountCategory;
use App\Model\SystemAccountLog;
use App\Model\Manager;
use App\Constants\Account;


class AccountDepositController extends Controller
{
   
    public function index()
    {
        //
    }

   
    public function create()
    {
        
    }

   
    public function store(Request $request)
    {
        //
    }

   
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        $account = SystemAccount::findOrFail($id);
        return view('finance.account.deposit.add-update',compact('account'));
    }

    
    public function update(Request $request, $id)
    {
        
        $this->validate($request,[
            'amount' => 'required'
        ],[
            'amount.min' => 'Please enther the amount.',
        ]);

        $abstract = '';
        $data = $request->all();
        $data['amount'] = stringToDouble($data['amount']);
        if($data['amount'] == 0) return back()->withInput()->withErrors(['amount' => 'Please enther the amount.']);
        
        DB::beginTransaction();
        try{
            if(!Manager::checkPayPassword($data['pay_password'])) 
                return back()->withInput()->withErrors(['pay_password' =>'Your pay password does not match!']); 

            $account = SystemAccount::findOrFail($id);
            if(!$account) 
                return redirect()->back()->withInput()->withError('There is no record found!');

            if($data['log_type'] == Account::LOG_TYPE_IN){
                $account->increment('balance',$data['amount']);
                $abstract = 'LANG_LABEL_DEPOSIT';
            }
            elseif($data['log_type'] == Account::LOG_TYPE_OUT){
                if($account->balance < $data['amount']){
                    return back()->withInput()->withErrors(['amount' =>'You amount is larger than current avaiable balance!']);
                }
                $account->decrement('balance',$data['amount']);
                $abstract = 'LANG_LABEL_WITHDRAW';
            }
            else{
                return redirect()->back()->withInput()->withError('Unknown Log Type!');
            }
            
            SystemAccountLog::create([
                'amount' => $data['amount'],
                'balance' => $account->balance,
                'abstract' => $abstract,
                'user_abstract' => $data['abstract'],
                'log_number' => SystemAccountLog::generateLogNumber($data['log_type']),
                'log_type' => $data['log_type'],
                'to_type' => Account::SYSTEM_ACCOUNT_TYPE,
                'account_id' => $id,
                'to_account_id' => $id,
                'manager_id' => auth()->id()
            ]);
            
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withInput()->withError('There was an error during operation!');
        }

        return redirect()->route('accounts.index')->withSuccess('You have just deposited an account with amount '.$request->amount.'!');
    }

   
    public function destroy($id)
    {
        //
    }
}
