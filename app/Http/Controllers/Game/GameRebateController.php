<?php

namespace App\Http\Controllers\Game;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\GameRebate;
use DB;

class GameRebateController extends Controller
{
    public function index()
    {   
        $rebates = GameRebate::with('category')->get()->groupBy('root_id');
        return view('game.rebate.index',compact('rebates'));
    }


    public function store(Request $request)
    {
        $this->saveToDB($request->all());
        return redirect()->route('games.rebates.index')->withSuccess('You have just updated game rebates successfully!');
    }


    public function saveToDB($data){ 
        
        foreach($data['rebates'] as $id => $row){ 
                $rebate = GameRebate::find($id);
                if($rebate){
                    DB::beginTransaction();
                    try{
                        $rebate->fill($row);
                        $rebate->save();
                        DB::commit();
                    }catch(Exception $ex){
                        DB::rollback();
                    }
                }
        }
    }
}
