<?php

namespace App\Http\Controllers\Game;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\GameCategory;
use DB;

class GameCategoryController extends Controller
{
    public function index()
    {   
        $categories = GameCategory::all();
        return view('game.category.index',compact('categories'));
    }


    public function store(Request $request)
    {
        $this->saveToDB($request->all());
        return redirect()->route('games.categories.index')->withSuccess('You have just updated game odds successfully!');
    }


    public function saveToDB($data){
        foreach($data['categories'] as $id => $row){ 
            $category = GameCategory::find($id);
            if($category){
                DB::beginTransaction();
                try{
                    $category->update(['rate' => $row]);
                    DB::commit();
                }catch(Exception $ex){
                    DB::rollback();
                }
            }
        }
    }
}
