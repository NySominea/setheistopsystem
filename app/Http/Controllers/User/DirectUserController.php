<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Pagination\LengthAwarePaginator;
use App\User;
use App\Constants\Account;

class DirectUserController extends Controller
{
    public function index()
    {   
        $state = request()->get('state') ?? 'all';
        $keyword = request()->get('keyword') ?? '';
        
        $users = User::with('cashAccount','children')->where(['com_direct' => 1]);
        $users = $users->when($state != 'all',function($q) use ($state){
                        return $q->where(['state' => $state]);
                    })->when($keyword != '',function($q) use ($keyword){
                        return $q->where('username','LIKE','%'.$keyword.'%')
                                 ->orWhere('account_number','LIKE','%'.$keyword.'%');
                    })->orderBy('id','DESC')->paginate(20);
        
        return view('user.direct-user.index',compact('users'));
    }
}
