<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Pagination\LengthAwarePaginator;
use App\User;
use App\Model\UserAccount;
use App\Constants\Account;
use App\Model\Province;
use App\Model\District;
use App\Model\Commune;
use Hash;

use App\Model\ManagerLog;

class SaleNetworkController extends Controller
{

    public function index(){   
        $parent_id = request()->get('parent_id') ?? 0;
        $state = request()->get('state') ?? 'all';
        $keyword = request()->get('keyword') ?? '';
       
        $users = User::with('cashAccount')
                    ->when($state != 'all',function($q) use ($state){
                        return $q->where('state' , $state);
                    })
                    ->when($keyword == '',function($q) use ($parent_id){
                        return $q->where('parent_id',$parent_id);
                    })
                    ->when($keyword != '',function($q) use ($keyword){
                        return $q->where('username','LIKE','%'.$keyword.'%')
                                 ->orWhere('account_number','LIKE','%'.$keyword.'%')
                                 ->orwhere('phone',$keyword);
                    })->orderBy('id','DESC')->paginate(20);

        $topLevelUsers = User::find($parent_id);
        $topLevelUsers = $topLevelUsers ? $topLevelUsers->ancestorsAndSelf()->orderBy('created_at','ASC')->get() : [];
                   
        return view('user.sale-network.index',compact('users','topLevelUsers'));
    }

    public function create(){
        $level = request('level');
        $parent_id = request('parent_id');
        $account_number = User::generateAccountNumber();

        $provinces = Province::all();
        $district = [];
        $commune = [];
        return view('user.sale-network.add-update', compact('level','account_number', 'parent_id','provinces','district','commune'));
    }

    public function store(Request $request){
         $request->validate([
             'username' => 'required|unique:users,username',
             'account_number' => 'required|unique:users,account_number',
             'phone' => 'required|unique:users,phone|regex:/^([0-9\s\-\+\(\)]*)$/|min:9',
             'password' => 'required|confirmed|min:6',
             'password_confirmation' => 'required|min:6',
             'payment_password' => 'required|confirmed|min:6',
             'payment_password_confirmation' => 'required|min:6'  
         ]);
         
        $user = $this->saveDB($request->all());

        return redirect()->route('users.sale-network.index',['level'=>$user->level-1, 'parent_id'=>$user->parent_id]);
    }

    public function edit($id){
        $user = User::find($id);
        $provinces = Province::all();
        $district = District::all();
        $commune = Commune::all();

        return view('user.sale-network.add-update', compact('user','provinces','district','commune')); 
    }

    public function update(Request $request, $id){     
        $request->validate([
            'username' => 'required|unique:users,username,'.$id,
            'account_number' => 'required|unique:users,account_number,'.$id
        ]);

        if($request->password){
            $this->validate($request,[
                'password' => 'required|confirmed|min:6',
                'password_confirmation' => 'required',
            ]);
        }

        if($request->pay_pass){
            $this->validate($request,[
                'pay_pass' => 'required|confirmed|min:6',
                'pay_pass_confirmation' => 'required'
            ]);
        }
        
       $user = $this->saveDB($request->all(), $id);
       
       return redirect()->route('users.sale-network.index', [ 'level'=>$user->level-1, 'parent_id'=>$user->parent_id ]);
    }

    public function saveDB($data, $id = null){   
            
        DB::beginTransaction();
        try {
            $user = isset($id) ? User::find($id) : new User;
            
            if(isset($id)){ 
                $data['password']  = isset($data['password']) ? Hash::make($data['password']) : $user->password;
                $data['payment_password']  = isset($data['payment_password']) ? Hash::make($data['payment_password']) : $payment_password;
                $data['parent_id'] = $user->parent_id;
            }else{
                $data['password'] = Hash::make($data['password']);
                $data['payment_password'] = Hash::make($data['payment_password']);
            }
           
            $user->fill($data);
            if($user->save()){
                UserAccount::createCashAccount($user->id);
                UserAccount::createBonus5DAccount($user->id);
                UserAccount::createBonusBallAccount($user->id);
            }
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
        }
        return $user;
    }

    public function agentApproval(){
        $is_approved = 0;
        
        $users = User::with('cashAccount')
                    ->where(['is_approved' => $is_approved])
                    ->orderBy('id','DESC')->paginate(20);
        
        return view('user.agent-approval.index',compact('users'));
    }

    public function getDistrict(){
   
        $data = [];
        $districts = District::where('province_id',request()->id)->get();
    
        foreach($districts as $district){
            $data [] = [
                'id' => $district->id,
                'name' => $district->name
            ];
        }
       
       return $data;
    }

    public function getCommune(){
        $data = [];
        $communes = Commune::where('district_id',request()->id)->get();
    
        foreach($communes as $commune){
            $data [] = [
                'id' => $commune->id,
                'name' => $commune->name
            ];
        }
       return $data;
    }

}

