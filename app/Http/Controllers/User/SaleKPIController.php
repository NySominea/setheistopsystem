<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Order;
use App\Model\UserAccountLog;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class SaleKPIController extends Controller
{
    public function selectionForm(){
        
        $users = User::where('state',1)->get();
        return view('finance.sale-kpi.selection',compact('users'));
    }

    public function queryData(){
        $userid = [];
        $userid  = request()->users ?? [];
        $start_dt = request()->start_dt ?? '';
        $end_dt = request()->end_dt ?? '';
      
        $data = [];
        $order = [];
        $user = User::all()->keyBy('id');
        foreach($userid as $id){
          
            $topUp_sale = UserAccountLog::with('user')->where('user_id',$id)->whereIn('log_type',[1,2])->where('abstract','LANG_LABEL_TRANS');
            $topUp_finance = UserAccountLog::with('user')->where('user_id',$id)->whereIn('abstract',['LANG_LABEL_RECHARGE','LANG_LABEL_WITHDRAW']);
            $order = Order::where('user_id',$id);
          
            if($start_dt && $end_dt){
                $topUp_sale->whereBetween('created_at',[$start_dt,$end_dt]);
                $topUp_finance->whereBetween('created_at',[$start_dt,$end_dt]);
                $order->whereBetween('created_at',[$start_dt,$end_dt]);
            }

            $topUp_sale = $topUp_sale->get()->groupBy('log_type');
            $topUp_finance  = $topUp_finance->get()->groupBy('abstract');
            $order = $order->get();

            $data [] = [
                'id' => $id,
                'username' => $user[$id]->username,
                'accountno' => $user[$id]->account_number,
                'province'  => $user[$id]->province,
                'amount' => $order->sum('amount'),
                'win_amount' => $order->sum('win_amount'),
                'finance' => [
                    'recharge' => isset($topUp_finance['LANG_LABEL_RECHARGE']) ? $topUp_finance['LANG_LABEL_RECHARGE']->sum('amount') : '0', 
                    'withdraw' => isset($topUp_finance['LANG_LABEL_WITHDRAW']) ?  $topUp_finance['LANG_LABEL_WITHDRAW']->sum('amount') : '0'
                ],
                'sale' => [
                    'recharge' => isset($topUp_sale[1]) ? $topUp_sale[1]->sum('amount') : '0',
                    'withdraw' => isset($topUp_sale[2]) ? $topUp_sale[2]->sum('amount') : '0'
                    ]
                ];
            }

            return $data ;
    } 

    public function showKpi(){
        $data = $this->queryData();
        return view('finance.sale-kpi.showkpi',compact('data'));
    }
    public function download(){

        $data = $this->queryData();
        $path = $this->writeExcel($data);
        return response()->json(['path' => $path, 'success' => true]);
    }

    public function writeExcel($data){
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Sale KPI');
        $spreadsheet = $this->writeDetailWorksheet($spreadsheet, $sheet, $data);
        
        $writer = new Xlsx($spreadsheet);
        if(!is_dir(public_path('backup'))) mkdir(public_path('backup'),0777,true);
        $writer->save(public_path('backup').'/'.'Sale KPI('.date('Y-m-d').').xlsx');
        $path = asset('backup/Sale KPI('.date('Y-m-d').').xlsx');
        return $path;
    }

    public function writeDetailWorksheet($spreadsheet, $sheet, $data){
        $columnNames = ['AccountNo','UserName','Province','Bet Amount',' Win Amount','Top Up Fianance','Top Up Sale','Withdraw Finace','Withdraw Sale'];
        $sheet = $this->writeHeaderWorksheet($sheet, $columnNames);
        $i = 2;
        foreach( $data as $l => $value){
            $sheet->setCellValue('A'.$i, isset($value['accountno']) ? $value['accountno'] : 'N/A');
            $sheet->setCellValue('B'.$i, isset($value['username']) ? $value['username'] : 'N/A');
            $sheet->setCellValue('C'.$i, isset($value['province']) && !empty($value['province']) ? $value['province'] : 'N/A');
            $sheet->setCellValue('D'.$i, isset($value['amount']) ? $value['amount'] : '0');
            $sheet->setCellValue('E'.$i, isset($value['win_amount']) ? $value['win_amount'] : '0');
            $sheet->setCellValue('F'.$i, isset($value['finance']['recharge']) ? $value['finance']['recharge'] : '0');
            $sheet->setCellValue('G'.$i, isset($value['finance']['withdraw']) ? $value['finance']['withdraw'] : '0');
            $sheet->setCellValue('H'.$i, isset($value['sale']['recharge']) ? $value['sale']['recharge'] : '0');
            $sheet->setCellValue('I'.$i, isset($value['sale']['withdraw']) ? $value['sale']['withdraw'] : '0');
            $i++;
        }
        
        return $spreadsheet;
    }

    public function writeHeaderWorksheet($sheet, $columnNames){
        $columns = [];
        $a = 'A';
        foreach($columnNames as $index => $value){
            array_push($columns, $a++);
        }

        for($iterator=0;$iterator<count($columns);$iterator++){
            $sheet->setCellValue($columns[$iterator].'1', $columnNames[$iterator]);
            $sheet->getStyle($columns[$iterator].'1')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('888888');

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->getColor()
                ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setSize(11);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setBold(true);
            $sheet->getColumnDimension($columns[$iterator])->setAutoSize(true);
        }

        return $sheet;
    }
}
