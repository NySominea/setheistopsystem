<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\UserAccountLog;
use App\Model\Manager;
use App\Constants\Account;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class AccountLogController extends Controller
{
    public function show($id)
    {
        $logs = $this->getLog($id);
        $user = User::findOrFail($id);
        $logs = $logs->orderBy('id','DESC')->paginate(20);
        return view('user.log.index',compact('user','logs'));
    }

    public function ajaxGetLogInformation($id){
        $data = [];
        $result = false;
        $log = UserAccountLog::with('user','manager','toUser')->whereId($id)->first();
        if($log){
            $result = true;
            $data = [
                'transaction' => $log->log_number,
                'username' => $log->user ? $log->user->username : '',
                'manager' => $log->manager ? $log->manager->username : '',		
                'logType' => $log->log_type == Account::LOG_TYPE_IN ? 'In' : 'Out',	
                'amount' => currencyFormat($log->amount),	
                'balance' => currencyFormat($log->balance),
                'toAccount' => $log->to_type == Account::SYSTEM_ACCOUNT_TYPE 
                            ? getAdminLanguageData()[Account::systemAccountType($log->to_type)]
                            : getAdminLanguageData()[Account::systemAccountType($log->to_type)].($log->toUser ? '('.$log->toUser->username.')' : ''),	
                'dateTime' => (string)$log->created_at,
                'abstract' => getAdminLanguageData()[$log->abstract],
                'auditor' => '',
                'customSignature' => ''
            ];
        }
        
        return response()->json(['success' => $result, 'log' => $data]);
    }



public function download($id){
    
    $logs = $this->getLog($id);
    $path = $this->writeExcel($logs->orderBy('id','DESC')->get());
    return response()->json(['path' => $path, 'success' => true]);
}
public function getLog($id){

    $log_type = request()->log_type ?? 'all';
    $begin_time = request()->begin_time ?? '';
    $end_time = request()->end_time ?? '';
    $abstract = request()->abstract ?? 'all';

    $logs = UserAccountLog::with('account','toUser')->where('user_id',$id);
    if($log_type != 'all'){
        $logs->where('log_type',$log_type);
    }
    if($abstract != 'all'){
        $logs->where('abstract',$abstract);
    }
    if($begin_time && $end_time){
        $logs->whereBetween('created_at',[$begin_time,$end_time]);
    }
    return $logs;
}
  
public function writeExcel($data){
    $spreadsheet = new Spreadsheet();
    $spreadsheet->setActiveSheetIndex(0); 
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setTitle('User Log List');
    $spreadsheet = $this->writeDetailWorksheet($spreadsheet, $sheet, $data);
    
    $writer = new Xlsx($spreadsheet);
    if(!is_dir(public_path('backup'))) mkdir(public_path('backup'),0777,true);
    $writer->save(public_path('backup').'/'.'User-Log('.date('Y-m-d').').xlsx');
    $path = asset('backup/User-Log('.date('Y-m-d').').xlsx');
    return $path;
}

public function writeDetailWorksheet($spreadsheet, $sheet, $data){
    $columnNames = ['Transaction','Date Time','Abstract','Account','Amount (R)','Balance (R)','Rebate','Win Amount','To Account','Province'];
    $sheet = $this->writeHeaderWorksheet($sheet, $columnNames);
    $languages = getAdminLanguageData();
  
    $i = 2;
    foreach( $data as $l => $log){
        $sheet->setCellValueExplicit('A'.$i, $log->log_number, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
        $sheet->setCellValue('B'.$i, isset($log->created_at) ? $log->created_at : '');
        $sheet->setCellValue('C'.$i, $log->abstract != 'LANG_LABEL_TRANS' ? $languages[$log->abstract] : ($log->log_type == Account::LOG_TYPE_IN ? 'Received' : 'Transfered'));
        $sheet->setCellValue('D'.$i, $log->account ? $log->account->title : '');
        $sheet->setCellValue('E'.$i, isset($log->amount) ? $log->amount  : '0');
        $sheet->setCellValue('F'.$i, $log->balance);
        $sheet->setCellValue('G'.$i, isset($log->commission) ? $log->commission : '0');
        $sheet->setCellValue('H'.$i, isset($log->win_money) ? $log->win_money : '0');
   
        if($log->to_type == Account::SYSTEM_ACCOUNT_TYPE) {
            $sheet->setCellValueExplicit('I'.$i, $languages[Account::systemAccountType($log->to_type)], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
        }elseif($log->to_type == Account::USER_ACCOUNT_TYPE){
                $user = $log->toUser ? $log->toUser->username : '';
            $sheet->setCellValueExplicit('I'.$i,'('.$user.')' , \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
        }

        if($log->abstract == 'LANG_LABEL_TRANS'){
            $sheet->setCellValue('J'.$i, isset($log->toUser) ? $log->toUser->province : '');
        }
      
      
        $i++;
    }
    
    return $spreadsheet;
}

public function writeHeaderWorksheet($sheet, $columnNames){
    $columns = [];
    $a = 'A';
    foreach($columnNames as $index => $value){
        array_push($columns, $a++);
    }

    for($iterator=0;$iterator<count($columns);$iterator++){
        $sheet->setCellValue($columns[$iterator].'1', $columnNames[$iterator]);
        $sheet->getStyle($columns[$iterator].'1')
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('888888');

        $sheet->getStyle($columns[$iterator].'1')
            ->getFont()
            ->getColor()
            ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

        $sheet->getStyle($columns[$iterator].'1')
            ->getFont()
            ->setSize(11);

        $sheet->getStyle($columns[$iterator].'1')
            ->getFont()
            ->setBold(true);
        $sheet->getColumnDimension($columns[$iterator])->setAutoSize(true);
    }

    return $sheet;
}
    
}
