<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Account;
use App\Model\UserAccount;
use DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Response;

class AccountController extends Controller
{
    public function show($id)
    {
        DB::beginTransaction();
        try{
            $user = User::with('userAccounts')->whereId($id)->first();
            if(!$user)
                return redirect()->back()->withInput()->withError('Unknown User!');
            $accounts = $user->userAccounts;
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withInput()->withError('There was an error during operation!');
        }
        return view('user.account.index',compact('user','accounts'));
    }

    public function download(){

        ini_set('max_execution_time', 3600);
        ini_set('memory_limit', '-1');
        
        
        $users = User::with('cashAccount','ballCashAccount')
                        ->whereState(1)
                        ->whereIsTest(0)
                        ->whereIn('level',[0,2,3,4,5,6,7,8])
                        ->get(); 
        
        
        $path = $this->writeExcel($users);
        return response()->download($path);
        // return Response::download($path);
        // return response()->json(['path' => $path, 'success' => true]);
    }

    public function writeExcel($data){
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('List of Users');
        $spreadsheet = $this->writeDetailWorksheet($spreadsheet, $sheet, $data);
        
        $writer = new Xlsx($spreadsheet);
        if(!is_dir(public_path('backup'))) mkdir(public_path('backup'),0777,true);
        $writer->save(public_path('backup').'/'.'List-of-Users.xlsx');
        $path = asset('backup/List-of-Users.xlsx');
        return $path;
    }

    public function writeDetailWorksheet($spreadsheet, $sheet, $data){
        $columnNames = ['Account ID', 'Account Name','User Type','Province','5D Balance','Ball Balance'];
        $sheet = $this->writeHeaderWorksheet($sheet, $columnNames);
        
        $i = 2;
        foreach( $data as $l => $user){
            $sheet->setCellValueExplicit('A'.$i, $user->account_number, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $sheet->setCellValue('B'.$i, $user->username);
            $sheet->setCellValue('C'.$i, $this->getUserType($user->level));
            $sheet->setCellValue('D'.$i, $user->province);
            $sheet->setCellValue('E'.$i, $user->cashAccount ? $user->cashAccount->balance : 0);
            $sheet->setCellValue('F'.$i, $user->ballCashAccount ? $user->ballCashAccount->balance : 0);
          
            $i++;
        }
        
        return $spreadsheet;
    }

    public function writeHeaderWorksheet($sheet, $columnNames){
        $columns = [];
        $a = 'A';
        foreach($columnNames as $index => $value){
            array_push($columns, $a++);
        }

        for($iterator=0;$iterator<count($columns);$iterator++){
            $sheet->setCellValue($columns[$iterator].'1', $columnNames[$iterator]);
            $sheet->getStyle($columns[$iterator].'1')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('888888');

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->getColor()
                ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setSize(11);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setBold(true);
            $sheet->getColumnDimension($columns[$iterator])->setAutoSize(true);
        }

        return $sheet;
    }

    public function getUserType($lv){
        $type = "";

        switch ($lv) {
            case 8:
                $type = "Agent";
                break;
            
            case 7:
                $type = "Master Agent";
                break;

            case 6:
                $type = "Network Sale";
                break;
            
            case 5:
                $type = "Network Team Leader";
                break;

            case 4:
                $type = "Network Master";
                break;

            case 3:
                $type = "District Sale";
                break;

            case 2:
                $type = "Provincial Manager";
                break;

            case 0:
                $type = "Direct User";
                break;

            default:
                $type = "";
                break;
        }

        return $type;
    }
}