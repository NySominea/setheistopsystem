<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Validator;

use App\Model\BalanceRequest;
use App\User;

class BalanceRequestController extends Controller
{
    public function index()
    {
        $keyword = request()->get('keyword','');
        $requests = BalanceRequest::with('user')
                                    ->when($keyword != '',function($q) use ($keyword){
                                        $q->whereHas('user', function ($query) use ($keyword) {
                                            return $query->where('username','LIKE','%'.$keyword.'%')
                                                ->orWhere('account_number','LIKE','%'.$keyword.'%')
                                                ->orWhere('phone','LIKE','%'.$keyword.'%');
                                        })
                                        ->orWhere('agent_name',$keyword)
                                        ->orWhere('agent_ref_no',$keyword);
                                    })
                                    ->when(request()->get('operation','all') != 'all', function($q){
                                        $q->whereOperation(request()->operation);
                                    })
                                    ->when(request()->get('status','all') != 'all', function($q){
                                        $q->whereStatus(request()->status);
                                    })
                                    ->orderBy('created_at','DESC')
                                    ->paginate(30);

        return view('balance-request.index',compact('requests'));
    }

    public function edit($id)
    {
        $request = BalanceRequest::with('user')->whereId($id)->first();

        return view('balance-request.add-update',compact('request'));
    }

    public function update(Request $request, $id)
    {

        DB::beginTransaction();
        try {

            $item = BalanceRequest::with('user')->whereId($id)->first();

            if(!$item){
                return back()->withError('Someting went wrong! Please try again...');
            }
            
            $item->update(['status' => $request->status]);
            DB::commit();

            return redirect()->route('users.balance-request.index')->withSuccess('You have successfully updated the request of user '.$item->user->username);
        } catch (Exception $ex) {
            DB::rollback();

            return back()->withError('Someting went wrong! Please try again...');
        }
    }

}
