<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Constants\UserAccountConstant;
use App\User;
use App\Model\Manager;
use App\Model\UserAccount;
use App\Model\UserAccountLog;
use App\Model\SystemAccount;
use App\Model\SystemAccountLog;
use App\Constants\Account;
use App\Model\ManagerLog;
use App\Model\BalanceRequest;
use DB;
use Hash;

class AccountTakeCashController extends Controller
{
    public function edit($id)
    {
        $accounts = SystemAccount::whereState(1)
                                ->where('id',2)
                                ->pluck('title','id');
        $user = User::with('cashAccount')->whereId($id)->first();
        if(!$user) return redirect()->back()->withError('Unknown User!');
        return view('user.takecash.add-update',compact('accounts','user'));
    }

    public function update(Request $request, $id){
        $this->validate($request,[
            'amount' => 'required|min:1',
            'pay_password' => 'required',
            'user_pay_password' => 'required',
        ],[
            'amount.min' => 'Please enter the amount.',
            'pay_password.required' => 'The pay password field is required.',
            'user_pay_password.required' => 'The user pay password field is required.'
        ]);
        $data = $request->all();
        $data['amount'] = stringToDouble($data['amount']);
        $auth = auth()->user();
        
        DB::beginTransaction();
        try{
            $user = User::with('cashAccount')->whereId($id)->first();

            if(!Manager::checkPayPassword($data['pay_password'])) 
                return back()->withInput()->withErrors(['pay_password' =>'The pay password does not match!']);             
            
            if(!User::checkPayPassword($user->id,$data['user_pay_password'])) 
                return back()->withInput()->withErrors(['user_pay_password' =>'The user pay password does not match!']); 
               
            $account = SystemAccount::find($data['account']); 
            if(!$account) 
                return redirect()->back()->withInput()->withErrors(['account' => 'Unknown Account!']);

            
            if(!$user) 
                return redirect()->back()->withInput()->withError('Unknown User!');

            if($user->is_shop)
                return redirect()->back()->withInput()->withError('The operation does not work with shop account!');
            
            $userCashAccount = $user->cashAccount ?:UserAccount::createCashAccount($user->id);

            if($userCashAccount->balance < $data['amount'])
                return back()->withInput()->withErrors(['amount' =>'The amount is larger than the user account balance!']);

            if($account->balance < $data['amount'])
                return back()->withInput()->withError('The amount is larger than the system account balance!');

            $userCashAccount->decrement('balance',$data['amount']);
            $account->decrement('balance',$data['amount']);
            
            SystemAccountLog::create([
                'amount' => $data['amount'],
                'balance' => $account->balance,
                'abstract' => 'LANG_LABEL_WITHDRAW',
                'log_number' => SystemAccountLog::generateLogNumber(Account::LOG_TYPE_OUT),
                'log_type' => Account::LOG_TYPE_OUT,
                'to_type' => Account::USER_ACCOUNT_TYPE,
                'account_id' => $account->id,
                'to_account_id' => $user->id,
                'manager_id' => $auth->id
            ]);

            UserAccountLog::create([
                'user_id' => $user->id,
                'account_id' => $userCashAccount->id,
                'log_type' => Account::LOG_TYPE_OUT,
                'is_transfer' => 0,
                'amount' => $data['amount'],
                'balance' => $userCashAccount->balance,
                'commission' => 0,
                'win_money' => 0,
                'to_type' => Account::SYSTEM_ACCOUNT_TYPE,
                'to_user_id' => 0,
                'to_account_id' => $account->id,
                'abstract' => 'LANG_LABEL_WITHDRAW',
                'manager_id' => $auth->id,
                'log_number' => UserAccountLog::generateLogNumber(Account::LOG_TYPE_OUT,$user->id),
            ]);

            // Auto Issue Balance Request
            if(request()->br){
                $br = BalanceRequest::whereUserId($id)
                                    ->whereId(request()->br)
                                    ->first(); 
                $br->update(['status' => UserAccountConstant::COMPLETED]);
            }
            // End Auto Issue Balance Request
            
            pushNotificationToUser( 
                api_trans('notification.balance_withdraw'),
                api_trans('notification.withdraw_msg',['amount' => currencyFormat($data['amount']), 'name' => $auth->username]),
                [(string)$user->id]
            );

            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
            return redirect()->back()->withInput()->withError('There was an error during operation!');
        }

        // CREATE ManagerLog in TakeCask  Witdraw
        $arr = ['action' => 'withdraw',
                'user_id' => $user->id,
                'user_befor_balance' => $userCashAccount->balance + $data['amount'],
                'user_new_balance' => $userCashAccount->balance ,
                'sys_account_id' => $account->id,
                'sys_account_before_balance' => $account->balance + $data['amount'],
                'sys_account_new_balance' => $account->balance  ];
         $mangerId = auth()->id();
         ManagerLog::createManagerLog($mangerId, $arr); 

        return redirect()->route('users.accounts.show',$user->id)->withSuccess('You have just recharge your cash account with amount of '.$request->amount.'!');
    }
}