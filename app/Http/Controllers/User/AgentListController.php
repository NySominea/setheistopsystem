<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use DB;
use App\Model\ManagerLog;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class AgentListController extends Controller
{
  
    public function index()
    {
        $begin_time = request()->begin_time ?? '';
        $end_time = request()->end_time ?? '';
        $state = request()->state ?? 1;
        $keyword = request()->keyword ?? '';
        $data = $this->getUser($begin_time,$end_time,$state,$keyword);
      
        $id = $data['id'];
        $user = $data['user'];
        $users = $user->paginate(15);
        return view('user.agent-list.index',compact('id','users'));
    }


    public function edit($id)
    {
        $user = User::find($id);
        return view('user.agent-list.update', compact('user')); 
    }

    
    public function update(Request $request, $id)
    {
        $request->validate([
            'username' => 'required|unique:users,username,'.$id,
            'account_number' => 'required|unique:users,account_number,'.$id
        ]);

        $change = 'edit setting, ';

        if($request->password){
            $this->validate($request,[
                'password' => 'required|confirmed|min:6',
                'password_confirmation' => 'required',
            ]);
            $change = $change.'edit passowrd, ';
        }

        if($request->pay_pass){
            $this->validate($request,[
                'pay_pass' => 'required|confirmed|min:6',
                'pay_pass_confirmation' => 'required'
            ]);
            $change = $change.'edit pay pass, ';
        }
        
       $this->saveDB($request->all(), $id);
       // Create ManagerLog for update user
       $arr = ['action' => 'edit user', 'to_user_id' => $id,
                'changed' => isset($request->password) ? $change : $change ];
       $mangerId = auth()->id();
       ManagerLog::createManagerLog($mangerId, $arr);

       return redirect()->route('users.agent-lists.index');
    }

    
    public function destroy($id)
    {
        //
    }
    
    public function getUser($begin_time,$end_time,$state,$keyword){

        $users = User::where('level',8)->where('is_shop',0)->where('is_test',0)->with('parent');
      
        $users = $users->when($state != 'all',function($q) use ($state){
            return $q->where('state',$state);
        })
        ->when($keyword != '',function($q) use($keyword){
            return $q->where('username','like','%'.$keyword.'%')
                    ->orWhere('account_number','like','%'.$keyword.'%');
        })
        ->when(($begin_time && $end_time) != '',function($q) use($begin_time,$end_time){
            return $q->whereBetween('created_at',[$begin_time,$end_time]);
        })
        ->orderBy('id','DESC');
        
        $id = User::whereIN('username',['new PP','Pursat','kampong thom A','Prey Veng'])->pluck('id')->toArray();
        
        // dd($users->get(),$state);
        return ['user' => $users, 'id' => $id];
    }
 
    public function saveDB($data, $id = null){

        $keys = ['username','account_number','real_name','email','phone','address','contact_number','sale_supervisor',
        'sale_area_manager','sale_director','province','district'];

        foreach($keys as $value){
            $data[$value] = $data[$value];
         }
        if(!isset($id)){
            $data['parent_id'] = $data['parent_id'];
            $level = $data['level'];
            $data['level'] = $level;    

            if($data['parent_id']>0){
                $data['level'] = $level + 1;
                $parent = User::find($data['parent_id']);
                
                for($i=1;$i<$parent->level;$i++){
                    $data['l'.$i.'_id'] = $parent->{'l'.$i.'_id'};
                }
                $data['l'.$parent->level.'_id'] = $parent->id;
                
                }  
            }
        
        DB::beginTransaction();
        try {
            $user = isset($id) ? User::find($id) : new User;
            if(!$user) return redirect()->back()->withError('There is no record found!');
            $data['pass_salt'] = isset($id) ? $user->pass_salt : rand(10000000,99999999);

            if(isset($data['user_type'])){                
                $data['user_type'] = $data['user_type'];
            }

            if($data['password'] && ($data['password'] == $data['password_confirmation'])){                
                $data['password'] = md5($data['password'].$data['pass_salt']);
            }else{
                unset($data['password']);
            }
            if($data['pay_pass'] && ($data['pay_pass'] == $data['pay_pass_confirmation'])){
                $data['pay_pass'] = md5($data['pay_pass'].$data['pass_salt']);
            }else{
                unset($data['pay_pass']);
            }

            if(isset($id)){
                $data['level'] = $user->level;
                $data['parent_id'] = $user->parent_id;
            }
            $user->fill($data);
            $user->save();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
        }
        
    }
    
    public function download(){

        ini_set('max_execution_time', 3600);
        ini_set('memory_limit', '-1');

        $begin_time = request()->begin_time ?? '';
        $end_time = request()->end_time ?? '';
        $state = request()->state ?? '1';
        $keyword = request()->keyword ?? '';
        $data = $this->getUser($begin_time,$end_time,$state,$keyword);
    
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        
        $sheet->setTitle('Agents list');
        $spreadsheet = $this->writeAgentWorksheet($spreadsheet, $sheet, $data);
        $writer = new Xlsx($spreadsheet);

        if(!is_dir(public_path('backup'))) mkdir(public_path('backup'),0777,true);
        $writer->save(public_path('backup').'/'.'Agent Lists('.date('Y-m-d').').xlsx');
        $path = asset('backup/Agent Lists('.date('Y-m-d').').xlsx');
        return response()->json(['path' => $path, 'success' => true]);
    }

    public function writeAgentWorksheet($spreadsheet, $sheet, $data){
        $users = [];
        $users = $data['user'];
        $users = $users->get();
        $columnNames = [
        'AccountNo',
        'UserName',
        'Contact',
        'Address',
        'Province',
        'District',
        'Create Date',
        'Sale Supervisor',
        'Sale Area Manager',
        'Sale Director',
        'Sale KPI'

        ];
        
        $sheet = $this->writeHeaderWorksheet($sheet, $columnNames);
        $i = 2;
        foreach($users as $value){
            $sheet->setCellValue('A'.$i,$value->account_number);
            $sheet->setCellValue('B'.$i,$value->username);
            $sheet->setCellValueExplicit('C'.$i,$value->phone,\PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $sheet->setCellValue('D'.$i,$value->address);
            $sheet->setCellValue('E'.$i,$value->province);
            $sheet->setCellValue('F'.$i,$value->district);
            $sheet->setCellValue('G'.$i,$value->created_at);
            $sheet->setCellValue('H'.$i,$value->sale_supervisor);
            $sheet->setCellValue('I'.$i,$value->sale_area_manager);
            $sheet->setCellValue('J'.$i,$value->sale_director);
            if(in_array($value->l1_id,$data['id']) || in_array($value->l2_id,$data['id'])){
                $sheet->setCellValue('K'.$i,$value->parent->username);
            }else{
                $sheet->setCellValue('K'.$i,$value->parent->parent->username);
            }
            
            $i++;
        }

        return $spreadsheet;
    }

    public function writeHeaderWorksheet($sheet, $columnNames){
        $columns = [];
        $a = 'A';
        foreach($columnNames as $index => $value){
            array_push($columns, $a++);
        }

        for($iterator=0;$iterator<count($columns);$iterator++){
            $sheet->setCellValue($columns[$iterator].'1', $columnNames[$iterator]);
            $sheet->getStyle($columns[$iterator].'1')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('888888');

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->getColor()
                ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setSize(11);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setBold(true);

                $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setBold(true);
            $sheet->getColumnDimension($columns[$iterator])->setAutoSize(true);
        }

        return $sheet;
    }

}
