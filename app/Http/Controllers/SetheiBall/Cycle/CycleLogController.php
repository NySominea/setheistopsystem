<?php

namespace App\Http\Controllers\SetheiBall\Cycle;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SetheiBall\Cycle;
use Illuminate\Pagination\LengthAwarePaginator;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class CycleLogController extends Controller
{
    public function __construct(){
        $this->middleware('permission:view-5d-cycle-log')->only(['index']);
    }

    public function index()
    {
        $logs = Cycle::where('state',0)
                    ->where('has_released',1)
                    ->orderBy('id','DESC')
                    ->paginate(30);

        return view('sethei-ball.cycle.log.index',compact('logs'));
    }
}
