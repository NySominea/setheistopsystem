<?php

namespace App\Http\Controllers\SetheiBall\Game;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SetheiBall\GameRebate;
use App\Model\SetheiBall\GameType;
use DB;

class GameController extends Controller
{
    public function __construct(){
        $this->middleware('permission:view-ball-history-report')->only(['index']);
        $this->middleware('permission:ball-game-setting-modification')->only(['store']);
    }

    public function index()
    {   
        $rebates = GameRebate::with('gameType')->get();
        $games = GameType::all();
        return view('sethei-ball.game.index',compact('rebates','games'));
    }


    public function store(Request $request)
    {
        $this->saveToDB($request->all());
        return redirect()->route('ball.game.setting.index')->withSuccess('You have just updated game rebates successfully!');
    }


    public function saveToDB($data){ 
        
        foreach($data['rebates'] as $id => $row){ 
            $rebate = GameRebate::find($id);
            if($rebate){
                DB::beginTransaction();
                try{
                    $rebate->fill($row);
                    $rebate->save();
                    DB::commit();
                }catch(Exception $ex){
                    DB::rollback();
                }
            }
        }
    }
}
