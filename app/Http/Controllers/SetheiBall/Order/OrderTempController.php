<?php

namespace App\Http\Controllers\SetheiBall\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SetheiBall\OrderTemp;
use App\Model\SetheiBall\Order;
use App\Model\SetheiBall\Cycle;
use App\User;
use DB;
use App\Model\SetheiBall\GameType;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class OrderTempController extends Controller
{
    private $table;

    public function __construct(){
        $this->middleware('permission:view-ball-order-list')->only(['ajaxGetOrderDetail','index']);
        $this->middleware('permission:export-ball-order-list')->only(['downloadExcel']);
    }

    public static function withTable( $table ) {
        $instance = new self();
        $instance->table = $table;
        return $instance;
    }

    public function index(Request $request)
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');

        $this->table = OrderTemp::getModel()->getTable();

        $data = request()->user_id ? $this->getOrderDataByUser() : $this->getOrderDataSummary();
       
        return view('sethei-ball.order.temp.index',compact('data'));
    }

    public function downloadExcel(){
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        $table = 'order_temps'; 
        $historyReport = false;
        $request = request();  
        $data = $this->structureTicketDataForEachUser($table,$request);
        $path = $this->writeExcel($data);
        return response()->json(['path' => $path, 'success' => true]);
    }

    public function writeExcel($data, $isHistoryList = false){
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Sale Report');
        $spreadsheet = $this->writeDetailWorksheet($spreadsheet, $sheet, $data['data'], $isHistoryList);
        
        $writer = new Xlsx($spreadsheet);
        if(!is_dir(public_path('backup'))) mkdir(public_path('backup'),0777,true);
        $filename = "Ball_Sale_Report(".date('Y-m-d').").xlsx";
        if(request()->cycle_id){
            $cycle = Cycle::whereId(request()->cycle_id)->first();
            if($cycle) $filename = "Ball_Sale_Report(".$cycle->cycle_sn.").xlsx";
        }
        $writer->save(public_path('backup').'/'.$filename);
        $path = asset('backup/'.$filename);
        return $path;
    }

    public function writeDetailWorksheet($spreadsheet, $sheet, $data, $isHistoryList){
        $columnNames = $isHistoryList 
                        ? ['Account No','User','Ticket','Post Time', 'Order Bet','Bet Amount (R)','Ticket Amount (R)', 'Ticket Win Amount (R)']
                        : ['Account No','User','Ticket','Post Time', 'Order Bet','Bet Amount (R)','Ticket Amount (R)'];
        
        $sheet = $this->writeHeaderWorksheet($sheet, $columnNames);
        $i = 2;
        foreach( $data as $l => $user){
            $sheet->setCellValue('A'.$i, $user['accountno']);
            $sheet->setCellValue('B'.$i, $user['username']);
            $j = 0; 
            foreach($user['ticket'] as $key => $ticket){
                $sheet->setCellValueExplicit('C'.($i + $j), $ticket['ticket'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $sheet->setCellValue('D'.($i + $j), $ticket['created_at']);
                $sheet->setCellValue('G'.($i + $j), preg_replace("/([^0-9\\.])/i", "", $ticket['ticketAmountR']));
                if($isHistoryList)
                    $sheet->setCellValue('H'.($i + $j), preg_replace("/([^0-9\\.])/i", "", $ticket['ticketWinAmountR']));
                foreach($ticket['orders'] as $index => $o){
                    $sheet->setCellValueExplicit('E'.($i + $j + $index), implode(", ",[$o['no1'],$o['no2'],$o['no3'],$o['no4'],$o['no5'],$o['no6']]), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $sheet->setCellValue('F'.($i + $j + $index), preg_replace("/([^0-9\\.])/i", "", $o['amountR']));
                }
                $j+=count($ticket['orders']);
            }
            $i+=$j;
        }
        
        return $spreadsheet;
    }

    public function writeHeaderWorksheet($sheet, $columnNames){
        $columns = [];
        $a = 'A';
        foreach($columnNames as $index => $value){
            array_push($columns, $a++);
        }

        for($iterator=0;$iterator<count($columns);$iterator++){
            $sheet->setCellValue($columns[$iterator].'1', $columnNames[$iterator]);
            $sheet->getStyle($columns[$iterator].'1')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('888888');

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->getColor()
                ->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setSize(11);

            $sheet->getStyle($columns[$iterator].'1')
                ->getFont()
                ->setBold(true);
            $sheet->getColumnDimension($columns[$iterator])->setAutoSize(true);
        }

        return $sheet;
    }

    public function getOrderDataSummary(){ 
        $data = [];
        $total = [];
        $rielToDollar = exchangeRateFromRielToDollar();
        $relationship = $this->table == OrderTemp::getModel()->getTable() ? 'ballOrderTemps' : 'ballOrders';
        $orderRebateRelationship = $this->table == OrderTemp::getModel()->getTable() ? 'ballOrderRebateTemps' : 'ballOrderRebates';
        
        // Get List of users for current orders
        $users = User::with($relationship,$orderRebateRelationship)
                    ->when(request()->keyword != '',function($q){
                        return $q->where('username','LIKE','%'.request()->keyword.'%')
                                ->orWhere('account_number','LIKE','%'.request()->keyword.'%')
                                ->orWhere('phone','LIKE','%'.request()->keyword.'%');
                    })
                    ->orderBy('account_number','ASC')
                    ->has($relationship, '>', 0)
                    ->when(request()->has('cycle_id'),function($query) use ($relationship){
                        $query->whereHas($relationship,function($q){
                            $q->where('cycle_id', request()->cycle_id);
                        });
                    })
                    ->when(request()->has('begin_date') && request()->has('end_date'),function($query) use ($relationship){
                        $query->whereHas($relationship,function($q){
                            $q->whereBetween('created_at', [request()->begin_date, request()->end_date]);
                        });
                    })
                    ->select('id','username','account_number','phone','parent_id')
                    ->get();
        
        // End Get List of users
        
        $grandTotalOrder = 0;
        $grandTotalTicket = 0; 
        $grandTotalAmount = 0;
        $grandTotalRebate = 0;
        $grandTotalParentRebate = 0;
        $grandTotalWinAmount = 0;
        $grandTotalProfitAmount = 0;
        
        foreach($users as $user){
            $totalTicket = $user->{$relationship}->groupBy('ticket')->count();
            $totalOrder = $user->{$relationship}->count();
            $totalAmount = $user->{$relationship}->sum('amount');
            $totalRebate = $user->{$orderRebateRelationship}->sum('rebate_amount');
            $totalParentRebate = $user->ancestors()->take(4)->get()->sum(function($q) use($orderRebateRelationship) { 
                                    return $q->{$orderRebateRelationship}->sum('rebate_amount');
                                });
            $totalWinAmount = $user->{$relationship}->sum('win_amount');

            $data[] = [
                'user'      => $user->toArray(),
                'totalTicket'   => $totalTicket,
                'totalOrder'    => $totalOrder,
                'totalAmountR'    => $totalAmount,
                'totalAmountD'    => $totalAmount * $rielToDollar,
                'totalRebateR' => $totalRebate,   
                'totalRebateD' => $totalRebate * $rielToDollar,
                'totalParentRebateR' => $totalParentRebate,   
                'totalParentRebateD' => $totalParentRebate * $rielToDollar,
                'totalWinAmountR'=> $totalWinAmount,
                'totalWinAmountD'=> $totalWinAmount * $rielToDollar,
                'totalProfitAmountR' => $totalAmount - $totalWinAmount - $totalRebate,
                'totalProfitAmountD' => ($totalAmount - $totalWinAmount - $totalRebate)  * $rielToDollar,
            ]; 
            
            $grandTotalTicket += $totalTicket;
            $grandTotalOrder += $totalOrder;
            $grandTotalAmount += $totalAmount;
            $grandTotalRebate += $totalRebate;
            $grandTotalParentRebate += $totalParentRebate;
            $grandTotalWinAmount += $totalWinAmount;
            $grandTotalProfitAmount += $totalAmount - $totalWinAmount - $totalRebate - $totalParentRebate;
            
        }
        $total = [
            'grandTotalUser' => $users->count(),
            'grandTotalTicket' => $grandTotalTicket,
            'grandTotalOrder' => $grandTotalOrder,
            'grandTotalAmountR' => $grandTotalAmount,
            'grandTotalAmountD' => $grandTotalAmount * $rielToDollar,
            'grandTotalRebateR' => $grandTotalRebate,
            'grandTotalRebateD' => $grandTotalRebate * $rielToDollar,
            'grandTotalParentRebateR' => $grandTotalParentRebate,
            'grandTotalParentRebateD' => $grandTotalParentRebate * $rielToDollar,
            'grandTotalWinAmountR' => $grandTotalWinAmount,
            'grandTotalWinAmountD' => $grandTotalWinAmount * $rielToDollar,
            'grandTotalProfitAmountR' => $grandTotalProfitAmount,
            'grandTotalProfitAmountD' => $grandTotalProfitAmount * $rielToDollar
        ];
        
        return ['data' => $data, 'total' => $total];
    }

    public function getOrderDataByUser(){
        $data = [];
        $totalAmount = 0;
        $totalWinAmount = 0;
        $user_id = request()->user_id;
        $rielToDollar = exchangeRateFromRielToDollar();
        $query = $this->table == Order::getModel()->getTable() ? Order::select()
                                    : OrderTemp::select();
        $userOrders = $query
                    ->when($user_id ,function($q) use ($user_id){
                        return $q->where(['user_id' => $user_id]);
                    })
                    ->when($user_id && request()->cycle_id,function($q) use ($user_id){
                        return $q->where(['user_id' => $user_id, 'cycle_id' => request()->cycle_id]);
                    })
                    ->when(request()->cycle_id,function($q) {
                        return $q->where('cycle_id',request()->cycle_id);
                    })
                    ->when($user_id && request()->begin_date && request()->end_date,function($q) use ($user_id){
                        return $q->where(['user_id' => $user_id])->whereBetween('created_at', [request()->begin_date, request()->end_date]);
                    })
                    ->when(request()->begin_date && request()->end_date,function($q){
                        return $q->whereBetween('created_at', [request()->begin_date, request()->end_date]);
                    })
                    ->with('cycle')
                    ->orderBy('created_at')
                    ->get(); 
                          
        $totalWinAmount = 0;
        $totalAmount = 0; 
        $users = User::whereIn('id',array_keys($userOrders->groupBy('user_id')->toArray()))->get()->keyBy('id');
        foreach($userOrders->groupBy('user_id') as $userId => $user){ 
            $t = []; 
            foreach($user->groupBy('ticket') as $no => $ticket){ 
                $o = [];
                foreach($ticket as $order){
                    $o[] = [
                        'amountR' => $order->amount,
                        'amountD' => $order->amount * $rielToDollar,
                        'winAmountR' =>$order->win_amount,
                        'winAmountD' => $order->win_amount  * $rielToDollar,
                        'prize' => $order->win_prize,
                        'no1' => $order->no1,
                        'no2' => $order->no2,
                        'no3' => $order->no3,
                        'no4' => $order->no4,
                        'no5' => $order->no5,
                        'no6' => $order->no6
                    ];
                }
                $t[] = [
                    'cycle'     => isset($ticket[0]->cycle) ? $ticket[0]->cycle->cycle_sn : '',
                    'ticket'    => $ticket[0]->ticket,
                    'created_at'=> date('Y-m-d H:i A', strtotime($ticket[0]->created_at)),
                    'ticketAmountR' => $ticket->sum('amount'),
                    'ticketAmountD' => $ticket->sum('amount') * $rielToDollar,
                    'ticketWinAmountR'=> $ticket->sum('win_amount'),
                    'ticketWinAmountD'=> $ticket->sum('win_amount') * $rielToDollar,
                    'orders'    => $o
                ];
            } 
            
            if(isset($users[$userId])){ 
                $data[] = [ 
                    'username'  => $users[$userId]->username,
                    'account_number' => $users[$userId]->account_number,
                    'amountR' => $user->sum('amount'),
                    'amountD' => $user->sum('amount') * $rielToDollar,
                    'winAmountR' => $user->sum('win_amount'),
                    'winAmountD' => $user->sum('win_amount') * $rielToDollar,
                    'orders_no' => $user->count(),
                    'tickets'    => $t,
                ]; 
                $totalWinAmount += $user->sum('win_amount');
                $totalAmount += $user->sum('amount');
            }
        }   
        $total = [
            'totalWinAmountR' => $totalWinAmount,
            'totalWinAmountD' => $totalWinAmount * $rielToDollar,
            'totalAmountR' => $totalAmount,
            'totalAmountD' => $totalAmount * $rielToDollar
        ];
        
        return ['data' => $data, 'total' => $total];
    }

    public function ajaxGetOrderDetail($ticket){
        $success = false;
        $data = [];
        $orders = OrderTemp::with('user')->whereTicket($ticket)->get();
        if($orders && $orders->count() > 0){
            $data = [
                'ticket' => $orders[0]->ticket,
                'betAmount' => currencyFormat($orders->sum('amount')),
                'winAmount' => currencyFormat($orders->sum('win_amount')),
                'created_at'=> date('Y-m-d H:i A', strtotime($orders[0]->created_at)),
            ];

            // if($orders[0]->user->level != 0){
            //     for($i=1;$i<$orders[0]->user->level + 1;$i++){ 
            //         $data['rebates'][] = [
            //             'levelTitle' => getUserLevelTitle($i),
            //             'username' => $orders[0]->parent($i)->username,
            //             'rebate' => currencyFormat($orders->sum('l'.$i.'_rebate'))
            //         ];
            //     }
            // }else{
            //     $data['rebates'][] = [
            //         'levelTitle' => "Direct User",
            //         'username' => $orders[0]->user->username,
            //         'rebate' => currencyFormat(0)
            //     ];
            // }
            $success = true;
        }
        return response()->json(['success' => $success, 'data' => $data]);
    }
}
