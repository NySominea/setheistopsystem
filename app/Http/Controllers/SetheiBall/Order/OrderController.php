<?php

namespace App\Http\Controllers\SetheiBall\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SetheiBall\Cycle;
use App\Model\SetheiBall\OrderTemp;
use App\Model\SetheiBall\Order;
use App\User;
use DB;
use App\Model\SetheiBall\GameType;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Http\Controllers\SetheiBall\Order\OrderTempController;

class OrderController extends Controller
{
    public function __construct(){
        $this->middleware('permission:view-ball-history-report')->only(['ajaxGetOrderDetail','showSelectionForm','index']);
        $this->middleware('permission:export-ball-history-report')->only(['downloadExcel','downloadSumBet']);
    }

    public function index(Request $request)
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');

        $data = [];
        $cycle = request()->cycle_id ? Cycle::findOrFail(request()->cycle_id) : null;
        $orderTempController = OrderTempController::withTable(Order::getModel()->getTable());

        $data = request()->user_id ? $orderTempController->getOrderDataByUser() : $orderTempController->getOrderDataSummary();
        
        return view('sethei-ball.order.report.index',compact('data','cycle'));
    }

    public function downloadExcel(){
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');

        $orderTempController = new OrderTempController;
        $table = Order::getModel()->getTable();  
        $request = request();
        $data = $orderTempController->structureTicketDataForEachUser($table,$request);
        $path = $orderTempController->writeExcel($data,true);
        return response()->json(['path' => $path, 'success' => true]);
    }

    public function showSelectionForm(){
        $cycles = Cycle::orderBy('cycle_sn','DESC')->whereStateAndHasReleased(0,1)->pluck('cycle_sn','id')->toArray();
        $cycles = ['0' => 'Choose'] + $cycles;
        return view('sethei-ball.order.report.selection-form',compact('cycles'));
    }

    public function ajaxGetOrderDetail($ticket){
        $success = false;
        $data = [];
        $orders = Order::with('user')->whereTicket($ticket)->get();
        if($orders && $orders->count() > 0){
            $data = [
                'ticket' => $orders[0]->ticket,
                'betAmount' => currencyFormat($orders->sum('amount')),
                'winAmount' => currencyFormat($orders->sum('win_amount')),
                'created_at'=> date('Y-m-d H:i A', strtotime($orders[0]->created_at)),
            ];

            // if($orders[0]->user->level != 0){
            //     for($i=1;$i<$orders[0]->user->level + 1;$i++){ 
            //         $data['rebates'][] = [
            //             'levelTitle' => getUserLevelTitle($i),
            //             'username' => $orders[0]->parent($i)->username,
            //             'rebate' => currencyFormat($orders->sum('l'.$i.'_rebate'))
            //         ];
            //     }
            // }else{
            //     $data['rebates'][] = [
            //         'levelTitle' => "Direct User",
            //         'username' => $orders[0]->user->username,
            //         'rebate' => currencyFormat(0)
            //     ];
            // }
            $success = true;
        }
        return response()->json(['success' => $success, 'data' => $data]);
    }
}
