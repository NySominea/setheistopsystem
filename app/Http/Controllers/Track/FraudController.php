<?php

namespace App\Http\Controllers\Track;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Cycle;
use App\User;
use App\Model\Order;
use App\Model\OrderTemp;
use DB;
use Location;
use Spatie\Geocoder\Geocoder;

class FraudController extends Controller
{
    function detailFraud(){
        $user = User::find(request()->get('user_id'));
        $cycle = Cycle::find(request()->get('cycle_id'));

        if($cycle->has_released){
            $orders = Order::orderBy('id','desc');
        }else{
            $orders = OrderTemp::orderBy('id','desc');
        }

        $orders->where('cycle_id',$cycle->id)->where('user_id',$user->id);
        $orders = $orders->get();
        $client = new \GuzzleHttp\Client();
        $geocoder = new Geocoder($client);
        $geocoder->setApiKey(config('geocoder.key'));
        $final_orders = [];

        foreach($orders as $order){
            $temp = $order;
            if($order->ip){
                $location = Location::get($order->ip);
		$result = $geocoder->getAddressForCoordinates($location->latitude,$location->longitude);
		$temp->ip = '';
		foreach($result['address_components'] as $component){
			$temp->ip .= $component->long_name.', ';
		}
               
            }
            $final_orders[] = $temp;
        }

        return view('tracking.fraud_agent.detail',compact('orders','final_orders'));
    }

    function index(){
        $cycles = Cycle::orderBy('id','desc')->take(40)->pluck('cycle_sn','id');
        $temp = [];

        if(request()->get('cycle_id')){
            $cycle = Cycle::find(request()->get('cycle_id'));
            if($cycle->has_released){
                $result = collect(
                    DB::select(
                        DB::raw(
                            "SELECT
                                outside.username, 
                                outside.account_number,
                                outside.id,
                                (select u.username from users u where u.id = outside.l4_id)  as director,
                                (select u.username from users u where u.id = outside.l5_id)  as area_manager
                                from d_orders, 
                                users outside where 
                                d_orders.user_id = outside.id 
                                and device_name is not null 
                                and device_uuid is not null
                                and cycle_id = $cycle->id group by 
                            outside.username,
                            outside.account_number,
                            outside.id,device_name,
                            device_uuid,ip,
                            outside.l4_id,outside.l5_id"
                        )
                ));
	    }else{
                $result = collect(DB::select(
                    DB::raw(
                       "SELECT
                            outside.username, 
                            outside.account_number,
                            outside.id,
                            (select u.username from users u where u.id = outside.l4_id)  as director,
                            (select u.username from users u where u.id = outside.l5_id)  as area_manager
                            from d_order_temps, 
                            users outside where 
                            d_order_temps.user_id = outside.id 
                            and device_name is not null 
                            and device_uuid is not null
                            and cycle_id = 2087 group by 
                            outside.username,
                            outside.account_number,
                            outside.id,device_name,
                            device_uuid,ip,
                            outside.l4_id,outside.l5_id"
                    )
                ));
            }

            $temp =  $result->groupBy('username');
        }

        return view('tracking.fraud_agent.index',compact('cycles','temp'));
    }
}
