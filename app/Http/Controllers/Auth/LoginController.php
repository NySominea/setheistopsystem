<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Session;
use App\Model\Manager;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/dashboard';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {   
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

    public function validateLogin(Request $request)
    {   
        $request->validate([
            'username' => 'required|string',
            'password' => 'required|string',
        ]);
    }

    public function attemptLogin(Request $request)
    {   
        $user = Manager::where(['username' => $request->username])->first();
        if(!$user || $user->password != md5($request->password.$user->pass_salt) 
            || $user->state !=1){
            throw ValidationException::withMessages([
                'failed' => [trans('auth.failed')],
            ]);
        }
        
        return $user ? auth()->login($user,$request->filled('remember')) : false;
    }

    public function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            'failed' => [trans('auth.failed')],
        ]);
    }

    public function logout(Request $request)
    {
        auth()->logout();

        $request->session()->invalidate();

        return redirect()->route('login');
    }
}
