<?php

namespace App\Http\Services;

use App\Model\Sethei5D\OrderBetTemp;
use App\Model\Sethei5D\OrderTemp;
use App\Model\GameCategory;
use App\Model\Sethei5D\Cycle;
use App\Model\Sethei5D\Order;
use App\Model\OrderNumberTemp;
use App\Model\Sethei5D\GameRebate;
use Jenssegers\Agent\Agent;
use App\User;
use Carbon\Carbon;
use Exception;
use Str;
use DB;
use Batch;
use Request;

class OrderService {

    private const S = 'S';
    private const X = 'X';
    private const X3 = 'X3';
    private const RANGE = 'RANGE';
    private const NORMAL = 'NORMAL';

    private const GAME_2D = '2D';
    private const GAME_3D = '3D';
    private const GAME_5D = '5D';

    private $bet = [];
    private $bet_contents = [];
    private $order_bets = [];
    private $cycle;


    private $bets = [];
    private $amount = 0;
    private $amount2d = 0;
    private $amount3d = 0;
    private $amount5d = 0;
    private $system_rebates = [];
    private $user_rebates = [];
    private $ticket;
    private $order;
    private $bonus_account;
    private $default_account;
    private $profit_account;
    private $device_name;
    private $device_uuid;

    private $values = [];
    private $cases = [];

    private $user;

    function __construct($bets,$device_name,$device_uuid){
        $this->bets = $bets;
        $this->user = auth()->user();
        $this->device_name = $device_name;
        $this->device_uuid = $device_uuid;

        if(!$this->user->can_bet){
          //  throw new Exception(api_trans('auth.cannot_bet'));
        }
    }

    private function init(){
        $this->bet_contents = [];
        $this->order_bets = [];
        $this->bet = [];

        $this->cycle = Cycle::whereHasReleased(0)
                            ->orderBy('id','desc')
                            ->first();
        
        if(!$this->cycle) throw new Exception(api_trans('cycle.no_cycle'));

        if(time() > $this->cycle->stopped_time) throw new Exception(api_trans('cycle.cycle_stop'));

        $this->ticket = $this->get_ticket();
    }

    public function process_bet(){
        
        $this->init();
        
        foreach($this->bets as $bet){
            $this->process($bet);
        }

        $this->order_bets = collect($this->order_bets);
        $this->amount = $this->order_bets->sum('amount');
        $this->amount2d = $this->order_bets->where('type',self::GAME_2D)->sum('amount');
        $this->amount3d = $this->order_bets->where('type',self::GAME_3D)->sum('amount');
        $this->amount5d = $this->order_bets->where('type',self::GAME_5D)->sum('amount');

        if(!$this->can_afford()){
            throw new Exception(api_trans('account.insufficient_balance'));
        }

        $this->order = new OrderTemp();

        $this->settle_rebate();
        $this->insert_data();
        $this->settle_balance_user();
        $this->user->is_participate = 1;
        $this->user->save();

        return responseSuccess([
            'username' => $this->user->username,
            'account_number' => $this->user->account_number,
            'bet_time' => Carbon::parse($this->order->created_at)->format('Y-m-d h:i A'),
            'ticket' => $this->order->ticket,
            'result_time' => date('Y-m-d h:i A',$this->cycle->result_time),
            'bet' => json_encode($this->order->bet_content),
            'total' => number_format($this->order->amount).' (R)',
            'balance' => number_format($this->default_account->balance).' (R)',
            'bonus' => number_format($this->bonus_account->balance).' (R)'
        ]);
    }

    private function insert_data(){
        $this->insert_order_bet();
        $this->insert_order();
        $this->insert_user_rebates();
        $this->insert_order_number();
    }

    private function insert_order_number(){
        $order_number_table = OrderNumberTemp::getModel()->getTable();
        $values = implode(',',$this->values);
        $cases = implode(" ",$this->cases);


        $sql = "INSERT INTO $order_number_table (prize,cycle_id,number,type,amount) VALUES ".$values." ON DUPLICATE KEY UPDATE amount = amount + CASE ".$cases." END";
        DB::connection('sethei_mysql')->statement($sql);
    }

    private function insert_user_rebates(){
        $table = User::getModel()->getTable();
        $cases = [];
        $ids = [];
        $params = [];

        foreach ($this->user_rebates as $id => $value) {
            $id = (int) $id;
            $cases[] = "WHEN {$id} then ?";
            $params[] = $value;
            $ids[] = $id;
        }

        $ids = implode(',', $ids);
        $cases = implode(' ', $cases);

        if(!$this->user->com_direct && $ids){
            DB::update("UPDATE `{$table}` 
            SET `current_line_commission_5d` = 
                `current_line_commission_5d` + (CASE `id` {$cases} END),is_participate = 1 WHERE `id` in ({$ids})", $params);
        }
    }

    private function insert_order(){

        $this->order->fill([
            'parent_id' => $this->user->parent_id,
            'cycle_id' => $this->cycle->id,
            'user_id' => $this->user->id,
            'ticket' => $this->ticket,
            'amount' => $this->amount,
            'bet' => $this->bet,
            'bet_content' => $this->bet_contents,
            'user_type' => $this->user->user_type,
        ]);
       
        $this->order->save();
    }

    private function insert_order_bet(){
        DB::raw('LOCK TABLES order_temps,order_bet_temps WRITE');

        OrderBetTemp::insert($this->order_bets->toArray());

        $last_insert_id = OrderBetTemp::orderBy('id','desc')->first()->id;

        $orderKeys = range($last_insert_id - $this->order_bets->count() + 1,$last_insert_id);
        
        $this->bet = array_combine($orderKeys, $this->bet);
        // $this->bet_contents = json_encode($this->bet_contents);

        DB::raw('UNLOCK TABLES');
    }

    private function process($bet){
    
        switch($bet['play_type']){
            case self::X: 
                $this->process_x($bet);
                break;
            case self::X3:
                $this->process_x3($bet);
                break;
            case self::S:
                $this->process_s($bet);
                break;
            case self::NORMAL:
                $this->process_normal($bet);
                break;
            case self::RANGE:
                $this->process_range($bet);
                break;
        }
    }

    private function settle_balance_user(){
        
        if($this->bonus_account){
            $this->bonus_account->balance -= $this->amount;

            if($this->bonus_account->balance < 0){
                $this->default_account->balance += $this->bonus_account->balance;
                $this->bonus_account->balance = 0;
            }
            $this->bonus_account->save();
        }else{
            $this->default_account->balance -= $this->amount;
        }

        $this->default_account->save();
    }

    private function can_afford(){
        $this->bonus_account = $this->user->bonus5dAccount()->first();
    
        $this->default_account = $this->user->cashAccount()->first();

        if($this->bonus_account){
            if($this->bonus_account->balance >= $this->amount) return true;
            if($this->bonus_account->balance + $this->default_account->balance >= $this->amount) return true;
        } else {
            if($this->default_account->balance >= $this->amount) return true;
        }

        return false;

    }

    private function settle_rebate(){
        if(!$this->user->com_direct){
            $level = $this->user->level;
            $this->user_rebates = [];
            $rebate_set = GameRebate::with('category')->get();
            $this->system_rebate = $rebate_set;


            $temp = [];
            foreach($this->system_rebate as $rebate){
                for($i=0;$i<5;$i++){
                    $arr_rebate[$i] = $rebate->{'l'.($i+1).'_rebate'};
                }
                $temp[$rebate->category->name] = $arr_rebate;
            }

            $this->system_rebate = $temp;

            $parentAndSelf = $this->user->ancestorsAndSelf()->pluck('id')->toArray();

            for($i=0;$i<5;$i++){

                if(!isset($parentAndSelf[$i])) break;


                $rebate_2d = $this->amount2d * $this->system_rebate['2D'][4-$i];
                $rebate_3d = $this->amount3d * $this->system_rebate['3D'][4-$i];
                $rebate_5d = $this->amount5d * $this->system_rebate['5D'][4-$i];
                $total_rebate = ($rebate_2d + $rebate_3d + $rebate_5d) / 100;

                if($parentAndSelf[$i] == $this->user->id){
                    $this->user->increment('current_own_commission_5d', $total_rebate);
                }else{
                    $this->user_rebates[$parentAndSelf[$i]] = $total_rebate;
                }
            }

        }else{
            
        }
    }

    private function process_range($bet){
        $numbers = explode('-',$bet['number']);
        $num1 = $numbers[0];
        $num2 = $numbers[1];

        if(!in_array(strlen($num1),[2,3,5]) || !in_array(strlen($num2),[2,3,5]) || strlen($num1) != strlen($num2)){
            throw new Exception('Invalid RANGE Format (0)');
        }

        if($num2 <= $num1){
            throw new Exception('Invalid RANGE Format (1)');
        }

        $prize = $bet['prize'];
        $g_type = $bet_type = $bet['bet_type'];
        $number = $bet['number'];
        $single = $bet['single_amount'];
        $total = count($prize) * ($num2 - $num1 + 1) * $single;

        $cycle_id = $this->cycle->id;
        foreach($prize as $p){
            for($i=$num1;$i<=$num2;$i++){
                $this->values[] = "('$p',$cycle_id,$i,'$g_type',$single)";
                $this->cases[] = "
                    WHEN prize = '$p' AND cycle_id = $cycle_id AND number = $i AND type = '$g_type' THEN $single
                ";
            }
        }

        $this->add_bet(
            $prize,
            self::RANGE,
            $g_type,
            $number,
            $single
        );

        $this->add_bet_content(
            $prize,
            $number,
            $total,
            $single,
            $bet_type,
            self::RANGE
        );
    }

    private function process_normal($bet){
        if(!in_array(strlen($bet['number']),[2,3,5])){
            throw new Exception('Invalid NORMAL Format (0)');
        }
        
        $single = $bet['single_amount'];
        $prize = $bet['prize'];
        $total = $single * count($prize);
        $g_type = strlen($bet['number']).'D';
        $number = $bet['number'];
        $bet_type = $bet['bet_type'];
        $play_type = $bet['play_type'];

        $cycle_id = $this->cycle->id;
        foreach($prize as $p){
            $this->values[] = "('$p',$cycle_id,$number,'$g_type',$single)";
            $this->cases[] = " WHEN prize = '$p' AND cycle_id = $cycle_id AND number = $number AND type = '$g_type' THEN $single ";
        }

        $this->add_bet(
            $prize,
            self::NORMAL, 
            $g_type,
            $number,
            $single
        );

        $this->add_bet_content(
            $prize,
            $number,
            $total,
            $single,
            $bet_type,
            $play_type
        );
    }

    private function process_s($bet){
        if(!in_array(strlen($bet['number']),[2,3,5])){
            throw new Exception('Invalid S Format (0)');
        }

        if(substr_count($bet['number'],'s') == 0 || substr_count($bet['number'],'s') > 2){
            throw new Exception('Invalid S Format (1)');
        }

        $prize = $bet['prize'];
        $g_type = $bet_type = $bet['bet_type'];
        $number = $bet['number'];
        $single = $bet['single_amount'];

        $numbers = [];

        if(substr_count($bet['number'],'s') === 1){
            $total = count($prize) * 10 * $single;
        }else{
            $total = count($prize) * 100 * $single;
        }

        if(substr_count($number,'s') === 1){
            for($i=0;$i<10;$i++){
                $numbers[] = Str::replaceArray('s', [$i], $number);
            }
        }else{
            for($i=0;$i<10;$i++){
                for($j=0;$j<10;$j++){
                    $numbers[] = Str::replaceArray('s', [$i,$j], $number);
                }
            }
        }
        $cycle_id = $this->cycle->id;
        foreach($prize as $p){
            for($i=0;$i<count($numbers);$i++){
                $this->values[] = "('$p',$cycle_id,$numbers[$i],'$g_type',$single)";
                $this->cases[] = "
                    WHEN prize = '$p' AND cycle_id = $cycle_id AND number = $numbers[$i] AND type = '$g_type' THEN $single
                    ";
            }
        }

        $this->add_bet(
            $prize,
            self::S,
            $g_type,
            $number,
            $single
        );

        $this->add_bet_content(
            $prize,
            $number,
            $total,
            $single,
            $bet_type,
            self::S
        );
    }

    private function process_x3($bet){
        if($bet['bet_type'] !== self::GAME_5D || strlen($bet['number']) !== 5){
            throw new Exception('Invalid X3 Format (0)');
        }
        $prize = $bet['prize'];
        $single = $bet['single_amount'];
        $results = array();
        $str = $bet['number']; 
        $str = str_split($str);
        x3($str, $results);

        $results = collect($results)->reject(function ($number) {
            return strlen($number) !== 3;
        });
        $results = array_unique($results->toArray());
        $results = array_values($results);
        while(count($results) != 0){
            $temp_x = [];
            $str = $results[0];
            permute($str,0,strlen($str),$temp_x);
            $results = array_values(array_diff($results,$temp_x));
            
            $this->process_x([
                'prize' => $prize,
                'bet_type' => self::GAME_3D,
                "play_type" => self::X,
                "number" =>  $str,
                "single_amount" => $single
            ]);
        }
    }

    private function process_x($bet){
        if(!in_array(strlen($bet['number']),[2,3,5])){
            throw new Exception("Invalid X Format (0)");
        }

        $prize = $bet['prize'];
        $single = $bet['single_amount'];
        $g_type = $bet_type = strlen($bet['number']).'D';
        $number = $bet['number'];
        $numbers = [];
        permute($number,0,strlen($number),$numbers);

        $numbers = array_unique($numbers);
        $total = count($numbers) * count($prize) * $single;
        $cycle_id = $this->cycle->id;

        foreach($prize as $p){
            foreach($numbers as $num){
                $this->values[] = "('$p',$cycle_id,$num,'$g_type',$single)";
                $this->cases[] = "
                    WHEN prize = '$p' AND cycle_id = $cycle_id AND number = $num AND type = '$g_type' THEN $single
                    ";
            }
        }
        
        $this->add_bet(
            $prize,
            self::X,
            $g_type,
            $number,
            $single
        );

        $this->add_bet_content(
            $prize,
            $number,
            $total,
            $single,
            $bet_type,
            self::X
        );
    }

    private function add_bet(
        array $prize,
        $type,
        $g_type,
        $number,
        $single
    ){
        $this->bet[] = [
            'prize' => $prize,
            'type' => $type,
            'g_type' => $g_type,
            'number' => $number,
            'single' => $single
        ];
    }

    private function add_bet_content(
        $prize,
        $number,
        $total,
        $single,
        $bet_type,
        $play_type
    ){
        $prize = $this->get_prize_format($prize,$play_type);
        $number = $this->get_number_format($number,$play_type,$bet_type);

        $this->bet_contents[] = [
            $prize,$number,$total,($single/100).'R',$bet_type,0
        ];

        $this->order_bets[] = [
            'ticket' => $this->ticket,
            'prize' => $prize,
            'bet' => $number,
            'unit' => $single,
            'amount' => $total,
            'type' => $bet_type,
        ];
    }

    private function get_prize_format($prize,$play_type){
        $prize = implode('',$prize);
        switch($play_type){
            case self::X : return $prize.'(X)';
            case self::S : return $prize.'(S)';
            default: return $prize;
        }
    }

    private function get_number_format($number,$play_type,$bet_type){
        switch($play_type){
            case self::NORMAL:
                if($bet_type == self::GAME_2D){
                    return $number.'[000]';
                }else if($bet_type == self::GAME_3D){
                    return '[00]'.$number;
                }else{
                    return $number;
                }
            case self::X:
                if($bet_type == self::GAME_2D){
                    return $number.'x[000]';
                }else if($bet_type == self::GAME_3D){
                    return '[00]'.$number.'x';
                }else{
                    return $number.'x';
                }
            case self::S:
                if($bet_type == self::GAME_2D){
                    return $number;
                }else if($bet_type == self::GAME_3D){
                    return '[00-99]'.$number;
                }else{
                    return $number;
                }
            case self::RANGE:
                $numbers = explode('-',$number);
                $num1 = $numbers[0];
                $num2 = $numbers[1];
                if($bet_type == self::GAME_2D){
                    return $num1.'[000]-'.$num2.'[000]';
                }else if($bet_type == self::GAME_3D){
                    return '[00]'.$num1.'-[00]'.$num2;
                }else{
                    return $number;
                }
        }
    }

    private function get_ticket(){
        $ticket = $this->cycle->cycle_sn.mt_rand(1000000, 9999999);
        $temp = Order::whereTicket($ticket)->first();
        if(!$temp){
            $temp = OrderTemp::whereTicket($ticket)->first();
        }
        
        if($temp) {
            return $this->get_ticket();
        } else {
            return $ticket;
        }
    }
}
