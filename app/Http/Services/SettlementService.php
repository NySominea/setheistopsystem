<?php

namespace App\Http\Services;
use App\Model\Sethei5D\Cycle;
use Exception;
use Str;

class SettlementService {

    private const S = 'S';
    private const X = 'X';
    private const X3 = 'X3';
    private const RANGE = 'RANGE';
    private const NORMAL = 'NORMAL';

    private const GAME_2D = '2D';
    private const GAME_3D = '3D';
    private const GAME_5D = '5D';

    private $rewards = [];

    private $cycle;
    private $result2D = [];
    private $result3D = [];
    private $result5D = [];

    private $win_info = [];
    private $win_bet_info = [];
    private $bet_content_info = [];

    private $order;
    private $is_win = false;

    private $prizes = ['A','B','C','D']; 

    function __construct($order,$rewards,Cycle $cycle){
        $this->cycle = $cycle;
        $this->order = $order;
        $this->rewards = $rewards;
        $this->win_info = [
            'win_2D_amount' => 0,
            'win_3D_amount' => 0,
            'win_5D_amount' => 0
        ];
        $this->win_bet_info = [];

        if(!$this->cycle) throw new Exception('Cycle not found');

        foreach($this->prizes as $prize){
            $this->result2D[$prize] = $this->cycle->{$prize.'1'}.$this->cycle->{$prize.'2'};
            $this->result3D[$prize] = $this->cycle->{$prize.'3'}.$this->cycle->{$prize.'4'}.$this->cycle->{$prize.'5'};
            $this->result5D[$prize] = $this->result2D[$prize].$this->result3D[$prize];
        }
    }

    public function process_bet(){       
        $bets = $this->order->bet;
        $bet_contents = $this->order->bet_content;
        
        $index = 0;
        foreach($bets as $id => $bet){
            $amount = $this->process($id,$bet);
            $bet_contents[$index][5] = $amount;
            $this->bet_content_info[] = $bet_contents[$index];
            $index++;
        }

        return [
            'win_bet_info' => $this->win_bet_info,
            'win_info' => $this->is_win ? [
                $this->order->id => $this->win_info
            ] : [],
            'bet_content_info' => $this->is_win ? [
                'id' => $this->order->id,
                'bet_content' => json_encode($this->bet_content_info)
            ] : []
        ];
    }

    private function process($id,$bet){
        switch($bet['type']){
            case self::NORMAL:
                return $this->settle_normal($id,$bet);
                break;
            case self::RANGE:
                return $this->settle_range($id,$bet);
                break;
            case self::X:
                return $this->settle_x($id,$bet);
                break;
            case self::S:
                return $this->settle_s($id,$bet);
                break;
        }
    }

    private function settle_s($id,$bet){
        $number = $bet['number'];
        $g_type = $bet['g_type'];
        $prizes = $bet['prize'];
        if(substr_count($number,'s') === 1){
            for($i=0;$i<10;$i++){
                $numbers[] = Str::replaceArray('s', [$i], $number);
            }
        }else{
            for($i=0;$i<10;$i++){
                for($j=0;$j<10;$j++){
                    $numbers[] = Str::replaceArray('s', [$i,$j], $number);
                }
            }
        }

        $bet_content_amount = 0;
        foreach($prizes as $prize){
            if(in_array($this->{'result'.$g_type}[$prize],$numbers)){
                $single = $bet['single'];
                $amount = $single * $this->rewards[$g_type];

                $bet_content_amount += $this->add_win_info($id,$amount,$g_type);
            }
        }

        return $bet_content_amount;
    }

    private function settle_x($id,$bet){
        $number = $bet['number'];
        $g_type = $bet['g_type'];
        $prizes = $bet['prize'];

        $numbers = [];
        permute($number,0,strlen($number),$numbers);
        $numbers = array_unique($numbers);

        $bet_content_amount = 0;
        foreach($numbers as $num){
            foreach($prizes as $prize){
                if($this->{'result'.$g_type}[$prize] == $this->get_number($num,$g_type)){
                    $single = $bet['single'];
                    $amount = $single * $this->rewards[$g_type];
                    $bet_content_amount += $this->add_win_info($id,$amount,$g_type);
                }
            }
        }

        return $bet_content_amount;
    }

    private function settle_range($id,$bet){
        $number = $bet['number'];
        $g_type = $bet['g_type'];
        $prizes = $bet['prize'];

        $numbers = explode('-',$number);
        $num1 = $numbers[0];
        $num2 = $numbers[1];

        $bet_content_amount = 0;
        foreach($prizes as $prize){
            if($this->{'result'.$g_type}[$prize] >= $this->get_number($num1,$g_type) && $this->{'result'.$g_type}[$prize] <= $this->get_number($num2,$g_type)){
                $single = $bet['single'];
                $amount = $single * $this->rewards[$g_type];
                $bet_content_amount += $this->add_win_info($id,$amount,$g_type);
            }
        }

        return $bet_content_amount;
    }

    private function settle_normal($id,$bet){
        $number = $bet['number'];
        $g_type = $bet['g_type'];
        $prizes = $bet['prize'];

        $bet_content_amount = 0;
    
        foreach($prizes as $prize){
            if($this->{'result'.$g_type}[$prize] == $this->get_number($number,$g_type)){
                $single = $bet['single'];
                $amount = $single * $this->rewards[$g_type];
                $bet_content_amount += $this->add_win_info($id,$amount,$g_type);
            }
        }

        return $bet_content_amount;
    }

    private function add_win_info($id,$amount,$g_type){
        $this->is_win = true;
        $this->win_bet_info[] = [
            'id' => $id,
            'is_win' => 1,
            'win_amount' => $amount
        ];
        $this->win_info['win_'.$g_type.'_amount'] += $amount;

        return $amount;
    }

    private function get_number($num,$type){
        switch($type){
            case "2D": return str_pad($num,2,'0',STR_PAD_LEFT);
            case "3D": return str_pad($num,3,'0',STR_PAD_LEFT);
            default: return str_pad($num,5,'0',STR_PAD_LEFT);
        }
    }
}