<?php

namespace App\Http\Middleware;

use Closure;

class StateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check()){
            if(auth()->user()->state){
                return $next($request);
            }
        }
        return responseError(api_trans('auth.user_disabled'));
    }
}
