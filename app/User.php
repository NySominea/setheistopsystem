<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use App\Model\UserAccount;
use App\Constants\Account;
use App\Constants\UserAccountConstant;
use App\Constants\UserBallAccount;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\Models\Media;
use App\Model\UserAccountBall;

use App\Model\SetheiBall\Order as BallOrder;
use App\Model\SetheiBall\OrderTemp as BallOrderTemp;
use App\Model\SetheiBall\OrderRebateTemp as BallOrderRebateTemp;
use App\Model\SetheiBall\OrderRebate as BallOrderRebate;
use App\Model\Sethei5D\Order as DOrder;
use App\Model\Sethei5D\OrderTemp as DOrderTemp;
use App\Model\Sethei5D\OrderRebate as DOrderRebate;
use App\Model\Sethei5D\OrderRebateTemp as DOrderRebateTemp;

use \Staudenmeir\LaravelAdjacencyList\Eloquent\HasRecursiveRelationships;
use Hash;

class user extends Authenticatable implements HasMedia
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    use HasApiTokens,Notifiable,HasMediaTrait,HasRecursiveRelationships;

    protected $table = 'users';


    protected $fillable = [ 
        'id',
        'username',
        'account_number',
        'email',
        'phone',
        'national_id_number',
        'password',
        'payment_password',
        'current_own_commission_5d',
        'current_line_commission_5d',
        'current_own_commission_ball',
        'current_line_commission_ball',
        'level',
        'parent_id',
        'state',
        'address',
        'province_id',
        'district_id',
        'commune_id',
        'is_test',
        'is_approved',
        'created_at',
        'updated_at'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function getParentKeyName()
    {
        return 'parent_id';
    }

    public function ancestorsAndSelf5Level(){
        return $this->ancestorsAndSelf()->take(5)->get();
    }

    public function userAccounts(){
        return $this->hasMany(UserAccount::class,'user_id','id');
    }

    public function cashAccount(){
        return $this->hasOne(UserAccount::class,'user_id','id')->where('type_id', UserAccountConstant::MAIN_ACCOUNT);
    }

    public function bonus5dAccount(){
        return $this->hasOne(UserAccount::class,'user_id','id')->where('type_id', UserAccountConstant::BONUS_5D_ACCOUNT);
    }
    public function bonusBallAccount(){
        return $this->hasOne(UserAccount::class,'user_id','id')->where('type_id', UserAccountConstant::BONUS_BALL_ACCOUNT);
    }

    public function dOrderTemps(){
        return $this->hasMany(DOrderTemp::class);
    }

    public function dOrders(){
        return $this->hasMany(DOrder::class);
    }

    public function ballOrderTemps(){
        return $this->hasMany(BallOrderTemp::class);
    }

    public function ballOrders(){
        return $this->hasMany(BallOrder::class);
    }

    public function ballOrderRebates(){
        return $this->hasMany(BallOrderRebate::class);
    }

    public function ballOrderRebateTemps(){
        return $this->hasMany(BallOrderRebateTemp::class);
    }

    public function children(){
        return $this->hasMany(User::class,'parent_id','id');
    }

    public function sale(){
        return $this->belongsTo(User::class,'parent_id','id');
    }

    public function parent(){
        return $this->belongsTo(User::class,'parent_id','id');
    }

    public function district(){
        if($this->Level <= 3) return $this;
        return $this->parent->district();
    }

    public function province(){
        return $this->district()->parent;
    }    
    public function main_balance(){
        return $this->hasOne(UserAccount::class)->whereTypeId(1);
    }

    public function profit_account(){
        return $this->hasOne(UserAccount::class)->whereTypeId(4);
    }

    public function profit(){
        return $this->hasOne(UserAccount::class)->whereTypeId(4)->first();
    }

    public function default_account(){
        return $this->hasOne(UserAccount::class)->whereTypeId(1)->first();
    }

    public function bonus_account(){
        return $this->hasOne(UserAccount::class)->whereTypeId(5)->first();
    }

    public static function generateAccountNumber(){
        $prefix = "TJ";
        $st_no = addPrefixStringPad(1,8,'0');;
        $acc_no = '';

        $lastestUser = User::orderBy('account_number','desc')->first();

        if($lastestUser){
            $next_no = ((int) substr($lastestUser->account_number, 2)) + 1;
            $next_no = addPrefixStringPad($next_no,8,'0');
            $acc_no = $prefix.$next_no;
        }else{
            $acc_no = $prefix.$st_no;
        }

        return $acc_no;
    }

    public static function checkPayPassword($userId,$pwd){
        $user = User::find($userId);
        if(!$user || !Hash::check($pwd,$user->password) || !$user->state){
            return false;
		}
        return true;
    }
   
}
