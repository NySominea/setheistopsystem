<?php

namespace App\Console\Commands\Import;

use Illuminate\Console\Command;
use DB;
use App\Model\Province as modelProvince;

class Province extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:province';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::connection('sethei_mysql')->table('provinces')->orderBy('id','asc')->chunk(500,function($netProvinces){
            $data = [];
            foreach($netProvinces as $netProvince){
                $data[] = [
                    'id' => $netProvince->id,
                    'code'  => $netProvince->code, 
                    'name' => $netProvince->name,
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ];
            }
            modelProvince::insert($data);
        });
    }
}
