<?php

namespace App\Console\Commands\Import;

use Illuminate\Console\Command;
use DB;
use App\Model\Commune as modelCommune;

class Commune extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:commune';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::connection('sethei_mysql')->table('communes')->orderBy('id','asc')->chunk(500,function($netCommunes){
            $data = [];
          foreach ($netCommunes as $key => $netCommune) {
              $data[] = [
                  'id' => $netCommune->id,
                  'district_id' => $netCommune->district_id,
                  'code' => $netCommune->code,
                  'name' => $netCommune->name
                ];
          } 
          modelCommune::insert($data);
        });
    }
}
