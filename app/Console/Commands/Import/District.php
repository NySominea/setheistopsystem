<?php

namespace App\Console\Commands\Import;

use Illuminate\Console\Command;
use DB;
use App\Model\District as modelDistrict;

class District extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:district';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::connection('sethei_mysql')->table('districts')->orderBy('id','asc')->chunk(500,function($netDistricts){
            $data = [];
            foreach($netDistricts as $netDistrict){
                $data[] = [
                    'id' => $netDistrict->id,
                    'code'  => $netDistrict->code, 
                    'name' => $netDistrict->name,
                    'province_id' => $netDistrict->province_id,
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ];
            }
            modelDistrict::insert($data);
        });
    }
}
