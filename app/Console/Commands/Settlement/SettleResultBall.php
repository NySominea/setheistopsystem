<?php

namespace App\Console\Commands\Settlement;

use Illuminate\Console\Command;
use App\Model\SetheiBall\Cycle;
use App\Model\SetheiBall\OrderTemp;
use App\Model\SetheiBall\Order;
use App\Model\SetheiBall\OrderRebateTemp;
use App\Model\SetheiBall\OrderRebate;
use App\Model\SetheiBall\GameType;
use App\User;
use App\Model\UserAccountLog;
use DB;
use Batch;
use App\Constants\Account;
use App\Model\UserAccount;

class SettleResultBall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'settle:result-ball {cycle_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private $cycle;
    private $game;
    private $rielToDollar;
    private $users;
    private $totalBetAmountByUser;
    private $totalWinAmountByUser;
    private $totalWinAmountExceptJackpot;
    private $userIds;
    private $jackpotOrders;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {   
        ini_set("memory_limit",-1);
        ini_set('max_execution_time', 0);
        
        DB::beginTransaction();
        try{
            // Get game config
            $this->getGameConfig();
            
            if($this->cycle){ 
                $this->totalBetAmountByUser = [];
                $this->totalWinAmountByUser = [];
                $this->totalWinAmountExceptJackpot = 0;
                $this->userIds = [];
                $this->jackpotOrders = [];
                $columns = $this->getOrderFields();
                
                // Move updated order temps to order table
                OrderTemp::whereCycleId($this->cycle->id)
                        ->where('game_type_id',$this->cycle->game_type_id)
                        ->where('is_test',0)
                        ->chunk(500,function($orders) 
                        use ($columns){
                            if($orders && count($orders) > 0){
                                $orders_updated_data = [];
                                $orders_inserted_data = [];
                                foreach($orders as $index => $order){ 
                                    $win_num = [];
                                    $type = null; 
                                    $win_amount = 0;
                                    $is_win = 1;
                                    $bet_num = [];
                                    array_push($bet_num,$order->no1,$order->no2,$order->no3,$order->no4,$order->no5,$order->no6);
                                    $win_num = array_values(array_intersect($bet_num,$this->cycle->result_number));
                                    switch (count($win_num)) {
                                        case 3:
                                            $type = '3D';
                                            $win_amount = $this->game->win_prize_3d ?: 0;
                                            $this->totalWinAmountExceptJackpot += $win_amount;
                                            break;

                                        case 4:  
                                            $type = '4D';
                                            $win_amount = $this->game->win_prize_4d ?: 0;
                                            $this->totalWinAmountExceptJackpot += $win_amount;
                                            break;

                                        case 5:
                                            $type = '5D';
                                            $win_amount = $this->game->win_prize_5d ?: 0;
                                            $this->totalWinAmountExceptJackpot += $win_amount;
                                            break;

                                        case 6:
                                            $type = 'jackpot';
                                            $this->jackpotOrders[] = $order->ticket;
                                            break;
                                        
                                        default:
                                            $is_win = 0;
                                            break;
                                    }
                                    
                                    $orders_inserted_data[] = [
                                        $order->ticket,	
                                        1,
                                        $is_win,
                                        $type,
                                        json_encode($win_num),
                                        $win_amount,
                                        $order->amount,
                                        $order->no1,
                                        $order->no2,
                                        $order->no3,
                                        $order->no4,
                                        $order->no5,
                                        $order->no6,
                                        $order->parent_id,
                                        $order->state,
                                        $order->cycle_id,
                                        $order->user_id,
                                        $this->game->id,
                                        $order->created_at,
                                        $order->updated_at
                                    ];
                                    
                                    $this->totalBetAmountByUser[$order->user_id] = isset($this->totalBetAmountByUser[$order->user_id]) 
                                                                                    ? $this->totalBetAmountByUser[$order->user_id] + $order->amount
                                                                                    : $order->amount;
                                    
                                    $this->totalWinAmountByUser[$order->user_id] = isset($this->totalWinAmountByUser[$order->user_id]) 
                                                                                    ? $this->totalWinAmountByUser[$order->user_id] + $win_amount
                                                                                    : $win_amount;
                                    
                                    // get id of users who involves in the cycle
                                    $owner = User::find($order->user_id);
                                    if($owner){
                                        $this->userIds = array_merge($this->userIds,$owner->ancestorsAndSelf5Level()->pluck('id')->toArray());
                                        $this->userIds = array_values(array_unique($this->userIds));
                                    }
                                }
                                
                                Batch::insert(new Order(), $columns, $orders_inserted_data, 500);
                            }
                        });
                
                DB::delete('delete from ball_order_temps'); 
                
                // Move order_rebate_temps to order_rebates table
                $this->moveOrderRebate();
                
                // Share Jackpot Prize
                $this->shareJackpot();
                
                // Get all active users
                $this->getActiveUsers();

                // Create logs
                $this->insertUserLogs();

                // Update user accounts
                $this->updateUserAccounts();

                // Update user accounts
                $this->updateUsers();

                // Update Cycle
                $this->cycle->update([
                    'is_jackpot' => count($this->jackpotOrders) > 0 ? 1 : 0
                ]);
                
                // Broadcash Notification
                // $this->broadcashNotification();
                
            } 
            
            DB::commit();
        }catch(Exception $exception){
            DB::rollback();
        }
    }

    private function getOrderFields(){
        return [
            'ticket',	
            'is_settle',
            'is_win',
            'win_prize',
            'win_number',
            'win_amount',
            'amount',
            'no1',
            'no2',
            'no3',
            'no4',
            'no5',
            'no6',
            'parent_id',
            'state',
            'cycle_id',
            'user_id',
            'game_type_id',
            'created_at',
            'updated_at'
        ];
    }

    private function moveOrderRebate(){
        OrderRebate::insert(OrderRebateTemp::all()->toArray());
        DB::delete('delete from ball_order_rebate_temps');
    }

    private function shareJackpot(){
        if(count($this->jackpotOrders) > 0){
            $orders_win_jackpot = Order::whereIn('ticket',$this->jackpotOrders)->whereWinPrize('jackpot')->get();
            if($orders_win_jackpot && count($orders_win_jackpot) > 0){
                $jackpot_prize = 0;
                if(count($orders_win_jackpot) > 1){ // More than 1 Jackpot Winner
                    $jackpot_prize = $this->cycle->prize / count($this->jackpotOrders);
                }
                else{ // Only 1 Jackpot Winner
                    $jackpot_prize = $this->cycle->prize - $this->totalWinAmountExceptJackpot;
                }
                $order_jackpot_prize_updated_data = [];
                foreach($orders_win_jackpot as $order){
                    $order_jackpot_prize_updated_data[] = [
                        'id' => $order->id,
                        'win_amount' => $jackpot_prize
                    ];
                    // $this->totalWinAmountByUser[$order->user_id] = isset($this->totalWinAmountByUser[$order->user_id]) 
                    //                                             ? $this->totalWinAmountByUser[$order->user_id] + ($this->cycle->prize / count($this->jackpotOrders))
                    //                                             : ($this->cycle->prize / count($this->jackpotOrders));
                }
                Batch::update(new Order(), $order_jackpot_prize_updated_data, 'id');
            }
        }
    }

    private function updateUserAccounts(){
        if($this->users && $this->users->count() > 0){
            foreach($this->users as $id => $user){
                $totalAmount = $user->current_own_commission_ball 
                                + $user->current_line_commission_ball 
                                + (isset($this->totalWinAmountByUser[$id]) ? $this->totalWinAmountByUser[$id] : 0);
                
                if(!$user->cashAccount){
                    $user->cashAccount = UserAccount::createCashAccount($user->id); 
                }

                $user->cashAccount->increment('balance', $totalAmount);
                
            }
            
        }
    }

    private function insertUserLogs(){
        $user_log_inserted_data = [];
        $log_columns = [
            'amount',
            'balance',
            'commission',
            'win_money',
            'is_transfer',
            'log_type',
            'abstract',
            'to_type',
            'user_id',
            'account_id',
            'to_user_id',
            'to_account_id',
            'manager_id',
            'created_at',
            'updated_at'
        ];
        if($this->users && $this->users->count() > 0){
            foreach($this->users as $id => $user){
                if(!isset($user->cashAccount)) 
                    $user->cashAccount = $user->cashAccount = UserAccount::createCashAccount($user->id); 
                $userWinAmount = isset($this->totalWinAmountByUser[$id]) ? $this->totalWinAmountByUser[$id] : 0;
                if($userWinAmount > 0){
                    //  Log betting summary
                    $user_log_inserted_data[] = [
                        $this->totalBetAmountByUser[$id],
                        $user->cashAccount->balance,
                        0,
                        0,
                        0,
                        Account::LOG_TYPE_IN,
                        'LANG_LABEL_BALL_BET_SUM',
                        Account::LOG_TYPE_IN,
                        $id,
                        $user->cashAccount->id,
                        0,
                        0,
                        0,
                        date('Y-m-d H:i:s'),
                        date('Y-m-d H:i:s')
                    ];

                    //  Log Award Summary 
                    if($this->totalWinAmountByUser[$id] > 0){
                        $user_log_inserted_data[] = [
                            $this->totalWinAmountByUser[$id],
                            $user->cashAccount->balance + $this->totalWinAmountByUser[$id],
                            0,
                            0,
                            0,
                            Account::LOG_TYPE_IN,
                            'LANG_LABEL_BALL_WIN_SUM',
                            Account::LOG_TYPE_IN,
                            $id,
                            $user->cashAccount->id,
                            0,
                            0,
                            0,
                            date('Y-m-d H:i:s'),
                            date('Y-m-d H:i:s')
                        ];
                    }
                }
                
                // Log own commission
                $user_log_inserted_data[] = [
                    $user->current_own_commission_ball,
                    $user->cashAccount->balance + $userWinAmount + $user->current_own_commission_ball,
                    0,
                    0,
                    0,
                    Account::LOG_TYPE_IN,
                    'LANG_LABEL_BALL_OWN_REBATE_SUM',
                    Account::LOG_TYPE_IN,
                    $id,
                    $user->cashAccount->id,
                    0,
                    0,
                    0,
                    date('Y-m-d H:i:s'),
                    date('Y-m-d H:i:s')
                ];

                // Log downline commission
                $user_log_inserted_data[] = [
                    $user->current_line_commission_ball,
                    $user->cashAccount->balance + $userWinAmount + $user->current_own_commission_ball + $user->current_line_commission_ball,
                    0,
                    0,
                    0,
                    Account::LOG_TYPE_IN,
                    'LANG_LABEL_BALL_DOWNLINE_REBATE_SUM',
                    Account::LOG_TYPE_IN,
                    $id,
                    $user->cashAccount->id,
                    0,
                    0,
                    0,
                    date('Y-m-d H:i:s'),
                    date('Y-m-d H:i:s')
                ];
            }
        }
        Batch::insert(new UserAccountLog(), $log_columns, $user_log_inserted_data);
    }

    private function updateUsers(){
        if($this->users && $this->users->count() > 0){
            $users_updated_data = [];
            foreach($this->users as $id => $user){
                $users_updated_data[] = [
                    'id' => $id, 
                    'current_own_commission_ball' => 0,
                    'current_line_commission_ball' => 0
                ];
            }
            Batch::update(new User(), $users_updated_data, 'id');
        }
    }

    private function getActiveUsers(){
        $this->users = User::with('cashAccount')
                            ->whereIn('id',$this->userIds)
                            ->orderBy('account_number','ASC')
                            ->get()
                            ->keyBy('id');
    }

    private function getGameConfig(){ 
        $this->cycle = Cycle::find($this->argument('cycle_id'));
        $this->game = GameType::whereCode('L101')->first();
        $this->rielToDollar = exchangeRateFromDollarToRiel();
    }

    private function broadcashNotification(){
        $title = api_trans('notification.ball_result_title_msg');
        $body = "Cycle: ".$this->cycle->cycle_sn."\n".api_trans('notification.result_time').": ".date('Y-m-d H:i A',$this->cycle->result_time)."\n".api_trans('notification.result').": ".implode(' - ',$this->cycle->result_number);
        pushNotification($title,$body);

        $winMsg = "";
        $hasBigWin = false;
        foreach($this->totalWinAmountByUser as $id => $amount){
            if(doubleval($amount) >= 400000){
                $hasBigWin = true;
                $winMsg .= (string) api_trans('notification.win_msg',['user' => $this->users[$id]->username, 'amount' => currencyFormat(doubleval($amount) * exchangeRateFromRielToDollar())])."\n";
            }
        }
        
        if($hasBigWin){
            pushNotification(
                api_trans('notification.congratulation'),
                $winMsg
            );
        }
    }
}
