<?php

namespace App\Console\Commands\Settlement;

use Illuminate\Console\Command;
use App\Http\Services\SettlementService;
use App\Model\Sethei5D\OrderTemp;
use App\Model\Sethei5D\OrderBetTemp;
use App\Model\UserAccountLog;
use App\Model\Sethei5D\Order;
use App\Model\Sethei5D\OrderBet;
use App\Model\OrderNumberTemp;
use App\Model\OrderNumber;
use App\Model\UserAccount;
use App\Model\Sethei5D\GameCategory;
use App\Model\Sethei5D\Cycle;
use App\User;
use Carbon\Carbon;
use DB;
use Batch;

class SettleResult extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'system:settle {cycle_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Settle result';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //user win information include which order win
        $win_info = [];
        //order bet that win (for update purpose)
        $win_bet_info = [];
        // hold info which user win certain amount
        $win_log_info = [];
        $bet_log_info = [];
        $bet_content_info = [];
        $order_info = [];

        $cycle = Cycle::find($this->argument('cycle_id'));

        if(!$cycle) return;

        $rewards = GameCategory::pluck('rate','name')->toArray();
    
        OrderTemp::chunk(1000,function($orders) 
        use (
            &$win_info,
            &$win_bet_info,
            &$bet_log_info,
            &$bet_content_info,
            $cycle,
            $rewards 
        ){
            foreach($orders as $order){
                $settlement = new SettlementService($order,$rewards,$cycle);
                $order_info = $settlement->process_bet();

                if(!isset($win_info[$order->user_id])){
                    $win_info[$order->user_id] = [];
                }
                $win_info[$order->user_id] += $order_info['win_info'];

                $win_bet_info = array_merge($win_bet_info,$order_info['win_bet_info']);

                if($order_info['bet_content_info'])
                    $bet_content_info[] = $order_info['bet_content_info'];

                if(!isset($bet_log_info[$order->user_id])){
                    $bet_log_info[$order->user_id] = [
                        'balance' => 0
                    ];
                }

                $bet_log_info[$order->user_id]['balance'] += $order->amount;
            }
        });
    
        $bet_content_info = array_values($bet_content_info);
        
        // $win_info = collect($win_info);
        foreach($win_info as $user_id => $orders){
            foreach($orders as $order_id => $order){
                $order = (object) $order;
                $order_info[] = [
                    'id' => $order_id,
                    'win_2d_amount' => $order->win_2D_amount,
                    'win_3d_amount' => $order->win_3D_amount,
                    'win_5d_amount' => $order->win_5D_amount,
                    'win_amount' => $order->win_2D_amount + $order->win_3D_amount + $order->win_5D_amount,
                    'is_settle' => 1,
                    'is_win' => 1
                ];
            }
            $orders = collect($orders);
            $win2d = $orders->sum('win_2D_amount');
            $win3d = $orders->sum('win_3D_amount');
            $win5d = $orders->sum('win_5D_amount');

            $total = $win2d + $win3d + $win5d;
            if($total > 0)
                $win_log_info[$user_id] = $total;
        }

        //update bet content of order
        Batch::update(new OrderTemp,$bet_content_info);

        //insert log for users
        $this->insert_logs($win_log_info,$bet_log_info);

        //update order records
        Batch::update(new OrderTemp, $order_info);

        //update order bet records
        Batch::update(new OrderBetTemp, $win_bet_info);

        //mark settle of all orders
        OrderTemp::query()->update(['is_settle' => 1]);

        //copy from order temp to order table
        $this->insert_to_order_table();

        //copy from order bet temp to order bet table
        $this->insert_to_order_bet_table();

        //copy from order number temp to order number
        $this->insert_to_order_number_table();

        //delete records from table order temp
        $this->delete_from_order_temp();

        //delete records from table order bet temp
        $this->delete_from_order_bet_temp();

        //delete records from table order number temp
        $this->delete_from_order_number_temp();

        // update cycle
        // $cycle->update(['has_released' => 1]);

        // pushNotification(trans('notification.5d_result_title',[],'kh'),
        //     trans('notification.result_time',[],'kh').': '.date('Y-m-d h:i A',$cycle->result_time)."\n".
        //     'A: '.$cycle->A1.' '.$cycle->A2.' '.$cycle->A3.' '.$cycle->A4.' '.$cycle->A5."\n".
        //     'B: '.$cycle->B1.' '.$cycle->B2.' '.$cycle->B3.' '.$cycle->B4.' '.$cycle->B5."\n".
        //     'C: '.$cycle->C1.' '.$cycle->C2.' '.$cycle->C3.' '.$cycle->C4.' '.$cycle->C5."\n".
        //     'D: '.$cycle->D1.' '.$cycle->D2.' '.$cycle->D3.' '.$cycle->D4.' '.$cycle->D5
        // );

    }

    private function insert_logs($win_log_info,$bet_log_info){
        $update_user_data = [];
        $commission_users = [];
        $user_win_log = [];
        $user_bet_log = [];
        $user_commission_log = [];
        
        $users = User::whereIsParticipate(1)->with('cashAccount')->get();
        
        foreach($users as $user){
            if(isset($win_log_info[$user->id])){
                //have win log
                $user_win_log[] = [
                    'user_id' => $user->id,
                    'account_id' => $user->cashAccount->id,
                    'log_type' => 1,
                    'is_transfer' => 0,
                    'amount' => $win_log_info[$user->id],
                    'win_money' => $win_log_info[$user->id] + $user->win_money_5d,
                    'commission' => 0,
                    'balance' => 0,
                    'to_type' => 1,
                    'to_user_id' => 0,
                    'to_account_id' => 0,
                    'abstract' => 'LANG_LABEL_WIN_SUM',
                    'manager_id' => 0,
                    'log_number' => null,
                    'created_at' => Carbon::now()
                ];
            }

            if(isset($bet_log_info[$user->id])){
                $user_bet_log[] = [
                    'user_id' => $user->id,
                    'account_id' => $user->cashAccount->id,
                    'log_type' => 1,
                    'is_transfer' => 0,
                    'amount' => $bet_log_info[$user->id]['balance'],
                    'win_money' => 0,
                    'commission' => 0,
                    'balance' => $user->cashAccount->balance,
                    'to_type' => 1,
                    'to_user_id' => 0,
                    'to_account_id' => 0,
                    'abstract' => 'LANG_LABEL_BET_SUM',
                    'manager_id' => 0,
                    'log_number' => null,
                    'created_at' => Carbon::now()
                ];
            }

            $user_commission_log[] = [
                'user_id' => $user->id,
                'account_id' => $user->cashAccount->id,
                'log_type' => 1,
                'is_transfer' => 0,
                'amount' => $user->current_own_commission_5d,
                'win_money' => 0,
                'commission' => $user->commission_5d + $user->current_own_commission_5d,
                'balance' => 0,
                'to_type' => 1,
                'to_user_id' => 0,
                'to_account_id' => 0,
                'abstract' => 'LANG_LABEL_REBATE_SUM',
                'manager_id' => 0,
                'log_number' => null,
                'created_at' => Carbon::now()
            ];

            $update_user_data[] = [
                'id' => $user->cashAccount->id,
                'balance' => $user->cashAccount->balance + (isset($win_log_info[$user->id]) ? $win_log_info[$user->id] : 0)
            ];

            $commission_users[] = [
                'id' => $user->id,
                'current_own_commission_5d' => $user->current_own_commission_5d + $user->current_line_commission_5d,
                'current_line_commission_5d' => 0,
                'is_participate' => 0
            ];
        }

        UserAccountLog::insert(array_merge($user_bet_log,$user_commission_log,$user_win_log));

        Batch::update(new User, $commission_users);
        Batch::update(new UserAccount,$update_user_data);
        
    }

    private function delete_from_order_number_temp(){
        OrderNumberTemp::query()->delete();
    }

    private function delete_from_order_temp(){
        OrderTemp::query()->delete();
    }

    private function delete_from_order_bet_temp(){
        OrderBetTemp::query()->delete();
    }

    private function insert_to_order_table(){
        $order_table = Order::getModel()->getTable();
        $order_temp_table = OrderTemp::getModel()->getTable();
        DB::statement("INSERT INTO
            $order_table (
                parent_id,
                cycle_id,
                user_id,
                is_settle,
                is_win,
                win_amount,
                win_2d_amount,
                win_3d_amount,
                win_5d_amount,
                ticket,
                amount,
                bet,
                bet_content,
                state,
                is_test,
                created_at
            ) SELECT 
            parent_id,
            cycle_id,
            user_id,
            is_settle,
            is_win,
            win_amount,
            win_2d_amount,
            win_3d_amount,
            win_5d_amount,
            ticket,
            amount,
            bet,
            bet_content,
            state,
            is_test,
            created_at
            FROM $order_temp_table
        ");
    }

    private function insert_to_order_bet_table(){
        $order_bet_table = OrderBet::getModel()->getTable();
        $order_bet_temp_table = OrderBetTemp::getModel()->getTable();

        DB::statement("INSERT INTO 
            $order_bet_table (
                ticket,
                prize,
                bet,
                unit,
                amount,
                type,
                is_win,
                win_amount
            ) SELECT 
            ticket,
            prize,
            bet,
            unit,
            amount,
            type,
            is_win,
            win_amount
            FROM $order_bet_temp_table
        ");
    }

    private function insert_to_order_number_table(){
        $order_number_temp_table = OrderNumberTemp::getModel()->getTable();
        $order_number_table = OrderNumber::getModel()->getTable();

        DB::connection('sethei_mysql')->statement("INSERT INTO 
            $order_number_table (
                prize,
                cycle_id,
                type,
                number,
                amount
            ) SELECT 
                prize,
                cycle_id,
                type,
                number,
                amount
            FROM $order_number_temp_table
        ");
    }
}
