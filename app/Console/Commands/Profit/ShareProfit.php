<?php

namespace App\Console\Commands\Profit;

use Illuminate\Console\Command;
use DB;
use App\Model\Order;
use App\User;
use Batch;
use App\Model\UserAccount;
use App\Model\UserAccountLog;
use App\Constants\UserAccountConstant;

class ShareProfit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'profit:share';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();
        try{
            $profitPercentage = 0.07;
            $profitAmountCondition = 100000;
            
            $start = date('Y-m-d H:i:s',strtotime('-1 days 17:30:00'));
            $end = date('Y-m-d H:i:s',strtotime('0 day 17:30:00'));
            
            $orders = Order::whereBetween('created_at',[$start,$end])
                            ->get()
                            ->groupBy('l8_id');
            $profitAmountByUser = [];
            $shareUserIds = [];
            foreach($orders as $userId => $userOrders){
                $profit = $userOrders->sum('amount') - $userOrders->sum('win_amount');
                if($profit >= $profitAmountCondition){
                    $profitAmountByUser[$userId] = $profit * $profitPercentage;
                    $shareUserIds[] = $userId;
                }
            }
    
            $users = User::with('profitAccount')->whereIn('id',$shareUserIds)->get();
            
            $profit_account_updated_data = [];
            $log_inserted_data = [];
            $log_column = [
                'amount',
                'balance',
                'is_transfer',
                'log_type',
                'to_type',
                'abstract',
                'user_id',
                'account_id',
                'created_at',
                'updated_at'
            ];
            foreach($users as $user){
                if(!$user->profitAccount) 
                    $user->profitAccount = UserAccount::createProfitAccount($user->id);
    
                $profit_account_updated_data[] = [
                    'id' => $user->profitAccount->id,
                    'balance' => $user->profitAccount->balance + $profitAmountByUser[$user->id]
                ];
                $log_inserted_data[] = [
                    $profitAmountByUser[$user->id],
                    $user->profitAccount->balance + $profitAmountByUser[$user->id],
                    1,
                    1,
                    1,
                    'LANG_LABEL_PROFIT_SHARING',
                    $user->id,
                    $user->profitAccount->id,
                    date('Y-m-d H:i:s'),
                    date('Y-m-d H:i:s')
                ];
            }
            
            Batch::update(new UserAccount(), $profit_account_updated_data, 'id');
            Batch::insert(new UserAccountLog(), $log_column, $log_inserted_data);
            DB::commit();
        }catch(Exception $ex){
            DB::rollback();
        }
    }
}
