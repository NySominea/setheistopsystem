<?php 

function responseSuccess($data = [],$message = ''){
    return [
        'code' => 200,
        'data' => $data,
        'message' => $message
    ];
}

function responseError($message,$code = 403){
    return [
        'code' => $code,
        'data' => null,
        'message' => $message
    ];
}