<?php 
use App\Model\Currency;
use App\Model\Exchange;
use App\Model\Language;
use App\Model\LanguageAdmin;
use App\User;

function pushNotification($title,$body){
    $pushNotifications = notificationInstance();
    $payload = [
        'title' => $title,
        'description' => $body,
        'channel_id' => str_random(10)
    ];

    $publishResponse = $pushNotifications->publishToInterests(
    ["result"],
    [
        "fcm" => [
            'data' => ['result' => json_encode($payload)],
            "notification" => [
                "title" => $title,
                "body" => $body,
            ],
        ],
    ]
    );
}

function pushNotificationToUser($title,$body,$userIds = []){
    $beamsClient = notificationInstance();
    $payload = [
        'title' => $title,
        'description' => $body,
        'channel_id' => str_random(10)
    ];
    $publishResponse = $beamsClient->publishToUsers(
        $userIds,
        array(
          "fcm" => array(
            'data' => ['result' => json_encode($payload)],
            "notification" => array(
              "title" => $title,
              "body" => $body
            )
          )
    ));
}

function notificationInstance(){
    return new \Pusher\PushNotifications\PushNotifications(array(
        "instanceId" => "a9c53072-ab8d-4ff5-9950-3ba6c0a95651",
        "secretKey" => "E9817E476B7951FFF4B450D715E3D93515383D0A744AC0F79DF899D341A6AFA1",
    ));
}

function getFrontendGeneralSetting(){ 
    $settings = [];
    $settings['exchangeRate'] = [
        'dollarToRiel' => exchangeRateFromDollarToRiel(),
        'rielToDollar' => exchangeRateFromRielToDollar(),
    ];
    $settings['language'] = getAdminLanguageData();
    $settings['avaiableLanguage'] = getAvailableLanguage();
    return $settings;
}

function getAvailableLanguage(){
    $language = Language::orderBy('sort','ASC')->get()->keyBy('language_field');
    return $language;
}

function getAdminLanguageData(){ 
    $currentLang = 'english';
    if(app()->getLocale() == 'en'){
        $currentLang = 'english';
    }elseif(app()->getLocale() == 'zh'){
        $currentLang = 'chinese';
    }elseif(app()->getLocale() == 'kh'){
        $currentLang = 'khmer';
    }
    return LanguageAdmin::all()->pluck($currentLang,'label')->toArray();
}

function api_trans($msg,$key = []){
    return trans($msg,$key, getRequestLanguage());
}

function getRequestLanguage(){
    $lang = request()->header('Accept-Language') ?? 'en';
    return $lang;
}

function getNumberOfAgentRequest(){
    $num = User::where(['is_approved' => 0])
                ->orderBy('id','DESC')->get()->count();
    return $num;
}

function getUserLevelTitle($level){
    switch($level){
        case 1: $title = 'Level 1'; break;
        case 2: $title = 'Level 2'; break;
        case 3: $title = 'Level 3'; break;
        case 4: $title = 'Level 4'; break;
        case 5: $title = 'Level 5'; break;
    }
    return $title;
}

function currencyFormat($amount,$decimal = 2){
    return number_format($amount, $decimal);
}
function stringToDouble($str){
    return doubleval(str_replace(',','',$str));
}
function exchangeRateFromRielToDollar(){
    $currency = Exchange::where('from_currency',1)->first();
    return $currency ? 1 / $currency->rate : 0;
}
function exchangeRateFromDollarToRiel(){
    $currency = Exchange::where('from_currency',1)->first();
    return $currency ? $currency->rate : 0;
}

function addPrefixStringPad($input,$length,$string){
    return str_pad($input, $length, $string, STR_PAD_LEFT);
}
function addSuffixStringPad($input,$length,$string){
    return str_pad($input, $length, $string, STR_PAD_RIGHT);
}

function walletAgetns(){
    return [
        'Wing' => 'Wing',
        'TrueMoney' => 'TrueMoney',
        'eMoney' => 'eMoney',
        'Dara Pay' => 'Dara Pay',
        'Asia Wei Luy' => 'Asia Wei Luy',
        'ABA' => 'ABA',
        'ACELEDA' => 'ACELEDA',
    ];
}

function permissions(){
    return [
        'en' => [
            'dashboard' => [
                'module' => 'Dashboard',
                'permissions' => [
                    ['text' => 'View Dashboard', 'value' => 'view-dasbhoard'], 
                ]
            ],

            'order-5d' => [
                'module' => '5D Order Report',
                'permissions' => [
                    ['text' => 'View 5D Order List', 'value' => 'view-5d-order-list'], 
                    ['text' => 'Export 5D Order List', 'value' => 'export-5d-order-list'],
                    ['text' => 'View 5D History Report', 'value' => 'view-5d-history-report'], 
                    ['text' => 'Export 5D History Report', 'value' => 'export-5d-history-report']
                ]
            ],
            '5d-game-setting' => [
                'module' => '5D Game Setting',
                'permissions' => [
                    ['text' => 'View 5D Game Setting', 'value' => 'view-5d-game-setting'],
                    ['text' => '5D Game Setting Modification', 'value' => '5d-game-setting-modification']
                ]
            ],
            '5d-cycle' => [
                'module' => '5D Cycle',
                'permissions' => [
                    ['text' => 'View 5D Cycle Log', 'value' => 'view-5d-cycle-log']
                ]
            ],
            'order-ball' => [
                'module' => 'Ball Order Report',
                'permissions' => [
                    ['text' => 'View Ball Order List', 'value' => 'view-ball-order-list'], 
                    ['text' => 'Export Ball Order List', 'value' => 'export-ball-order-list'],
                    ['text' => 'View Ball History Report', 'value' => 'view-ball-history-report'], 
                    ['text' => 'Export Ball History Report', 'value' => 'export-ball-history-report']
                ]
            ],
            'ball-game-setting' => [
                'module' => 'Ball Game Setting',
                'permissions' => [
                    ['text' => 'View Ball Game Setting', 'value' => 'view-ball-game-setting'],
                    ['text' => 'Ball Game Setting Modification', 'value' => 'ball-game-setting-modification']
                ]
            ],
            'ball-cycle' => [
                'module' => 'Ball Cycle',
                'permissions' => [
                    ['text' => 'View Ball Cycle Log', 'value' => 'view-ball-cycle-log']
                ]
            ],
            'user' => [
                'module' => 'User',
                'permissions' => [
                    ['text' => 'View User Account', 'value' => 'view-user-account'], 
                    ['text' => 'Request Add New User Account', 'value' => 'request-add-new-user-account'], 
                    ['text' => 'Add New User Account', 'value' => 'add-new-user-account'], 
                    ['text' => 'User Account Modification', 'value' => 'user-account-modification'],
                    ['text' => 'Update User Credentails Information', 'value' => 'update-user-credentials-info'],
                    ['text' => 'User Recharge', 'value' => 'user-recharge'],
                    ['text' => 'View User Account Log', 'value' => 'view-user-account-log'],
                    ['text' => 'User Take Cash', 'value' => 'user-take-cash']
                ]
            ],
            'balance-request' => [
                'module' => 'Balance Request',
                'permissions' => [
                    ['text' => 'View Balance Request', 'value' => 'view-balance-request'], 
                    ['text' => 'Balance Request Modification', 'value' => 'balance-request-modification']
                ]
            ],
            'finance' => [
                'module' => 'Finance',
                'permissions' => [
                    ['text' => 'View Transaction Log', 'value' => 'view-transaction-log'],
                    ['text' => 'Download Transaction Log', 'value' => 'download-transaction-log'],  
                    ['text' => 'View Platform Account', 'value' => 'view-platform-account'],
                    ['text' => 'Platform Account Access Transfer', 'value' => 'platform-account-access-transfer'],
                    ['text' => 'Add New Account', 'value' => 'add-new-account'],
                    ['text' => 'Account Modification', 'value' => 'account-modification'],
                    ['text' => 'Add Class Manager', 'value' => 'add-class-manager'],
                    ['text' => 'Balance Review', 'value' => 'balance-review'],
                    ['text' => 'View Agent Tracking' ,'value' => 'view-agent-tracking'],
                    ['text' => 'Download Agent Tracking' ,'value' => 'download-agent-tracking'],
                    ['text' => 'Download Profit Review' ,'value' => 'download-profit-review'],
                    ['text' => 'View Profit Review' ,'value' => 'view-profit-review'],
                   
                ]
            ],
            'cycle' => [
                'module' => 'Cycle Result',
                'permissions' => [
                    ['text' => 'Lottery Setting', 'value' => 'lottery-setting'], 
                    ['text' => 'Lottery Operation', 'value' => 'lottery-operation'],
                    ['text' => 'Billing Operation', 'value' => 'billing-operation'], 
                    ['text' => 'View Lottery Log', 'value' => 'view-lottery-log']
                ]
            ],
            'profit' => [
                'module' => 'Profit Sharing',
                'permissions' => [
                    ['text' => 'Monthly Lucky Draw', 'value' => 'lucky-draw'],
                ]
            ],
            'manager' => [
                'module' => 'Manager',
                'permissions' => [
                    ['text' => 'View Manager Account', 'value' => 'view-manager-account'], 
                    ['text' => 'Add New Manager Account', 'value' => 'add-new-manager-account'], 
                    ['text' => 'Manager Account Modification', 'value' => 'manager-account-modification'],
                    ['text' => 'View Role', 'value' => 'view-role'], 
                    ['text' => 'Add New Role', 'value' => 'add-new-role'],
                    ['text' => 'Role Modification', 'value' => 'role-modification'],
                    ['text' => 'View Manager Log', 'value' => 'view-manager-log']
                ]
            ]
        ],
    ];
}