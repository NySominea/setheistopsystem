<?php 

function permute($str,$i,$n,array &$arr) {
    if ($i == $n)
        $arr[] = $str;
    else {
         for ($j = $i; $j < $n; $j++) {
           swap($str,$i,$j);
           permute($str, $i+1, $n,$arr);
           swap($str,$i,$j); // backtrack.
        }
    }
}

function swap(&$str,$i,$j) {
    $temp = $str[$i];
    $str[$i] = $str[$j];
    $str[$j] = $temp;
} 