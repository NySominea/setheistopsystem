<?php



function getLocation($lat, $lng){
    $client = new \GuzzleHttp\Client();
    $geocoder = new Geocoder($client);
    // $geocoder->setApiKey(config('geocoder.key'));

    return $geocoder->getAddressForCoordinates($lat, $lng);
}