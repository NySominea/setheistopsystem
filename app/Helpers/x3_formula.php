<?php 

function x3(&$set, &$results)
{
    for ($i = 0; $i < count($set); $i++)
    {
        $results[] = $set[$i]; 
        $tempset = $set;
        array_splice($tempset, $i, 1);
        $tempresults = array();
        x3($tempset, $tempresults);
        foreach ($tempresults as $res)
        {
            $results[] = $set[$i] . $res;
        }
    }
}