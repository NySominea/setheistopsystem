<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderNumber extends Model
{
    protected $connection = 'sethei_mysql';

    protected $table = 'd_order_numbers';

}
