<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Promotion extends Model implements HasMedia
{
    use HasMediaTrait;
    protected $fillable = ['title', 'description', 'begin_date', 'end_date', 'state'];
}
