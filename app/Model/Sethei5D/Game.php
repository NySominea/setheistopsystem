<?php

namespace App\Model\Sethei5D;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $connection = 'sethei_mysql';
     protected $table = "d_games";

     protected $fillable = [
         'id',
         'name',
         'sort'
     ];
}
