<?php

namespace App\Model\Sethei5D;

use Illuminate\Database\Eloquent\Model;

class Cycle extends Model
{
    protected $connection = 'sethei_mysql';
    protected $table = "d_cycles";

    protected $fillable = [
            'id',
            'state',
            'cycle_sn',
            'has_released',
            'stopped_time',
            'result_time',
            'A1',
            'A2',
            'A3',
            'A4',
            'A5',
            'B1',
            'B2',
            'B3',
            'B4',
            'B5',
            'C1',
            'C2',
            'C3',
            'C4',
            'C5',
            'D1',
            'D2',
            'D3',
            'D4',
            'D5',
    ];

    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function order_temps(){
        return $this->hasMany(OrderTemp::class);
    }
}
