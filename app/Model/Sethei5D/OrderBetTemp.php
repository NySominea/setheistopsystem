<?php

namespace App\Model\Sethei5D;

use Illuminate\Database\Eloquent\Model;

class OrderBetTemp extends Model
{
    protected $table = "d_order_bet_temps";

    protected $fillable = [
        'id',
        'ticket',
        'prize',
        'bet',
        'unit',
        'amount',
        'type',
        'is_win',
        'win_amount', 
    ];
}
