<?php

namespace App\Model\Sethei5D;

use Illuminate\Database\Eloquent\Model;

class GameRebate extends Model
{
    protected $table = "d_game_rebates";

    protected $fillable = [
        'id',
        'category_id',
        'l1_rebate',
        'l2_rebate',
        'l3_rebate',
        'l4_rebate',
        'l5_rebate'
    ];

    
    
    public function category(){
        return $this->belongsTo(GameCategory::class,'category_id');
    }
}
