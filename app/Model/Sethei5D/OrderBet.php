<?php

namespace App\Model\Sethei5D;

use Illuminate\Database\Eloquent\Model;

class OrderBet extends Model
{
    protected $table = "d_order_bets";

    protected $fillable = [
           'id',
           'ticket',
           'prize',
           'bet',
           'unit',
           'amount',
           'type',
           'is_win',
           'win_amount'
        ];
}
