<?php

namespace App\Model\Sethei5D;

use Illuminate\Database\Eloquent\Model;

class OrderRebateTemp extends Model
{
    protected $table = "d_order_rebate_temps";
    protected $fillable = [
        'id',
        'user_id',
        'cycle_id',	
        'ticket',
        'rebate_amount',
        'created_at',
        'updated_at'
    ];
}
