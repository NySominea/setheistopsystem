<?php

namespace App\Model\Sethei5D;

use Illuminate\Database\Eloquent\Model;
use DB;

class GameCategory extends Model
{
    protected $connection = 'sethei_mysql';
    protected $table = "d_game_categories";

    protected $fillable = [
            'id',
            'game_id',
            'name',
            'rebate',
            'rate',
            'sort'
    ];

    public function game(){
        return $this->belongsTo(Game::class,'game_id');
    }

    public function game_rebate(){
        return $this->hasOne(GameRebate::class,'category_id','id');
    }
}
