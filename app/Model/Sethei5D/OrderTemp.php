<?php

namespace App\Model\Sethei5D;

use Illuminate\Database\Eloquent\Model;
use App\User;

class OrderTemp extends Model
{
    protected $connection = 'mysql';
    protected $table = "d_order_temps";

    protected $fillable = [
        'id',
        'parent_id',
        'cycle_id',
        'user_id',
        'is_settle',
        'is_win',
        'win_amount',
        'win_2d_amount',
        'win_3d_amount',
        'win_5d_amount',
        'ticket',
        'amount',
        'bet',
        'bet_content',
        'state'
          
    ];

    public function cycle(){
        return $this->belongsTo(Cycle::class);
    }

    public function user(){
        return $this->belongsTo(\App\User::class);
    }

    public function parent($lvid = null){
        return $lvid ? $this->belongsTo(\App\User::class,'l'.$lvid.'_id','id')->first() : $this->belongsTo(\App\User::class,'parent_id','id');
    }

    public function orderBet(){
		return $this->hasMany(OrderBet::class,'ticket','ticket');
	}

    protected $casts = [
        'bet' => 'array',
        'bet_content' => 'array',
    ];
}
