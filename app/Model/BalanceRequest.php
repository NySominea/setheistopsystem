<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use App\User;

class BalanceRequest extends Model implements HasMedia
{
    use HasMediaTrait;
    
    protected $fillable = [
        'id',
        'user_id',
        'amount',
        'note',
        'agent_name',
        'agent_ref_no',
        'operation',
        'status'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
    
}
