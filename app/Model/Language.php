<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $connection = 'sethei_mysql';
    protected $fillable = [
        'id',
        'language',
        'language_field',
        'sort'
    ];
}
