<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Model\Manager;
use App\Model\SystemAccount;

class SystemAccountLog extends Model
{
    protected $table = "system_account_logs";

    protected $fillable = [
            'id',
            'to_account_id',
            'manager_id',
            'account_id',
            'amount',
            'balance',
            'abstract',
            'user_abstract',
            'log_number',
            'log_type',
            'to_type'
        ];

        public static function generateLogNumber($logType){
            return $logType.addPrefixStringPad(auth()->id(),4,'0').date('Y').date('m').date('d').date('H').date('i').date('s');
        }
        public function manager(){
            return $this->belongsTo(Manager::class,'manager_id','id');
        }
    
        public function account(){ 
            return $this->belongsTo(SystemAccount::class,'account_id','id');
        }
    
        public function toSystemAccount(){
            return $this->belongsTo(SystemAccount::class,'to_account_id','id');
        }
        public function toUser(){
            return $this->belongsTo(User::class,'to_account_id','id');
        }
    
    
}
