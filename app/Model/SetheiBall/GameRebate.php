<?php

namespace App\Model\SetheiBall;

use Illuminate\Database\Eloquent\Model;

class GameRebate extends Model
{
    protected $connection = 'mysql';
    protected $table = "ball_game_rebates";
    
    protected $fillable = [
        'id',
        'l1_rebate',	
        'l2_rebate',
        'l3_rebate',
        'l4_rebate',
        'l5_rebate',
        'game_type_id'
    ];

    public function gameType(){
        return $this->belongsTo(GameType::class);
    }
}
