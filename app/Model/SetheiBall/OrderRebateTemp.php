<?php

namespace App\Model\SetheiBall;

use Illuminate\Database\Eloquent\Model;

class OrderRebateTemp extends Model
{
    protected $connection = 'mysql';
    protected $table = "ball_order_rebate_temps";
    
    protected $fillable = [
        'id',
        'user_id',
        'cycle_id',	
        'ticket',
        'rebate_amount',
        'created_at',
        'updated_at'
    ];
}
