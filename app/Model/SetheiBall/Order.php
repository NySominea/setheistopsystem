<?php

namespace App\Model\SetheiBall;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Order extends Model
{
    protected $connection = 'mysql';
    protected $table = 'ball_orders';

    protected $fillable = [
        'id',
        'ticket',	
        'is_settle',
        'is_win',
        'win_prize',
        'win_number',
        'win_amount',
        'amount',
        'no1',
        'no2',
        'no3',
        'no4',
        'no5',
        'no6',
        'parent_id',
        'state',
        'cycle_id',
        'user_id',
        'game_type_id',
        'is_test'
    ];

    public function cycle(){
        return $this->belongsTo(Cycle::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function parent($lvid = null){
        return $lvid ? $this->belongsTo(User::class,'l'.$lvid.'_id','id')->first() : $this->belongsTo(User::class,'parent_id','id');
    }

    protected $casts = [
        'win_number' => 'array',
    ];
}
