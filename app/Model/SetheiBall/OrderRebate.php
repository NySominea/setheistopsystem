<?php

namespace App\Model\SetheiBall;

use Illuminate\Database\Eloquent\Model;

class OrderRebate extends Model
{
    protected $connection = 'mysql';
    protected $table = "ball_order_rebates";
    
    protected $fillable = [
        'id',
        'user_id',
        'cycle_id',	
        'ticket',
        'rebate_amount',
        'created_at',
        'updated_at'
    ];
}
