<?php

namespace App\Model\SetheiBall;

use Illuminate\Database\Eloquent\Model;

class Cycle extends Model
{
    protected $connection = 'sethei_mysql';
    protected $table = 'ball_cycles';

    protected $fillable = [
        'id',
        'cycle_sn',
        'state',
        'has_released',
        'stopped_time',
        'result_time',
        'result_number',
        'is_jackpot',
        'prize',
        'prize_5d',
        'prize_4d',
        'prize_3d',
        'lucky_draw',
        'game_type_id'
    ];

    public function gameType(){
        return $this->belongsTo(GameType::class);
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function order_temps(){
        return $this->hasMany(OrderTemp::class);
    }

    protected $casts = [
        'result_number' => 'array',
    ];
}
