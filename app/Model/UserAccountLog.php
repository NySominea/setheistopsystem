<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\UserAccount;
use App\User;
use App\Model\Manager;

class UserAccountLog extends Model
{
    protected $table = "user_account_logs";

    protected $fillable = [
            'id',
            'user_id',
            'account_id',
            'log_type',
            'is_transfer',
            'amount',
            'balance',
            'commission',
            'win_money',
            'to_type',
            'to_user_id',
            'to_account_id',
            'abstract',
            'manager_id',
            'log_number',
            'meta',
            'created_at',
            'updated_at'
    ]; 

    public function account(){
        return $this->belongsTo(UserAccount::class);
    }
    public function user(){
        return $this->belongsTo(User::class); 
    }
    public function toUser(){
        return $this->belongsTo(User::class,'to_user_id','id');
    }
    public function manager(){
        return $this->belongsTo(Manager::class,'manager_id','id');
    }

    public function not_agent_user(){
        return $this->belongsTo(User::class,'to_user_id','id')->where('level','<>',8); 
    }

    public static function generateLogNumber($logType, $userId){
        return $logType.addPrefixStringPad($userId,4,'0').date('Y').date('m').date('d').date('H').date('i').date('s');
    }
}
