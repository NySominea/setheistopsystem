<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SecurityCode extends Model
{
    protected $table = 'security_code';

    protected $fillable = [
        'id',
        'code',
        'user_id'
    ];
}
