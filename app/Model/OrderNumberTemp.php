<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderNumberTemp extends Model
{
    protected $connection = 'sethei_mysql';

    protected $table = 'd_order_number_temps';

}
