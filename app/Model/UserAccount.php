<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Constants\UserAccountConstant;


class UserAccount extends Model
{
    protected $table = "user_accounts";

    protected $fillable = [
        'id',
        'user_id',
        'type_id',
        'category_id',
        'title',
        'number',
        'name',
        'balance',
        'frozen',
        'state',
        'sort',
        'created_at',
        'updated_at'
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }

    public static function createCashAccount($userId = null){
        $userId = $userId ?? auth()->id();
        return UserAccount::create([
            'title' => 'Main Account',
            'number' => 'Main Account',
            'name' => 'Main Account',
            'balance' => 0,
            'frozen' => 0,
            'state' => 1,
            'user_id' => $userId,
            'type_id' => UserAccountConstant::MAIN_ACCOUNT
        ]);
    }
    public static function createBonus5DAccount($userId = null){
        $userId = $userId ?? auth()->id();
        return UserAccount::create([
            'title' => 'Bonus  5D Account',
            'number' => 'Bonus 5D Account',
            'name' => 'Bonus Account',
            'balance' => 0,
            'frozen' => 0,
            'state' => 1,
            'user_id' => $userId,
            'type_id' => UserAccountConstant::BONUS_5D_ACCOUNT
        ]);
    }

    public static function createBonusBallAccount($userId = null){
        $userId = $userId ?? auth()->id();
        return UserAccount::create([
            'title' => 'Bonus  Ball Account',
            'number' => 'Bonus Ball Account',
            'name' => 'Bonus Account',
            'balance' => 0,
            'frozen' => 0,
            'state' => 1,
            'user_id' => $userId,
            'type_id' => UserAccountConstant::BONUS_BALL_ACCOUNT
        ]);
    }
  
}
