<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Exchange extends Model
{
    protected $connection = 'sethei_mysql';
    protected $fillable = [
        'id',
        'from_currency',
        'to_currency',
        'rate',
    ];

    public function from(){
        return $this->belongsTo(Currency::class,'from_currency','id');
    }
    public function to(){
        return $this->belongsTo(Currency::class,'to_currency','id');
    }
}
