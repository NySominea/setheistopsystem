<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $connection = 'sethei_mysql';
    protected $fillable = [
        'id',
        'name',
        'symbol',
        'unit',
    ];
}
