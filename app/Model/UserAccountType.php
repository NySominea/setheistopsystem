<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserAccountType extends Model
{
    protected $fillable = [
            'id','name'
    ];
}
