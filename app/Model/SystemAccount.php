<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SystemAccount extends Model
{
    protected $table = "system_accounts";
    
    protected $fillable = [
                    'id',
                    // 'type_id',
                    // 'category_id',
                    'title',
                    'number',
                    'name',
                    'balance',
                    'sort',
                    'state'
                ];

    // public function type(){
    //     return $this->belongsTo(SystemAccountType::class,'type_id','id');
    // }
    // public function category(){
    //     return $this->belongsTo(SystemAccountCategory::class,'category_id','id');
    // }
}
