if($('#dropzoneProfile').length > 0 || $('#dropzoneThumnail').length > 0 || 
    $('#leaderThumnail').length > 0 || $('#dropzonePromotionThumbnail').length > 0
    || $('#dropzoneCompanyThumbnail').length > 0 || $('#dropzoneSlider').length > 0 || $('#dropzoneProfile') || $('dropzoneCompanyBanner')){

    if($('#dropzoneProfile').length > 0){
        var myDropzone = $('#dropzoneProfile');
        var width = 200;
        var height = 200;
    }else if($('#dropzoneThumnail').length > 0){
        var myDropzone = $('#dropzoneThumnail');
        var width = 200;
        var height = 200;
    }else if($('#dropzoneCompanyThumbnail').length > 0 ){
        var myDropzone = $('#dropzoneCompanyThumbnail');
    }else if($('#leaderThumnail').length > 0){
        var myDropzone = $('#leaderThumnail');
        var width = 200;
        var height = 200;
    }else if($('#dropzonePromotionThumnail').length > 0){
       
        console.log("do");
        var myDropzone = $('#dropzonePromotionThumnail');
        var width = 200;
        var height = 200; 
    }else if($('#dropzoneSlider').length > 0){
        var myDropzone = $('#dropzoneSlider');
        var width = 200;
        var height = 200; 
    }else if($('#dropzoneProfile').length > 0){
        var myDropzone = $('#dropzoneProfile');
        var width = 200;
        var height = 200; 
    }else if($('#dropzoneCompanyBanner').length > 0){
        var myDropzone = $('#dropzoneCompanyBanner');
        var width = 200;
        var height = 200; 
    }
    
    myDropzone.dropzone({
        acceptedFiles: "image/jpeg, image/png, image/jpg, image/gif",
        maxFiles: 1,
        maxFilesize: 250,
        uploadMultiple: false,
        addRemoveLinks: true,
        dictRemoveFileConfirmation: "Are you sure you want to remove this File?",
        thumbnailWidth: width,
        thumbnailHeight: height,
    
        init: function(){
            var thisDropzone = this;
            if($('#action').val() == 'update'){
                var name = $('#image').data('name');
                var size = $('#image').data('size');
                var url  = $('#image').data('url');
                if(url){
                    myDropzone.css('height','auto');
                    var mockFile = { name: name, size: size, accepted: true };  
                    
                    thisDropzone.options.addedfile.call(thisDropzone, mockFile);
                    thisDropzone.options.thumbnail.call(thisDropzone, mockFile, url);
                    thisDropzone.files.push(mockFile);
                    thisDropzone.emit("complete", mockFile);
                    thisDropzone.options.maxFiles = thisDropzone.options.maxFiles;
                    thisDropzone._updateMaxFilesReachedClass();
                }
            }
            this.on('sending', function(file, xhr, formData){
                formData.append("_token", $("input[name='_token']").val());
                formData.append("_method", "POST");
            })
            this.on("success", function(file, response){ console.log(response.path)
                $('#image').val(response.path);
            }),
            this.on("addedfile", function(event) {
                while (thisDropzone.files.length > thisDropzone.options.maxFiles) {
                    thisDropzone.removeFile(thisDropzone.files[0]);
                }
                myDropzone.css('height','auto');
                console.log("file");
            });
            this.on("removedfile", function(file){
                file.previewElement.remove();
                if(this.files.length < 1){
                    myDropzone.css('height','');
                }
                
                if($('#action').val() == 'update'){
                    console.log($('#image').data('model-id'))
                $.ajax({
                    type:'DELETE',  
                    url:myDropzone.data('url-delete'),
                    dataType: "JSON",
                    data: {model_id: $('#image').data('model-id')},
                    success:function(data){
                        console.log('success');
                    },
                    error:function(){
                       console.log('error');
                    }
                });
                }
                $('#image').val("");
                
            })
        } 
    })
}