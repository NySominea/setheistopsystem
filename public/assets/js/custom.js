$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    if($('.datetimepicker').length > 0){
        $('.datetimepicker').daterangepicker({
            timePicker: true,
            singleDatePicker:true,
            timePicker24Hour:true,
            timePickerSeconds:true,
            locale: {
                format: 'YYYY-MM-DD HH:mm:ss'
            }
        });
    }
    // Download Excel
    
    if($('#downlod_modal').length > 0){
        var modal = $('#downlod_modal #downloadModalBody')
        $('.downloadExcelButton , #downloadExcelButton').click(function(){
            var url = $(this).data('route');
            
            $.ajax({
                type: 'GET',
                url: url + location.search,
                data: {},
                success: function(response){
                    if(response.success){
                        modal.html("<a class='btn btn-success' href='"+response.path+"' download>Download</a>");
                    }else{
                        modal.html('Error!!!')
                    }
                },
                error: function(){
                    modal.html('Error!!!')
                }
            })
        });
        $('#downlod_modal').on('hidden.bs.modal', function (e) {
            modal.html("<i class='icon-spinner2 spinner'></i> Downloading...")
        })
    }
    // End Download Excel

    // Popup Order Detail
    if($('#order_detail_modal').length > 0){
        $('.show-order-detail').click(function(){
            $('#body').html("<div class='text-center'><i class='icon-spinner2 spinner'></i></div>")
            $.ajax({
                type:'GET',  
                url:$(this).data('route'),
                dataType: "JSON",
                data: {},
                success:function(response){
                    if(response.success){
                        var data = response.data;
                        var table = "<table class='w-100 table table-bordered'>";
                        table += "<tbody>";
                        table += "<tr><td>Ticket</td><td colspan='2'>"+data.ticket+"</td></tr>";
                        table += "<tr><td>Amount (R)</td><td colspan='2'>"+data.betAmount+"</td></tr>";
                        table += "<tr><td>Win Amount (R)</td><td colspan='2'>"+data.winAmount+"</td></tr>";
                        table += "<tr><td>Post Date</td><td colspan='2'>"+data.created_at+"</td></tr>";
                        table += "<tr class='bg-slate-600'><td class='font-weight-bold'>Level</td><td class='font-weight-bold'>User Name</td><td class='font-weight-bold'>Rebate (R)</td></tr>"
                        $.each(data.rebates, function( index, value ) {
                            table += "<tr><td>"+value.levelTitle+"</td><td>"+value.username+"</td><td>"+value.rebate+"</td></tr>"
                        });
                        table += "</tbody></table>";
                        $('#body').html(table);
                    }else{
                        swalOnResult('Error While Loading Information!','error');
                    }
                },
                error:function(){
                    swalOnResult('Error While Loading Information!','error');
                }
            }); 
        });
        
    }
    // End Popup order detail

    // Print System Account Log
    if($('#print_preview_modal').length > 0){
        $('.print').click(function(){
            var transaction = $('#print_preview_modal .transaction');
            var manager = $('#print_preview_modal .manager');
            var account = $('#print_preview_modal .account');
            var accountNo = $('#print_preview_modal .account-no');
            var logType = $('#print_preview_modal .log-type');
            var amount = $('#print_preview_modal .amount');
            var balance = $('#print_preview_modal .balance');
            var toAccount = $('#print_preview_modal .to-account');
            var auditor = $('#print_preview_modal .auditor');
            var dateTime = $('#print_preview_modal .date-time');
            var abstract = $('#print_preview_modal .abstract');
            transaction.text('');
            manager.text('');
            account.text('');
            accountNo.text('');
            logType.text('');
            amount.text('');
            balance.text('');
            toAccount.text('');
            auditor.text('');
            dateTime.text('');
            abstract.text('');
            $.ajax({
                type:'GET',  
                url:$(this).data('route'),
                dataType: "JSON",
                data: {},
                success:function(data){
                    if(data.success){
                        transaction.text(data.log.transaction);
                        manager.text(data.log.manager);
                        account.text(data.log.account);
                        accountNo.text(data.log.accountNo);
                        logType.text(data.log.logType);
                        amount.text("(R)" + data.log.amount);
                        balance.text("(R)" + data.log.balance);
                        toAccount.text(data.log.toAccount);
                        auditor.text(data.log.auditor);
                        dateTime.text(data.log.dateTime);
                        abstract.text(data.log.abstract);
                    }else{
                        swalOnResult('Error While Loading Information!','error');
                    }
                },
                error:function(){
                    swalOnResult('Error While Loading Information!','error');
                }
            }); 
        });
        
    }
    // End System Print Account Log

    // Print User Account Log
    if($('#print_preview_user_modal').length > 0){
        $('.print').click(function(){
            var transaction = $('#print_preview_user_modal .transaction');
            var manager = $('#print_preview_user_modal .manager');
            var username = $('#print_preview_user_modal .username');
            var logType = $('#print_preview_user_modal .log-type');
            var amount = $('#print_preview_user_modal .amount');
            var balance = $('#print_preview_user_modal .balance');
            var toAccount = $('#print_preview_user_modal .to-account');
            var auditor = $('#print_preview_user_modal .auditor');
            var dateTime = $('#print_preview_user_modal .date-time');
            var abstract = $('#print_preview_user_modal .abstract');
            var customerSignature = $('#print_preview_user_modal .custom-signature');
            transaction.text('');
            manager.text('');
            username.text('');
            logType.text('');
            amount.text('');
            balance.text('');
            toAccount.text('');
            auditor.text('');
            dateTime.text('');
            abstract.text('');
            customerSignature.text('');
            $.ajax({
                type:'GET',  
                url:$(this).data('route'),
                dataType: "JSON",
                data: {},
                success:function(data){
                    if(data.success){
                        transaction.text(data.log.transaction);
                        manager.text(data.log.manager);
                        username.text(data.log.username);
                        logType.text(data.log.logType);
                        amount.text("(R)" + data.log.amount);
                        balance.text("(R)" + data.log.balance);
                        toAccount.text(data.log.toAccount);
                        auditor.text(data.log.auditor);
                        dateTime.text(data.log.dateTime);
                        abstract.text(data.log.abstract);
                        customerSignature.text(data.log.customerSignature);
                    }else{
                        swalOnResult('Error While Loading Information!','error');
                    }
                },
                error:function(){
                    swalOnResult('Error While Loading Information!','error');
                }
            }); 
        });
        
    }
    // End User Print Account Log

    // Delete Record
    if($('.delete').length > 0){
        $('.delete').click(function(){
            deleteRecord($(this).data('route'));
        });
    }

    // Delay remove Alert
    if($('.alert').length > 0){
        setTimeout(function() {
            $('.alert').fadeOut();
         }, 5000);
    }

    // Set checkbox on Role Set
    if($('.module.form-check').length > 0){
        $('.module.form-check').click(function(){
            if($(this).find('input').is(':checked')){
                $(this).parent().find('.permission').each(function(){
                    $(this).find('span').addClass('checked');
                    $(this).find('input').prop('checked', true);
                })
            }else{
                $(this).parent().find('.permission').each(function(){
                    $(this).find('span').removeClass('checked');
                    $(this).find('input').prop('checked', false);
                })
            }
        })
    }
    if($('.permission').length > 0){
        $('.permission').click(function(){
            var hasOnePermission = false;

            if($(this).find('input').is(':checked')){
                $(this).parent().find('.module').find('input').prop('checked', true);
                $(this).parent().find('.module').find('span').addClass('checked');
            }
            $(this).parent().find('.permission').each(function(){
                if($(this).find('input').is(':checked')){
                    hasOnePermission = true;
                }
            })
            if(!hasOnePermission){
                $(this).parent().find('.module').find('input').prop('checked', false);
                $(this).parent().find('.module').find('span').removeClass('checked');
            }

        });
    }
    // End checkbox on Role Set

    var deleteRecord = function(route){
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        }).then((isConfirm) => {
            if(isConfirm){
                $('.overlay').show();
                $.ajax({
                    type:'DELETE',  
                    url:route,
                    dataType: "JSON",
                    data: {},
                    success:function(data){
                        if(data.success){
                            swalOnResult('Deleted Successfully!','success');
                            location.reload();
                        }else{
                            swalOnResult('Error While Deleting!','error');
                        }
                    },
                    error:function(){
                        swalOnResult('Error While Deleting!','error');
                    }
                });      
            }
        });
    }

    var swalOnResult = function(title, type){
        swal({
            title: title,
            text: "You clicked the button!",
            icon: type,
            timer: 1500,
        });
    }
});

// Set Input Currency Format
$(document).ready(function(){
    if($('.input-amount').length > 0){
        $('.input-amount').keyup(function(event){
            var amount = $(this).val() == '' ? '0' : parseInt($(this).val().replace(/[^0-9.]/g, ""));
            var amountFormat = amount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
            $(this).val(amountFormat);
        });
    }
});

// Check Sidebar Active
$(document).ready(function(){
    var current = (location.origin).concat(location.pathname).concat(location.hash);
    $('.nav-item a').each(function(){
        if ($(this).attr('href') == current) {
            $(this).parents('.nav-item-submenu').addClass('nav-item-expanded nav-item-open');
            $(this).addClass('active');
        }else{
            if(!$(this).parents('.nav-item-submenu').hasClass('nav-item-expanded nav-item-open')){
                if(!$(this).data('active') && $(this).data('active') != 1){
                    $(this).removeClass('active');
                }else{
                    $(this).addClass('active');
                }
            }
        } 
    })
})

// User Sale Network Search
$(document).ready(function(){
    if($('#state').length > 0){
        $('#state').change(function(){ 
            window.location = replaceUrlParam(window.location.href,'state',$('#state').val());
        });
    }
    if($('.appendQueryBtn').length > 0){
        $('.appendQueryBtn').click(function(){
            // $(this).parents('form').submit(function(e){
            //     e.preventDefault(); 
            // }); 
            // window.location = replaceUrlParam(window.location.href,'keyword',$('#keyword').val());
        })
    }
    function replaceUrlParam(url, paramName, paramValue)
    {
        if (paramValue == null) {
            paramValue = '';
        }
        var pattern = new RegExp('\\b('+paramName+'=).*?(&|#|$)');
        if (url.search(pattern)>=0) {
            return url.replace(pattern,'$1' + paramValue + '$2');
        }
        url = url.replace(/[?#]$/,'');
        return url + (url.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue;
    }
})