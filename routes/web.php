<?php
use App\Model\Order;
use App\User;
use App\Model\UserAccount;
use App\Model\UserAccountLog;
use App\Constants\UserAccountConstant;

Route::get('deep',function(){
    return '<a href="intent://open?link_click_id=123456#Intent;scheme=shop;package=com.fighter.sethei_shop;">Click Herre</a>';
});

Route::get('/',function(){
   return redirect()->route('login');
});

Route::group(['namespace' => 'Auth'], function(){
    Route::get('login', ['as' => 'login', 'uses' => 'LoginController@showLoginForm']);
    Route::post('login', ['as' => 'login.post', 'uses' => 'LoginController@login']);
    Route::post('logout', ['as' => 'logout', 'uses' => 'LoginController@logout']);
});

Route::group(['middleware' => 'auth'],function(){
    Route::get('/language/set-locale/{locale}',['uses' => 'SystemSet\Language\LanguageController@setLocale', 'as' => 'language.set-locale']);
    
    Route::get('/dashboard',['uses' => 'Dashboard\DashboardController@index','as' => 'dashboard']);

    Route::group(['namespace' => 'User'],function(){

        Route::get('/users/getDistrict',['uses' => 'SaleNetworkController@getDistrict','as' =>'users.district']);
        Route::get('/users/getCommune',['uses' => 'SaleNetworkController@getCommune','as' => 'users.coomune']);
        Route::get('/users/cash-export', 'AccountController@download');

        Route::group(['middleware' => ['permission:user-take-cash']], function () {
            Route::resource('/users/account/takecash', 'AccountTakeCashController',['as' => 'users.account']);
        });

        Route::group(['middleware' => ['permission:user-recharge']], function () {
            Route::resource('/users/account/recharge', 'AccountRechargeController',['as' => 'users.account']);
        });
        Route::group(['middleware' => ['permission:view-user-account-log']], function () {
            Route::resource('/users/account/logs', 'AccountLogController',['as' => 'users']);
        });
        Route::group(['middleware' => ['permission:view-user-account|add-new-user-account|user-account-modification']], function () {
            Route::resource('/users/accounts', 'AccountController',['as' => 'users']);
        });
        Route::group(['middleware' => ['permission:view-user-account|view-user-account-log|user-recharge|user-take-cash|add-new-user-account|user-account-modification']], function () {
            Route::get('/users/sale-network/agent-approval',['uses' => 'SaleNetworkController@agentApproval', 'as' => 'users.sale-network.agent-approval']);
            Route::resource('/users/sale-network', 'SaleNetworkController',['as' => 'users']);
            Route::resource('/users/direct-user', 'DirectUserController',['as' => 'users']);
        });

        Route::resource('/users/balance-request', 'BalanceRequestController',['as' => 'users']);

        Route::get('/user/log/download/{userId}',['uses' => 'AccountLogController@download','as' => 'user.logs.download']);
            

    });

    Route::group(['namespace' => 'Sethei5D'],function(){

        Route::group(['namespace' => 'Order'],function(){
            Route::get('/5d/orders/temp/download',['uses' => 'OrderTempController@downloadExcel', 'as' => '5d.orders.temp.download']);
            Route::get('/5d/orders/temp/{ticket}/get-order-detail',['uses' => 'OrderTempController@ajaxGetOrderDetail', 'as' => '5d.orders.temp.get-order-detail']);
            Route::resource('/5d/orders/temp', 'OrderTempController',['as' => '5d.orders']);
            
            Route::get('/5d/orders/report/download',['uses' => 'OrderController@downloadExcel', 'as' => '5d.orders.report.download']);
            Route::get('/5d/orders/sum/agent',['uses' => 'OrderController@downloadSumBet','as' => '5d.orders.sum.download']);

            Route::get('/5d/orders/report/{ticket}/get-order-detail',['uses' => 'OrderController@ajaxGetOrderDetail', 'as' => '5d.orders.report.get-order-detail']);
            Route::get('/5d/orders/report/selection-form',['uses' => 'OrderController@showSelectionForm','as' => '5d.orders.report.selection-form']);
            Route::resource('/5d/orders/report', 'OrderController',['as' => '5d.orders']);
        });

        Route::group(['namespace' => 'Cycle'],function(){
            Route::resource('/5d/cycles/logs', 'CycleLogController',['as' => '5d.cycles']);
        });

        Route::group(['namespace' => 'Game'],function(){
            Route::resource('/5d/game/setting', 'GameController',['as' => '5d.game']);
        });


    });

    Route::group(['namespace' => 'SetheiBall'],function(){
        Route::group(['namespace' => 'Order'],function(){
            Route::get('/ball/orders/temp/download',['uses' => 'OrderTempController@downloadExcel', 'as' => 'ball.orders.temp.download']);
            Route::get('/ball/orders/temp/{ticket}/get-order-detail',['uses' => 'OrderTempController@ajaxGetOrderDetail', 'as' => 'ball.orders.temp.get-order-detail']);
            Route::resource('/ball/orders/temp', 'OrderTempController',['as' => 'ball.orders']);
            
            Route::get('/ball/orders/report/download',['uses' => 'OrderController@downloadExcel', 'as' => 'ball.orders.report.download']);
            Route::get('/ball/orders/sum/agent',['uses' => 'OrderController@downloadSumBet','as' => 'ball.orders.sum.download']);

            Route::get('/ball/orders/report/{ticket}/get-order-detail',['uses' => 'OrderController@ajaxGetOrderDetail', 'as' => 'ball.orders.report.get-order-detail']);
            Route::get('/ball/orders/report/selection-form',['uses' => 'OrderController@showSelectionForm','as' => 'ball.orders.report.selection-form']);
            Route::resource('/ball/orders/report', 'OrderController',['as' => 'ball.orders']);
        });

        Route::group(['namespace' => 'Cycle'],function(){
            Route::resource('/ball/cycles/logs', 'CycleLogController',['as' => 'ball.cycles']);
        });

        Route::group(['namespace' => 'Game'],function(){
            Route::resource('/ball/game/setting', 'GameController',['as' => 'ball.game']);
        });

    });

    Route::group(['namespace' => 'Finance'],function(){
        Route::group(['middleware' => ['permission:download-transaction-log']], function () {
            Route::get('/finance/accounts/logs/download', ['uses' => 'AccountLogController@download','as' => 'accounts.logs.download']);
        });
        Route::group(['middleware' => ['permission:view-transaction-log']], function () {
            Route::resource('/finance/accounts/logs', 'AccountLogController',['as' => 'accounts']);    
        });
        Route::group(['middleware' => ['permission:platform-account-access-transfer']], function () {
            Route::resource('/finance/accounts/transfer', 'AccountTransferController',['as' => 'accounts']);
        });
        Route::group(['middleware' => ['permission:platform-account-access-transfer']], function () {
            Route::resource('/finance/accounts/deposit', 'AccountDepositController',['as' => 'accounts']);
        });   
        Route::group(['middleware' => ['permission:add-class-manager']], function () {
            Route::resource('/finance/accounts/categories', 'AccountCategoryController',['as' => 'accounts']);
        });   
        Route::group(['middleware' => ['permission:balance-review']], function () {
            
            Route::get('/finance/accounts/balance/download',['uses' => 'BalanceReviewController@download','as' => 'accounts.balance-review-download']);
            Route::get('/finance/accounts/review-form-selection', ['uses' => 'BalanceReviewController@formSelection', 'as' => 'accounts.review.form-selection']); 
            Route::resource('/finance/accounts/review', 'BalanceReviewController',['as' => 'accounts']); 
           
        });
        Route::group(['middleware' => ['permission:view-platform-account|add-new-account']], function () {
            Route::resource('/finance/accounts', 'AccountController'); 
        });
        Route::group(['middleware' => ['permission:view-agent-tracking|download-agent-tracking']], function () {
            Route::get('/finance/agent-tracking',['uses'=>'AgentTrackingController@index','as'=>'finance.agent-tracking']);
            Route::get('/finance/agent-tracking/download-by-user',['uses'=>'AgentTrackingController@downloadByUser','as'=>'finance.agent-tracking.download-by-user']);
            Route::get('/finance/agent-tracking/download',['uses'=>'AgentTrackingController@investigatedDownload','as'=>'finance.agent-tracking.download']);
        });
        
        Route::group(['middleware' => ['permission:|download-profit-review|download-profit-review']], function () {
            Route::get('/finance/form-selection',['uses'=>'ShopReviewController@formSelection','as'=>'finance.form-selection']);
            Route::get('/finance/profit-view',['uses'=>'ShopReviewController@index','as'=>'finance.profit-view']);
            Route::get('/finance/profit/download',['uses'=>'ShopReviewController@download','as'=>'finance.profit.download']);
        });
    });
   
    Route::get('get-system-account-log-info/{id}',['uses' => 'Finance\AccountLogController@ajaxGetLogInformation', 'as' => 'system.account.getLogInfo']);
    Route::get('get-user-account-log-info/{id}',['uses' => 'User\AccountLogController@ajaxGetLogInformation', 'as' => 'user.account.getLogInfo']);

    Route::group(['namespace' => 'Manager'],function(){
        Route::group(['middleware' => ['permission:add-new-role|view-role|role-modification']], function () {
            Route::resource('/managers/roles', 'ManagerRoleController',['as' => 'managers']);
        });
        Route::group(['middleware' => ['permission:view-manager-log']], function () {
            Route::resource('/managers/logs', 'ManagerLogController',['as' => 'managers']);
        });

        Route::resource('/managers/password','ManagerEditPasswordController',['as' => 'managers']);
        Route::group(['middleware' => ['permission:add-new-manager-account|view-manager-account|manager-account-modification']], function () {
            Route::resource('/managers', 'ManagerController');
        });
    });


    Route::group(['namespace' => 'Promotion'], function(){    
        Route::resource('/promotions', 'PromotionController');
        Route::post('/promotions-saveThumnail', ['uses' => 'PromotionController@saveThumnail', 'as' => 'promotions.saveThumnail' ]);
        Route::delete('/promotions-deleteThumnail', ['uses' => 'PromotionController@deleteThumnail', 'as' => 'promotions.deleteThumnail']);
    });

    // Ajax Routes
    Route::get('get-district-by-province',['uses' => 'SystemSet\Location\DistrictController@getDistrictsByProvinceId', 'as' => 'locations.get-districts-by-province']);
    Route::get('get-system-account-log-info/{id}',['uses' => 'Finance\AccountLogController@ajaxGetLogInformation', 'as' => 'system.account.getLogInfo']);
    Route::get('get-user-account-log-info/{id}',['uses' => 'User\AccountLogController@ajaxGetLogInformation', 'as' => 'user.account.getLogInfo']);
    
});
