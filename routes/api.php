<?php

Route::namespace('Api')->group(function(){
    // params: phone_or_account_number, password
    Route::post('login','AuthController@login');

    // params: username, phone, password, password_confirmation, parent_id(optional)
    Route::post('register','AuthController@register');

    // params: account_number
    Route::get('get-user-data','AuthController@getUserData');
    
   

    Route::middleware(['auth:api','state'])->group(function(){
        
        Route::post('verify','AuthController@verify');
        Route::post('resend-verify-code','AuthController@resendVerifyCode');
        Route::get('profile', ['uses' => 'UserController@getProfile']);
        Route::get('promotion', ['uses' => 'PromotionController@getPromotion']);

        Route::post('logout','AuthController@logout');
        Route::get('get-user-profile', ['uses' => 'UserController@getProfile']);
        
        Route::get('get-user-by-account-number','UserController@get_user_by_account_number');
        
        // params: old_password, new_password, confirm_new_password
        Route::post('request-change-password','AuthController@changePassword');
        
        // params: user_id, password, amount, attachment, note, file
        Route::post('request-balance-recharge','BalanceRequestController@requestBalanceRecharge');

        // params: user_id, password, amount, note
        Route::post('request-balance-withdraw','BalanceRequestController@requestBalanceWithdraw');

        // params: user_id, status = 0(Pending)|1(In Progress)|2(Completed)|3(Rejected) (optional)
        Route::get('get-request-balance-recharge','BalanceRequestController@getRequestBalanceRecharge');
        Route::get('get-request-balance-withdraw','BalanceRequestController@getRequestBalanceWithdraw');

        // ==== Sethei Ball ====
        Route::group(['namespace' => 'SetheiBall'],function(){
            // params: data
            Route::post('ball/create-orders','OrderController@createOrder');

            Route::get('ball/get-own-cycles','CycleController@getOwnCycles');
            Route::get('ball/get-cycles','CycleController@getCycles');

            // params: cycle_id
            Route::get('ball/get-tickets-by-cycle','CycleController@getTicketsByCycle');

            // params: ticket
            Route::get('ball/get-orders-by-ticket','CycleController@getOrdersByTicket');

            Route::get('ball/get-cycle-info','CycleController@getActiveCycle');
        });
        // ==== Sethei Ball ====


        Route::post('order','OrderController@order');

        Route::get('balance','BalanceController@get_balance');
        
        Route::post('account-to-main','BalanceController@account_to_main');
        Route::post('transfer-to-other-account','BalanceController@transfer_to_other_account');
        Route::get('get-username-by-account-number','BalanceController@get_username_by_account_number');

        Route::get('get-cycle-info','CycleController@get_cycle_info');
        Route::get('get-cycles','CycleController@get_cycles');
        Route::get('ticket-info','CycleController@ticket_info');
        Route::get('detail-ticket','CycleController@detail_ticket');
        Route::get('record-cycles','CycleController@record_cycles');

        Route::get('get-logs','AccountLogController@get_logs');
        Route::get('get-system-logs','AccountLogController@get_system_logs');

        Route::get('get-report','ReportController@get_report');
        Route::get('get-report-range','ReportController@get_report_range');
    });

    Route::namespace('ResultApp')->prefix('result-app')->group(function(){
        Route::get('get-current-cycle','Cycle5DController@get_current_cycle');
        Route::get('get-cycles-5d','Cycle5DController@get_cycles_5d');

        Route::get('get-current-cycle-ball','CycleBallController@get_current_cycle_ball');
        Route::get('get-cycles-ball','CycleBallController@get_cycles_ball');
    });
});